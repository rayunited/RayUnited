﻿using UnityEngine;

namespace Assets.Scripts.Settings
{
    public static class PlayerPreferences
    {
        public const string VSYNC_COUNT_PLAYER_PREFS_KEY = "VSYNC_COUNT";
        public const string TARGET_FRAMERATE_PLAYER_PREFS_KEY = "TARGET_FRAMERATE";

        public static FullScreenMode GetFullScreenMode()
        {
            return Screen.fullScreenMode;
        }

        public static Resolution GetCurrentResolution()
        {
            Resolution result = new Resolution
            {
                width = Screen.width,
                height = Screen.height,
                refreshRate = Screen.currentResolution.refreshRate
            };
            return result;
        }
    }
}
