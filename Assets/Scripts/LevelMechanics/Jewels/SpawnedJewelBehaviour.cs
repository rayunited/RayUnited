﻿using Assets.Scripts.Engine.Behaviours;
using UnityEngine;

namespace Assets.Scripts.LevelMechanics.Jewels
{
    public class SpawnedJewelBehaviour : GameplayOnlyMonoBehaviour
    {
        public float dropFlyingHeight;
        public float dropDistance;
        public Vector3 flyingDirection;

        private float flyingSpeed = 0.15f;

        private float currentDistance = 0f;

        private Vector3 initialPosition;
        private Vector3 targetPosition;

        protected override void BehaviourStart()
        {
            this.initialPosition = this.transform.position;
            this.targetPosition = this.initialPosition + this.flyingDirection.normalized * this.dropDistance;
            this.currentDistance = 0f;
        }

        protected override void GameplayFixedUpdate()
        {
            float currentAngle = (this.currentDistance / this.dropDistance) * 180f * Mathf.Deg2Rad;

            this.transform.position = this.initialPosition + this.dropFlyingHeight * Mathf.Clamp01(Mathf.Sin((1f / this.dropDistance) * currentAngle)) * Vector3.up
                + Vector3.ClampMagnitude(this.flyingDirection.normalized * this.currentDistance, this.dropDistance);

            this.currentDistance += this.flyingSpeed;
            if (Vector3.Distance(this.transform.position, this.targetPosition) < 0.01f)
            {
                GetComponent<CollectibleCrystal>().enabled = true;
                this.enabled = false;
            }
        }
    }
}
