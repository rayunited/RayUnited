﻿using Assets.Scripts.Common;
using UnityEngine;

namespace Assets.Scripts.LevelMechanics
{
    public static class ScenePrototypesHelper
    {
        public static GameObject GetGoldJewelPrototypeObject()
        {
            return GameObject.Find(PrototypesObjectsPaths.goldJewelPrototypePath);
        }
    }
}
