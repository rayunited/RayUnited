﻿using UnityEngine;

namespace Assets.Scripts.Sound
{
    public class BasicSoundsMap : MonoBehaviour
    {
        public AudioClip jump;
        public AudioClip targeting;
        public AudioClip fistCharging;
        public AudioClip helicopter;
        public AudioClip pigpotBreak;
        public AudioClip chargedFistShoot;
        public AudioClip nonChargedFistShoot;
        public AudioClip pigpotHit;
        public AudioClip switchHit;
        public AudioClip portalOpen;
        public AudioClip wallClimbing;
        public AudioClip airFlip;
        public AudioClip enterStrafing;

        public AudioClip[] chargedShootsVariants;
    }
}
