﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Animations.Curves
{
    public class AnimationCurveBuilder
    {
        private List<Keyframe> buildingKeys;

        public AnimationCurveBuilder()
        {
            this.buildingKeys = new List<Keyframe>();
        }

        public AnimationCurveBuilder AddValueForFrame(int frameNumber, int totalFrames, float value)
        {
            this.buildingKeys.Add(new Keyframe(frameNumber / (float)totalFrames, value));
            return this;
        }

        public AnimationCurve Build()
        {
            AnimationCurve result = new AnimationCurve
            {
                keys = this.buildingKeys.ToArray()
            };
            return result;
        }
    }
}
