﻿using Assets.Scripts.Animations.Curves;
using Assets.Scripts.Animations.Models.Model.Curves;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Animations.Models.Rayman
{
    public static class RaymanBonesHelper
    {
        public static List<string> GetRightHandGoverningBonesNames()
        {
            return new List<string>() { "Channel 11", "Channel 20", "Channel 5" };
        }

        public static List<string> GetLeftHandGoverningBonesNames()
        {
            return new List<string>() { "Channel 10", "Channel 19", "Channel 4" };
        }

        public static Dictionary<string, string> GetRightHandToLeftHandBonesMappings(string pathPrefix)
        {
            return new Dictionary<string, string>()
            {
                { pathPrefix + "Channel 11", pathPrefix + "Channel 10" },
                { pathPrefix + "Channel 20", pathPrefix + "Channel 19" },
                { pathPrefix + "Channel 5", pathPrefix + "Channel 4" }
            };
        }

        public static Dictionary<string, string> GetLeftHandToRightHandBonesMappings(string pathPrefix)
        {
            return new Dictionary<string, string>()
            {
                { pathPrefix + "Channel 10", pathPrefix + "Channel 11" },
                { pathPrefix + "Channel 19", pathPrefix + "Channel 20" },
                { pathPrefix + "Channel 4", pathPrefix + "Channel 5" }
            };
        }
    }

    public static class RaymanAnimationClipsCurvesHelper
    {
#if (UNITY_EDITOR)
        private static BonesLocationRotationScaleCurves
            GetBonesLocationRotationScaleCurves(
            AnimationClip animationClip,
            Transform animatorTransform,
            List<Transform> bonesInvolvedTransforms,
            List<string> bonesNames)
        {
            Dictionary<string, BoneTransTimeline> bonesCurves =
                new Dictionary<string, BoneTransTimeline>();

            foreach (string boneName in bonesNames)
            {
                string boneSceneHierarchyPath =
                    AnimationUtility.CalculateTransformPath(
                        bonesInvolvedTransforms.Where(x => x.gameObject.name.Equals(boneName)).First(),
                        animatorTransform
                        );

                bonesCurves.Add(boneSceneHierarchyPath,
                    BoneTransformsTimelineFactory
                        .GetBoneTransformKeyframesTimelineFromAnimationClip(
                        animationClip,
                        animatorTransform,
                        bonesInvolvedTransforms.Where(x => x.gameObject.name.Equals(boneName)).First(),
                        boneName));
            }

            return BonesLocationRotationScaleCurves.WithBonesKeyframesTimelines(bonesCurves);
        }

        public static BonesLocationRotationScaleCurves
            GetRightHandBonesLocationRotationScaleCurvesForAnimationClip(
            Transform animatorTransform,
            List<Transform> animatorBonesTransforms,
            AnimationClip animationClip)
        {
            List<string> rightHandGoverningBonesNames =
                RaymanBonesHelper.GetRightHandGoverningBonesNames();

            List<Transform> bonesInvolved = animatorBonesTransforms
                .Where(x => rightHandGoverningBonesNames.Contains(x.gameObject.name)).ToList();
            return GetBonesLocationRotationScaleCurves(
                animationClip, animatorTransform, bonesInvolved, rightHandGoverningBonesNames);
        }

        public static BonesLocationRotationScaleCurves
            GetLeftHandBonesLocationRotationScaleCurvesForAnimationClip(
            Transform animatorTransform,
            List<Transform> animatorBonesTransforms,
            AnimationClip animationClip)
        {
            List<string> leftHandGoverningBonesNames = RaymanBonesHelper.GetLeftHandGoverningBonesNames();

            List<Transform> bonesInvolved = animatorBonesTransforms
                .Where(x => leftHandGoverningBonesNames.Contains(x.gameObject.name)).ToList();
            return GetBonesLocationRotationScaleCurves(
                animationClip, animatorTransform, bonesInvolved, leftHandGoverningBonesNames);
        }

        public static Tuple<BonesLocationRotationScaleCurves, BonesLocationRotationScaleCurves>
            MirrorHandsCurvesTransformsWithEachOther(
            string pathPrefix,
            BonesLocationRotationScaleCurves leftHandAnimationCurves,
            BonesLocationRotationScaleCurves rightHandAnimationCurves)
        {
            // mirror everything along world space Vector3.up normal
            BonesLocationRotationScaleCurves mirroredRightHandAnimationCurves = rightHandAnimationCurves.ReflectViaWorldVector3RightNormal();
            BonesLocationRotationScaleCurves mirroredLeftHandAnimationCurves = leftHandAnimationCurves.ReflectViaWorldVector3RightNormal();

            BonesLocationRotationScaleCurves finalLeftHandAnimationCurves =
                mirroredRightHandAnimationCurves.MapPaths(
                    RaymanBonesHelper.GetRightHandToLeftHandBonesMappings(pathPrefix));

            BonesLocationRotationScaleCurves finalRightHandAnimationCurves =
                mirroredLeftHandAnimationCurves.MapPaths(
                    RaymanBonesHelper.GetLeftHandToRightHandBonesMappings(pathPrefix));

            return Tuple.Create(finalLeftHandAnimationCurves, finalRightHandAnimationCurves);
        }
#endif
    }
}
