﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Animations.Models.Rayman.Recipes
{
    public class NonOverridingAnimationConstructingRecipeImpl : IAnimConstrRecipe
    {
#if (UNITY_EDITOR)
        public AnimationClip GetDerivedAnimationClipAccordingToRecipe
            (Transform animatorTransform, List<Transform> animatorBonesTransforms,
            string newAnimationClipName, AnimationClip baseAnimationClip)
        {
            AnimationClip result = new AnimationClip
            {
                name = newAnimationClipName
            };
            return result;
        }
#endif
    }
}
