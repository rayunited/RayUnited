﻿using Assets.Scripts.Animations.Models.Model.Curves;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Animations.Models.Rayman.Recipes
{
    public class MissingLeftHandAnimationConstructingRecipeImpl : IAnimConstrRecipe
    {
#if (UNITY_EDITOR)
        public AnimationClip GetDerivedAnimationClipAccordingToRecipe
            (Transform animatorTransform, List<Transform> animatorBonesTransforms,
            string newAnimationClipName, AnimationClip baseAnimationClip)
        {
            AnimationClip result = new AnimationClip
            {
                name = newAnimationClipName
            };

            BonesLocationRotationScaleCurves leftHandAnimationCurves =
                RaymanAnimationClipsCurvesHelper
                .GetLeftHandBonesLocationRotationScaleCurvesForAnimationClip(
                    animatorTransform, animatorBonesTransforms, baseAnimationClip).ZeroOutScaleKeyframes();

            AnimationClipHelper.AddLocationRotationScaleCurvesForAnimationClip(leftHandAnimationCurves, result);

            return result;
        }
#endif
    }
}
