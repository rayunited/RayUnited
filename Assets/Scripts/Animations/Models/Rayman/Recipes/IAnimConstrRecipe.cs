﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Animations.Models.Rayman.Recipes
{
    public interface IAnimConstrRecipe
    {
#if (UNITY_EDITOR)
        public AnimationClip GetDerivedAnimationClipAccordingToRecipe(
            Transform animatorTransform, List<Transform> animatorBonesTransforms,
            string newAnimationClipName, AnimationClip baseAnimationClip);
#endif
    }
}
