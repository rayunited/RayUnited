﻿using System.Collections.Generic;

namespace Assets.Scripts.Animations.Models
{
    public static class RaymanAnimations
    {
        public static string WALKING_ANIMATION = "WALKING_ANIMATION";
        public static string IDLE_ANIMATION = "IDLE_ANIMATION";
        public static string EXITING_RUN_ANIMATION = "EXITING_RUN_ANIMATION";
        public static string RUNNING_ANIMATION = "RUNNING_ANIMATION";
        public static string FALLING_ANIMATION = "FALLING_ANIMATION";
        public static string JUMP_BOUNCE_TO_FALL = "JUMP_BOUNCE_TO_FALL";
        public static string FALLING_FROM_JUMP_FROM_RUNNING_ANIMATION = "FALLING_FROM_JUMP_FROM_RUNNING_ANIMATION";
        public static string JUMP_START_BOUNCE_FROM_GROUND = "JUMP_START_BOUNCE_FROM_GROUND";
        public static string JUMP_START_RUNNING_BOUNCE_FROM_GROUND = "JUMP_START_RUNNING_BOUNCE_FROM_GROUND";

        public static string LANDING_FROM_VERTICAL_JUMP_TO_STANDING = "LANDING_FROM_VERTICAL_JUMP_TO_STANDING";
        public static string LANDING_FROM_JUMP_TO_RUNNING = "LANDING_FROM_JUMP_TO_RUNNING";

        public static string AIR_ROLL_FROM_JUMP_START_RUNNING_BOUNCE_FROM_GROUND_ANIMATION =
            "AIR_ROLL_FROM_JUMP_START_RUNNING_BOUNCE_FROM_GROUND_ANIMATION";

        public static string ACTIVATE_HELICOPTER_FROM_FREE_FALLING = "ACTIVATE_HELICOPTER_FROM_FREE_FALLING";
        public static string HELICOPTER = "HELICOPTER";
        public static string DEACTIVATE_HELICOPTER_STANDING_LANDING_ON_THE_GROUND =
            "DEACTIVATE_HELICOPTER_STANDING_LANDING_ON_THE_GROUND";
        public static string DEACTIVATE_HELICOPTER_AND_STARTING_RUNNING_LANDING_ON_THE_GROUND =
            "DEACTIVATE_HELICOPTER_AND_STARTING_RUNNING_LANDING_ON_THE_GROUND";
        public static string DEACTIVATE_HELICOPTER_AND_STARTING_RUNNING_THEN_IDLE_LANDING_ON_THE_GROUND =
            "DEACTIVATE_HELICOPTER_AND_STARTING_RUNNING_THEN_IDLE_LANDING_ON_THE_GROUND";

        public static string FREE_FALLING_LANDING_AND_THEN_IDLE = "FREE_FALLING_LANDING_AND_THEN_IDLE";

        public static string FREE_FALLING_LANDING_AND_STARTING_RUNNING = "FREE_FALLING_LANDING_AND_STARTING_RUNNING";

        public static string HANGING_ON_LEDGE = "HANGING_ON_LEDGE";
        public static string STARTING_HANGING_ON_LEDGE_FROM_FALLING = "STARTING_HANGING_ON_LEDGE_FROM_FALLING";

        public static string CLIMBING_FROM_HANGING_ON_LEDGE_TO_STANDING = "CLIMBING_FROM_HANGING_ON_LEDGE_TO_STANDING";


        public static string CLIMBING_WALL_UP_RIGHT_45_DEGREES = "CLIMBING_WALL_UP_RIGHT_45_DEGREES";
        public static string CLIMBING_WALL_UP_LEFT_45_DEGREES = "CLIMBING_WALL_UP_LEFT_45_DEGREES";
        public static string CLIMBING_WALL_DOWN_RIGHT_45_DEGREES = "CLIMBING_WALL_DOWN_RIGHT_45_DEGREES";
        public static string CLIMBING_WALL_DOWN_LEFT_45_DEGREES = "CLIMBING_WALL_DOWN_LEFT_45_DEGREES";
        public static string CLIMBING_WALL_UP = "CLIMBING_WALL_UP";
        public static string CLIMBING_WALL_DOWN = "CLIMBING_WALL_DOWN";

        public static string CLIMBING_WALL_LEFT = "CLIMBING_WALL_LEFT";
        public static string CLIMBING_WALL_RIGHT = "CLIMBING_WALL_RIGHT";

        public static string CLIMBING_WALL_IDLE = "CLIMBING_WALL_IDLE";

        public static string ROOF_HANGING_STARTING_GOING_FORWARD = "ROOF_HANGING_STARTING_GOING_FORWARD";
        public static string ROOF_HANGING_IDLE = "ROOF_HANGING_IDLE";
        public static string ROOF_HANGING_ENDING_GOING_FORWARD = "ROOF_HANGING_ENDING_GOING_FORWARD";
        public static string ROOF_HANGING_LEAVING_TO_FREE_FALLING = "ROOF_HANGING_LEAVING_TO_FREE_FALLING";
        public static string ROOF_HANGING_GOING_FORWARD = "ROOF_HANGING_GOING_FORWARD";


        public static string FIST_CHARGING_START_STANDING_LEFT_HAND = "FIST_CHARGING_START_STANDING_LEFT_HAND";
        public static string FIST_CHARGING_ENDING_STANDING_LEFT_HAND = "FIST_CHARGING_ENDING_STANDING_LEFT_HAND";
        public static string FIST_CHARGING_STANDING_LEFT_HAND = "FIST_CHARGING_STANDING_LEFT_HAND";


        public static string FIST_CHARGING_START_STANDING_RIGHT_HAND = "FIST_CHARGING_START_STANDING_RIGHT_HAND";
        public static string FIST_CHARGING_ENDING_STANDING_RIGHT_HAND = "FIST_CHARGING_ENDING_STANDING_RIGHT_HAND";


        public static string FIST_CHARGING_STANDING_RIGHT_HAND = "FIST_CHARGING_STANDING_RIGHT_HAND";
        public static string FIST_SHOOTING_STRAFING_AIR_RIGHT_TO_LEFT = "FIST_SHOOTING_STRAFING_AIR_RIGHT_TO_LEFT";


        public static string FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING =
            "FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING";
        public static string FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER =
            "FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER";


        public static string EXTRA_ANIM_SHOOTING_RIGHT_HAND = "EXTRA_ANIM_SHOOTING_RIGHT_HAND";
        public static string EXTRA_ANIM_SHOOTING_LEFT_HAND = "EXTRA_ANIM_SHOOTING_LEFT_HAND";

        public static string EXTRA_ANIM_MISSING_RIGHT_HAND = "EXTRA_ANIM_MISSING_RIGHT_HAND";
        public static string EXTRA_ANIM_MISSING_LEFT_HAND = "EXTRA_ANIM_MISSING_LEFT_HAND";

        public static string NON_OVERRIDING_ANIMATION_LAYER_1 = "NON_OVERRIDING_ANIMATION_LAYER_1";
        public static string NON_OVERRIDING_ANIMATION_LAYER_2 = "NON_OVERRIDING_ANIMATION_LAYER_2";


        public static string STRAFING_IDLE = "STRAFING_IDLE";
        public static string STRAFING_IDLE_ATTACKING_FISTS_CYCLE_WAIT_IDLE = "STRAFING_IDLE_ATTACKING_FISTS_CYCLE_WAIT_IDLE";
        public static string STRAFING_RIGHT = "STRAFING_RIGHT";
        public static string STRAFING_LEFT = "STRAFING_LEFT";


        public static string STRAFING_FORWARD = "STRAFING_FORWARD";
        public static string STRAFING_BACKWARDS = "STRAFING_BACKWARDS";

        public static string STRAFING_LEFT_BACKWARDS = "STRAFING_LEFT_BACKWARDS";
        public static string STRAFING_RIGHT_BACKWARDS = "STRAFING_RIGHT_BACKWARDS";
        public static string STRAFING_RIGHT_FORWARD = "STRAFING_RIGHT_FORWARD";
        public static string STRAFING_LEFT_FORWARD = "STRAFING_LEFT_FORWARD";

        public static string STRAFING_JUMPING_BEGINNING_LEFT = "STRAFING_JUMPING_BEGINNING_LEFT";
        public static string STRAFING_JUMPING_ROLL_LEFT = "STRAFING_JUMPING_ROLL_LEFT";
        public static string STRAFING_JUMPING_LEFT_AFTER_ROLL_FALLING = "STRAFING_JUMPING_LEFT_AFTER_ROLL_FALLING";

        public static string STRAFING_JUMPING_BEGINNING_RIGHT = "STRAFING_JUMPING_BEGINNING_RIGHT";
        public static string STRAFING_JUMPING_RIGHT_FALLING_PHASE = "STRAFING_JUMPING_RIGHT_FALLING_PHASE";

        public static string STRAFING_JUMPING_FORWARD = "STRAFING_JUMPING_FORWARD";

        public static string STRAFING_JUMPING_BACKWARDS_BEGINNING = "STRAFING_JUMPING_BACKWARDS_BEGINNING";

        public static string STRAFING_JUMPING_BACKWARDS_AIR_FLIP = "STRAFING_JUMPING_BACKWARDS_AIR_FLIP";

        public static string STRAFING_JUMPING_BACKWARDS_AFTER_ROLL_FALLING =
            "STRAFING_JUMPING_BACKWARDS_AFTER_ROLL_FALLING";

        public static string STRAFING_JUMPING_RIGHT_CHANGING_DIRECTION_TO_LEFT_IN_AIR =
            "STRAFING_JUMPING_RIGHT_CHANGING_DIRECTION_TO_LEFT_IN_AIR";

        public static string STRAFING_JUMPING_FORWARD_FIRST_THEN_STOPPED_FORWARD_MOVEMENT_IN_AIR_AFTER =
            "STRAFING_JUMPING_FORWARD_FIRST_THEN_STOPPED_FORWARD_MOVEMENT_IN_AIR_AFTER";

        public static string STRAFING_ROLL_FORWARD = "STRAFING_ROLL_FORWARD";

        public static string STRAFING_ROLL_LEFT = "STRAFING_ROLL_LEFT";
        public static string STRAFING_ROLL_RIGHT = "STRAFING_ROLL_RIGHT";

        public static string STRAFING_FLIP_BACKWARDS = "STRAFING_FLIP_BACKWARDS";

        public static string STRAFING_EXIT_INTO_IDLE_GROUND = "STRAFING_EXIT_INTO_IDLE_GROUND";

        public static string STRAFING_ENTER_FROM_IDLE_GROUND = "STRAFING_ENTER_FROM_IDLE_GROUND";

        public static string TAKING_DAMAGE_BEFORE_IDLE_GROUND = "TAKING_DAMAGE_BEFORE_IDLE_GROUND";
        public static string TAKING_DAMAGE_STANDING_ON_GROUND = "TAKING_DAMAGE_STANDING_ON_GROUND";

        public static string TAKING_DAMAGE_AIR_FREE_FALLING = "TAKING_DAMAGE_AIR_FREE_FALLING";

        public static string DYING_FALLING_DOWN = "DYING_FALLING_DOWN";

        public static string DYING_R2_STYLE_FALLING_INTO_PARTS = "DYING_R2_STYLE_FALLING_INTO_PARTS";

        public static string MELEE_KICK_RIGHT_SHOE = "MELEE_KICK_RIGHT_SHOE";



        public static string LUM_SWINGING_SEQUENCE_END_POSE_AT_THE_FRONT_LEFT_HAND =
            "LUM_SWINGING_SEQUENCE_END_POSE_AT_THE_FRONT_LEFT_HAND";

        public static string LUM_SWINGING_SEQUENCE_START_POSE_AT_THE_BACK_LEFT_HAND =
            "LUM_SWINGING_SEQUENCE_START_POSE_AT_THE_BACK_LEFT_HAND";

        public static string LUM_SWINGING_SEQUENCE_POSES_IN_THE_MIDDLE_BACK_TO_FRONT_LEFT_HAND =
            "LUM_SWINGING_SEQUENCE_POSES_IN_THE_MIDDLE_BACK_TO_FRONT_LEFT_HAND";

        public static string LUM_SWINGING_SEQUENCE_POSES_IN_THE_MIDDLE_FRONT_TO_BACK_LEFT_HAND =
            "LUM_SWINGING_SEQUENCE_POSES_IN_THE_MIDDLE_FRONT_TO_BACK_LEFT_HAND";


        public static string LUM_SWINGING_SEQUENCE_POSES_IN_THE_MIDDLE_FRONT_TO_BACK_RIGHT_HAND =
            "LUM_SWINGING_SEQUENCE_POSES_IN_THE_MIDDLE_FRONT_TO_BACK_RIGHT_HAND";

        public static string LUM_SWINGING_SEQUENCE_START_POSE_AT_THE_BACK_RIGHT_HAND =
            "LUM_SWINGING_SEQUENCE_START_POSE_AT_THE_BACK_RIGHT_HAND";

        public static string LUM_SWINGING_SEQUENCE_POSES_IN_THE_MIDDLE_BACK_TO_FRONT_RIGHT_HAND =
            "LUM_SWINGING_SEQUENCE_POSES_IN_THE_MIDDLE_BACK_TO_FRONT_RIGHT_HAND";

        public static string LUM_SWINGING_SEQUENCE_END_POSE_AT_THE_FRONT_RIGHT_HAND =
            "LUM_SWINGING_SEQUENCE_END_POSE_AT_THE_FRONT_RIGHT_HAND";


        public static string WALKING_CYCLE_1 = "WALKING_CYCLE_1";

        public static string WALKING_CYCLE_2 = "WALKING_CYCLE_2";

        public static string WALKING_RUNNING_CYCLE_3 = "WALKING_RUNNING_CYCLE_3";

        public static string RUNNING_CYCLE_4 = "RUNNING_CYCLE_4";



        public static string UPRISING_FROM_GROUND_IDLE_TO_WALKING_CYCLE_1 = "UPRISING_FROM_GROUND_IDLE_TO_WALKING_CYCLE_1";

        public static string UPRISING_FROM_GROUND_IDLE_TO_RUNNING_CYCLE_4 = "UPRISING_FROM_GROUND_IDLE_TO_RUNNING_CYCLE_4";

        public static string EXITING_WALKING_CYCLE_1_THEN_TO_WALKING_CYCLE_2 = "EXITING_WALKING_CYCLE_1_THEN_TO_WALKING_CYCLE_2";

        public static string EXITING_WALKING_CYCLE_2_TO_GROUND_IDLE = "EXITING_WALKING_CYCLE_2_TO_GROUND_IDLE";

        public static string EXITING_RUNNING_CYCLE_4_TO_GROUND_IDLE_THEN_TO_RUNNING_CYCLE_4 =
            "EXITING_RUNNING_CYCLE_4_TO_GROUND_IDLE_THEN_TO_RUNNING_CYCLE_4";

        public static string EXITING_RUNNING_CYCLE_4_TO_GROUND_IDLE = "EXITING_RUNNING_CYCLE_4_TO_GROUND_IDLE";

        public static string EXITING_IMMEDIATELY_RUNNING_CYCLE_4_TO_GROUND_IDLE =
            "EXITING_IMMEDIATELY_RUNNING_CYCLE_4_TO_GROUND_IDLE";



        public static string LANDING_FROM_VERTICAL_JUMP_TO_GROUND_IDLE = "LANDING_FROM_VERTICAL_JUMP_TO_GROUND_IDLE";

        public static string LANDING_FROM_VERTICAL_JUMP_TO_WALKING_CYCLE_2 = "LANDING_FROM_VERTICAL_JUMP_TO_WALKING_CYCLE_2";

        public static string LANDING_FROM_HELICOPTER_TO_GROUND_IDLE = "LANDING_FROM_HELICOPTER_TO_GROUND_IDLE";

        public static string LANDING_FROM_HELICOPTER_TO_RUNNING_CYCLE_4 = "LANDING_FROM_HELICOPTER_TO_RUNNING_CYCLE_4";

        public static string LANDING_FROM_RUNNING_JUMP_AFTER_AIR_FLIP_TO_RUNNING_CYCLE_4 =
            "LANDING_FROM_RUNNING_JUMP_AFTER_AIR_FLIP_TO_RUNNING_CYCLE_4";

        public static string LANDING_FROM_RUNNING_JUMP_AFTER_AIR_FLIP_TO_GROUND_IDLE =
            "LANDING_FROM_RUNNING_JUMP_AFTER_AIR_FLIP_TO_GROUND_IDLE";

        public static string LANDING_FROM_RUNNING_JUMP_AFTER_AIR_FLIP_TO_WALKING_CYCLE_2 =
            "LANDING_FROM_RUNNING_JUMP_AFTER_AIR_FLIP_TO_WALKING_CYCLE_2";


        public static Dictionary<string, AnimationInfo> animations = new Dictionary<string, AnimationInfo>() {
            {  IDLE_ANIMATION, new AnimationInfo ("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 0", true, 1.0f ) },
            //{  EXITING_RUN_ANIMATION, new AnimationInfo ("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 88", false, 1.0f ) },
            {  WALKING_ANIMATION, new AnimationInfo ("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 1", true, 1.0f ) },
            {  RUNNING_ANIMATION, new AnimationInfo ("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 2", true, 2.3f ) },



            { LANDING_FROM_VERTICAL_JUMP_TO_STANDING,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 130", false, 2.0f) },
            { LANDING_FROM_JUMP_TO_RUNNING,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 131", false, 2.0f) },



            { FALLING_ANIMATION, new AnimationInfo ("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 6", true, 2.0f ) },
            { FALLING_FROM_JUMP_FROM_RUNNING_ANIMATION, new AnimationInfo ("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 31", true, 2.0f ) },
            { JUMP_START_BOUNCE_FROM_GROUND, new AnimationInfo ("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 5", true, 1.0f) },
            { JUMP_BOUNCE_TO_FALL, new AnimationInfo ("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 4", false, 2.5f ) },
            { JUMP_START_RUNNING_BOUNCE_FROM_GROUND, new AnimationInfo ("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 30", true, 2.5f ) },

            { AIR_ROLL_FROM_JUMP_START_RUNNING_BOUNCE_FROM_GROUND_ANIMATION,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 29", false, 2.8f) },

            { ACTIVATE_HELICOPTER_FROM_FREE_FALLING,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 14", false, 2.5f) },

            { HELICOPTER,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 15", true, 2.5f) },

            { DEACTIVATE_HELICOPTER_STANDING_LANDING_ON_THE_GROUND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 41", false, 2.5f) },

            { DEACTIVATE_HELICOPTER_AND_STARTING_RUNNING_LANDING_ON_THE_GROUND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 42", false, 2.5f) },

            { DEACTIVATE_HELICOPTER_AND_STARTING_RUNNING_THEN_IDLE_LANDING_ON_THE_GROUND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 58", false, 2.5f) },

            { FREE_FALLING_LANDING_AND_STARTING_RUNNING,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 56", false, 2.5f) },

            { FREE_FALLING_LANDING_AND_THEN_IDLE,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 130", false, 2.5f) },



             { HANGING_ON_LEDGE,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 12", true, 1.5f) },

             { STARTING_HANGING_ON_LEDGE_FROM_FALLING,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 13", false, 2.5f) },

             { CLIMBING_FROM_HANGING_ON_LEDGE_TO_STANDING,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 16", false, 2.5f) },



            #region wallClimbing
            { CLIMBING_WALL_UP_RIGHT_45_DEGREES,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 126", true, 2.0f) },
             { CLIMBING_WALL_DOWN_RIGHT_45_DEGREES,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 128", true, 2.0f) },
             { CLIMBING_WALL_UP_LEFT_45_DEGREES,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 127", true, 2.0f) },
             { CLIMBING_WALL_DOWN_LEFT_45_DEGREES,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 129", true, 2.0f) },
             { CLIMBING_WALL_UP,
               new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 18", true, 2.0f) },
             { CLIMBING_WALL_DOWN,
               new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 19", true, 2.0f) },

              { CLIMBING_WALL_LEFT,
               new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 20", true, 2.0f) },
              { CLIMBING_WALL_RIGHT,
               new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 21", true, 2.0f) },

              { CLIMBING_WALL_IDLE,
               new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 17", true, 2.0f) },
            #endregion

            #region roofHanging
            { ROOF_HANGING_STARTING_GOING_FORWARD,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 7", false, 2.0f) },
            { ROOF_HANGING_IDLE,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 8", true, 2.0f) },
            { ROOF_HANGING_GOING_FORWARD,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 123", true, 2.0f) },
            { ROOF_HANGING_ENDING_GOING_FORWARD,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 9", false, 2.0f) },
            { ROOF_HANGING_LEAVING_TO_FREE_FALLING,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 11", false, 2.0f) },

            #endregion

            #region shooting
            { FIST_CHARGING_START_STANDING_LEFT_HAND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 73", false, 2.0f) },
            { FIST_CHARGING_ENDING_STANDING_LEFT_HAND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 142", false, 2.0f) },

            { FIST_CHARGING_START_STANDING_RIGHT_HAND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 140", false, 2.0f) },
            { FIST_CHARGING_ENDING_STANDING_RIGHT_HAND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 143", false, 2.0f) },

            { FIST_CHARGING_STANDING_LEFT_HAND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 74", true, 2.0f) },
            { FIST_CHARGING_STANDING_RIGHT_HAND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 141", true, 2.0f) },

            { FIST_SHOOTING_STRAFING_AIR_RIGHT_TO_LEFT,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 103", false, 2.0f) },

            // this could be used (part of curves for hand's bones respectively as could not find
            // dedicated anim for just hands shooting :( it is tricky in general
            { FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 106", false, 3.0f) },
            { FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 107", false, 3.0f) },
            #endregion

            #region strafing
            { STRAFING_IDLE,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 89", true, 2.0f) },
            { STRAFING_IDLE_ATTACKING_FISTS_CYCLE_WAIT_IDLE,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 120", false, 2.0f) },


            { STRAFING_RIGHT,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 24", true, 2.0f) },
            { STRAFING_LEFT,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 25", true, 2.0f) },
            { STRAFING_FORWARD,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 26", true, 2.0f) },
            { STRAFING_BACKWARDS,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 27", true, 2.0f) },

            { STRAFING_LEFT_BACKWARDS,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 116", true, 2.0f) },
            { STRAFING_RIGHT_BACKWARDS,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 117", true, 2.0f) },
            { STRAFING_RIGHT_FORWARD,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 118", true, 2.0f) },
            { STRAFING_LEFT_FORWARD,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 119", true, 2.0f) },


            { STRAFING_JUMPING_BEGINNING_LEFT,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 93", true, 2.5f) },
            { STRAFING_JUMPING_ROLL_LEFT,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 94", false, 3.2f) },
            { STRAFING_JUMPING_LEFT_AFTER_ROLL_FALLING,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 95", true, 2.0f) },


            { STRAFING_JUMPING_BEGINNING_RIGHT,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 96", true, 2.5f) },
            { STRAFING_JUMPING_RIGHT_FALLING_PHASE,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 97", true, 2.0f) },


            { STRAFING_JUMPING_FORWARD,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 99", true, 2.5f) },

            { STRAFING_JUMPING_BACKWARDS_BEGINNING,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 100", true, 2.5f) },
            { STRAFING_JUMPING_BACKWARDS_AIR_FLIP,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 102", false, 2.8f) },
            { STRAFING_JUMPING_BACKWARDS_AFTER_ROLL_FALLING,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 101", true, 2.0f) },


            { STRAFING_JUMPING_RIGHT_CHANGING_DIRECTION_TO_LEFT_IN_AIR,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 103", false, 2.8f) },
            { STRAFING_JUMPING_FORWARD_FIRST_THEN_STOPPED_FORWARD_MOVEMENT_IN_AIR_AFTER,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 104", false, 2.5f) },


            { STRAFING_ROLL_FORWARD,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 64", false, 2.8f) },
            { STRAFING_ROLL_LEFT,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 67", false, 2.8f) },
            { STRAFING_ROLL_RIGHT,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 68", false, 2.8f) },
            { STRAFING_FLIP_BACKWARDS,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 65", false, 2.8f) },


            { STRAFING_EXIT_INTO_IDLE_GROUND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 69", false, 2.5f) },
            { STRAFING_ENTER_FROM_IDLE_GROUND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 105", false, 2.0f) },
            #endregion

            { TAKING_DAMAGE_BEFORE_IDLE_GROUND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 32", false, 2.0f) },

            { TAKING_DAMAGE_STANDING_ON_GROUND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 121", false, 2.0f) },

            { TAKING_DAMAGE_AIR_FREE_FALLING,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 45", false, 2.0f) },
            { DYING_FALLING_DOWN,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 43", false, 2.0f) },
            { DYING_R2_STYLE_FALLING_INTO_PARTS,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 49", false, 2.0f) },

            { MELEE_KICK_RIGHT_SHOE,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 61", false, 2.0f) },

            #region lumSwinging
            { LUM_SWINGING_SEQUENCE_END_POSE_AT_THE_FRONT_LEFT_HAND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 75", false, 2.0f) },
            { LUM_SWINGING_SEQUENCE_START_POSE_AT_THE_BACK_LEFT_HAND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 76", false, 2.0f) },
            // here we should adjust the speed of that animation to the arc length
            // that the player character does (depending on the swinging radius length)
            { LUM_SWINGING_SEQUENCE_POSES_IN_THE_MIDDLE_BACK_TO_FRONT_LEFT_HAND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 77", false, 2.0f) },
            { LUM_SWINGING_SEQUENCE_POSES_IN_THE_MIDDLE_FRONT_TO_BACK_LEFT_HAND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 78", false, 2.0f) },


            { LUM_SWINGING_SEQUENCE_POSES_IN_THE_MIDDLE_FRONT_TO_BACK_RIGHT_HAND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 79", false, 2.0f) },
            { LUM_SWINGING_SEQUENCE_START_POSE_AT_THE_BACK_RIGHT_HAND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 80", false, 2.0f) },
            { LUM_SWINGING_SEQUENCE_POSES_IN_THE_MIDDLE_BACK_TO_FRONT_RIGHT_HAND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 81", false, 2.0f) },
            { LUM_SWINGING_SEQUENCE_END_POSE_AT_THE_FRONT_RIGHT_HAND,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 82", false, 2.0f) },
            #endregion

            #region walking
            { WALKING_CYCLE_1,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 53", true, 2.3f) },
            { WALKING_CYCLE_2,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 1", true, 3f) },
            { WALKING_RUNNING_CYCLE_3,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 54", true, 1.0f) },
            { RUNNING_CYCLE_4,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 71", true, 1.0f) },
            #endregion

            #region uprisingsGroundWalkingAndRunning
            { UPRISING_FROM_GROUND_IDLE_TO_WALKING_CYCLE_1,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 83", false, 2.3f) },
            { UPRISING_FROM_GROUND_IDLE_TO_RUNNING_CYCLE_4,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 84", false, 2.3f) },
            { EXITING_WALKING_CYCLE_1_THEN_TO_WALKING_CYCLE_2,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 85", false, 2.3f) },
            { EXITING_WALKING_CYCLE_2_TO_GROUND_IDLE,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 86", false, 2.3f) },

            { EXITING_RUNNING_CYCLE_4_TO_GROUND_IDLE_THEN_TO_RUNNING_CYCLE_4,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 87", false, 2.3f) },
            { EXITING_RUNNING_CYCLE_4_TO_GROUND_IDLE,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 88", false, 2.9f) },
            { EXITING_IMMEDIATELY_RUNNING_CYCLE_4_TO_GROUND_IDLE,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 144", false, 1f) },
            #endregion

            #region landing
            { LANDING_FROM_VERTICAL_JUMP_TO_GROUND_IDLE,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 3", false, 2.3f) },
            { LANDING_FROM_VERTICAL_JUMP_TO_WALKING_CYCLE_2,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 191", false, 2.3f) },
            { LANDING_FROM_HELICOPTER_TO_GROUND_IDLE,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 41", false, 2.3f) },
            { LANDING_FROM_HELICOPTER_TO_RUNNING_CYCLE_4,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 42", false, 2.3f) },
            { LANDING_FROM_RUNNING_JUMP_AFTER_AIR_FLIP_TO_RUNNING_CYCLE_4,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 56", false, 2.3f) },
            { LANDING_FROM_RUNNING_JUMP_AFTER_AIR_FLIP_TO_GROUND_IDLE,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 130", false, 2.3f) },

            { LANDING_FROM_RUNNING_JUMP_AFTER_AIR_FLIP_TO_WALKING_CYCLE_2,
                new AnimationInfo("OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT_Animation 131", false, 2.3f) },
            #endregion
    };



        public static string IdleAnimationStateName()
        {
            return animations[IDLE_ANIMATION].animationName;
        }

        public static string JumpStartBounceFromGroundAnimationStateName()
        {
            return animations[JUMP_START_BOUNCE_FROM_GROUND].animationName;
        }

        public static string JumpStartRunningBounceFromGroundAnimationStateName()
        {
            return animations[JUMP_START_RUNNING_BOUNCE_FROM_GROUND].animationName;
        }

        public static string FallingAnimationStateName()
        {
            return animations[FALLING_ANIMATION].animationName;
        }

        public static string FallingFromJumpFromRunningAnimationStateName()
        {
            return animations[FALLING_FROM_JUMP_FROM_RUNNING_ANIMATION].animationName;
        }

        public static string ExitingRunAnimationStateName()
        {
            return animations[EXITING_RUN_ANIMATION].animationName;
        }

        public static string WalkingAnimationStateName()
        {
            return animations[WALKING_ANIMATION].animationName;
        }

        public static string RunningAnimationStateName()
        {
            return animations[RUNNING_ANIMATION].animationName;
        }

        public static string AirRollFromJumpStartRunningBounceFromGroundAnimationStateName()
        {
            return animations[AIR_ROLL_FROM_JUMP_START_RUNNING_BOUNCE_FROM_GROUND_ANIMATION].animationName;
        }

        public static string ActivateHelicopterFromFreeFallingAnimationStateName()
        {
            return animations[ACTIVATE_HELICOPTER_FROM_FREE_FALLING].animationName;
        }

        public static string HelicopterStateName()
        {
            return animations[HELICOPTER].animationName;
        }

        public static string StartingHangingOnLedgeFromFallingStateName()
        {
            return animations[STARTING_HANGING_ON_LEDGE_FROM_FALLING].animationName;
        }

        public static string HangingOnLedgeStateName()
        {
            return animations[HANGING_ON_LEDGE].animationName;
        }

        public static string ClimbingFromHangingOnLedgeStateName()
        {
            return animations[CLIMBING_FROM_HANGING_ON_LEDGE_TO_STANDING].animationName;
        }

        public static string WallClimbingIdleStateName()
        {
            return animations[CLIMBING_WALL_IDLE].animationName;
        }

        public static string WallClimbingUpStateName()
        {
            return animations[CLIMBING_WALL_UP].animationName;
        }

        public static string WallClimbingDownStateName()
        {
            return animations[CLIMBING_WALL_DOWN].animationName;
        }

        public static string WallClimbingDownLeftStateName()
        {
            return animations[CLIMBING_WALL_DOWN_LEFT_45_DEGREES].animationName;
        }

        public static string WallClimbingDownRightStateName()
        {
            return animations[CLIMBING_WALL_DOWN_RIGHT_45_DEGREES].animationName;
        }

        public static string WallClimbingLeftStateName()
        {
            return animations[CLIMBING_WALL_LEFT].animationName;
        }

        public static string WallClimbingRightStateName()
        {
            return animations[CLIMBING_WALL_RIGHT].animationName;
        }

        public static string WallClimbingUpLeftStateName()
        {
            return animations[CLIMBING_WALL_UP_LEFT_45_DEGREES].animationName;
        }

        public static string WallClimbingUpRightStateName()
        {
            return animations[CLIMBING_WALL_UP_RIGHT_45_DEGREES].animationName;
        }

        public static string RoofHangingIdleStateName()
        {
            return animations[ROOF_HANGING_IDLE].animationName;
        }

        public static string RoofHangingLeavingStateName()
        {
            return animations[ROOF_HANGING_LEAVING_TO_FREE_FALLING].animationName;
        }

        public static string RoofHangingGoingForwardStateName()
        {
            return animations[ROOF_HANGING_GOING_FORWARD].animationName;
        }

        public static string RoofHangingStartingGoingForwardStateName()
        {
            return animations[ROOF_HANGING_STARTING_GOING_FORWARD].animationName;
        }

        public static string RoofHangingEndingGoingForwardStateName()
        {
            return animations[ROOF_HANGING_ENDING_GOING_FORWARD].animationName;
        }

        public static string ExtraAnimShootingRightHandStateName()
        {
            return EXTRA_ANIM_SHOOTING_RIGHT_HAND;
        }

        public static string ExtraAnimMissingRightHandStateName()
        {
            return EXTRA_ANIM_MISSING_RIGHT_HAND;
        }

        public static string ExtraAnimShootingLeftHandStateName()
        {
            return EXTRA_ANIM_SHOOTING_LEFT_HAND;
        }

        public static string ExtraAnimMissingLeftHandStateName()
        {
            return EXTRA_ANIM_MISSING_LEFT_HAND;
        }

        public static string NonOverridingAnimationLayer1StateName()
        {
            return NON_OVERRIDING_ANIMATION_LAYER_1;
        }

        public static string NonOverridingAnimationLayer2StateName()
        {
            return NON_OVERRIDING_ANIMATION_LAYER_2;
        }

        public static string StrafingIdleStateName()
        {
            return animations[STRAFING_IDLE].animationName;
        }

        public static string StrafingForwardStateName()
        {
            return animations[STRAFING_FORWARD].animationName;
        }

        public static string StrafingBackwardsStateName()
        {
            return animations[STRAFING_BACKWARDS].animationName;
        }

        public static string StrafingForwardRightStateName()
        {
            return animations[STRAFING_RIGHT_FORWARD].animationName;
        }

        public static string StrafingForwardLeftStateName()
        {
            return animations[STRAFING_LEFT_FORWARD].animationName;
        }

        public static string StrafingRightStateName()
        {
            return animations[STRAFING_RIGHT].animationName;
        }

        public static string StrafingLeftStateName()
        {
            return animations[STRAFING_LEFT].animationName;
        }

        public static string StrafingBackwardsRightStateName()
        {
            return animations[STRAFING_RIGHT_BACKWARDS].animationName;
        }

        public static string StrafingBackwardsLeftStateName()
        {
            return animations[STRAFING_LEFT_BACKWARDS].animationName;
        }

        public static string JumpStartBounceFromGroundForwardStrafingAirAnimationStateName()
        {
            return animations[STRAFING_JUMPING_FORWARD].animationName;
        }

        public static string JumpStartBounceFromGroundBackwardsStrafingAirAnimationStateName()
        {
            return animations[STRAFING_JUMPING_BACKWARDS_BEGINNING].animationName;
        }

        public static string JumpStartBounceFromGroundLeftStrafingAirAnimationStateName()
        {
            return animations[STRAFING_JUMPING_BEGINNING_LEFT].animationName;
        }

        public static string JumpStartBounceFromGroundRightStrafingAirAnimationStateName()
        {
            return animations[STRAFING_JUMPING_BEGINNING_RIGHT].animationName;
        }

        public static string StrafingEnterFromIdleGroundStateName()
        {
            return animations[STRAFING_ENTER_FROM_IDLE_GROUND].animationName;
        }

        public static string StrafingExitIntoIdleGroundStateName()
        {
            return animations[STRAFING_EXIT_INTO_IDLE_GROUND].animationName;
        }

        public static string StrafingAirJumpExtremumAirRollForwardAnimationStateName()
        {
            return animations[STRAFING_JUMPING_FORWARD].animationName;
        }

        public static string StrafingAirJumpExtremumAirRollBackwardsAnimationStateName()
        {
            return animations[STRAFING_JUMPING_BACKWARDS_AIR_FLIP].animationName;
        }

        public static string StrafingAirJumpExtremumAirRollLeftAnimationStateName()
        {
            return animations[STRAFING_JUMPING_ROLL_LEFT].animationName;
        }

        public static string StrafingAirJumpExtremumAirRollRightAnimationStateName()
        {
            return animations[STRAFING_JUMPING_RIGHT_CHANGING_DIRECTION_TO_LEFT_IN_AIR].animationName;
        }

        public static string StrafingAirFallingAfterExtremumAirRollForwardAnimationStateName()
        {
            return animations[STRAFING_JUMPING_FORWARD].animationName;
        }

        public static string StrafingAirFallingAfterExtremumAirRollBackwardsAnimationStateName()
        {
            return animations[STRAFING_JUMPING_BACKWARDS_AFTER_ROLL_FALLING].animationName;
        }

        public static string StrafingAirFallingAfterExtremumAirRollLeftAnimationStateName()
        {
            return animations[STRAFING_JUMPING_LEFT_AFTER_ROLL_FALLING].animationName;
        }

        public static string StrafingAirFallingAfterExtremumAirRollRightAnimationStateName()
        {
            return animations[STRAFING_JUMPING_RIGHT_FALLING_PHASE].animationName;
        }

        public static string StrafingRollForwardStateName()
        {
            return animations[STRAFING_ROLL_FORWARD].animationName;
        }

        public static string StrafingRollBackwardsStateName()
        {
            return animations[STRAFING_FLIP_BACKWARDS].animationName;
        }

        public static string StrafingRollLeftStateName()
        {
            return animations[STRAFING_ROLL_LEFT].animationName;
        }

        public static string StrafingRollRightStateName()
        {
            return animations[STRAFING_ROLL_RIGHT].animationName;
        }

        public static string LumSwingingSequencePosesInTheMiddleBackToFrontRightHandStateName()
        {
            return animations[LUM_SWINGING_SEQUENCE_POSES_IN_THE_MIDDLE_BACK_TO_FRONT_RIGHT_HAND].animationName;
        }

        public static string LumSwingingSequencePosesInTheMiddleFrontToBackRightHandStateName()
        {
            return animations[LUM_SWINGING_SEQUENCE_POSES_IN_THE_MIDDLE_FRONT_TO_BACK_RIGHT_HAND].animationName;
        }

        public static string WalkingCycle1StateName()
        {
            return animations[WALKING_CYCLE_1].animationName;
        }

        public static string WalkingCycle2StateName()
        {
            return animations[WALKING_CYCLE_2].animationName;
        }

        public static string WalkingRunningCycle3StateName()
        {
            return animations[WALKING_RUNNING_CYCLE_3].animationName;
        }

        public static string RunningCycle4StateName()
        {
            return animations[RUNNING_CYCLE_4].animationName;
        }

        public static string UprisingFromGroundIdleToWalkingCycle1StateName()
        {
            return animations[UPRISING_FROM_GROUND_IDLE_TO_WALKING_CYCLE_1].animationName;
        }

        public static string UprisingFromGroundIdleToRunningCycle4StateName()
        {
            return animations[UPRISING_FROM_GROUND_IDLE_TO_RUNNING_CYCLE_4].animationName;
        }

        public static string ExitingRunningCycle4ToGroundIdleStateName()
        {
            return animations[EXITING_RUNNING_CYCLE_4_TO_GROUND_IDLE].animationName;
        }

        public static string ExitingWalkingCycle2ToGroundIdleStateName()
        {
            return animations[EXITING_WALKING_CYCLE_2_TO_GROUND_IDLE].animationName;
        }

        public static string LandingFromVerticalJumpToGroundIdleStateName()
        {
            return animations[LANDING_FROM_VERTICAL_JUMP_TO_GROUND_IDLE].animationName;
        }

        public static string LandingFromVerticalJumpToWalkingCycle2StateName()
        {
            return animations[LANDING_FROM_VERTICAL_JUMP_TO_WALKING_CYCLE_2].animationName;
        }

        public static string LandingFromHelicopterToGroundIdleStateName()
        {
            return animations[LANDING_FROM_HELICOPTER_TO_GROUND_IDLE].animationName;
        }

        public static string LandingFromHelicopterToRunningCycle4StateName()
        {
            return animations[LANDING_FROM_HELICOPTER_TO_RUNNING_CYCLE_4].animationName;
        }

        public static string LandingFromRunningJumpAfterAirFlipToRunningCycle4StateName()
        {
            return animations[LANDING_FROM_RUNNING_JUMP_AFTER_AIR_FLIP_TO_RUNNING_CYCLE_4].animationName;
        }

        public static string LandingFromRunningJumpAfterAirFlipToGroundIdleStateName()
        {
            return animations[LANDING_FROM_RUNNING_JUMP_AFTER_AIR_FLIP_TO_GROUND_IDLE].animationName;
        }

        public static string LandingFromRunningJumpAfterAirFlipToWalkingCycle2StateName()
        {
            return animations[LANDING_FROM_RUNNING_JUMP_AFTER_AIR_FLIP_TO_WALKING_CYCLE_2].animationName;
        }

        public static string JumpBounceToFall()
        {
            return animations[JUMP_BOUNCE_TO_FALL].animationName;
        }
    }
}
