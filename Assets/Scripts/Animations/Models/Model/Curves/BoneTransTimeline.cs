﻿using Assets.Scripts.Animations.Curves;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Animations.Models.Model.Curves
{
    public static class FloatCurvesTimelineHelper
    {
        public static List<int>
            GetUniformFramesNumbersForEstablishedKeyframing(
                Tuple<AnimationCurve, AnimationCurve, AnimationCurve> positionTransformsCurves,
                Tuple<AnimationCurve, AnimationCurve, AnimationCurve, AnimationCurve> rotationTransformCurves,
                Tuple<AnimationCurve, AnimationCurve, AnimationCurve> scaleTransformCurves,
                float frameRate,
                int animationFrames)
        {
            int maxFramesPosition =
                (int)Math.Round(
                    positionTransformsCurves.Item1.keys[positionTransformsCurves.Item1.length - 1].time * frameRate);

            int maxFramesRotation =
                (int)Math.Round(
                    rotationTransformCurves.Item1.keys[rotationTransformCurves.Item1.length - 1].time * frameRate);

            int maxFramesScale =
                (int)Math.Round(
                    scaleTransformCurves.Item1.keys[scaleTransformCurves.Item1.length - 1].time * frameRate);

            int maxFramesOverall = (new List<int>() { maxFramesPosition, maxFramesRotation, maxFramesScale })
                .Max();

            //return animationFrames;

            List<int> result = new List<int>
            {
                1
            };
            for (int i = 2; i < animationFrames; i += 3)
            {
                result.Add(i);
            }
            result.Add(animationFrames);

            //if (maxFramesOverall == maxFramesPosition)
            //{
            //    result = positionTransformsCurves.Item1.keys.Select(x => (int)Math.Round(x.time * frameRate)).ToList();
            //}
            //else if (maxFramesOverall == maxFramesRotation)
            //{
            //    result = rotationTransformCurves.Item1.keys.Select(x => (int)Math.Round(x.time * frameRate)).ToList();
            //}
            //else
            //{
            //    result = scaleTransformCurves.Item1.keys.Select(x => (int)Math.Round(x.time * frameRate)).ToList();
            //}

            //if (!result.Contains(animationFrames))
            //{
            //    result.Add(animationFrames);
            //}

            //if (!result.Contains(1))
            //{
            //    result.Add(1);
            //}

            return result;
        }

        public static float GetProperValueForCurve
            (AnimationCurve animationCurve,
             int frameNumber,
             float frameRate,
             int overallAnimationFrames)
        {
            Keyframe closestProperKeyframe = animationCurve.keys.OrderBy(
                x => Mathf.Abs(frameNumber - (x.time * frameRate))).First();

            int closestProperKeyframeFrameNumber = (int)Math.Round(closestProperKeyframe.time * frameRate);

            if (frameNumber >= (int)Math.Round(animationCurve.keys[animationCurve.length - 1].time * frameRate))
            {
                return closestProperKeyframe.value;
            }
            else if (frameNumber == closestProperKeyframeFrameNumber)
            {
                return closestProperKeyframe.value;
            }
            else
            {
                Keyframe startInterpolationKeyframe;
                Keyframe endInterpolationKeyframe;

                startInterpolationKeyframe =
                    animationCurve.keys.Where(
                        x => (int)Math.Round(x.time * frameRate) <= frameNumber)
                        .OrderBy(x => (int)Math.Round(x.time * frameRate)).Last();

                endInterpolationKeyframe =
                    animationCurve.keys.Where(
                        x => (int)Math.Round(x.time * frameRate) >= frameNumber)
                        .OrderBy(x => (int)Math.Round(x.time * frameRate)).First();

                int startInterpolationKeyframeFrame = (int)Math.Round(startInterpolationKeyframe.time * frameRate);
                int endInterpolationKeyframeFrame = (int)Math.Round(endInterpolationKeyframe.time * frameRate);

                int framesDifference = endInterpolationKeyframeFrame - startInterpolationKeyframeFrame;

                return Mathf.Lerp(
                    GetProperValueForCurve(
                        animationCurve, startInterpolationKeyframeFrame, frameRate, overallAnimationFrames),
                    GetProperValueForCurve(
                        animationCurve, endInterpolationKeyframeFrame, frameRate, overallAnimationFrames),
                    (frameNumber - startInterpolationKeyframeFrame) / (float)framesDifference);
            }
        }
    }

    public static class FloatCurvesKeyframesHelper
    {
        //private static int keyframeStep = 3;

        public static List<Tuple<int, BoneKeyframeTransform>>
            IterateCombinedKeyframeTransforms(
            Tuple<AnimationCurve, AnimationCurve, AnimationCurve> positionTransformsCurves,
            Tuple<AnimationCurve, AnimationCurve, AnimationCurve, AnimationCurve> rotationTransformCurves,
            Tuple<AnimationCurve, AnimationCurve, AnimationCurve> scaleTransformCurves,
            float frameRate,
            int animationFrames)
        {
            // assume that keyframes numbers are exactly same for all coordinates in respective curves
            // that is what we expect anyway

            List<Tuple<int, BoneKeyframeTransform>> result = new List<Tuple<int, BoneKeyframeTransform>>();

            foreach (int frameNumberForKeyframe in
                FloatCurvesTimelineHelper
                    .GetUniformFramesNumbersForEstablishedKeyframing(
                        positionTransformsCurves,
                        rotationTransformCurves,
                        scaleTransformCurves,
                        frameRate,
                        animationFrames))
            {
                float positionXValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            positionTransformsCurves.Item1,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                float positionYValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            positionTransformsCurves.Item2,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                float positionZValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            positionTransformsCurves.Item3,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);



                float rotationWValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            rotationTransformCurves.Item1,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                float rotationXValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            rotationTransformCurves.Item2,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                float rotationYValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            rotationTransformCurves.Item3,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                float rotationZValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            rotationTransformCurves.Item4,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                float scaleXValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            scaleTransformCurves.Item1,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                float scaleYValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            scaleTransformCurves.Item2,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                float scaleZValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            scaleTransformCurves.Item3,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                int currentFrame = frameNumberForKeyframe;

                result.Add(Tuple.Create(
                    currentFrame,
                    new BoneKeyframeTransformBuilder()
                        .WithPosition(new Vector3(positionXValue, positionYValue, positionZValue))
                        .WithRotation(new Quaternion(rotationXValue, rotationYValue, rotationZValue, rotationWValue))
                        .WithScale(new Vector3(scaleXValue, scaleYValue, scaleZValue))
                        .Build()));
            }

            return result;
        }
    }

    public struct BoneKeyframeTransform
    {
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 scale;

        public BoneKeyframeTransform ReflectViaWorldVector3RightNormal()
        {
            BoneKeyframeTransform result = new BoneKeyframeTransform
            {
                position = Vector3.Reflect(this.position, Vector3.right),

                rotation = new Quaternion(this.rotation.x, -this.rotation.y, -this.rotation.z, this.rotation.w),

                // should we mirror also scale?
                //result.scale = Vector3.Reflect(scale, Vector3.right);
                scale = this.scale
            };
            return result;
        }

        public BoneKeyframeTransform WithZeroScale()
        {
            BoneKeyframeTransform result = new BoneKeyframeTransform
            {
                position = this.position,
                rotation = this.rotation,
                scale = new Vector3(0f, 0f, 0f)
            };
            return result;
        }
    }

    public class BoneTransTimeline
    {
#if (UNITY_EDITOR)
        public Dictionary<int, BoneKeyframeTransform> keyframes =
            new Dictionary<int, BoneKeyframeTransform>();

        public static BoneTransTimeline
            WithPositionRotationScaleTransformsCurves(
            Tuple<AnimationCurve, AnimationCurve, AnimationCurve> positionTransformsCurves,
            Tuple<AnimationCurve, AnimationCurve, AnimationCurve, AnimationCurve> rotationTransformCurves,
            Tuple<AnimationCurve, AnimationCurve, AnimationCurve> scaleTransformCurves,
            float frameRate,
            int animationFrames)
        {
            BoneTransTimeline result = new BoneTransTimeline();

            foreach (Tuple<int, BoneKeyframeTransform> positionRotationScaleCombinedKeyframeTransformInfo in
                FloatCurvesKeyframesHelper.IterateCombinedKeyframeTransforms(
                    positionTransformsCurves, rotationTransformCurves,
                    scaleTransformCurves, frameRate, animationFrames))
            {
                int keyframeNumber = positionRotationScaleCombinedKeyframeTransformInfo.Item1;
                BoneKeyframeTransform boneKeyframeTransform = positionRotationScaleCombinedKeyframeTransformInfo.Item2;
                result.keyframes.Add(keyframeNumber, boneKeyframeTransform);
            }

            return result;
        }

        public List<Tuple<AnimationCurve, EditorCurveBinding>>
            GetUnityAnimationCurvesWithBindingInfos(string path, int totalFrames)
        {
            AnimationCurveBuilder positionXAnimationCurveBuilder =
                new AnimationCurveBuilder();
            EditorCurveBinding positionXEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalPosition.x");

            AnimationCurveBuilder positionYAnimationCurveBuilder =
                new AnimationCurveBuilder();
            EditorCurveBinding positionYEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalPosition.y");

            AnimationCurveBuilder positionZAnimationCurveBuilder =
                new AnimationCurveBuilder();
            EditorCurveBinding positionZEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalPosition.z");


            AnimationCurveBuilder rotationWAnimationCurveBuilder =
                new AnimationCurveBuilder();
            EditorCurveBinding rotationWEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalRotation.w");

            AnimationCurveBuilder rotationXAnimationCurveBuilder =
                new AnimationCurveBuilder();
            EditorCurveBinding rotationXEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalRotation.x");

            AnimationCurveBuilder rotationYAnimationCurveBuilder =
                new AnimationCurveBuilder();
            EditorCurveBinding rotationYEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalRotation.y");

            AnimationCurveBuilder rotationZAnimationCurveBuilder =
                new AnimationCurveBuilder();
            EditorCurveBinding rotationZEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalRotation.z");


            AnimationCurveBuilder scaleXAnimationCurveBuilder =
                new AnimationCurveBuilder();
            EditorCurveBinding scaleXEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalScale.x");

            AnimationCurveBuilder scaleYAnimationCurveBuilder =
                new AnimationCurveBuilder();
            EditorCurveBinding scaleYEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalScale.y");

            AnimationCurveBuilder scaleZAnimationCurveBuilder =
                new AnimationCurveBuilder();
            EditorCurveBinding scaleZEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalScale.z");


            foreach (KeyValuePair<int, BoneKeyframeTransform> keyframeItem in this.keyframes)
            {
                int frameNumber = keyframeItem.Key;
                BoneKeyframeTransform boneKeyframeTransform = keyframeItem.Value;

                positionXAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.position.x);
                positionYAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.position.y);
                positionZAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.position.z);

                rotationWAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.rotation.w);
                rotationXAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.rotation.x);
                rotationYAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.rotation.y);
                rotationZAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.rotation.z);

                scaleXAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.scale.x);
                scaleYAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.scale.y);
                scaleZAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.scale.z);
            }

            return new List<Tuple<AnimationCurve, EditorCurveBinding>>()
            {
                Tuple.Create(positionXAnimationCurveBuilder.Build(), positionXEditorCurveBinding),
                Tuple.Create(positionYAnimationCurveBuilder.Build(), positionYEditorCurveBinding),
                Tuple.Create(positionZAnimationCurveBuilder.Build(), positionZEditorCurveBinding),

                Tuple.Create(rotationWAnimationCurveBuilder.Build(), rotationWEditorCurveBinding),
                Tuple.Create(rotationXAnimationCurveBuilder.Build(), rotationXEditorCurveBinding),
                Tuple.Create(rotationYAnimationCurveBuilder.Build(), rotationYEditorCurveBinding),
                Tuple.Create(rotationZAnimationCurveBuilder.Build(), rotationZEditorCurveBinding),

                Tuple.Create(scaleXAnimationCurveBuilder.Build(), scaleXEditorCurveBinding),
                Tuple.Create(scaleYAnimationCurveBuilder.Build(), scaleYEditorCurveBinding),
                Tuple.Create(scaleZAnimationCurveBuilder.Build(), scaleZEditorCurveBinding),
            };
        }

        public int GetFramesCount()
        {
            return this.keyframes.Keys.Max();
        }

        public BoneTransTimeline ReflectViaWorldVector3RightNormal()
        {
            BoneTransTimeline result = new BoneTransTimeline();

            foreach (KeyValuePair<int, BoneKeyframeTransform> keyframeItem in this.keyframes)
            {
                result.keyframes.Add(
                    keyframeItem.Key,
                    keyframeItem.Value.ReflectViaWorldVector3RightNormal());
            }

            return result;
        }

        public BoneTransTimeline ZeroOutScaleKeyframes()
        {
            BoneTransTimeline result = new BoneTransTimeline();

            foreach (KeyValuePair<int, BoneKeyframeTransform> keyframeItem in this.keyframes)
            {
                result.keyframes.Add(
                    keyframeItem.Key,
                    keyframeItem.Value.WithZeroScale());
            }

            return result;
        }
#endif
    }
}
