﻿using Assets.Scripts.Multiplayer2.Messages;
using Assets.Scripts.Multiplayer2.Monitors;
using ReactUnity.UGUI;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2
{
    public class UDPReceivedMessagesQueue : MonoBehaviour
    {
        public ConcurrentQueue<UDPNetworkMessage> messages = new ConcurrentQueue<UDPNetworkMessage>();

        protected UDPReceivedMessagesQueueMonitor monitor;

        private void Start()
        {
            var reactRendererUGUI = FindObjectOfType<ReactRendererUGUI>();
            if (!reactRendererUGUI.Globals.ContainsKey("udpReceivedMessagesQueueMonitor"))
            {
                reactRendererUGUI.Globals["udpReceivedMessagesQueueMonitor"] = new UDPReceivedMessagesQueueMonitor();
            }
            monitor = reactRendererUGUI.Globals["udpReceivedMessagesQueueMonitor"] as UDPReceivedMessagesQueueMonitor;
        }

        private void Update()
        {
            monitor.queueMessages = messages.Count;
        }
    }
}
