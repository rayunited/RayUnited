﻿using Assets.Scripts.Multiplayer2.Messages;
using Assets.Scripts.Multiplayer2.Monitors;
using ReactUnity.UGUI;
using System.Collections.Concurrent;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2
{
    public class TCPSendMessagesQueue : MonoBehaviour
    {
        public ConcurrentQueue<TCPNetworkMessage> messages = new ConcurrentQueue<TCPNetworkMessage>();

        protected TCPSendMessagesQueueMonitor monitor;

        private void Start()
        {
            var reactRendererUGUI = FindObjectOfType<ReactRendererUGUI>();
            if (!reactRendererUGUI.Globals.ContainsKey("tcpSendMessagesQueueMonitor"))
            {
                reactRendererUGUI.Globals["tcpSendMessagesQueueMonitor"] = new TCPSendMessagesQueueMonitor();
            }
            monitor = reactRendererUGUI.Globals["tcpSendMessagesQueueMonitor"] as TCPSendMessagesQueueMonitor;
        }

        private void Update()
        {
            monitor.queueMessages = messages.Count;
        }
    }
}
