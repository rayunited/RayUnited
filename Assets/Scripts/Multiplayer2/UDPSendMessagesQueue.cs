﻿using Assets.Scripts.Multiplayer2.Messages;
using Assets.Scripts.Multiplayer2.Monitors;
using ReactUnity.UGUI;
using System.Collections.Concurrent;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2
{
    public class UDPSendMessagesQueue : MonoBehaviour
    {
        public ConcurrentQueue<UDPNetworkMessage> messages = new ConcurrentQueue<UDPNetworkMessage>();

        protected UDPSendMessagesQueueMonitor monitor;

        private void Start()
        {
            var reactRendererUGUI = FindObjectOfType<ReactRendererUGUI>();
            if (!reactRendererUGUI.Globals.ContainsKey("udpSendMessagesQueueMonitor"))
            {
                reactRendererUGUI.Globals["udpSendMessagesQueueMonitor"] = new UDPSendMessagesQueueMonitor();
            }
            monitor = reactRendererUGUI.Globals["udpSendMessagesQueueMonitor"] as UDPSendMessagesQueueMonitor;
        }

        private void Update()
        {
            monitor.queueMessages = messages.Count;
        }
    }
}
