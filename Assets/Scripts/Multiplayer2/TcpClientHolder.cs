﻿using System.Net.Sockets;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2
{
    public class TcpClientHolder : MonoBehaviour
    {
        protected ClientConfig config;

        public TcpClient tcpClient;

        public bool initialized = false;

        private void Start()
        {
            config = FindObjectOfType<ClientConfig>();
        }

        private void Update()
        {
            if (config.initialized && config.successfulConfig && !initialized)
            {
                tcpClient = new TcpClient(config.serverHost, config.serverPort);
                initialized = true;
            }
        }
    }
}
