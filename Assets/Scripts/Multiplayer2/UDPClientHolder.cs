﻿using System.Net;
using System.Net.Sockets;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2
{
    public class UDPClientHolder : MonoBehaviour
    {
        public UdpClient udpClient;
        public UdpClient udpClientListener;

        protected ClientConfig config;

        protected TcpClientHolder tcpClientHolder;

        public bool initialized = false;

        private void Start()
        {
            config = FindObjectOfType<ClientConfig>();
            tcpClientHolder = FindObjectOfType<TcpClientHolder>();
        }

        private void Update()
        {
            if (config.initialized && tcpClientHolder.initialized && !initialized && config.successfulConfig)
            {
                Debug.Log($"UDP listening port {(tcpClientHolder.tcpClient.Client.LocalEndPoint as IPEndPoint).Port}");
                udpClient = new UdpClient();
                udpClient.Connect(config.serverHost, config.serverPort);

                Debug.Log($"UDP sending address {udpClient.Client.RemoteEndPoint}");
                
                udpClientListener = new UdpClient((tcpClientHolder.tcpClient.Client.LocalEndPoint as IPEndPoint).Port);
                //udpClientListener.Client.Bind(default);
                //udpClientListener.Connect(config.serverHost, config.serverPort);
                initialized = true;
            }
        }
    }
}
