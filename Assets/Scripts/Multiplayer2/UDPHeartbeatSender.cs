﻿using Assets.Scripts.Multiplayer2.Messages;
using Assets.Scripts.Multiplayer2.Messages.UDP.Msg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2
{

    public class UDPHeartbeatSender : MonoBehaviour
    {
        protected UDPSendMessagesQueue sendQueue;

        private void Start()
        {
            sendQueue = FindObjectOfType<UDPSendMessagesQueue>();
        }

        private void Update()
        {
            //sendQueue.messages.Enqueue(new UDPHeartbeatNetworkMessage());
        }
    }
}
