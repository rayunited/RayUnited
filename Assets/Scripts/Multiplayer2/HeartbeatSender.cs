﻿using Assets.Scripts.Multiplayer2.Messages;
using Assets.Scripts.Multiplayer2.Messages.TCP.Msg;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2
{
    public class HeartbeatSender : MonoBehaviour
    {
        protected TCPSendMessagesQueue sendQueue;

        private void Start()
        {
            sendQueue = FindObjectOfType<TCPSendMessagesQueue>();
        }

        private void Update()
        {
            sendQueue.messages.Enqueue(new TCPHeartbeatNetworkMessage());
        }
    }
}
