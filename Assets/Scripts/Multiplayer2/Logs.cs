﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;

namespace Assets.Scripts.Multiplayer2
{
    public class LogMessage
    {
        public string message;
        public int id;

        public LogMessage(string message, int id)
        {
            this.message = message;
            this.id = id;
        }
    }

    public class Logs
    {
        protected ConcurrentQueue<LogMessage> logs = new ConcurrentQueue<LogMessage>();

        protected int currentLogId = 0;

        public void Log(string message) { 
            logs.Enqueue(new LogMessage(message, currentLogId));
            Interlocked.Increment(ref currentLogId);
        }

        public List<LogMessage> PollLogs()
        {
            var result = new List<LogMessage>();
            while (logs.TryDequeue(out LogMessage msg))
            {
                result.Add(msg);
               
            }
            return result;
        }
    }
}
