﻿using Assets.Scripts.Multiplayer2.Messages;
using Assets.Scripts.Multiplayer2.Monitors;
using ReactUnity.UGUI;
using System.Collections.Concurrent;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2
{
    public class TCPReceivedMessagesQueue : MonoBehaviour
    {
        public ConcurrentQueue<TCPNetworkMessage> messages = new ConcurrentQueue<TCPNetworkMessage>();

        protected TCPReceivedMessagesQueueMonitor monitor;

        private void Start()
        {
            var reactRendererUGUI = FindObjectOfType<ReactRendererUGUI>();
            if (!reactRendererUGUI.Globals.ContainsKey("tcpReceivedMessagesQueueMonitor"))
            {
                reactRendererUGUI.Globals["tcpReceivedMessagesQueueMonitor"] = new TCPReceivedMessagesQueueMonitor();
            }
            monitor = reactRendererUGUI.Globals["tcpReceivedMessagesQueueMonitor"] as TCPReceivedMessagesQueueMonitor; ;
        }

        private void Update()
        {
            monitor.queueMessages = messages.Count;
        }
    }
}
