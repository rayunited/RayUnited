﻿using Assets.Scripts.Animations;
using Assets.Scripts.Engine.Input;
using Assets.Scripts.Multiplayer2.Messages.UDP.Msg;
using Assets.Scripts.Multiplayer2.Queues;
using Assets.Scripts.Multiplayer2.Queues.Que;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Aspects.Shooting;
using Assets.Scripts.PlayerMechanics.Camera;
using Assets.Scripts.PlayerMechanics.Shooting;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Multiplayer2.Player
{
    public class PlayerPositioner : MonoBehaviour
    {
        protected PlayerIdentifierHolder playerIdentifierHolder;
        protected PlayerStateOutcomingQueue playerStateOutcomingQueue;

        protected UDPReceivingQueueDispatcher udpReceivingQueueDispatcher;

        protected Dictionary<int, GameObject> cubes = new Dictionary<int, GameObject>();

        public GameObject debugPositionGao;

        protected ClientConfig clientConfig;


        void Start()
        {
            playerIdentifierHolder = FindObjectOfType<PlayerIdentifierHolder>();

            udpReceivingQueueDispatcher = FindObjectOfType<UDPReceivingQueueDispatcher>();
            playerStateOutcomingQueue = FindObjectOfType<PlayerStateOutcomingQueue>();

            clientConfig = FindObjectOfType<ClientConfig>();
        }

        private void Update()
        {
            if (playerIdentifierHolder.initialized)
            {
                foreach (var entry in udpReceivingQueueDispatcher.playersStates)
                {
                    if (entry.Key != playerIdentifierHolder.identifier)
                    {
                        if (!cubes.ContainsKey(entry.Key))
                        {
                            var gao = Instantiate(debugPositionGao);
                            cubes[entry.Key] = gao;
                            cubes[entry.Key].transform.position = entry.Value.position;
                        }
                        cubes[entry.Key].GetComponent<PlayerInputHub>().moveInputRaw = entry.Value.moveInputRaw;
                        cubes[entry.Key].transform.position = entry.Value.position;                      
                        var playerController = cubes[entry.Key].GetComponent<PlayerController>();
                        if (playerController.controllerContext != null) {
                            playerController.controllerContext.programmaticBaseCameraOrientedForwardDirectionForPlayer =
                                entry.Value.programmaticBaseCameraOrientedForwardDirectionForPlayer;
                        }
                        playerController.GetComponentInChildren<AnimationController>()
                            .transform.rotation = entry.Value.animatedPartRotation;
                        playerController.transform.rotation = entry.Value.playerRotation;
                        playerController.GetComponent<PlayerInputHub>().jumpButton = entry.Value.jumpButton;
                        playerController.GetComponent<PlayerInputHub>().fireButton = entry.Value.fireButton;
                        playerController.GetComponent<PlayerInputHub>().walkingButton = entry.Value.walkingButton;
                        playerController.GetComponent<PlayerInputHub>().strafingButton = entry.Value.strafingButton;
                        playerController.GetComponent<PlayerInputHub>().groundRollButton = entry.Value.groundRollButton;

                        playerController.controllerContext.gravityDirection = -entry.Value.gravityUp;
                        playerController.controllerContext.gravityForward = entry.Value.gravityForward;
                        playerController.controllerContext.gravityRight = entry.Value.gravityRight;

                        if (!entry.Value.currentTarget.Equals(""))
                        {
                            playerController.GetComponent<TargettingAspect>().currentTarget = GameObject.Find(entry.Value.currentTarget);
                        } else
                        {
                            playerController.GetComponent<TargettingAspect>().currentTarget = null;
                        }

                        playerController.GetComponentInChildren<Text>().rectTransform.rotation = 
                            Quaternion.LookRotation(
                                -(FindObjectOfType<CameraActorHandle>().transform.position - playerController.transform.position));
                        playerController.GetComponentInChildren<Text>().text = entry.Value.nickname;

                        playerController.GetComponentsInChildren<Renderer>().ToList().ForEach(
                            x => x.material.color = new Color(entry.Value.colorShift, 1f - entry.Value.colorShift, entry.Value.colorShift));

                        //cubes[entry.Key].transform.position = FindObjectOfType<PlayerActorHandle>().transform.position;
                    }
                }

                var playerActor = FindObjectsOfType<PlayerController>().First(x => !x.playerParams.controlledProgrammatically);
                playerActor.GetComponentInChildren<Text>().rectTransform.rotation = Quaternion.LookRotation(
                                -(FindObjectOfType<CameraActorHandle>().transform.position - playerActor.transform.position));
                playerActor.GetComponentInChildren<Text>().text = clientConfig.playerNickname;
                var targetingAspect = playerActor.GetComponent<TargettingAspect>();

                if (udpReceivingQueueDispatcher.playersStates.ContainsKey(playerIdentifierHolder.identifier))
                {
                    var colorShift = udpReceivingQueueDispatcher.playersStates[playerIdentifierHolder.identifier].colorShift;

                    playerActor.GetComponentsInChildren<Renderer>().ToList().ForEach(
                        x => x.material.color = new Color(colorShift, 1f - colorShift, colorShift));
                }
                

                playerStateOutcomingQueue.messages.Enqueue(
                    new UDPPlayerStateNetworkMessage(
                        playerIdentifierHolder.identifier,
                        playerActor.transform.position,
                        FindObjectOfType<GameStateInfo>().GetComponent<PlayerInputHub>().moveInputRaw,
                        playerActor.controllerContext.cameraController.controllerContext.baseCameraOrientedForwardDirectionForPlayer,
                        playerActor.GetComponentInChildren<AnimationController>().transform.rotation,
                        FindObjectOfType<GameStateInfo>().GetComponent<PlayerInputHub>().jumpButton,
                        FindObjectOfType<GameStateInfo>().GetComponent<PlayerInputHub>().fireButton,
                        FindObjectOfType<GameStateInfo>().GetComponent<PlayerInputHub>().walkingButton,
                        FindObjectOfType<GameStateInfo>().GetComponent<PlayerInputHub>().strafingButton,
                        FindObjectOfType<GameStateInfo>().GetComponent<PlayerInputHub>().groundRollButton,
                        playerActor.transform.rotation,
                        targetingAspect.currentTarget != null ? targetingAspect.currentTarget.name : "",
                        playerActor.controllerContext.gravityForward,
                        playerActor.controllerContext.gravityRight,
                        -playerActor.controllerContext.gravityDirection,
                        clientConfig.playerNickname,
                        clientConfig.colorShift));
            }
        }
    }
}
