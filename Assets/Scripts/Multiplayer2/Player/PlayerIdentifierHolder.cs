﻿using Assets.Scripts.Multiplayer2.Queues;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2.Player
{
    public class PlayerIdentifierHolder : MonoBehaviour
    {
        public int identifier;
        public bool initialized = false;

        protected PlayerIdentityIncomingQueue playerIdentityIncomingQueue;

        private void Start()
        {
            playerIdentityIncomingQueue = FindObjectOfType<PlayerIdentityIncomingQueue>();
        }

        private void Update()
        {
            if (playerIdentityIncomingQueue.messages.TryDequeue(out var message))
            {
                identifier = message.identifier;
                initialized = true;
                Debug.Log($"Received player identifier {identifier}");
            }
        }
    }
}
