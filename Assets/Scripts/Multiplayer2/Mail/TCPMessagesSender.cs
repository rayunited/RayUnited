﻿using Assets.Scripts.Multiplayer2.Messages;
using Assets.Scripts.Multiplayer2.Monitors;
using ReactUnity.UGUI;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2
{
    public class TCPMessagesSender : MonoBehaviour
    {
        protected TcpClientHolder tcpClientHolder;
        protected TCPSendMessagesQueue queue;

        protected bool initialized = false;

        protected TCPMessagesSenderMonitor monitor;

        protected Thread thread;

        private void Start()
        {
            tcpClientHolder = FindObjectOfType<TcpClientHolder>();
            queue = FindObjectOfType<TCPSendMessagesQueue>();

            var reactRendererUGUI = FindObjectOfType<ReactRendererUGUI>();
            if (!reactRendererUGUI.Globals
                .ContainsKey("tcpMessagesSenderMonitor"))
            {
                reactRendererUGUI.Globals["tcpMessagesSenderMonitor"] =
                    new TCPMessagesSenderMonitor();         
            }
            monitor = reactRendererUGUI.Globals["tcpMessagesSenderMonitor"] as
                   TCPMessagesSenderMonitor;
        }

        private void Update()
        {
            if (tcpClientHolder.initialized && !initialized)
            {
                NetworkStream stream = tcpClientHolder.tcpClient.GetStream();
                thread = new Thread(new ThreadStart(() =>
                {
                    while (true)
                    {
                        if (queue.messages.TryDequeue(out TCPNetworkMessage message)) {
                            byte[] data = message.Serialize();
                            stream.Write(data, 0, data.Length);
                        }
                        Thread.Sleep(10);
                    }
                }));
                thread.Start();

                initialized = true;
            }

            monitor.threadRunning = thread?.IsAlive ?? false;
        }
    }
}
