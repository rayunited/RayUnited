﻿using Assets.Scripts.Multiplayer2.Messages;
using Assets.Scripts.Multiplayer2.Monitors;
using ReactUnity.UGUI;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2.Mail
{
    public class TCPMessagesReceiver : MonoBehaviour
    {
        protected TcpClientHolder tcpClientHolder;
        protected TCPReceivedMessagesQueue queue;

        protected bool initialized = false;

        protected TCPMessagesReceiverMonitor monitor;

        protected Thread thread;

        private void Start()
        {
            tcpClientHolder = FindObjectOfType<TcpClientHolder>();
            queue = FindObjectOfType<TCPReceivedMessagesQueue>();

            var reactRendererUGUI = FindObjectOfType<ReactRendererUGUI>();
            if (!reactRendererUGUI.Globals.ContainsKey("tcpMessagesReceiverMonitor"))
            {
                reactRendererUGUI.Globals["tcpMessagesReceiverMonitor"]
                    = new TCPMessagesReceiverMonitor();
            }
            monitor = reactRendererUGUI.Globals["tcpMessagesReceiverMonitor"] as
                TCPMessagesReceiverMonitor;
        }

        private void Update()
        {
            if (tcpClientHolder.initialized && !initialized)
            {
                NetworkStream stream = tcpClientHolder.tcpClient.GetStream();

                byte[] buffer = new byte[1024];
                thread = new Thread(new ThreadStart(() =>
                {
                    while (true)
                    {
                        stream.Read(buffer, 0, buffer.Length);
                        TCPNetworkMessage? message = TCPNetworkMessageDeserializer.Deserialize(buffer);
                        if (message != null)
                        {
                            queue.messages.Enqueue(message);
                        }
                        Thread.Sleep(10);
                    }
                }));

                thread.Start();

                initialized = true;
            }

            monitor.threadRunning = thread?.IsAlive ?? false;
        }
    }
}
