﻿using Assets.Scripts.Multiplayer2.Messages;
using Assets.Scripts.Multiplayer2.Monitors;
using QuickJS.Utils;
using ReactUnity.UGUI;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2.Mail
{
    public class UDPMessagesSender : MonoBehaviour
    {
        protected bool initialized = false;

        protected UDPClientHolder udpClientHolder;
        protected TcpClientHolder tcpClientHolder;
        protected UDPSendMessagesQueue queue;

        protected UDPMessagesSenderMonitor monitor;

        protected Thread thread;

        private void Start()
        {
            udpClientHolder = FindObjectOfType<UDPClientHolder>();
            tcpClientHolder = FindObjectOfType<TcpClientHolder>();
            queue = FindObjectOfType<UDPSendMessagesQueue>();
            var reactRendererUGUI = FindObjectOfType<ReactRendererUGUI>();
            if (!reactRendererUGUI.Globals.ContainsKey("udpMessagesSenderMonitor"))
            {
                reactRendererUGUI.Globals["udpMessagesSenderMonitor"] = new UDPMessagesSenderMonitor();
            }
            monitor = reactRendererUGUI.Globals["udpMessagesSenderMonitor"] as UDPMessagesSenderMonitor;
        }

        private void Update()
        {
            if (udpClientHolder.initialized && !initialized && tcpClientHolder.initialized)
            {
                thread = new Thread(new ThreadStart(() =>
                {
                    while (true)
                    {
                        //udpClientHolder.udpClient.Connect(tcpClientHolder.tcpClient.Client.RemoteEndPoint as IPEndPoint);
                        if (queue.messages.TryDequeue(out UDPNetworkMessage message))
                        {
                            byte[] bytes = message.Serialize();
                            udpClientHolder.udpClient.Send(bytes, bytes.Length);
                        }
                        Thread.Sleep(10);
                    }
                }));
                thread.Start();
                initialized = true;
            }

            monitor.threadRunning = thread?.IsAlive ?? false;
        }
    }
}
