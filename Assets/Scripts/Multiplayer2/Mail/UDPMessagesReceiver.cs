﻿using Assets.Scripts.Multiplayer2.Messages;
using Assets.Scripts.Multiplayer2.Messages.UDP.Msg;
using Assets.Scripts.Multiplayer2.Monitors;
using Assets.Scripts.Multiplayer2.Queues;
using ReactUnity.UGUI;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2.Mail
{
    public class UDPMessagesReceiver : MonoBehaviour
    {
        protected UDPReceivedMessagesQueue queue;

        protected UDPClientHolder udpClientHolder;
        protected TcpClientHolder tcpClientHolder;

        protected ClientConfig config;

        protected bool initialized = false;

        protected UDPMessagesReceiverMonitor monitor;

        protected Thread thread;

        protected UDPReceivingQueueDispatcher udpReceivingQueueDispatcher;

        private void Start()
        {
            queue = FindObjectOfType<UDPReceivedMessagesQueue>();
            udpClientHolder = FindObjectOfType<UDPClientHolder>();
            config = FindObjectOfType<ClientConfig>();
            tcpClientHolder = FindObjectOfType<TcpClientHolder>();
            var reactRendererUGUI = FindObjectOfType<ReactRendererUGUI>();
            if (!reactRendererUGUI.Globals.ContainsKey("udpMessagesReceiverMonitor"))
            {
                reactRendererUGUI.Globals["udpMessagesReceiverMonitor"] = new UDPMessagesReceiverMonitor();
            }
            monitor = reactRendererUGUI.Globals["udpMessagesReceiverMonitor"] as UDPMessagesReceiverMonitor;
            udpReceivingQueueDispatcher = FindObjectOfType<UDPReceivingQueueDispatcher>();
        }

        private void Update()
        {
            IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
            if (udpClientHolder.initialized && config.initialized && tcpClientHolder.initialized && config.successfulConfig && !initialized)
            {
                thread = new Thread(
                    new ThreadStart(
                        () =>
                        {
                            while (true)
                            {                                
                                byte[] receiveBytes = udpClientHolder.udpClientListener.Receive(ref RemoteIpEndPoint);
                                UDPNetworkMessage? message = UDPNetworkMessageDeserializer.Deserialize(receiveBytes);
                                if (message != null && message is UDPPlayerStateNetworkMessage)
                                {
                                    var msg = message as UDPPlayerStateNetworkMessage;
                                    udpReceivingQueueDispatcher.playersStates[msg.playerId] = msg;
                                }
                                else if (message != null)
                                {
                                    queue.messages.Enqueue(message);
                                }
                            }
                        }    
                    )
                );

                thread.Start();
                initialized = true;
            }

            monitor.threadRunning = thread?.IsAlive ?? false;
        }
    }
}
