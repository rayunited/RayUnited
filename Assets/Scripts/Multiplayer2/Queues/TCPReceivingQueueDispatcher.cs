﻿using Assets.Scripts.Multiplayer2.Messages.TCP.Msg;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2.Queues
{
    public class TCPReceivingQueueDispatcher : MonoBehaviour
    {
        protected TCPReceivedMessagesQueue queue;

        protected PlayerIdentityIncomingQueue playerIdentityIncomingQueue; 

        private void Start()
        {
            queue = FindObjectOfType<TCPReceivedMessagesQueue>();
            playerIdentityIncomingQueue = FindObjectOfType<PlayerIdentityIncomingQueue>();
        }

        private void Update()
        {
            if (queue.messages.TryDequeue(out var message))
            {
                if (message is TCPPlayerIdentifierFromServer)
                {
                    playerIdentityIncomingQueue.messages.Enqueue(message as TCPPlayerIdentifierFromServer);
                } else
                {
                    queue.messages.Enqueue(message);
                }
            }
        }
    }
}
