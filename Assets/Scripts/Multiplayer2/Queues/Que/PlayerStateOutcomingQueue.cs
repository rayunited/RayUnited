﻿using Assets.Scripts.Multiplayer2.Messages.UDP.Msg;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2.Queues.Que
{
    public class PlayerStateOutcomingQueue : MonoBehaviour
    {
        public ConcurrentQueue<UDPPlayerStateNetworkMessage> messages = new ConcurrentQueue<UDPPlayerStateNetworkMessage> ();
    }
}
