﻿using Assets.Scripts.Multiplayer2.Messages.TCP.Msg;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2.Queues
{
    public class PlayerIdentityIncomingQueue : MonoBehaviour
    {
        public ConcurrentQueue<TCPPlayerIdentifierFromServer> messages = new ConcurrentQueue<TCPPlayerIdentifierFromServer>();
    }
}
