﻿using Assets.Scripts.Multiplayer2.Messages.UDP.Msg;
using System.Collections.Concurrent;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2.Queues.Que
{
    public class PlayerStateIncomingQueue : MonoBehaviour
    {
        public ConcurrentQueue<UDPPlayerStateNetworkMessage> messages = new ConcurrentQueue<UDPPlayerStateNetworkMessage>();
    }
}
