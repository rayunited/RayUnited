﻿using Assets.Scripts.Multiplayer2.Messages.UDP.Msg;
using Assets.Scripts.Multiplayer2.Queues.Que;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2.Queues
{
    public class UDPSendingQueueAggregator : MonoBehaviour
    {
        protected UDPSendMessagesQueue sendingQueue;

        protected PlayerStateOutcomingQueue playerStateOutcomingQueue;

        private void Start()
        {
            sendingQueue = FindObjectOfType<UDPSendMessagesQueue>();
            playerStateOutcomingQueue = FindObjectOfType<PlayerStateOutcomingQueue>();
        }

        private void Update()
        {
            if (playerStateOutcomingQueue.messages.TryDequeue(out var message))
            {
                if (sendingQueue.messages.Count > 10)
                {
                    sendingQueue.messages.TryDequeue(out var msg);
                }

                sendingQueue.messages.Enqueue(message);
            }
        }
    }
}
