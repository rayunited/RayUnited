﻿using Assets.Scripts.Multiplayer2.Messages.TCP.Msg;
using Assets.Scripts.Multiplayer2.Messages.UDP.Msg;
using Assets.Scripts.Multiplayer2.Queues.Que;
using System.Collections.Concurrent;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2.Queues
{
    public class UDPReceivingQueueDispatcher : MonoBehaviour
    {
        protected UDPReceivedMessagesQueue queue;

        protected PlayerStateIncomingQueue playerStateIncomingQueue;

        public ConcurrentDictionary<int, UDPPlayerStateNetworkMessage> playersStates = new ConcurrentDictionary<int, UDPPlayerStateNetworkMessage>();

        private void Start()
        {
            queue = FindObjectOfType<UDPReceivedMessagesQueue>();
            playerStateIncomingQueue = FindObjectOfType<PlayerStateIncomingQueue>();
        }

        private void Update()
        {
            //if (queue.messages.TryDequeue(out var message))
            //{
            //    if (message is UDPPlayerStateNetworkMessage)
            //    {
            //        var msg = message as UDPPlayerStateNetworkMessage;
            //        playersStates[msg.playerId] = msg;
            //    } else
            //    {
            //        queue.messages.Enqueue(message);
            //    }
            //}
        }

    }
}
