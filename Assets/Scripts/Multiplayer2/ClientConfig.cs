﻿using ReactUnity.UGUI;
using System;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2
{
    public class ClientConfig : MonoBehaviour
    {
        protected Logs logger;

        public string serverHost;
        public int serverPort;
        public string playerNickname;
        public float colorShift;

        public bool initialized = false;
        public bool successfulConfig = false;

        protected void EnsureLogsAreInitialized()
        {
            var reactRendererUGUI = FindObjectOfType<ReactRendererUGUI>();
            if (!reactRendererUGUI.Globals.ContainsKey("logs"))
            {
                reactRendererUGUI.Globals["logs"] = new Logs();
            }
            this.logger = reactRendererUGUI.Globals["logs"] as Logs;
        }

        private void Start()
        {
            EnsureLogsAreInitialized();

            var commandLineArguments = Environment.GetCommandLineArgs();

            int? resolutionWidth = null;
            int? resolutionHeight = null;

            bool serverHostCriteria = false;
            bool serverPortCriteria = false;
            bool playerNicknameCriteria = false;
            bool playerColorshiftCriteria = false;

            if (!commandLineArguments.Contains("-rayunite-resolution-width"))
            {
                logger.Log($"-rayunite-resolution-width command line argument not found");
            } else
            {
                int cmdIndex = Array.IndexOf(commandLineArguments, "-rayunite-resolution-width");
                resolutionWidth = Int32.Parse(commandLineArguments[cmdIndex + 1]);
            }

            if (!commandLineArguments.Contains("-rayunite-resolution-height"))
            {
                logger.Log($"-rayunite-resolution-height command line argument not found");
            } else
            {
                int cmdIndex = Array.IndexOf(commandLineArguments, "-rayunite-resolution-height");
                resolutionHeight = Int32.Parse(commandLineArguments[cmdIndex + 1]);
            }

            if (resolutionWidth != null && resolutionHeight != null)
            {
                logger.Log($"Setting resolution to {resolutionWidth}x{resolutionHeight}");
                Screen.SetResolution(width: (int)resolutionWidth, height: (int)resolutionHeight, FullScreenMode.Windowed);
            }

            if (!commandLineArguments.Contains("-rayunite-server"))
            {
                logger.Log($"-rayunite-server command line argument not found");
            } else
            {
                int cmdIndex = Array.IndexOf(commandLineArguments, "-rayunite-server");
                serverHost = commandLineArguments[cmdIndex + 1];
                serverHostCriteria = true;
            }

            if (!commandLineArguments.Contains("-rayunite-server-port"))
            {
                logger.Log($"-rayunite-server-port command line argument not found");
            } else
            {
                int cmdIndex = Array.IndexOf(commandLineArguments, "-rayunite-server-port");
                serverPort = Int32.Parse(commandLineArguments[cmdIndex + 1]);
                serverPortCriteria = true;
            }

            if (!commandLineArguments.Contains("-rayunite-player-nickname"))
            {
                logger.Log($"-rayunite-player-nickname command line argument not found");
            }
            else
            {
                int cmdIndex = Array.IndexOf(commandLineArguments, "-rayunite-player-nickname");
                playerNickname = commandLineArguments[cmdIndex + 1];
                playerNicknameCriteria = true;
            }

            if (!commandLineArguments.Contains("-rayunite-player-colorshift"))
            {
                logger.Log($"-rayunite-player-colorshift command line argument not found");
            }
            else
            {
                int cmdIndex = Array.IndexOf(commandLineArguments, "-rayunite-player-colorshift");
                colorShift = float.Parse(commandLineArguments[cmdIndex + 1]);
                playerColorshiftCriteria = true;
            }

            successfulConfig = serverHostCriteria && serverPortCriteria && playerNicknameCriteria && playerColorshiftCriteria;
            initialized = true;
        }
    }
}
