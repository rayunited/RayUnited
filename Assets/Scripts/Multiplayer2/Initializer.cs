﻿using Assets.Scripts.Multiplayer2.Monitors;
using ReactUnity.UGUI;
using System;
using System.Threading;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2
{
    public class Initializer : MonoBehaviour
    {
        private void Start()
        {
            var reactRendererUGUI = FindObjectOfType<ReactRendererUGUI>();

            if (!reactRendererUGUI.Globals.ContainsKey("logs"))
            {
                reactRendererUGUI.Globals["logs"] = new Logs();
            }

            if (!reactRendererUGUI.Globals.ContainsKey("tcpClientHolderMonitor"))
            {
                reactRendererUGUI.Globals["tcpClientHolderMonitor"] = new TCPClientHolderMonitor();
            }

            if (!reactRendererUGUI.Globals.ContainsKey("tcpMessagesReceiverMonitor"))
            {
                reactRendererUGUI.Globals["tcpMessagesReceiverMonitor"] = new TCPMessagesReceiverMonitor();
            }

            if (!reactRendererUGUI.Globals.ContainsKey("tcpMessagesSenderMonitor"))
            {
                reactRendererUGUI.Globals["tcpMessagesSenderMonitor"] = new TCPMessagesSenderMonitor();
            }

            if (!reactRendererUGUI.Globals.ContainsKey("tcpReceivedMessagesQueueMonitor"))
            {
                reactRendererUGUI.Globals["tcpReceivedMessagesQueueMonitor"] = new TCPReceivedMessagesQueueMonitor();
            }

            if (!reactRendererUGUI.Globals.ContainsKey("tcpSendMessagesQueueMonitor"))
            {
                reactRendererUGUI.Globals["tcpSendMessagesQueueMonitor"] = new TCPSendMessagesQueueMonitor();
            }

            if (!reactRendererUGUI.Globals.ContainsKey("udpClientHolderMonitor"))
            {
                reactRendererUGUI.Globals["udpClientHolderMonitor"] = new UDPClientHolderMonitor();
            }

            if (!reactRendererUGUI.Globals.ContainsKey("udpMessagesReceiverMonitor"))
            {
                reactRendererUGUI.Globals["udpMessagesReceiverMonitor"] = new UDPMessagesReceiverMonitor();
            }

            if (!reactRendererUGUI.Globals.ContainsKey("udpMessagesSenderMonitor"))
            {
                reactRendererUGUI.Globals["udpMessagesSenderMonitor"] = new UDPMessagesSenderMonitor();
            }

            if (!reactRendererUGUI.Globals.ContainsKey("udpReceivedMessagesQueueMonitor"))
            {
                reactRendererUGUI.Globals["udpReceivedMessagesQueueMonitor"] = new UDPReceivedMessagesQueueMonitor();
            }

            if (!reactRendererUGUI.Globals.ContainsKey("udpSendMessagesQueueMonitor"))
            {
                reactRendererUGUI.Globals["udpSendMessagesQueueMonitor"] = new UDPSendMessagesQueueMonitor();
            }
        }

        private void Update()
        {
            
        }
    }
}
