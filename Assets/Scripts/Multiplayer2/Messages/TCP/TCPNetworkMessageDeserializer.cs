﻿using Assets.Scripts.Multiplayer2.Messages.TCP.Msg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Multiplayer2.Messages
{
    public static class TCPNetworkMessageDeserializer
    {
        public static TCPNetworkMessage? Deserialize(byte[] buffer)
        {
            if (buffer[1] == TCPHeartbeatNetworkMessage.TYPE_CODE)
            {
                return new TCPHeartbeatNetworkMessage();
            } else if (buffer[1] == TCPPlayerIdentifierFromServer.TYPE_CODE)
            {
                return TCPPlayerIdentifierFromServer.Deserialize(buffer);
            } else
            {
                return null;
            }
        }
    }
}
