﻿using System;

namespace Assets.Scripts.Multiplayer2.Messages
{
    public abstract class TCPNetworkMessage
    {
        public abstract byte[] Serialize();
    }
}
