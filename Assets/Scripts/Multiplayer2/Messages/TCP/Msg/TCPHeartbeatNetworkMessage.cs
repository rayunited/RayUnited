﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Multiplayer2.Messages.TCP.Msg
{
    public class TCPHeartbeatNetworkMessage : TCPNetworkMessage
    {
        public const int TYPE_CODE = 4;

        public override byte[] Serialize()
        {
            return new byte[] { 0, TYPE_CODE };
        }
    }
}
