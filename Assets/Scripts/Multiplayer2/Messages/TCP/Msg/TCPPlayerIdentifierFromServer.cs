﻿using Assets.Scripts.Multiplayer2.Messages.UDP.Msg;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Multiplayer2.Messages.TCP.Msg
{
    public class TCPPlayerIdentifierFromServer : TCPNetworkMessage
    {
        public const int TYPE_CODE = 3;
        public int identifier;

        public TCPPlayerIdentifierFromServer(int identifier) {
            this.identifier = identifier;   
        }

        public override byte[] Serialize()
        {
            return
                new byte[] { 0 }
                .Concat
                (new byte[] { TYPE_CODE })
                .Concat
                (Encoding.ASCII.GetBytes(JsonUtility.ToJson(this)))
                .ToArray();
        }

        public static TCPPlayerIdentifierFromServer Deserialize(byte[] bytes)
        {
            return JsonUtility.FromJson<TCPPlayerIdentifierFromServer>(
                System.Text.Encoding.ASCII.GetString(bytes.Skip(2).ToArray()));
        }
    }
}
