﻿namespace Assets.Scripts.Multiplayer2.Messages
{
    public abstract class UDPNetworkMessage
    {
        public abstract byte[] Serialize();
    }
}
