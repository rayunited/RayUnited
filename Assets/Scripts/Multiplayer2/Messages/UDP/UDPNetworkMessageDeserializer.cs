﻿using Assets.Scripts.Multiplayer2.Messages.UDP.Msg;
using System;

namespace Assets.Scripts.Multiplayer2.Messages
{
    public static class UDPNetworkMessageDeserializer
    {
        public static UDPNetworkMessage? Deserialize(byte[] bytes)
        {
            if (bytes[1] == UDPHeartbeatNetworkMessage.TYPE_CODE)
            {
                return new UDPHeartbeatNetworkMessage();
            } else if (bytes[1] == UDPPlayerStateNetworkMessage.TYPE_CODE)
            {
                return UDPPlayerStateNetworkMessage.Deserialize(bytes);
            } else
            {
                return null;
            }
        }
    }
}
