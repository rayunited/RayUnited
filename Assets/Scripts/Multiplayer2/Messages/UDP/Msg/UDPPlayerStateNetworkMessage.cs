﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UIElements;

namespace Assets.Scripts.Multiplayer2.Messages.UDP.Msg
{
    public static class QuaternionCodingHelper
    {
        public static byte[] EncodeToBytesBigEndian(Quaternion quad)
        {
            var x = BitConverter.GetBytes(quad.x);
            var y = BitConverter.GetBytes(quad.y);
            var z = BitConverter.GetBytes(quad.z);
            var w = BitConverter.GetBytes(quad.w);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(x);
                Array.Reverse(y);
                Array.Reverse(z);
                Array.Reverse(w);
            }

            var bytes = x.Concat(y).Concat(z).Concat(w).ToArray();
            return bytes;
        }

        public static Quaternion DecodeBytesBigEndian(byte[] bytes)
        {
            var xBytes = bytes.Take(4).ToArray();
            var yBytes = bytes.Skip(4).Take(4).ToArray();
            var zBytes = bytes.Skip(4).Skip(4).Take(4).ToArray();
            var wBytes = bytes.Skip(4).Skip(4).Skip(4).Take(4).ToArray();

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(xBytes);
                Array.Reverse(yBytes);
                Array.Reverse(zBytes);
                Array.Reverse(wBytes);
            }

            var x = BitConverter.ToSingle(xBytes);
            var y = BitConverter.ToSingle(yBytes);
            var z = BitConverter.ToSingle(zBytes);
            var w = BitConverter.ToSingle(wBytes);
            return new Quaternion(x, y, z, w);
        }
    }

    public static class Vector2CodingHelper {
        public static byte[] EncodeToBytesBigEndian(Vector2 vec)
        {
            var x = BitConverter.GetBytes(vec.x);
            var y = BitConverter.GetBytes(vec.y);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(x);
                Array.Reverse(y);
            }

            var bytes = x.Concat(y).ToArray();
            return bytes;
        }

        public static Vector2 DecodeBytesBigEndian(byte[] bytes)
        {
            var xBytes = bytes.Take(4).ToArray();
            var yBytes = bytes.Skip(4).Take(4).ToArray();

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(xBytes);
                Array.Reverse(yBytes);
            }

            var x = BitConverter.ToSingle(xBytes);
            var y = BitConverter.ToSingle(yBytes);
            return new Vector2(x, y);
        }
    }

    public static class Vector3CodingHelper
    {
        public static byte[] EncodeToBytesBigEndian(Vector3 vec)
        {
            var x = BitConverter.GetBytes(vec.x);
            var y = BitConverter.GetBytes(vec.y);
            var z = BitConverter.GetBytes(vec.z);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(x);
                Array.Reverse(y);
                Array.Reverse(z);
            }

            var bytes = x.Concat(y).Concat(z).ToArray();
            return bytes;
        }

        public static Vector3 DecodeBytesBigEndian(byte[] bytes)
        {
            var xBytes = bytes.Take(4).ToArray();
            var yBytes = bytes.Skip(4).Take(4).ToArray();
            var zBytes = bytes.Skip(4).Skip(4).Take(4).ToArray();

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(xBytes);
                Array.Reverse(yBytes);
                Array.Reverse(zBytes);
            }

            var x = BitConverter.ToSingle(xBytes);
            var y = BitConverter.ToSingle(yBytes);
            var z = BitConverter.ToSingle(zBytes);
            return new Vector3(x, y, z);
        }
    }

    public class UDPPlayerStateNetworkMessage : UDPNetworkMessage
    {
        public const int TYPE_CODE = 1;

        public int playerId;
        public Vector3 position;
        public Vector2 moveInputRaw;
        public Vector3 programmaticBaseCameraOrientedForwardDirectionForPlayer;
        public Quaternion animatedPartRotation;
        public bool jumpButton;
        public bool fireButton;
        public bool walkingButton;
        public bool strafingButton;
        public bool groundRollButton;
        public Quaternion playerRotation;
        public string currentTarget;

        public Vector3 gravityForward;
        public Vector3 gravityRight;
        public Vector3 gravityUp;

        public string nickname;
        public float colorShift;

        public UDPPlayerStateNetworkMessage(
            int playerId, Vector3 position, Vector2 moveInputRaw,
            Vector3 programmaticBaseCameraOrientedForwardDirectionForPlayer,
            Quaternion animatedPartRotation,
            bool jumpButton, bool fireButton, bool walkingButton, bool strafingButton, bool groundRollButton, Quaternion playerRotation,
            string currentTarget, Vector3 gravityForward, Vector3 gravityRight, Vector3 gravityUp, string nickname,
            float colorShift)
        {
            this.playerId = playerId;
            this.position = position;
            this.moveInputRaw = moveInputRaw;
            this.programmaticBaseCameraOrientedForwardDirectionForPlayer = programmaticBaseCameraOrientedForwardDirectionForPlayer;
            this.animatedPartRotation = animatedPartRotation;
            this.jumpButton = jumpButton;
            this.fireButton = fireButton;
            this.walkingButton = walkingButton;
            this.strafingButton = strafingButton;
            this.groundRollButton = groundRollButton;
            this.playerRotation = playerRotation;
            this.currentTarget = currentTarget;
            this.gravityForward = gravityForward;
            this.gravityRight = gravityRight;
            this.gravityUp = gravityUp;
            this.nickname = nickname;
            this.colorShift = colorShift;
        }

        public override byte[] Serialize()
        {
            var playerIdBytes = BitConverter.GetBytes(playerId);

            var jumpButtonBytes = BitConverter.GetBytes(jumpButton);
            var fireButtonBytes = BitConverter.GetBytes(fireButton);
            var walkingButtonBytes = BitConverter.GetBytes(walkingButton);
            var strafingButtonBytes = BitConverter.GetBytes(strafingButton);
            var groundRollBytes = BitConverter.GetBytes(groundRollButton);

            var currentTargetBytes = ASCIIEncoding.ASCII.GetBytes(currentTarget);
            var nicknameBytes = ASCIIEncoding.ASCII.GetBytes(nickname);

            var colorShiftBytes = BitConverter.GetBytes(colorShift);

            Array.Resize(ref currentTargetBytes, 128);
            Array.Resize(ref nicknameBytes, 64);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(playerIdBytes);
                Array.Reverse(jumpButtonBytes);
                Array.Reverse(fireButtonBytes);
                Array.Reverse(colorShiftBytes);
            }

            return 
                new byte[] { 0 }
                .Concat
                (new byte[] { TYPE_CODE })
                .Concat
                (playerIdBytes)
                .Concat(Vector3CodingHelper.EncodeToBytesBigEndian(position))
                .Concat(Vector2CodingHelper.EncodeToBytesBigEndian(moveInputRaw))
                .Concat(Vector3CodingHelper.EncodeToBytesBigEndian(programmaticBaseCameraOrientedForwardDirectionForPlayer))
                .Concat(QuaternionCodingHelper.EncodeToBytesBigEndian(animatedPartRotation))
                .Concat(jumpButtonBytes)
                .Concat(fireButtonBytes)
                .Concat(walkingButtonBytes)
                .Concat(strafingButtonBytes)
                .Concat(groundRollBytes)
                .Concat(QuaternionCodingHelper.EncodeToBytesBigEndian(playerRotation))
                .Concat(currentTargetBytes)
                .Concat(Vector3CodingHelper.EncodeToBytesBigEndian(gravityForward))
                .Concat(Vector3CodingHelper.EncodeToBytesBigEndian(gravityRight))
                .Concat(Vector3CodingHelper.EncodeToBytesBigEndian(gravityUp))
                .Concat(nicknameBytes)
                .Concat(colorShiftBytes)
                .ToArray();
        }

        public static UDPPlayerStateNetworkMessage Deserialize(byte[] bytes)
        {
            byte[] bytesToOperateOn = bytes.Skip(2).ToArray();

            var playerIdBytes = bytesToOperateOn.Take(4).ToArray();

            var position = Vector3CodingHelper.DecodeBytesBigEndian(bytesToOperateOn.Skip(4).ToArray());
            var moveInputRaw = Vector3CodingHelper.DecodeBytesBigEndian(bytesToOperateOn.Skip(4).Skip(12).ToArray());
            var programmaticBaseCameraOrientedForwardDirectionForPlayer = 
                Vector3CodingHelper.DecodeBytesBigEndian(bytesToOperateOn.Skip(4).Skip(12).Skip(8).ToArray());
            var animatedPartRotation = QuaternionCodingHelper.DecodeBytesBigEndian(bytesToOperateOn.Skip(4).Skip(12).Skip(8).Skip(12).ToArray());


            var jumpButtonBytes = bytesToOperateOn.Skip(4).Skip(12).Skip(8).Skip(12).Skip(16).Take(1).ToArray();
            var fireButtonBytes = bytesToOperateOn.Skip(4).Skip(12).Skip(8).Skip(12).Skip(16).Skip(1).Take(1).ToArray();
            var walkingButtonBytes = bytesToOperateOn.Skip(4).Skip(12).Skip(8).Skip(12).Skip(16).Skip(1).Skip(1).Take(1).ToArray();
            var strafingButtonBytes = bytesToOperateOn.Skip(4).Skip(12).Skip(8).Skip(12).Skip(16).Skip(1).Skip(1).Skip(1).Take(1).ToArray();
            var groundRollButtonBytes = bytesToOperateOn.Skip(4).Skip(12).Skip(8).Skip(12).Skip(16).Skip(1).Skip(1).Skip(1).Skip(1).Take(1).ToArray();

            var playerRotation = QuaternionCodingHelper.DecodeBytesBigEndian(
                bytesToOperateOn.Skip(4).Skip(12).Skip(8).Skip(12).Skip(16).Skip(1).Skip(1).Skip(1).Skip(1).Skip(1).ToArray());

            var currentTarget = ASCIIEncoding.ASCII.GetString(
                bytesToOperateOn.Skip(4).Skip(12).Skip(8).Skip(12).Skip(16).Skip(1).Skip(1).Skip(1).Skip(1).Skip(1).Skip(16).ToArray());

            var gravityForward = Vector3CodingHelper.DecodeBytesBigEndian(
                bytesToOperateOn.Skip(4).Skip(12).Skip(8).Skip(12).Skip(16).Skip(1).Skip(1).Skip(1).Skip(1).Skip(1).Skip(16).Skip(128).ToArray());
            var gravityRight = Vector3CodingHelper.DecodeBytesBigEndian(
                bytesToOperateOn.Skip(4).Skip(12).Skip(8).Skip(12).Skip(16).Skip(1).Skip(1).Skip(1).Skip(1).Skip(1).Skip(16).Skip(128).Skip(12).ToArray());
            var gravityUp = Vector3CodingHelper.DecodeBytesBigEndian(
                bytesToOperateOn.Skip(4).Skip(12).Skip(8).Skip(12).Skip(16).Skip(1).Skip(1).Skip(1).Skip(1).Skip(1).Skip(16).Skip(128).Skip(12).Skip(12).ToArray());

            var nickname = ASCIIEncoding.ASCII.GetString(
                bytesToOperateOn.Skip(4).Skip(12).Skip(8).Skip(12).Skip(16).Skip(1).Skip(1).Skip(1).Skip(1).Skip(1).Skip(16).Skip(128).Skip(12).Skip(12).Skip(12).ToArray());

            var colorShiftBytes = bytesToOperateOn.Skip(4).Skip(12).Skip(8).Skip(12).Skip(16).Skip(1).Skip(1).Skip(1).Skip(1).Skip(1).Skip(16).Skip(128).Skip(12).Skip(12).Skip(12).Skip(64).Take(4).ToArray();

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(playerIdBytes);
                Array.Reverse(jumpButtonBytes);
                Array.Reverse(fireButtonBytes);
                Array.Reverse(walkingButtonBytes);
                Array.Reverse(strafingButtonBytes);
                Array.Reverse(groundRollButtonBytes);
                Array.Reverse(colorShiftBytes);
            }

            var playerId = BitConverter.ToInt32(playerIdBytes);
            var jumpButton = BitConverter.ToBoolean(jumpButtonBytes);
            var fireButton = BitConverter.ToBoolean(fireButtonBytes);
            var walkingButton = BitConverter.ToBoolean(walkingButtonBytes);
            var strafingButton = BitConverter.ToBoolean(strafingButtonBytes);
            var groundRollButton = BitConverter.ToBoolean(groundRollButtonBytes);
            var colorShift = BitConverter.ToSingle(colorShiftBytes);

            return new UDPPlayerStateNetworkMessage(
                playerId, position, moveInputRaw, programmaticBaseCameraOrientedForwardDirectionForPlayer, animatedPartRotation,
                jumpButton, fireButton, walkingButton, strafingButton, groundRollButton, playerRotation, currentTarget, gravityForward, gravityRight, gravityUp,
                nickname, colorShift);
        }
    }
}
