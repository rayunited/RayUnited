﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Multiplayer2.Messages.UDP.Msg
{
    public class UDPHeartbeatNetworkMessage : UDPNetworkMessage
    {
        public const int TYPE_CODE = 2;

        public override byte[] Serialize()
        {
            return new byte[] { 0, TYPE_CODE };
        }
    }
}
