﻿using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules.Helper
{
    public static class LumSwingingHelper
    {
        public static float CalculateDeflectionAngle(
            float velocity,
            float gravityAcceleration,
            float swingingRadius)
        {
            float v2 = velocity * velocity;

            return Mathf.Rad2Deg * Mathf.Acos(1 - (v2 / (2 * gravityAcceleration * swingingRadius)));
        }
    }
}
