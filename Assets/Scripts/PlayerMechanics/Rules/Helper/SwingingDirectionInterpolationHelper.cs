﻿using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules.Helper
{
    public static class SwingingDirectionInterpolationHelper
    {
        public static float GetAngleToRotateInFlatDirection(
            Vector3 swingingVelocityPlaneNormal,
            Vector3 playerInputTranslationWorldSpace,
            Vector3 gravityDirection,
            float swingingDirectionInterpolationStep)
        {
            Vector3 currentSwingingFlatDirection =
                Vector3.ProjectOnPlane(swingingVelocityPlaneNormal, -gravityDirection).normalized;

            Vector3 newSwingingVelocityPlaneNormal =
                (swingingVelocityPlaneNormal +
                Vector3.ProjectOnPlane(playerInputTranslationWorldSpace, swingingVelocityPlaneNormal)).normalized;

            return Vector3.SignedAngle(swingingVelocityPlaneNormal, newSwingingVelocityPlaneNormal, -gravityDirection) *
                swingingDirectionInterpolationStep;
        }
    }
}
