﻿using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models;
using Assets.Scripts.Collectibles;
using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.GameMechanics.Entities.Camera;
using Assets.Scripts.GameMechanics.Entities.Portal;
using Assets.Scripts.HUD.ScoreCounting;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics.Aspects;
using Assets.Scripts.PlayerMechanics.Aspects.Shooting;
using Assets.Scripts.PlayerMechanics.Rules.Common;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public static class RaymanAnimationsGroundHelper
    {
        public static bool IsUprisingWalkingRunningAnimation(string animationName)
        {
            return animationName.Equals(
               RaymanAnimations.UprisingFromGroundIdleToWalkingCycle1StateName())
               ||
               animationName.Equals(
                   RaymanAnimations.UprisingFromGroundIdleToRunningCycle4StateName());
        }

        public static bool IsExitingWalkingRunningAnimation(string animationName)
        {
            return animationName.Equals(
                RaymanAnimations.ExitingRunningCycle4ToGroundIdleStateName())
                ||
                animationName.Equals(
                    RaymanAnimations.ExitingWalkingCycle2ToGroundIdleStateName());
        }

        public static bool IsLandingAnimation(string animationName)
        {
            return
                animationName.Equals(RaymanAnimations.LandingFromHelicopterToGroundIdleStateName())
                ||
                animationName.Equals(RaymanAnimations.LandingFromHelicopterToRunningCycle4StateName())
                ||
                animationName.Equals(RaymanAnimations.LandingFromRunningJumpAfterAirFlipToGroundIdleStateName())
                ||
                animationName.Equals(RaymanAnimations.LandingFromRunningJumpAfterAirFlipToRunningCycle4StateName())
                ||
                animationName.Equals(RaymanAnimations.LandingFromRunningJumpAfterAirFlipToWalkingCycle2StateName())
                ||
                animationName.Equals(RaymanAnimations.LandingFromVerticalJumpToGroundIdleStateName())
                ||
                animationName.Equals(RaymanAnimations.LandingFromVerticalJumpToWalkingCycle2StateName())
                ;
        }
    }

    public static class CurvedShootingDirectionHelper
    {
        public static Vector3 GetCurvedShootingDirection(
            Vector3 playerMovementDirection,
            Vector3 playerForward,
            Vector3 playerRight,
            Vector3 gravityDirection)
        {
            float angleRight = Vector3.Angle(playerMovementDirection.normalized, playerRight.normalized);
            float additionalAngle = 20f;

            if (angleRight < 80f)
            {
                // shooting right
                return Quaternion.AngleAxis(additionalAngle, -gravityDirection) *
                    ((playerForward.normalized + playerRight.normalized) / 2.0f).normalized;
            }
            else
            {
                // shooting left
                return Quaternion.AngleAxis(-additionalAngle, -gravityDirection) *
                    ((playerForward.normalized + (-playerRight.normalized)) / 2.0f).normalized;
            }
        }
    }
}
