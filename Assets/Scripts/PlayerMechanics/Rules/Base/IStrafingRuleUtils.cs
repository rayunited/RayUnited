﻿using Assets.Scripts.HUD.Targetting;

namespace Assets.Scripts.PlayerMechanics.Rules.Base
{
    public interface IStrafingRuleUtils
    {
        public ShootSightDirection GetShootSightDirection();
    }
}
