﻿using Assets.Scripts.GameMechanics.Entities.Camera;
using Assets.Scripts.GameMechanics.Entities.Portal;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics
{
    public class PlayerMovementMetrics : MonoBehaviour
    {
        //public Vector3 forwardDirection = new Vector3(0, 0, 0);
        public Vector3 intentionalMovingDirection = new Vector3(0, 0, 0);

        public float angleFromAbsoluteForward = 0.0f;

        public CameraRuleEnum cameraRule = CameraRuleEnum.CAMERA_FOLLOW;

        public static float gravityAcceleration = 0.012f;
        public static float limitFallSpeed = 0.3f;
        public static float limitFallSpeedHelicopter = 0.05f;

        protected PlayerController playerController;
        protected CameraController cameraController;

        private void Awake()
        {
            this.playerController = FindObjectOfType<PlayerController>();
            this.cameraController = FindObjectOfType<CameraController>();
        }

        public void UpdateForwardDirectionAndRightDirection(
            Vector3 forwardDirection, float angleFromAbsoluteForward)
        {
            this.angleFromAbsoluteForward = angleFromAbsoluteForward;
        }

        public void OrientatePlayerAccordingToPortals(
            Transform fromPortal, Transform toPortal)
        {
            var fromPortalRend = fromPortal.GetComponent<PortalRenderingBehaviour>();
            var toPortalRend = toPortal.GetComponent<PortalRenderingBehaviour>();

            this.intentionalMovingDirection = 
                toPortalRend.TransformDirection(
                    fromPortalRend.InverseTransformDirection(this.intentionalMovingDirection));

            //this.angleFromAbsoluteForward =
            //    Vector3.SignedAngle(
            //        this.playerController.controllerContext.cameraGravityForward,
            //        this.cameraController.controllerContext.baseCameraOrientedForwardDirectionForPlayer,
            //        -this.playerController.controllerContext.cameraGravityDirection);
        }
    }
}
