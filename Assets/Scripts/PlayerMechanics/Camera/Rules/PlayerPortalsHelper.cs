﻿using Assets.Scripts.GameMechanics.Entities.Portal;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Camera.Rules
{
    public static class PlayerPortalsHelper
    {  
        public static Vector3 TransformWorldDirectionToPortalsProjectedDirectionThroughChainOfPortalsTransitions(
            List<Tuple<PortalController, PortalController>> portalsTransitionsChain,
            Vector3 baseNonProjectedWorldDirection,
            Vector3 baseNonProjectedGravityDirection
            )
        {
            var resultDirection = baseNonProjectedWorldDirection;
            for (int i = portalsTransitionsChain.Count - 1; i >= 0; i--)
            {
                var portalsTransition = portalsTransitionsChain[i];

                resultDirection =
                    portalsTransition.Item2.controllerContext.renderingBehaviour.InverseTransformDirection(resultDirection);
                resultDirection =
                    portalsTransition.Item1.controllerContext.renderingBehaviour.TransformDirection(resultDirection);
            }
            return resultDirection;
        }

        public static Vector3 TransformPortalsProjectedDirectionToWorldDirectionThroughChainOfPortalsTransitions(
            List<Tuple<PortalController, PortalController>> portalsTransitionsChain,
            Vector3 basePortalsProjectedDirection,
            Vector3 gravityDirection)
        {
            var resultDirection = basePortalsProjectedDirection;
            foreach (var portalsTransition in portalsTransitionsChain)
            {
                resultDirection =
                    portalsTransition.Item1.controllerContext.renderingBehaviour.InverseTransformDirection(resultDirection);
                resultDirection =
                    portalsTransition.Item2.controllerContext.renderingBehaviour.TransformDirection(resultDirection);
            }
            return resultDirection;
        }

        public static Vector3 TransformWorldPositionToPortalsProjectedPositionThroughChainOfPortalsTransitions(
            List<Tuple<PortalController, PortalController>> portalsTransitionsChain,
            Vector3 baseNonProjectedWorldPosition,
            Vector3 baseNonProjectedGravityDirection
            )
        {
            var resultPosition = baseNonProjectedWorldPosition;
            for (int i = portalsTransitionsChain.Count - 1; i >= 0; i--)
            {
                var portalsTransition = portalsTransitionsChain[i];

                resultPosition =
                    portalsTransition.Item2.controllerContext.renderingBehaviour.InverseTransformPoint(resultPosition);
                resultPosition =
                    portalsTransition.Item1.controllerContext.renderingBehaviour.TransformPoint(resultPosition);
            }
            return resultPosition;
        }

        public static float GetSegmentedDistanceFromWorldPositionToWorldPositionThroughChainOfPortalsTransitions(
            List<Tuple<PortalController, PortalController>> portalsTransitionsChain,
            Vector3 worldPositionA, Vector3 worldPositionB)
        {
            float resultSegmentedDistance = 0f;
            Vector3 currentHitTransitionPoint = Vector3.zero;

            if (portalsTransitionsChain.Count > 0)
            {
                currentHitTransitionPoint =
                    portalsTransitionsChain[0].Item1.controllerContext
                        .renderingBehaviour.GetPortalCollider().ClosestPoint(worldPositionA);
                resultSegmentedDistance = Vector3.Distance(
                    worldPositionA,
                    currentHitTransitionPoint);
            }
            else
            {
                resultSegmentedDistance = Vector3.Distance(worldPositionA, worldPositionB);
            }
            
            for (int i = 1; i <= portalsTransitionsChain.Count - 1; i++)
            {
                currentHitTransitionPoint =
                    portalsTransitionsChain[i - 1].Item2
                        .controllerContext.renderingBehaviour.TransformPoint(
                            portalsTransitionsChain[i - 1].Item1.controllerContext
                                .renderingBehaviour.InverseTransformPoint(currentHitTransitionPoint)
                        );

                var newHitTransitionPoint = 
                    portalsTransitionsChain[i].Item1.controllerContext
                        .renderingBehaviour.GetPortalCollider().ClosestPoint(currentHitTransitionPoint);

                resultSegmentedDistance += 
                    Vector3.Distance(
                        currentHitTransitionPoint,
                        newHitTransitionPoint
                    );

                currentHitTransitionPoint = newHitTransitionPoint;
            }

            if (portalsTransitionsChain.Count > 0)
            {
                resultSegmentedDistance += 
                    Vector3.Distance(
                        portalsTransitionsChain[portalsTransitionsChain.Count - 1].Item2
                            .controllerContext.renderingBehaviour
                            .GetPortalCollider().ClosestPoint(worldPositionB),
                        worldPositionB
                    );
            }

            return resultSegmentedDistance;
        }
    }
}
