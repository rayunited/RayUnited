﻿using Assets.Scripts.GameMechanics.Entities.Camera;
using Assets.Scripts.GameMechanics.Entities.Portal;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Camera
{
    public class CameraActorHandle : MonoBehaviour
    {
        PortalRenderingBehaviour[] portals;

        protected CameraController cameraController;

        private void Awake()
        {
            this.portals = FindObjectsOfType<PortalRenderingBehaviour>(includeInactive: true);
            this.cameraController = GetComponent<CameraController>();
        }

        public void DisableGameplayCameraRules()
        {
            this.cameraController.controllerContext.cameraRulesTransitions.EnterCutscenePlaceholder();
        }

        public void SetCameraPosition(Vector3 position)
        {
            this.transform.position = position;
        }

        public void EnableGameplayCameraRules()
        {
            this.cameraController.controllerContext.cameraRulesTransitions.EnterFollow();
        }

        public void LookAt(Vector3 targetPosition)
        {
            this.transform.LookAt(targetPosition);
        }
    }
}
