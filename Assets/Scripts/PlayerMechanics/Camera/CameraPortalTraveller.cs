﻿using Assets.Scripts.GameMechanics.Entities.Camera;
using Assets.Scripts.GameMechanics.Entities.Player;
using Assets.Scripts.GameMechanics.Entities.Portal;
using Assets.Scripts.GameMechanics.Portals;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using System;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Camera
{
    public class CameraPortalTraveller : PortalTraveller
    {
        protected PlayerController playerController;
        protected CameraController cameraController;
        protected PlayerPortalTraveller playerPortalTraveller;

        private void Awake()
        {
            this.playerController = FindObjectsOfType<PlayerController>().First(x => x.playerParams.controlledProgrammatically == false);
            this.cameraController = FindObjectOfType<CameraController>();
            this.playerPortalTraveller = FindObjectsOfType<PlayerController>().First(x => x.playerParams.controlledProgrammatically == false).GetComponent<PlayerPortalTraveller>();
        }

        public override void Teleport(
            Transform fromPortal, Transform toPortal, Vector3 position, Quaternion rotation, Vector3 gravityDirection, Vector3 gravityForward)
        {
            toPortal.GetComponent<PortalRenderingBehaviour>().ProtectScreenFromClipping(position);
            base.Teleport(fromPortal, toPortal, position, rotation, gravityDirection, gravityForward);
        }

        protected override void OnPortalTravel(
            Transform fromPortal, Transform toPortal, Vector3 gravityDirection, Vector3 gravityForward)
        {
            RedoForwardDirection(
                fromPortal.GetComponent<PortalRenderingBehaviour>(),
                toPortal.GetComponent<PortalRenderingBehaviour>(),
                gravityDirection, gravityForward);

            Tuple<PortalController, PortalController> nextPortalsTransition = 
                this.playerPortalTraveller.passedPortals.FirstOrDefault();

            if (nextPortalsTransition != null &&
                nextPortalsTransition.Item1.name.Equals(fromPortal.name) &&
                nextPortalsTransition.Item2.name.Equals(toPortal.name)) {

                this.playerPortalTraveller.passedPortals.RemoveAt(0);
            } else
            {
                var newPart = Tuple.Create(
                        toPortal.GetComponent<PortalController>(),
                        fromPortal.GetComponent<PortalController>());
                if (nextPortalsTransition != null)
                {
                    this.playerPortalTraveller.passedPortals.Insert(0, newPart);
                } else
                {
                    this.playerPortalTraveller.passedPortals.Add(newPart);
                }
            }

            //FindObjectOfType<PlayerMovementMetrics>().UpdateForwardDirectionAndRightDirection(
            //    Vector3.forward,
            //    Vector3.SignedAngle(
            //        this.playerController.controllerContext.cameraGravityForward,
            //        this.cameraController.controllerContext.baseCameraOrientedForwardDirectionForPlayer,
            //        -this.playerController.controllerContext.cameraGravityDirection));
        }

        public void RedoForwardDirection(
            PortalRenderingBehaviour fromPortal, PortalRenderingBehaviour toPortal,
            Vector3 gravityDirection, Vector3 gravityForward)
        {
            this.playerController.controllerContext.cameraGravityDirection =
               toPortal.transform.TransformDirection(
                   fromPortal.InverseTransformDirection(this.playerController.controllerContext.cameraGravityDirection)).normalized;
            this.playerController.controllerContext.cameraGravityRight =
                 toPortal.transform.TransformDirection(fromPortal.InverseTransformDirection(
                     this.playerController.controllerContext.cameraGravityRight)).normalized;
            this.playerController.controllerContext.cameraGravityForward =
                 toPortal.transform.TransformDirection(fromPortal.InverseTransformDirection(
                     this.playerController.controllerContext.cameraGravityForward)).normalized;

            var linkedEntrancePortal = fromPortal.GetComponent<PortalController>()
                .portalParams.linkedPortal.GetComponent<PortalRenderingBehaviour>();

            var forward = Vector3.ProjectOnPlane(
                (FindObjectsOfType<PlayerActorHandle>().Where(x => x.GetComponent<PlayerController>()?.playerParams?.controlledProgrammatically == false).First().transform.position - this.transform.position).normalized,
                -this.playerController.controllerContext.cameraGravityDirection).normalized;

            FindObjectsOfType<PlayerMovementMetrics>().Where(x => x.GetComponent<PlayerController>()?.playerParams?.controlledProgrammatically == false).First()
                .intentionalMovingDirection = forward;          
        }

        public override Vector3 GetGravityDirection()
        {
            return this.playerController.controllerContext.cameraGravityDirection;
        }

        public override Vector3 GetGravityForward()
        {
            return this.playerController.controllerContext.cameraGravityForward;
        }
    }
}
