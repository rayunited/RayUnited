﻿using Assets.Scripts.Engine;
using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.Engine.Waypoints;
using Assets.Scripts.PlayerMechanics.Rules.Common;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics
{
    public class PlayerWaypointPath
    {
        public List<PlayerWaypoint> waypoints =
            new List<PlayerWaypoint>();
        public int currentWaypointIndex = 0;
        public bool isActive = false;

        public PlayerWaypoint GetCurrentWaypoint()
        {
            return this.waypoints[this.currentWaypointIndex];
        }

        public void ProceedToNextWaypoint()
        {
            this.currentWaypointIndex++;
            if (this.currentWaypointIndex >= this.waypoints.Count)
            {
                this.isActive = false;
            }
        }
    }

    public static class WaypointsUtils
    {
        public static bool HasReachedWaypointPosition(
            Vector3 entityPosition,
            Vector3 waypointPosition,
            float proximityEpsilon = 3f)
        {
            return Vector3.Distance(entityPosition, waypointPosition) <= proximityEpsilon;
        }
    }

    public class PlayerActorHandle : GameplayOnlyMonoBehaviour
    {
        private PlayerMovementInput playerMovementInput;
        private PlayerWaypointPath waypointPath = new PlayerWaypointPath();

        private void Awake()
        {
            this.playerMovementInput = FindObjectOfType<PlayerMovementInput>();
        }

        public void DisablePlayerControl()
        {
            this.playerMovementInput.DisablePlayerControl();
        }
        public void EnablePlayerControl()
        {
            this.playerMovementInput.EnablePlayerControl();
        }

        public void FollowWaypointPath(List<PlayerWaypoint> waypoints)
        {
            this.waypointPath.waypoints = waypoints;
            this.waypointPath.currentWaypointIndex = 0;
            this.waypointPath.isActive = true;
        }

        public bool HasEndedCurrentWaypointPath()
        {
            return this.waypointPath.isActive == false;
        }

        protected override void GameplayUpdate(float deltaTime)
        {
            if (this.waypointPath.isActive)
            {
                Vector3 calculatedInputTranslation =
                    PlayerMovementInputWaypointPathHelper.
                        CalculatePlayerInputGroundTranslationToFollowWaypoint(
                            this.transform.position,
                            this.waypointPath.GetCurrentWaypoint().transform.position,
                            this.waypointPath.GetCurrentWaypoint().movementType
                        );

                this.playerMovementInput.SetMovementInput(
                    this.transform.InverseTransformDirection(calculatedInputTranslation));

                if (WaypointsUtils.HasReachedWaypointPosition(
                        this.transform.position,
                        this.waypointPath.GetCurrentWaypoint().transform.position))
                {
                    this.waypointPath.ProceedToNextWaypoint();
                }
            }
        }
    }
}
