﻿using Assets.Scripts.Common;
using Assets.Scripts.Utils;
using System;
using System.Diagnostics;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Collisions
{
    public static class RayColliderRules
    {
        public static bool IsLegitSolidGroundOrWallForCollision(RaycastHit hit)
        {
            return !hit.collider.gameObject.tag.Equals(Tags.gemsTag) &&
                !hit.collider.gameObject.tag.Equals(Tags.projectilesTag);
        }

        public static bool IsStandingOnMovingPlatform(
            Transform transform, Vector3 gravityDirection, out RaycastHit hit,
            Vector3? intentTranslation = null,
            float maxGroundRaycastDistance = 1.2f, bool debug = false)
        {
            if (intentTranslation != null)
            {
                intentTranslation = Vector3ProjectionHelper.ProjectConsideringDirection(
                    (Vector3)intentTranslation, gravityDirection).normalized;
            }
            else
            {
                intentTranslation = gravityDirection;
            }

            return PhysicsRaycaster.Raycast(transform.position + (-gravityDirection),
                (Vector3)intentTranslation, out hit, maxGroundRaycastDistance,
                Layers.movableEnvironmentLayerMask, debug: debug) &&
                IsLegitSolidGroundOrWallForCollision(hit);
        }

        public static bool IsHittingFlatGround(
            Transform transform, Vector3 gravityDirection, out RaycastHit hit,
            Vector3? intentTranslation = null,
            float maxGroundRaycastDistance = 1.2f, bool debug = false)
        {
            if (intentTranslation != null)
            {
                intentTranslation = Vector3.Project(
                    (Vector3)intentTranslation, gravityDirection).normalized;
            }
            else
            {
                intentTranslation = gravityDirection;
            }

            return PhysicsRaycaster.Raycast(transform.position + (-gravityDirection),
                (Vector3)intentTranslation, out hit, maxGroundRaycastDistance,
                Layers.generalEnvironmentLayersMask, debug: debug) &&
                IsLegitSolidGroundOrWallForCollision(hit);
        }

        public static bool IsHittingSlope(
            Transform transform, Vector3 gravityDirection, out RaycastHit hit,
            Vector3? intentTranslation = null,
            float maxGroundSlopeRaycastDistance = 1.5f, bool debug = false)
        {
            if (intentTranslation != null)
            {
                intentTranslation = Vector3.Project(
                    (Vector3)intentTranslation, gravityDirection).normalized;
            }
            else
            {
                intentTranslation = gravityDirection;
            }

            float angleThresholdDegrees = 1;
            if (PhysicsRaycaster.Raycast(
                transform.position + (-gravityDirection),
                (Vector3)intentTranslation, out hit,
                maxGroundSlopeRaycastDistance,
                Layers.generalEnvironmentLayersMask, debug: debug))
            {
                return (Vector3.Angle(-gravityDirection, hit.normal) > angleThresholdDegrees) &&
                    IsLegitSolidGroundOrWallForCollision(hit);
            }
            return false;
        }

        public static bool IsHittingTheWall(
            Transform transform, out RaycastHit hit,
            Vector3 gravityDirection,
            Vector3 gravityForward,
            float circleWallCollisionCheckingRadius = 1.1f,
            bool debug = false,
            Color? debugColor = null)
        {
            int layers = 10;
            int raysInLayer = 30;
            float layersIntervalHeight = 0.1f;
            float startLayersOffset = 0.6f;

            for (int i = 0; i < layers; i++)
            {
                if (PhysicsHelper.CastRaysInCircleVertical(
                transform.position + (-gravityDirection) * startLayersOffset + (-gravityDirection) * layersIntervalHeight * i,
                gravityForward,
                gravityDirection,
                circleWallCollisionCheckingRadius,
                raysInLayer,
                out hit,
                Layers.generalEnvironmentLayersMask,
                debug,
                debugColor))
                {
                    if (Mathf.Abs(Vector3.Dot(-gravityDirection, hit.normal)) < 0.5 &&
                        IsLegitSolidGroundOrWallForCollision(hit))
                    {
                        hit.normal = Vector3.ProjectOnPlane(hit.normal, gravityDirection).normalized;
                        return true;
                    }
                }
            }
            hit = new RaycastHit();
            return false;
        }

        public static bool HitsTheWallCheckedInStackedRays(
            Vector3 originPosition, Vector3 raysDirection,
            Vector3 gravityDirection,
            out RaycastHit hit,
            float wallCollisionCheckingRadius = 0.8f,
            bool debug = false,
            Color? debugColor = null)
        {
            int layers = 10;
            float layersIntervalHeight = 0.1f;
            float startLayersOffset = 0.6f;

            for (int i = 0; i < layers; i++)
            {
                if (PhysicsRaycaster.Raycast(
                originPosition + (-gravityDirection) * startLayersOffset + (-gravityDirection) * layersIntervalHeight * i,
                raysDirection,
                out hit,
                wallCollisionCheckingRadius,
                Layers.generalEnvironmentLayersMask,
                debug: debug,
                debugColor: debugColor
                ))
                {
                    if (Mathf.Abs(Vector3.Dot(-gravityDirection, hit.normal)) < 0.5 &&
                        IsLegitSolidGroundOrWallForCollision(hit))
                    {
                        hit.normal = Vector3.ProjectOnPlane(hit.normal, gravityDirection).normalized;
                        return true;
                    }
                }
            }
            hit = new RaycastHit();
            return false;
        }

        public static bool HasHitTheSolidObstacle(
            Transform transform, out RaycastHit hit,
            Vector3 gravityDirection, Vector3 gravityForward,
            float wallDistanceCheck = 1.1f,
            float slopeDistanceCheck = 1.5f,
            float flatGroundDistanceCheck = 1.2f)
        {
            return IsHittingTheWall(transform, out hit, gravityDirection, gravityForward, wallDistanceCheck)
                || IsHittingSlope(transform, gravityDirection, out hit, maxGroundSlopeRaycastDistance: slopeDistanceCheck)
                || IsHittingFlatGround(transform, gravityDirection, out hit, maxGroundRaycastDistance: flatGroundDistanceCheck);
        }

        public static bool HasHitTheSolidObstacle(Transform transform, Vector3 gravityDirection, Vector3 gravityForward)
        {
            RaycastHit hit = new RaycastHit();
            return HasHitTheSolidObstacle(transform, out hit, gravityDirection, gravityForward);
        }

        public static bool IsHittingBooster(Transform transform, Vector3 gravityDirection, out RaycastHit boosterHit, float maxGroundRaycastDistance)
        {
            return PhysicsRaycaster.Raycast(transform.position + (-gravityDirection),
                gravityDirection, out boosterHit, maxGroundRaycastDistance,
                Layers.boostersLayerMask, debug: false);
        }
    }
}
