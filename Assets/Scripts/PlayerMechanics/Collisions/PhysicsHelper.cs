﻿using Assets.Scripts.Common;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Collisions
{
    public static class PhysicsHelper
    {
        private static bool debug = false;

        public static bool CastRaysInCircleVertical(
            Vector3 origin,
            Vector3 direction,
            Vector3 gravityDirection,
            float radius,
            int raysCount,
            out RaycastHit hit,
            int layerMask = Layers.defaultLayerMask,
            bool debug = false,
            Color? debugColor = null
            )
        {
            for (int i = 0; i < raysCount; i++)
            {
                Vector3 rayVector = (Quaternion.AngleAxis((360.0f / raysCount) * i, -gravityDirection) * (direction)).normalized;
                if (PhysicsRaycaster.Raycast(
                    origin,
                    rayVector,
                    out hit,
                    radius,
                    layerMask,
                    debug: debug,
                    debugColor: debugColor))
                {
                    return true;
                }
            }
            hit = new RaycastHit();
            return false;
        }

        public static List<RaycastHit> CastRaysCollidersInCircleVerticalGetUniqueColliders(
            Vector3 origin,
            Vector3 direction,
            Vector3 gravityDirection,
            float radius,
            int raysCount,
            Func<RaycastHit, bool> endingColliderFilterCriteria,
            int layerMask = Layers.defaultLayerMask
            )
        {
            List<RaycastHit> result = new List<RaycastHit>();
            for (int i = 0; i < raysCount; i++)
            {
                Vector3 rayVector = (Quaternion.AngleAxis((360.0f / raysCount) * i, -gravityDirection) * (direction)).normalized;
                RaycastHit hit = new RaycastHit();
                if (PhysicsRaycaster.Raycast(
                    origin,
                    rayVector,
                    out hit,
                    radius,
                    layerMask,
                    debug: debug) && endingColliderFilterCriteria(hit))
                {
                    result.Add(hit);
                }
            }
            return result.ToLookup(x => x.collider.gameObject.GetInstanceID()).Select(x => x.First()).ToList();
        }

        public static bool CastRaysInCircleVerticalListPoints(
            Vector3 origin,
            Vector3 direction,
            Vector3 gravityDirection,
            float radius,
            int raysCount,
            out List<RaycastHit> hits,
            int layerMask = Layers.defaultLayerMask,
            bool debug = false,
            Color? debugColor = null
            )
        {
            hits = new List<RaycastHit>();
            for (int i = 0; i < raysCount; i++)
            {
                RaycastHit hitAtGivenMoment = new RaycastHit();
                Vector3 rayVector = (Quaternion.AngleAxis((360.0f / raysCount) * i, -gravityDirection) * (direction)).normalized;
                if (PhysicsRaycaster.Raycast(
                    origin,
                    rayVector,
                    out hitAtGivenMoment,
                    radius,
                    layerMask,
                    debug: debug,
                    debugColor: debugColor))
                {
                    hits.Add(hitAtGivenMoment);
                }
            }
            return hits.Count > 0;
        }

        public static bool CastRaysInCyllinderVertical(
            Vector3 origin,
            Vector3 gravityDirection,
            Vector3 gravityForward,
            float radius,
            int raysCountInLayer,
            out RaycastHit hit,
            float layersDistanceInterval,
            int layers,
            int layerMask = Layers.defaultLayerMask
            )
        {
            for (int i = 0; i < layers; i++)
            {
                if (PhysicsHelper.CastRaysInCircleVertical(
                    origin + (-gravityDirection) * layersDistanceInterval * i,
                    gravityForward,
                    gravityDirection,
                    radius,
                    raysCountInLayer,
                    out hit,
                    layerMask))
                {
                    return true;
                }
            }
            hit = new RaycastHit();
            return false;
        }

        public static List<RaycastHit> CastRaysInCyllinderVerticalGetUniqueColliders(
            Vector3 origin,
            Vector3 gravityDirection,
            Vector3 gravityForward,
            float radius,
            int raysCountInLayer,
            float layersDistanceInterval,
            int layers,
            Func<RaycastHit, bool> endingColliderFilterCriteria,
            int layerMask = Layers.defaultLayerMask
            )
        {
            List<RaycastHit> result = new List<RaycastHit>();
            for (int i = 0; i < layers; i++)
            {
                result.AddRange(
                    PhysicsHelper.CastRaysCollidersInCircleVerticalGetUniqueColliders(
                        origin + (-gravityDirection) * layersDistanceInterval * i,
                        gravityForward,
                        gravityDirection,
                        radius,
                        raysCountInLayer,
                        endingColliderFilterCriteria,
                        layerMask
                    ));
            }
            return result.ToLookup(x => x.collider.gameObject.GetInstanceID()).Select(x => x.First()).ToList();
        }        
    }
}
