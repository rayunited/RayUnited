﻿using Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy.LumSwinging;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics.Aspects;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.PlayerMechanics.Shooting;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Targeting
{
    public static class GrabbingLumHandOrientationHelper
    {
        public static void OrientateHand(
            GameObject handObject,
            GameObject playerActorObject,
            GameObject lumForSwingingObject)
        {
            Transform animatedPlayerPartTransform =
                playerActorObject.GetComponentInChildren<Animator>().transform;
            Vector3 animatedPlayerPartForwardDirection =
                -animatedPlayerPartTransform.forward;

            Vector3 lumForSwingingForwardDirection =
                lumForSwingingObject.transform.right;

            float angle = Vector3.Angle(
                    animatedPlayerPartForwardDirection,
                    lumForSwingingForwardDirection);

            Vector3 lumForSwingingForwardDirectionNormalizedWithPlayerForward =
                angle > 90f ?
                    -lumForSwingingForwardDirection :
                    lumForSwingingForwardDirection;

            handObject.transform.forward =
                lumForSwingingForwardDirectionNormalizedWithPlayerForward;
        }
    }

    public class SwingLumHitHdlBeh : TargetHitHandlingBeh
    {
        public float minimalSwingingDeflectionAngleDegrees = 60f;
        public float maximalSwingingDeflectionAngleDegrees = 70f;
        public float maxSwingingRadius = 3f;

        public override void OnTargetHit(
            HandProjectileBehaviour handProjectileBehaviour)
        {
            GameObject playerActorObject =
                handProjectileBehaviour.playerShootingAspect.gameObject;

            GameObject handObject =
                HandShootingProjectilesFactory.SpawnStaticHandObjectForLumSwinging(
                this.transform.position + playerActorObject.GetComponent<PlayerController>().controllerContext.gravityDirection * 0.5f,
                new Vector3(1.5f, 1.5f, 1.5f));
            
            playerActorObject.GetComponentsInChildren<AudioSource>()[1].Stop();

            GrabbingLumHandOrientationHelper
                .OrientateHand(
                    handObject,
                    playerActorObject,
                    this.gameObject);
            // remove player shot hand from the scene for the time of swinging on the lum
            // we will recreate it flying towards the player to regain it
            // when the player leaves the lum
            Destroy(handProjectileBehaviour.gameObject);

            LegacySwingLumHitHdlBehToNewLumSwingingTransitions
                .EnterLumSwingingFromSwingLumHitHdlBeh(
                    playerActorObject.GetComponent<PlayerMovementStateInfo>(),
                    playerActorObject.GetComponent<PlayerController>(),
                    this.gameObject,
                    playerActorObject.GetComponent<PlayerController>().controllerContext.gravityDirection,
                    playerActorObject.GetComponent<PlayerController>().controllerContext.gravityForward,
                    handProjectileBehaviour.isRightHand,
                    handObject,
                    this.minimalSwingingDeflectionAngleDegrees,
                    this.maximalSwingingDeflectionAngleDegrees,
                    this.maxSwingingRadius
                );
        }
    }
}
