﻿using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Aspects.Shooting
{
    public class SpinningFistChargBeh : MonoBehaviour
    {
        public float currentSpinningAngle = 0.0f;

        public void Spin(
            Vector3 rotationCenter,
            Vector3 rotationAxis,
            Vector3 gravityDirection,
            float rotationRadius,
            float spinningAngularDegreesSpeed)
        {
            this.currentSpinningAngle += spinningAngularDegreesSpeed;
            this.transform.position = rotationCenter +
                (Quaternion.AngleAxis(this.currentSpinningAngle, rotationAxis) * (-gravityDirection)) * rotationRadius;
        }
    }
}
