﻿using Assets.Scripts.Animations;
using Assets.Scripts.Common;
using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.GameMechanics.Entities.Camera;
using Assets.Scripts.GameMechanics.Entities.NewEntity.Aspects;
using Assets.Scripts.GameMechanics.Entities.Player;
using Assets.Scripts.GameMechanics.Entities.Portal;
using Assets.Scripts.HUD;
using Assets.Scripts.HUD.Targetting;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics.Camera;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.PlayerMechanics.Rules;
using Assets.Scripts.PlayerMechanics.Rules.Base;
using Assets.Scripts.Sound;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Aspects.Shooting
{
    public static class TargettablesHelper
    {
        public static List<GameObject>
            GetTargettableGameObjectsInTheScene()
        {
            return
                GameObject.FindGameObjectsWithTag(Tags.lumForHangingTag).ToList()
                    .Concat(GameObject.FindGameObjectsWithTag(Tags.pigPotCrystalsTag).ToList())
                    .Concat(GameObject.FindGameObjectsWithTag(Tags.targetableSwitchTag).ToList())
                    .Concat(GameObject.FindGameObjectsWithTag(Tags.enemyTag).ToList()).ToList();
        }

        public static GameObject
            GetClosestValidTarget(
                GameObject playerObject,
                Vector3 playerLookingDirection,
                List<GameObject> targettables,
                Vector3 gravityDirection)
        {
            Transform targetMinimalDistance = null;
            float minimalDistance = Mathf.Infinity;
            float minimalAngle = Mathf.Infinity;
             
            Vector3 currentPosition = playerObject.transform.position;
            List<Tuple<GameObject, Vector3>> potentialTargets = new List<Tuple<GameObject, Vector3>>();
            foreach (GameObject targetObject in targettables)
            {
                float minimalDistanceTargetThresholdConsideration = 18f;
                Vector3 distanceDirection = (targetObject.transform.position - playerObject.transform.position);

                TargetableProperties targetableProperties = 
                    targetObject.GetComponent<TargetableProperties>();

                if (targetableProperties)
                {
                    minimalDistanceTargetThresholdConsideration = targetableProperties.maxTargetingDistance;
                }

                float distance =
                    Vector3.Distance(
                        targetObject.transform.position, currentPosition);

                Vector3 directionToTarget =
                    (targetObject.transform.position - playerObject.transform.position).normalized;

                float angleFromPlayerLookingDirection =
                    Vector3.Angle(directionToTarget, playerLookingDirection);

                if (
                    distance < minimalDistanceTargetThresholdConsideration
                    && (Vector3.Angle(
                            Vector3.ProjectOnPlane(directionToTarget, -gravityDirection).normalized,
                                playerLookingDirection) < 80f ||
                        Vector3.Angle(-gravityDirection, directionToTarget.normalized) < 5f)
                    )
                {
                    potentialTargets.Add(Tuple.Create(targetObject, directionToTarget));
                    targetMinimalDistance = targetObject.transform;
                    minimalAngle = angleFromPlayerLookingDirection;
                    minimalDistance = distance;
                }
            }

            return potentialTargets.OrderBy(x =>
            Vector3.Angle(
                playerLookingDirection,
                Vector3.ProjectOnPlane(x.Item2, -gravityDirection)))
                .OrderBy(x => Mathf.Abs(x.Item1.transform.position.y - playerObject.transform.position.y))
                .FirstOrDefault()?.Item1;
        }

        public static bool IsTooFarToTargetAnything(
            GameObject playerObject,
            GameObject currentTarget,
            Vector3 playerLookingDirection,
            List<GameObject> targettables,
            Vector3 gravityDirection)
        {
            if (currentTarget)
            {
                var targetableProperties = 
                    currentTarget.GetComponent<TargetableProperties>();

                float minimalDistanceTargetThresholdConsideration = 18f;

                if (targetableProperties)
                {
                    minimalDistanceTargetThresholdConsideration = targetableProperties.maxTargetingDistance;
                }
                return Vector3.Distance(currentTarget.transform.position,
                    playerObject.transform.position) >= minimalDistanceTargetThresholdConsideration;
            }
            else
            {
                return GetClosestValidTarget(
                    playerObject,
                    playerLookingDirection,
                    targettables,
                    gravityDirection) == null;
            }
        }
    }

    public static class TargetShootSightScreenPositioningHelper
    {
        public static Vector3 GetScreenPositionForTarget(
            UnityEngine.Camera playerCamera, GameObject target)
        {
            Vector3 projectedPos = 
                playerCamera.GetComponent<CameraController>()
                    .ProjectWorldPositionToPortalsProjectedPositionThroughChainOfPortalsTransitions(target.transform.position);

            Vector3 guiPosition = playerCamera.WorldToScreenPoint(projectedPos);
            guiPosition.y = Screen.height - guiPosition.y;

            return guiPosition;
        }
    }

    public class TargettingAspect : GameplayOnlyMonoBehaviour
    {
        private GameObject playerObject;
        private GameObject animatedPlayerPart;
        private UnityEngine.Camera playerCamera;
        private PlayerMovementStateInfo playerMovementStateInfo;
        private List<PortalRenderingBehaviour> portals;
        private PlayerController playerController;
        private PlayerPortalTraveller playerPortalTraveller;

        public GameObject currentTarget;
        public Vector3 currentTargetProjectedPosition
        {
            get
            {
                return PlayerPortalsHelper.TransformWorldPositionToPortalsProjectedPositionThroughChainOfPortalsTransitions(
                    this.playerPortalTraveller.passedPortals,
                    this.currentTarget.transform.position,
                    this.playerController.controllerContext.gravityDirection
                );
            }
        }

        private DrawHudTargetShootSight hudTargetShootSightDrawing;

        public List<GameObject> targettables = new List<GameObject>();

        private IStrafingRuleUtils groundStrafingRule;
        private IStrafingRuleUtils airStrafingRule;

        private void Awake()
        {
            this.playerObject = this.gameObject;
            this.playerCamera = FindObjectOfType<CameraActorHandle>().GetComponent<UnityEngine.Camera>();
            this.hudTargetShootSightDrawing = FindObjectOfType<DrawHudTargetShootSight>();
            this.playerMovementStateInfo = GetComponent<PlayerMovementStateInfo>();
            this.portals = FindObjectsOfType<PortalRenderingBehaviour>(includeInactive: true).ToList();
            this.playerController = GetComponent<PlayerController>();
            this.playerPortalTraveller = GetComponent<PlayerPortalTraveller>();

            this.animatedPlayerPart = GetComponentInChildren<AnimationController>().gameObject;
        }

        public Vector3 GetPlayerLookingDirectionTowardsTarget(Vector3 playerPosition, Vector3 gravityDirection)
        {
            if (this.currentTarget)
            {
                return Vector3.ProjectOnPlane(
                    this.currentTarget.transform.position - playerPosition,
                    -gravityDirection).normalized;
            }
            else
            {
                return new Vector3();
            }
        }

        protected override void BehaviourStart()
        {
            this.targettables = TargettablesHelper.GetTargettableGameObjectsInTheScene();
        }

        private Tuple<IStrafingRuleUtils, bool> GetCurrentStrafingRule()
        {
            if (this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_GROUND))
            {
                return Tuple.Create(this.groundStrafingRule, true);
            }
            else if (this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_AIR))
            {
                return Tuple.Create(this.airStrafingRule, true);
            }
            else
            {
                return Tuple.Create<IStrafingRuleUtils, bool>(null, false);
            }
        }

        protected override void GameplayFixedUpdate()
        {
            if (IsAllowedRuleCurrentlyForTargeting() && !this.GetComponent<PlayerController>().playerParams.controlledProgrammatically)
            {
                if (this.currentTarget == null || GetCurrentStrafingRule().Item2 == false)
                {
                    var prev = this.currentTarget;

                    //Debug.Log("Changing target " + currentTarget);
                    this.currentTarget = TargettablesHelper.GetClosestValidTarget(
                        this.playerObject, -this.animatedPlayerPart.transform.forward, this.targettables,
                        this.playerController.controllerContext.gravityDirection);
                    if ((prev == null || prev.name != this.currentTarget?.name) && this.currentTarget != null)
                    {
                        AudioSource.PlayClipAtPoint(FindObjectOfType<BasicSoundsMap>().targeting, this.currentTarget.transform.position);
                    }
                    
                }

                if (TargettablesHelper.IsTooFarToTargetAnything(
                    this.playerObject, this.currentTarget, -this.animatedPlayerPart.transform.forward,
                    this.targettables, this.playerController.controllerContext.gravityDirection))
                {
                    this.currentTarget = null;
                }

                if (this.currentTarget && !this.GetComponent<PlayerController>().playerParams.controlledProgrammatically)
                {
                    TargetableProperties targetableProperties =
                        this.currentTarget.GetComponent<TargetableProperties>();

                    bool drawShootSight = true;

                    if (targetableProperties)
                    {
                        drawShootSight = targetableProperties.drawShootSight;
                    }

                    // this.hudTargetShootSightDrawing = FindObjectOfType<DrawHudTargetShootSight>();
                    this.hudTargetShootSightDrawing
                        .SetShootSightScreenCoordinates(
                            TargetShootSightScreenPositioningHelper
                                .GetScreenPositionForTarget(this.playerCamera, this.currentTarget)
                    );

                    Tuple<IStrafingRuleUtils, bool> currentStrafingRuleInfo = GetCurrentStrafingRule();

                    if (currentStrafingRuleInfo.Item2 == true)
                    {
                        this.hudTargetShootSightDrawing.SetShootingDirection(
                            ShootSightDirection.UP_ARROW_STRAIGHT);
                    }
                    else
                    {
                        this.hudTargetShootSightDrawing.SetShootingDirection(ShootSightDirection.DOWN_ARROW_STRAIGHT);
                    }

                    this.hudTargetShootSightDrawing.SetIsShootSightVisible(
                        this.currentTarget.GetComponentInChildren<Renderer>().isVisible && drawShootSight);
                }
                else if (!this.GetComponent<PlayerController>().playerParams.controlledProgrammatically)
                {
                    this.hudTargetShootSightDrawing.SetIsShootSightVisible(false);
                }
            }
            else if (!this.GetComponent<PlayerController>().playerParams.controlledProgrammatically)
            {
                this.hudTargetShootSightDrawing.SetIsShootSightVisible(false);
            }
        }

        private bool IsAllowedRuleCurrentlyForTargeting()
        {
            return this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_GROUND) ||
                this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_AIR) ||
                this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_AIR) ||
                this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_GROUND) ||

                this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.LEGACY_EMPTY_RULE);
        }

        public bool IsTargetVisible()
        {
            return this.currentTarget ?
                this.currentTarget.GetComponentInChildren<Renderer>().isVisible : false;
        }
    }
}
