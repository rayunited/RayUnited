﻿using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models;
using Assets.Scripts.Common;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics.Shooting;
using Assets.Scripts.Sound;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Aspects.Shooting
{
    public static class FistChargingAnimationHelper
    {
        private static float handSpinningRadiusAroundOrigin = 0.5f;
        private static float handSpinningAngularDegreesSpeed = 40.0f;

        public static void SpinRightFistChargingPrefab(
            Transform animatedPlayerPart, Transform currentChargingFistPrefabTransform,
            Vector3 gravityDirection, Vector3 gravityRight)
        {
            Vector3 handOrigin = animatedPlayerPart.transform.position + (-gravityDirection) * 1.2f +
                (-animatedPlayerPart.TransformDirection(Vector3.right));

            Vector3 rotationAxis = -animatedPlayerPart.TransformDirection(Vector3.right);

            currentChargingFistPrefabTransform.GetComponent<SpinningFistChargBeh>()
                .Spin(handOrigin, rotationAxis, gravityDirection, 0.7f, handSpinningAngularDegreesSpeed);
        }

        public static void SpinLeftFistChargingPrefab(
            Transform animatedPlayerPart, Transform currentChargingFistPrefabTransform,
            Vector3 gravityDirection, Vector3 gravityRight)
        {
            Vector3 handOrigin = animatedPlayerPart.transform.position + (-gravityDirection) * 1.2f +
                (-animatedPlayerPart.TransformDirection(-Vector3.right));

            Vector3 rotationAxis = -animatedPlayerPart.TransformDirection(-Vector3.right);

            currentChargingFistPrefabTransform.GetComponent<SpinningFistChargBeh>()
                .Spin(handOrigin, rotationAxis, gravityDirection, 0.7f, handSpinningAngularDegreesSpeed);
        }

        public static Transform
            InstantiateRightFistPrefabForChargingAndHideRightActualModelHand(
            Transform animatedPlayerPart,
            Vector3 gravityDirection,
            Vector3 gravityRight)
        {
            GameObject handObj = GameObject.Instantiate(GameObject.Find("RIGHT_HAND_OBJ"));
            animatedPlayerPart.GetComponent<AnimationController>().ChangeAnimationState(
                RaymanAnimations.ExtraAnimMissingRightHandStateName(), 1);
            handObj.tag = Tags.projectilesTag;
            handObj.GetComponent<TrailRenderer>().autodestruct = false;
            handObj.transform.position = animatedPlayerPart.transform.position + (-gravityDirection) * 1.2f +
                (-animatedPlayerPart.TransformDirection(gravityRight));

            handObj.AddComponent<SpinningFistChargBeh>();
            return handObj.transform;
        }

        public static Transform
            InstantiateLeftFistPrefabForChargingAndHideLeftActualModelHand(
            Transform animatedPlayerPart,
            Vector3 gravityDirection,
            Vector3 gravityRight)
        {
            GameObject handObj = GameObject.Instantiate(GameObject.Find("RIGHT_HAND_OBJ"));
            animatedPlayerPart.GetComponent<AnimationController>().ChangeAnimationState(
                RaymanAnimations.ExtraAnimMissingLeftHandStateName(), 2);
            handObj.tag = Tags.projectilesTag;
            handObj.GetComponent<TrailRenderer>().autodestruct = false;
            handObj.transform.position = animatedPlayerPart.transform.position + (-gravityDirection) * 1.2f +
                (-animatedPlayerPart.TransformDirection(-gravityRight));

            handObj.AddComponent<SpinningFistChargBeh>();
            return handObj.transform;
        }

        public static void DestroyRespectiveFistChargingPrefab(
            Transform currentChargingFistPrefabTransform,
            bool isChargingRightHandCurrently)
        {
            if (currentChargingFistPrefabTransform && currentChargingFistPrefabTransform.gameObject)
            {
                GameObject.Destroy(currentChargingFistPrefabTransform.gameObject);
            }
        }
    }

    public class FistChargingAspect : MonoBehaviour
    {
        private float currentChargePower = 1.0f;
        private float maxChargePower = HandProjectileConstants.MAX_CHARGE_POWER;
        private float lastKeepChargingCycleTime;

        public bool isChargingRightHandCurrently = false;
        private bool isInValidChargingCycle = false;

        private Transform animatedPlayerPart;
        private Transform currentChargingFistPrefabTransform;
        private PlayerController playerController;

        private float fistChargingSpeed = 0.1f;

        public float powerNormalized {
            get {
                return this.currentChargePower / this.maxChargePower;
            }
        }

        private void Start()
        {
            this.lastKeepChargingCycleTime = Time.time;
            this.animatedPlayerPart = GetComponentInChildren<AnimationController>().gameObject.transform;
            this.playerController = GetComponent<PlayerController>();
        }

        public void KeepChargingCycle()
        {
            var a = this.transform.GetComponentsInChildren<AudioSource>()[3];          
            if (!a.isPlaying)
            {
                a.clip = GameObject.FindObjectOfType<BasicSoundsMap>().fistCharging;
                //a.volume = 0.1f;
                a.loop = true;
                a.Play();
            }
            


            this.lastKeepChargingCycleTime = Time.time;
            if (this.isChargingRightHandCurrently)
            {
                FistChargingAnimationHelper
                    .SpinRightFistChargingPrefab(
                        this.animatedPlayerPart,
                        this.currentChargingFistPrefabTransform,
                        this.playerController.controllerContext.gravityDirection,
                        this.playerController.controllerContext.gravityRight);
            }
            else
            {
                FistChargingAnimationHelper.SpinLeftFistChargingPrefab(
                    this.animatedPlayerPart, this.currentChargingFistPrefabTransform,
                    this.playerController.controllerContext.gravityDirection,
                    this.playerController.controllerContext.gravityRight);
            }
            this.currentChargePower += this.fistChargingSpeed;
            if (this.currentChargePower > this.maxChargePower)
            {
                this.currentChargePower = this.maxChargePower;
            }
        }

        public void InitiateFistChargingCycleForHand(bool isRightHand)
        {
            this.isChargingRightHandCurrently = isRightHand;
            this.isInValidChargingCycle = true;
            if (this.isChargingRightHandCurrently)
            {
                this.currentChargingFistPrefabTransform = FistChargingAnimationHelper
                    .InstantiateRightFistPrefabForChargingAndHideRightActualModelHand(
                        this.animatedPlayerPart,
                        this.playerController.controllerContext.gravityDirection,
                        this.playerController.controllerContext.gravityRight);
            }
            else
            {
                this.currentChargingFistPrefabTransform = FistChargingAnimationHelper
                    .InstantiateLeftFistPrefabForChargingAndHideLeftActualModelHand(
                        this.animatedPlayerPart,
                        this.playerController.controllerContext.gravityDirection,
                        this.playerController.controllerContext.gravityRight);
            }
        }

        public (float, float) FinishChargingCycleAndGetEventualChargePowerAndMaxPower()
        {
            float result = this.currentChargePower;
            this.currentChargePower = 1f;
            this.isInValidChargingCycle = false;

            FistChargingAnimationHelper
              .DestroyRespectiveFistChargingPrefab(
                 this.currentChargingFistPrefabTransform, this.isChargingRightHandCurrently);
            return (result, this.maxChargePower);
        }

        public bool IsChargingRightHandCurrently()
        {
            return this.isChargingRightHandCurrently && IsInValidCycleCurrently();
        }

        public bool IsInValidCycleCurrently()
        {
            return this.isInValidChargingCycle;
        }

        public bool IsChargingLeftHandCurrently()
        {
            return !this.isChargingRightHandCurrently && IsInValidCycleCurrently();
        }
    }
}
