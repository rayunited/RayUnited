﻿using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models;
using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.GameMechanics.Entities.Portal;
using Assets.Scripts.NewPlayerMechanics;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Aspects
{
    public class PlayerGroundRollingAspect : GameplayOnlyMonoBehaviour
    {
        private AnimationController animationController;
        private GameObject animatedPlayerPart;
        private PlayerController playerController;

        public float groundRollForwardSpeed = 0.22f;
        public float groundRollStrafingForwardSpeed = 0.17f;
        public float groundRollLeftRightSpeed = 0.20f;
        public float groundRollBackwardsSpeed = 0.15f;
        private Vector3 currentForwardRollDirection;
        private Vector3 currentBackwardsRollDirection;
        private Vector3 currentLeftRightRollDirection;

        public void OrientatePlayerAccordingToPortals(Transform fromPortal, Transform toPortal)
        {
            var fromPortalRend = fromPortal.GetComponent<PortalRenderingBehaviour>();
            var toPortalRend = toPortal.GetComponent<PortalRenderingBehaviour>();

            Vector3 projectedForwardRollDirection = 
                toPortalRend.TransformDirection(fromPortalRend.InverseTransformDirection(this.currentForwardRollDirection));
            Vector3 projectedBackwardsRollDirection =
                 toPortalRend.TransformDirection(fromPortalRend.InverseTransformDirection(this.currentBackwardsRollDirection));
            Vector3 projectedLeftRightRollDirection =
                 toPortalRend.TransformDirection(fromPortalRend.InverseTransformDirection(this.currentLeftRightRollDirection));

            this.currentForwardRollDirection = projectedForwardRollDirection;
            this.currentBackwardsRollDirection = projectedBackwardsRollDirection;
            this.currentLeftRightRollDirection= projectedLeftRightRollDirection;
        }

        private void Awake()
        {
            this.animationController = GetComponentInChildren<AnimationController>();
            this.animatedPlayerPart = this.animationController.gameObject;
            this.playerController = GetComponent<PlayerController>();
        }

        public bool IsPerformingGroundRoll()
        {
            return
                (
                    this.animationController.GetCurrentAnimationState().Equals(RaymanAnimations.StrafingRollForwardStateName())
                    ||
                    this.animationController.GetCurrentAnimationState().Equals(RaymanAnimations.StrafingRollBackwardsStateName())
                    ||
                    this.animationController.GetCurrentAnimationState().Equals(RaymanAnimations.StrafingRollLeftStateName())
                    ||
                    this.animationController.GetCurrentAnimationState().Equals(RaymanAnimations.StrafingRollRightStateName())
                ) && !this.animationController.HasAnimationEnded();
        }

        public bool HasEndedGroundRoll()
        {
            return
                (
                    this.animationController.GetCurrentAnimationState().Equals(RaymanAnimations.StrafingRollForwardStateName())
                    ||
                    this.animationController.GetCurrentAnimationState().Equals(RaymanAnimations.StrafingRollBackwardsStateName())
                    ||
                    this.animationController.GetCurrentAnimationState().Equals(RaymanAnimations.StrafingRollLeftStateName())
                    ||
                    this.animationController.GetCurrentAnimationState().Equals(RaymanAnimations.StrafingRollRightStateName())
                ) && this.animationController.HasAnimationEnded();
        }

        public void StartPerformingForwardGroundRoll(Vector3 rollDirection)
        {
            this.animationController.ChangeAnimationStateWithPriority(RaymanAnimations.StrafingRollForwardStateName(), priority: 10);
            this.currentForwardRollDirection = rollDirection.normalized;
        }

        public void BreakTheRollCycle()
        {
            this.animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.StrafingRollForwardStateName(), newPriority: 0);
            this.animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.StrafingRollBackwardsStateName(), newPriority: 0);
            this.animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.StrafingRollLeftStateName(), newPriority: 0);
            this.animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.StrafingRollRightStateName(), newPriority: 0);
        }

        public Vector3 GetMovementVelocityAppropriateForCurrentForwardRollCycle()
        {
            return this.currentForwardRollDirection * this.groundRollForwardSpeed;
        }

        public Vector3 GetMovementVelocityAppropriateForCurrentBackwardsRollCycle()
        {
            return this.currentBackwardsRollDirection * this.groundRollBackwardsSpeed;
        }

        public Vector3 GetMovementVelocityAppropriateForCurrentLeftRightRollCycle()
        {
            return this.currentLeftRightRollDirection * this.groundRollLeftRightSpeed;
        }

        public void StartPerformingStrafingGroundRoll(
            Vector3 playerForwardDirection,
            Vector3 playerRightDirection,
            Vector3 rollInitialDirection)
        {
            float angleBetweenDirectionAndAnimatedPlayerPartForward =
                Vector3.Angle(
                   playerForwardDirection.normalized, rollInitialDirection.normalized);

            float angleBetweenDirectionAndAnimatedPlayerPartRight =
                Vector3.Angle(
                    playerRightDirection.normalized, rollInitialDirection.normalized
                    );

            if (angleBetweenDirectionAndAnimatedPlayerPartForward < 20f)
            {
                // roll forward
                //animationController.ChangeAnimationStateWithPriority(
                //    RaymanAnimations.StrafingRollForwardStateName(), priority: 10);
                StartPerformingForwardGroundRoll(rollInitialDirection);
                this.playerController.playerParams.soundParams
                    .strafingGroundSounds.rollForwardSound
                    .PlayOnceWithAudioSource(
                        this.playerController.controllerContext.playerAudioHandles
                        .groundAudioHandles.rollAudioSource);
                return;
            }
            else if (angleBetweenDirectionAndAnimatedPlayerPartForward < 160f)
            {
                // roll left or right
                if (angleBetweenDirectionAndAnimatedPlayerPartRight < 90f)
                {
                    // roll right
                    this.animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.StrafingRollRightStateName(), priority: 10);
                    this.playerController.playerParams.soundParams
                        .strafingGroundSounds.rollSideSound
                        .PlayOnceWithAudioSource(
                            this.playerController.controllerContext.playerAudioHandles
                            .groundAudioHandles.rollAudioSource);
                }
                else
                {
                    // roll left
                    this.animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.StrafingRollLeftStateName(), priority: 10);
                    this.playerController.playerParams.soundParams
                        .strafingGroundSounds.rollSideSound
                        .PlayOnceWithAudioSource(
                            this.playerController.controllerContext.playerAudioHandles
                            .groundAudioHandles.rollAudioSource);
                }

                this.currentLeftRightRollDirection = rollInitialDirection;
            }
            else
            {
                // roll backwards
                this.animationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.StrafingRollBackwardsStateName(), priority: 10);
                this.currentBackwardsRollDirection = rollInitialDirection;
                this.playerController.playerParams.soundParams
                    .strafingGroundSounds.rollBackwardSound
                    .PlayOnceWithAudioSource(
                        this.playerController.controllerContext.playerAudioHandles
                        .groundAudioHandles.rollAudioSource);
            }
        }

        public bool IsPerformingGroundBackwardsRoll()
        {
            return this.animationController.GetCurrentAnimationState()
                .Equals(RaymanAnimations.StrafingRollBackwardsStateName())
                && !this.animationController.HasAnimationEnded();
        }

        public bool IsPerformingGroundForwardRoll()
        {
            return this.animationController.GetCurrentAnimationState()
                .Equals(RaymanAnimations.StrafingRollForwardStateName())
                && !this.animationController.HasAnimationEnded();
        }

        public bool IsPerformingGroundLeftOrRightRoll()
        {
            return
                (
                    this.animationController.GetCurrentAnimationState()
                    .Equals(RaymanAnimations.StrafingRollRightStateName())
                    ||
                    this.animationController.GetCurrentAnimationState()
                    .Equals(RaymanAnimations.StrafingRollLeftStateName())
                )
                && !this.animationController.HasAnimationEnded();
        }

        protected override void GameplayFixedUpdate()
        {
            if (HasEndedGroundRoll())
            {
                BreakTheRollCycle();
            }
        }
    }
}
