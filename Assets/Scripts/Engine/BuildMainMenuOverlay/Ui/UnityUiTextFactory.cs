﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.BuildMainMenuOverlay.Ui
{
    public static class TextLabelDimensionHelper
    {
        public static float GetPreferredWidthFor(
            Text text)
        {
            TextGenerator textGen = new TextGenerator();
            Vector2 rect = text.rectTransform.rect.size;

            TextGenerationSettings generationSettings =
                text.GetGenerationSettings(rect);
            return textGen.GetPreferredWidth(text.text, generationSettings);
        }

        public static float GetPreferredHeightFor(Text text)
        {
            TextGenerator textGen = new TextGenerator();
            Vector2 rect = text.rectTransform.rect.size;

            TextGenerationSettings generationSettings =
                text.GetGenerationSettings(rect);
            return textGen.GetPreferredHeight(text.text, generationSettings);
        }
    }

    public static class UnityUiTextFactory
    {
        public static Text GetCenteredTextLabel(
            string name, Font font, int fontSize,
            string caption, Color textColor,
            float? boxWidth = null)
        {
            return GetTextLabel(name, font, fontSize, caption, textColor,
                TextAnchor.MiddleCenter, boxWidth);
        }

        public static Text GetLeftAlignedTextLabel(
            string name, Font font, int fontSize,
            string caption, Color textColor,
            float? boxWidth = null)
        {
            return GetTextLabel(name, font, fontSize, caption, textColor,
                TextAnchor.MiddleLeft, boxWidth);
        }

        public static Text GetRightAlignedTextLabel(
            string name, Font font, int fontSize,
            string caption, Color textColor,
            float? boxWidth = null)
        {
            return GetTextLabel(name, font, fontSize, caption, textColor,
                TextAnchor.MiddleRight, boxWidth);
        }

        public static Text GetTextLabel(
            string name, Font font, int fontSize,
            string caption, Color textColor,
            TextAnchor textAnchor,
            float? boxWidth = null)
        {
            GameObject labelObject = new GameObject(name);

            Text text = labelObject.AddComponent<Text>();
            text.font = font;
            text.fontSize = fontSize;
            text.text = caption;
            text.color = textColor;

            text.rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
            text.rectTransform.anchorMax = new Vector2(0.5f, 0.5f);

            text.rectTransform.anchoredPosition = new Vector2(0f, 0f);
            text.alignment = textAnchor;

            if (boxWidth == null)
            {
                boxWidth = TextLabelDimensionHelper.GetPreferredWidthFor(text);
            }

            text.rectTransform.sizeDelta = new Vector2(
                (float)boxWidth, fontSize + 10);

            return text;
        }
    }
}
