﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.BuildMainMenuOverlay.Ui
{
    public static class UnityUiImageFactory
    {
        public static Image GetSimpleImagePanel(
            string name, Color backgroundColor, float width, float height,
            Vector2? rectTransformPivot = null,
            Vector2? rectTransformAnchorMin = null,
            Vector2? rectTransformAnchorMax = null)
        {
            if (rectTransformPivot == null)
            {
                rectTransformPivot = Vector2.zero;
            }
            if (rectTransformAnchorMin == null)
            {
                rectTransformAnchorMin = new Vector2(0.5f, 0.5f);
            }
            if (rectTransformAnchorMax == null)
            {
                rectTransformAnchorMax = new Vector2(0.5f, 0.5f);
            }


            GameObject imageObject = new GameObject(name);

            Image image = imageObject.AddComponent<Image>();
            image.rectTransform.pivot = (Vector2)rectTransformPivot;

            image.rectTransform.anchorMin = (Vector2)rectTransformAnchorMin;
            image.rectTransform.anchorMax = (Vector2)rectTransformAnchorMax;

            image.color = backgroundColor;

            image.rectTransform.anchoredPosition =
                new Vector2((-width) / 2f, (-height) / 2f);

            image.rectTransform.sizeDelta = new Vector2(width, height);
            return image;
        }
    }
}
