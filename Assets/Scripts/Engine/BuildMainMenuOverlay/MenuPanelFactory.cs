﻿using Assets.Scripts.Engine.BuildMainMenuOverlay.Ui;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.BuildMainMenuOverlay
{
    public static class MenuPanelParts
    {
        public const string backgroundBox = "BackgroundBox";
        public const string focusBoxesContainer = "FocusBoxesContainer";
        public const string dialogsContainer = "DialogsContainer";
    }

    public static class MenuPanelFactory
    {
        public static GameObject CreatePanelObjectWithBoxBackgroundSizeReferenceResolution(
            GameObject menuCanvasObject,
            Vector2 referenceResolution,
            string panelObjectName,
            float normalizedBoxWidth,
            float normalizedBoxHeight,
            Color backgroundColor,
            Vector2? anchoredPosition = null,
            Vector2? anchorMin = null,
            Vector2? anchorMax = null,
            Vector2? rectTransformPivot = null,
            Vector2? rectTransformAnchorMin = null,
            Vector2? rectTransformAnchorMax = null)
        {
            if (anchoredPosition == null)
            {
                anchoredPosition = Vector2.zero;
            }
            if (anchorMin == null)
            {
                anchorMin = new Vector2(0.5f, 0.5f);
            }
            if (anchorMax == null)
            {
                anchorMax = new Vector2(0.5f, 0.5f);
            }

            GameObject panelObject = new GameObject(panelObjectName, typeof(RectTransform));
            panelObject.transform.SetParent(
                menuCanvasObject.transform, worldPositionStays: false);

            panelObject.AddComponent<CanvasGroup>();

            RectTransform panelObjectRectTransform = panelObject.GetComponent<RectTransform>();
            panelObjectRectTransform.anchoredPosition = (Vector2)anchoredPosition;
            panelObjectRectTransform.anchorMin = (Vector2)anchorMin;
            panelObjectRectTransform.anchorMax = (Vector2)anchorMax;

            Image boxImage = UnityUiImageFactory
                .GetSimpleImagePanel(
                    MenuPanelParts.backgroundBox, backgroundColor,
                    normalizedBoxWidth * referenceResolution.x,
                    normalizedBoxHeight * referenceResolution.y,
                    rectTransformPivot, rectTransformAnchorMin, rectTransformAnchorMax);
            boxImage.transform.SetParent(panelObject.transform, worldPositionStays: false);
            return panelObject;
        }

        public static GameObject GetBackgroundBoxObject(
            GameObject panelObject)
        {
            return panelObject.transform.Find(MenuPanelParts.backgroundBox).gameObject;
        }

        public static GameObject
            CreatePanelObjectWithCenteredBoxBackgroundMarginReferenceResolution(
                GameObject menuCanvasObject,
                Vector2 referenceResolution,
                string panelObjectName,
                float normalizedBoxMarginX,
                float normalizedBoxMarginY,
                Color backgroundColor)
        {
            float normalizedBoxWidth = 1f - 2 * normalizedBoxMarginX;
            float normalizedBoxHeight = 1f - 2 * normalizedBoxMarginY;

            return CreatePanelObjectWithBoxBackgroundSizeReferenceResolution(
                menuCanvasObject,
                referenceResolution,
                panelObjectName,
                normalizedBoxWidth,
                normalizedBoxHeight,
                backgroundColor
                );
        }

        public static GameObject CreateEmptyPlaceholderPanelObject(
            GameObject menuCanvasObject,
            Vector2 referenceResolution,
            string panelObjectName)
        {
            throw new NotImplementedException();
        }
    }
}
