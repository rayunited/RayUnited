﻿using Assets.Scripts.Engine.BuildMainMenuOverlay.Ui;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.BuildMainMenuOverlay
{
    public static class SettingControlParts
    {
        public const string labelText = "labelText";
        public const string valueText = "valueText";
        public const string descriptionText = "descriptionText";
        public const string descriptionTextContainer = "descriptionTextContainer";

        public const string labelValueContainer = "labelValueContainer";
    }

    public static class SettingsMenuSectionBuildingHelper
    {
        public static SettingControlBehaviour AddSettingControlUnderSettingControlsContainer(
            GameObject settingControlsContainer,
            SettingControlInfo controlInfo,
            Font font,
            Color textColor,
            int baseFontSize,
            int descriptionFontSize,
            float elementHeight)
        {
            GameObject settingControlContainerPanel =
                new GameObject("Control - " + controlInfo.controlId, typeof(RectTransform));

            RectTransform settingControlsContainerRectTransform =
                settingControlsContainer.GetComponent<RectTransform>();

            RectTransform settingControlContainerPanelRectTransform =
                settingControlContainerPanel.GetComponent<RectTransform>();
            settingControlContainerPanelRectTransform.sizeDelta =
                new Vector2(
                    settingControlsContainerRectTransform.sizeDelta.x,
                    elementHeight
                    );

            GameObject labelValuePanelContainer =
                new GameObject(SettingControlParts.labelValueContainer, typeof(RectTransform));

            VerticalLayoutGroup verticalLayoutGroupSettingControlContainer =
                settingControlContainerPanel.AddComponent<VerticalLayoutGroup>();
            //verticalLayoutGroupSettingControlContainer.childControlHeight = false;
            verticalLayoutGroupSettingControlContainer.childControlWidth = false;
            //verticalLayoutGroupSettingControlContainer.childForceExpandHeight = false;
            verticalLayoutGroupSettingControlContainer.childForceExpandWidth = false;

            float elementsSpacing = 20;

            verticalLayoutGroupSettingControlContainer.spacing = elementsSpacing;

            //verticalLayoutGroup.spacing = -baseFontSize;
            HorizontalLayoutGroup horizontalLayoutGroupLabelValueContainer =
                labelValuePanelContainer.AddComponent<HorizontalLayoutGroup>();
            horizontalLayoutGroupLabelValueContainer.childControlHeight = false;
            horizontalLayoutGroupLabelValueContainer.childControlWidth = false;
            horizontalLayoutGroupLabelValueContainer.childForceExpandHeight = false;
            horizontalLayoutGroupLabelValueContainer.childForceExpandWidth = false;


            Text labelText = UnityUiTextFactory.GetLeftAlignedTextLabel(
                SettingControlParts.labelText, font, baseFontSize, controlInfo.label, textColor);
            labelText.rectTransform.sizeDelta = new Vector2(
                settingControlContainerPanelRectTransform.sizeDelta.x / 2,
                labelText.rectTransform.sizeDelta.y
                );

            Text valueText = UnityUiTextFactory.GetRightAlignedTextLabel(
                SettingControlParts.valueText, font, baseFontSize, "test", textColor);
            valueText.rectTransform.sizeDelta = new Vector2(
                settingControlContainerPanelRectTransform.sizeDelta.x / 2,
                valueText.rectTransform.sizeDelta.y
                );

            Text descriptionText = UnityUiTextFactory.GetLeftAlignedTextLabel(
                SettingControlParts.descriptionText, font,
                descriptionFontSize,
                controlInfo.description,
                textColor);

            GameObject descriptionTextContainer =
                new GameObject(
                    SettingControlParts.descriptionTextContainer,
                    typeof(RectTransform));
            RectTransform descriptionTextContainerRectTransform =
                descriptionTextContainer.GetComponent<RectTransform>();

            descriptionTextContainerRectTransform.sizeDelta = new Vector2(
                settingControlContainerPanelRectTransform.sizeDelta.x,
                descriptionText.rectTransform.sizeDelta.y
                );

            descriptionText.rectTransform.sizeDelta = new Vector2(
                settingControlContainerPanelRectTransform.sizeDelta.x,
                descriptionText.rectTransform.sizeDelta.y
                );

            labelValuePanelContainer.transform.SetParent(
                settingControlContainerPanel.transform, false);

            labelText.transform.SetParent(labelValuePanelContainer.transform, false);
            valueText.transform.SetParent(labelValuePanelContainer.transform, false);

            labelText.rectTransform.pivot = new Vector2(0f, 0.5f);

            descriptionText.transform.SetParent(
                descriptionTextContainer.transform, false);

            descriptionTextContainer.transform.SetParent(
                settingControlContainerPanel.transform, false);

            settingControlContainerPanel.transform.SetParent(
                settingControlsContainer.transform, false);

            SettingControlBehaviour settingControlBehaviour = settingControlContainerPanel
                .AddComponent<SettingControlBehaviour>();
            settingControlBehaviour.labelText = labelText;
            settingControlBehaviour.valueText = valueText;
            return settingControlBehaviour;
        }
    }
}
