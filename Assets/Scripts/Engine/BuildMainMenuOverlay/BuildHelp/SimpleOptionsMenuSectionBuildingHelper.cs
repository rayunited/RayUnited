﻿using Assets.Scripts.Engine.BuildMainMenuOverlay.Ui;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Base;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.BuildMainMenuOverlay
{
    public static class SimpleOptionsMenuSectionBuildingHelper
    {
        public static Text AddSimpleMenuLabelOptionAdjustingBackgroundBox(
            GameObject boxBackgroundObject,
            SimpleMenuOptionInfo option,
            Font font,
            Color textColor,
            int fontSize)
        {
            //GameObject optionLabelObject = new GameObject("Option - " + option.caption);
            //optionLabelObject.transform.parent = boxBackgroundObject.transform;

            Image boxBackgroundImage = boxBackgroundObject.GetComponent<Image>();

            Text text = UnityUiTextFactory.GetCenteredTextLabel(
                "Option - " + option.caption, font, fontSize, option.caption, textColor,
                boxBackgroundImage.rectTransform.sizeDelta.x);
            text.transform.parent = boxBackgroundImage.transform;

            //Text text = optionLabelObject.AddComponent<Text>();
            //text.font = font;
            //text.fontSize = fontSize;
            //text.text = option.caption;
            //text.color = textColor;

            //text.rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
            //text.rectTransform.anchorMax = new Vector2(0.5f, 0.5f);

            //text.rectTransform.anchoredPosition = new Vector2(0f, 0f);
            //text.alignment = TextAnchor.MiddleCenter;

            //text.rectTransform.sizeDelta = new Vector2(
            //    boxBackgroundImage.rectTransform.sizeDelta.x, fontSize + 10);

            return text;
        }
    }
}
