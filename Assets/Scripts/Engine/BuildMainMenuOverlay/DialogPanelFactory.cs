﻿using Assets.Scripts.Engine.MainMenuOverlay.Sections.Dialogs;
using System;
using UnityEngine;

namespace Assets.Scripts.Engine.BuildMainMenuOverlay
{
    public static class DialogParts
    {
        public const string message = "message";
        public const string confirmLabel = "confirmLabel";
        public const string rejectLabel = "rejectLabel";

        public const string dialogBackgroundBox = "dialogBackgroundBox";
        public const string dialogButtonsContainer = "dialogButtonsContainer";
    }

    public static class DialogPanelFactory
    {
        public static DialogSectionBehaviour CreateDialogPanel(
            GameObject dialogsContainer,
            Color backgroundColor,
            Color textColor,
            Font font,
            int fontSize,
            DialogClass dialogClass,
            string message, string confirmCaption, string rejectCaption,
            string dialogId)
        {
            GameObject dialogPanelObject =
                new GameObject("Dialog - " + dialogId, typeof(RectTransform));
            dialogPanelObject.transform.SetParent(
                dialogsContainer.transform, worldPositionStays: false);

            DialogSectionBehaviour dialogSectionBehaviour = null;

            if (dialogClass == DialogClass.RESTART_SCENE_DIALOG)
            {
                dialogSectionBehaviour = dialogPanelObject
                    .AddComponent<RestartGameDialogBehaviour>();
            }
            else if (dialogClass == DialogClass.EXIT_GAME_DIALOG)
            {
                dialogSectionBehaviour = dialogPanelObject.AddComponent<ExitGameDialogBehaviour>();
            }
            else if (dialogClass == DialogClass.MAIN_MENU_DIALOG)
            {
                dialogSectionBehaviour = dialogPanelObject.AddComponent<MainMenuDialogBehaviour>();
            }
            else if (dialogClass == DialogClass.SETTING_CONTROL_VALUE_APPLY_DIALOG)
            {
                dialogSectionBehaviour = dialogPanelObject
                    .AddComponent<SettingControlValueApplyDialogBehaviour>();
            }
            else
            {
                throw new InvalidOperationException(
                    "Not implemented other dialog classes handling!");
            }

            float messageAndButtonsSpacing = 40;
            float messageMargin = 40;

            dialogSectionBehaviour.ReinitUiState(
                dialogId,
                dialogsContainer,
                messageAndButtonsSpacing,
                messageMargin,
                fontSize, font, textColor, backgroundColor);
            dialogSectionBehaviour.RepropagateDialog(
                message, confirmCaption, rejectCaption
                );
            return dialogSectionBehaviour;
        }
    }
}
