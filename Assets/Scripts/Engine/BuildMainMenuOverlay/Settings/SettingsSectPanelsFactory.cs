﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.Categories;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.Scrolling;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.BuildMainMenuOverlay.Settings
{
    public static class SettingsMenuSectionParts
    {
        public const string settingScrollControlsContainer =
            "settingControlsContainerView";
        public const string settingControlsViewport =
            "settingControlsViewport";
        public const string settingControlsContent =
            "settingControlsContent";

        public const string settingsScrollUpArrowContainer = "settingsScrollUpArrowContainer";
        public const string settingsScrollUpArrowContent = "settingsScrollUpArrowContent";

        public const string settingsScrollDownArrowContainer = "settingsScrollDownArrowContainer";
        public const string settingsScrollDownArrowContent = "settingsScrollDownArrowContent";

        public const string settingsCategoryPreviousPromptContainer =
            "settingsCategoryPreviousPromptContainer";
        public const string settingsCategoryNextPromptContainer =
            "settingsCategoryNextPromptContainer";
        public const string settingsCategoriesSwitchingPromptsContainer =
            "settingsCategoriesSwitchingPromptsContainer";
        public const string selectedCategoryPromptPanel =
            "selectedCategoryPromptPanel";
    }

    public static class SettingsSectPanelsFactory
    {
        public static
            Tuple<
                GameObject,
                SettingsScrollViewportBehaviour,
                float>
            GetSettingsControlsContentContainerUnder(
                GameObject parentPanelObject,
                int maximalAmountOfControlsVisibleAtOnce,
                float minimalMarginBottom,
                Font font, int fontSize, Color textColor,
                Color scrollActiveColor, Color scrollInactiveColor)
        {
            int marginLeftTopRight = 40;
            int marginTop = 40;

            GameObject containerScrollViewPanelObject = new GameObject(
                SettingsMenuSectionParts.settingScrollControlsContainer,
                typeof(RectTransform));

            RectTransform rectTransform =
                containerScrollViewPanelObject.GetComponent<RectTransform>();

            rectTransform.anchorMin = new Vector2(0.5f, 1f);
            rectTransform.anchorMax = new Vector2(0.5f, 1f);

            rectTransform.pivot = new Vector2(0.5f, 1f);

            RectTransform parentRectTransform =
                parentPanelObject.GetComponent<RectTransform>();

            rectTransform.sizeDelta =
                new Vector2(parentRectTransform.sizeDelta.x - marginLeftTopRight * 2,
                    parentRectTransform.sizeDelta.y - marginTop - minimalMarginBottom);
            rectTransform.anchoredPosition = new Vector2(0, -marginTop);

            containerScrollViewPanelObject.transform.SetParent(
                parentPanelObject.transform, false);

            VerticalLayoutGroup verticalLayoutGroup =
                containerScrollViewPanelObject.AddComponent<VerticalLayoutGroup>();

            verticalLayoutGroup.childForceExpandHeight = false;
            verticalLayoutGroup.childForceExpandWidth = false;
            verticalLayoutGroup.childControlWidth = false;
            verticalLayoutGroup.childControlHeight = false;

            float elementsSpacing = 40;

            GameObject upScrollingArrowPanel =
                ScrollingViewportHelperElementsFactory.GetUpScrollingArrowPanel(
                    containerScrollViewPanelObject,
                    font, fontSize, textColor
                    );
            RectTransform upScrollingArrowPanelRectTransform =
                upScrollingArrowPanel.GetComponent<RectTransform>();

            GameObject downScrollingArrowPanel =
                ScrollingViewportHelperElementsFactory.GetDownScrollingArrowPanel(
                    containerScrollViewPanelObject,
                    font, fontSize, textColor
                    );
            RectTransform downScrollingArrowPanelRectTransform =
                downScrollingArrowPanel.GetComponent<RectTransform>();

            Tuple<GameObject, GameObject> viewportAndContent =
                SettingsControlsContentFactory
                    .GetViewportWithContentChildUnder(
                        elementsSpacing, containerScrollViewPanelObject,
                        upScrollingArrowPanelRectTransform.sizeDelta.y,
                        downScrollingArrowPanelRectTransform.sizeDelta.y);

            RectTransform viewportRectTransform =
                viewportAndContent.Item1.GetComponent<RectTransform>();

            upScrollingArrowPanel.transform.SetParent(
                containerScrollViewPanelObject.transform, false);
            viewportAndContent.Item1.transform.SetParent(
                containerScrollViewPanelObject.transform, false);
            downScrollingArrowPanel.transform.SetParent(
                containerScrollViewPanelObject.transform, false);

            //rectTransform.sizeDelta =
            //    new Vector2(parentRectTransform.sizeDelta.x - marginLeftTopRight * 2,
            //        parentRectTransform.sizeDelta.y - marginTop - minimalMarginBottom);
            //rectTransform.anchoredPosition = new Vector2(0, -marginTop);

            float desiredListElementHeight =
                (viewportRectTransform.sizeDelta.y
                    / maximalAmountOfControlsVisibleAtOnce)
                    - elementsSpacing;

            SettingsScrollViewportBehaviour settingsScrollViewportBehaviour =
                containerScrollViewPanelObject
                    .AddComponent<SettingsScrollViewportBehaviour>();

            ScrollingPromptBehaviour upScrollingPromptHandle =
                upScrollingArrowPanel.AddComponent<ScrollingPromptBehaviour>();
            upScrollingPromptHandle.text =
                upScrollingPromptHandle.GetComponent<Text>();
            upScrollingPromptHandle.activeColor = scrollActiveColor;
            upScrollingPromptHandle.inactiveColor = scrollInactiveColor;

            ScrollingPromptBehaviour downScrollingPromptHandle =
                downScrollingArrowPanel.AddComponent<ScrollingPromptBehaviour>();
            downScrollingPromptHandle.text =
                downScrollingPromptHandle.GetComponent<Text>();
            downScrollingPromptHandle.activeColor = scrollActiveColor;
            downScrollingPromptHandle.inactiveColor = scrollInactiveColor;

            settingsScrollViewportBehaviour.upScrollPromptHandle =
                upScrollingPromptHandle;
            settingsScrollViewportBehaviour.downScrollPromptHandle =
                downScrollingPromptHandle;
            settingsScrollViewportBehaviour.scrollableContent =
                viewportAndContent.Item2.GetComponent<RectTransform>();
            settingsScrollViewportBehaviour.elementHeight =
                desiredListElementHeight + elementsSpacing;

            return Tuple.Create(
                viewportAndContent.Item2,
                settingsScrollViewportBehaviour,
                desiredListElementHeight);
        }

        public static SelectedCategoryPromptBehaviour
            GetSelectedCategoryPromptUnder(
            GameObject panelObject,
            float panelWidth, float panelHeight,
            Font font, int fontSize, Color textColor)
        {
            SelectedCategoryPromptBehaviour selectedCategoryPromptBehaviour =
                SelectedCategoryPromptFactory.GetSelectedCategoryPrompt(
                    panelObject, panelWidth, panelHeight, font, fontSize, textColor);
            selectedCategoryPromptBehaviour.transform.SetParent(panelObject.transform, false);
            return selectedCategoryPromptBehaviour;
        }

        public static CategoriesSwitchPromptsContainerBehaviour
            GetCategoryChangingPromptsUnder(
            Font font,
            int fontSize,
            int categoryChangeKeyPromptFontSize,
            Color textColor,
            Color categoryChangePromptKeyTextColor,
            GameObject panelObject,
            float referenceResolutionWidth,
            float referenceResolutionHeight,
            float desiredPanelWidth,
            float desiredPanelHeight,
            float optionOscillationSpeed)
        {
            PreviousCategoryPromptBehaviour previousCategoryPromptPanel =
                CategoriesSwitchingElementsFactory
                    .GetPreviousCategoryPromptPanel(
                        font, fontSize, categoryChangeKeyPromptFontSize,
                        textColor, categoryChangePromptKeyTextColor,
                        desiredPanelWidth, desiredPanelHeight,
                        optionOscillationSpeed);
            NextCategoryPromptBehaviour nextCategoryPromptPanel =
                CategoriesSwitchingElementsFactory
                    .GetNextCategoryPromptPanel(
                        font, fontSize, categoryChangeKeyPromptFontSize,
                        textColor, categoryChangePromptKeyTextColor,
                        desiredPanelWidth, desiredPanelHeight,
                        optionOscillationSpeed);

            CategoriesSwitchPromptsContainerBehaviour promptsContainerPanel =
                CategoriesSwitchingElementsFactory
                    .GetCategoriesSwitchPromptsContainerPanel(
                        referenceResolutionWidth, referenceResolutionHeight);

            promptsContainerPanel.previousCategoryPromptPanel =
                previousCategoryPromptPanel;
            promptsContainerPanel.nextCategoryPromptPanel =
                nextCategoryPromptPanel;

            //RectTransform
            //    previousCategoryPromptPanelRectTransform =
            //        previousCategoryPromptPanel.GetComponent<RectTransform>();

            //RectTransform
            //    nextCategoryPromptPanelRectTransform =
            //        nextCategoryPromptPanel.GetComponent<RectTransform>();

            RectTransform prevCatRectTransform =
                previousCategoryPromptPanel.GetComponent<RectTransform>();
            prevCatRectTransform.anchorMin = new Vector2(0f, 0.5f);
            prevCatRectTransform.anchorMax = new Vector2(0f, 0.5f);
            prevCatRectTransform.pivot = new Vector2(0f, 0.5f);

            RectTransform nextCatRectTransform =
                nextCategoryPromptPanel.GetComponent<RectTransform>();
            nextCatRectTransform.anchorMin = new Vector2(1f, 0.5f);
            nextCatRectTransform.anchorMax = new Vector2(1f, 0.5f);
            nextCatRectTransform.pivot = new Vector2(1f, 0.5f);

            previousCategoryPromptPanel.transform.SetParent(
                promptsContainerPanel.transform, false);
            nextCategoryPromptPanel.transform.SetParent(
                promptsContainerPanel.transform, false);

            promptsContainerPanel.transform.SetParent(panelObject.transform, false);

            //RectTransform promptsContainerPanelRectTransform =
            //    promptsContainerPanel.GetComponent<RectTransform>();

            //RectTransform panelObjectRectTransform = 
            //    panelObject.GetComponent<RectTransform>();

            //throw new NotImplementedException();

            return promptsContainerPanel;
        }
    }
}
