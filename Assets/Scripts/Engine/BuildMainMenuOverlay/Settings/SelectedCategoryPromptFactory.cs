﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.Categories;
using UnityEngine;

namespace Assets.Scripts.Engine.BuildMainMenuOverlay.Settings
{
    public static class SelectedCategoryPromptFactory
    {
        public static SelectedCategoryPromptBehaviour
            GetSelectedCategoryPrompt(
                GameObject panelObject,
                float panelWidth,
                float panelHeight,
                Font font,
                int fontSize,
                Color textColor)
        {
            GameObject selectedCategoryPromptPanel =
                new GameObject(
                    SettingsMenuSectionParts.selectedCategoryPromptPanel,
                    typeof(RectTransform));
            SelectedCategoryPromptBehaviour behaviour =
                selectedCategoryPromptPanel
                    .AddComponent<SelectedCategoryPromptBehaviour>();
            RectTransform rectTransform =
                selectedCategoryPromptPanel.GetComponent<RectTransform>();

            rectTransform.anchorMin = new Vector2(0.5f, 0f);
            rectTransform.anchorMax = new Vector2(0.5f, 0f);
            rectTransform.pivot = new Vector2(0.5f, 0f);

            behaviour.ReinitUiState(font, fontSize, textColor, panelWidth, panelHeight);
            behaviour.RepropagatePanel("Category");

            return behaviour;
        }
    }
}
