﻿using Assets.Scripts.Common;
using Assets.Scripts.Engine.LevelEditor.Behaviours;
using System;
using UnityEngine;
using UnityEngine.Rendering;

namespace Assets.Scripts.Engine.LevelEditor.Factories
{
    //public class ScaleHandleFactory
    //{
    //    public static GameObject CreateScaleHandle(
    //        Vector3 position,
    //        Action<EditorHandleHighlightBehaviour> OnMouseEnterCallback,
    //        Action<EditorHandleHighlightBehaviour> OnMouseExitCallback,
    //        Action<EditorHandleHighlightBehaviour, Vector3, HandleAxis> OnAxisDrag,
    //        Vector3 up, Vector3 right, Vector3 forward
    //    )
    //    {
    //        var scaleHandle = new GameObject();
    //        scaleHandle.transform.position = position;
    //        scaleHandle.layer = Layers.levelEditorLayerIndex;

    //        var xAxis = GameObject.CreatePrimitive(PrimitiveType.Cube);

    //        xAxis.transform.SetParent(scaleHandle.transform, false);
    //        xAxis.transform.localScale = new Vector3(10f, 1f, 1f);
    //        xAxis.transform.localPosition = new Vector3(5f, 0f, 0f);
    //        xAxis.layer = Layers.levelEditorLayerIndex;
    //        xAxis.GetComponent<Renderer>().material.color = Color.red;
    //        var xHighlight = xAxis.AddComponent<EditorHandleHighlightBehaviour>();
    //        xAxis.AddComponent<BoxCollider>();

    //        xHighlight.OnMouseEnterCallback = OnMouseEnterCallback;
    //        xHighlight.OnMouseExitCallback = OnMouseExitCallback;
    //        xHighlight.OnAxisDrag = (element, previousCursorPosition) =>
    //            OnAxisDrag.Invoke(element, previousCursorPosition, HandleAxis.X);

    //        // Create a custom game object
    //        var xCone = GameObject.CreatePrimitive(PrimitiveType.Cube);

    //        var xConeHighlight = xCone.AddComponent<EditorHandleHighlightBehaviour>();
    //        xConeHighlight.OnMouseEnterCallback = OnMouseEnterCallback;
    //        xConeHighlight.OnMouseExitCallback = OnMouseExitCallback;

    //        xConeHighlight.OnAxisDrag = (element, previousCursorPosition) => OnAxisDrag.Invoke(element, previousCursorPosition, HandleAxis.X);

    //        xCone.transform.localScale = Vector3.one * 3;
    //        xCone.transform.SetParent(scaleHandle.transform, false);
    //        xCone.layer = Layers.levelEditorLayerIndex;
    //        xCone.transform.localPosition = new Vector3(10f, 0f, 0f);
    //        xCone.transform.rotation = Quaternion.LookRotation(Vector3.right, Vector3.up);

    //        xCone.GetComponent<Renderer>().material.color = Color.red;







    //        var yAxis = GameObject.CreatePrimitive(PrimitiveType.Cube);
    //        yAxis.transform.SetParent(scaleHandle.transform, false);
    //        yAxis.transform.localScale = new Vector3(1f, 10f, 1f);
    //        yAxis.transform.localPosition = new Vector3(0f, 5f, 0f);
    //        yAxis.GetComponent<Renderer>().material.color = Color.green;
    //        yAxis.layer = Layers.levelEditorLayerIndex;
    //        var yHighlight = yAxis.AddComponent<EditorHandleHighlightBehaviour>();
    //        yAxis.AddComponent<BoxCollider>();

    //        yHighlight.OnMouseEnterCallback = OnMouseEnterCallback;
    //        yHighlight.OnMouseExitCallback = OnMouseExitCallback;
    //        yHighlight.OnAxisDrag = (element, previousCursorPosition) => OnAxisDrag.Invoke(element, previousCursorPosition, HandleAxis.Y);

    //        // Create a custom game object
    //        var yCone = GameObject.CreatePrimitive(PrimitiveType.Cube);
    //        yCone.transform.SetParent(scaleHandle.transform, false);
    //        yCone.layer = Layers.levelEditorLayerIndex;
    //        yCone.transform.localPosition = new Vector3(0f, 10f, 0f);

    //        var yConeHighlight = yCone.AddComponent<EditorHandleHighlightBehaviour>();
    //        yConeHighlight.OnMouseEnterCallback = OnMouseEnterCallback;
    //        yConeHighlight.OnMouseExitCallback = OnMouseExitCallback;

    //        yConeHighlight.OnAxisDrag = (element, previousCursorPosition) => OnAxisDrag.Invoke(element, previousCursorPosition, HandleAxis.Y);

    //        // Add Cone component
    //        yCone.transform.localScale = Vector3.one * 3;
    //        // Set default lit material
    //        //ycone.Material = new Material(Shader.Find("Unlit/Color"));

    //        yCone.transform.rotation = Quaternion.LookRotation(Vector3.up, Vector3.forward);

    //        yCone.GetComponent<Renderer>().material.color = Color.green;









    //        var zAxis = GameObject.CreatePrimitive(PrimitiveType.Cube);
    //        zAxis.transform.SetParent(scaleHandle.transform, false);
    //        zAxis.transform.localScale = new Vector3(1f, 1f, 10f);
    //        zAxis.transform.localPosition = new Vector3(0f, 0f, 5f);
    //        zAxis.layer = Layers.levelEditorLayerIndex;
    //        zAxis.GetComponent<Renderer>().material.color = Color.blue;
    //        var zHighlight = zAxis.AddComponent<EditorHandleHighlightBehaviour>();
    //        zAxis.AddComponent<BoxCollider>();

    //        zHighlight.OnMouseEnterCallback = OnMouseEnterCallback;
    //        zHighlight.OnMouseExitCallback = OnMouseExitCallback;
    //        zHighlight.OnAxisDrag = (element, previousCursorPosition) => OnAxisDrag.Invoke(element, previousCursorPosition, HandleAxis.Z);

    //        // Create a custom game object
    //        var zCone = GameObject.CreatePrimitive(PrimitiveType.Cube);
    //        zCone.transform.SetParent(scaleHandle.transform, false);
    //        zCone.transform.localPosition = new Vector3(0f, 0f, 10f);
    //        zCone.layer = Layers.levelEditorLayerIndex;

    //        var zConeHighlight = zCone.AddComponent<EditorHandleHighlightBehaviour>();
    //        zConeHighlight.OnMouseEnterCallback = OnMouseEnterCallback;
    //        zConeHighlight.OnMouseExitCallback = OnMouseExitCallback;

    //        zConeHighlight.OnAxisDrag = (element, previousCursorPosition) => OnAxisDrag.Invoke(element, previousCursorPosition, HandleAxis.Z);


    //        zCone.transform.localScale = Vector3.one * 3;

    //        zCone.transform.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);

    //        zCone.GetComponent<Renderer>().material.color = Color.blue;

    //        scaleHandle.transform.rotation = Quaternion.LookRotation(forward, up);

    //        return scaleHandle;
    //    }
    //}
}
