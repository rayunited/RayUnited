﻿using Assets.Scripts.Common;
using Assets.Scripts.Engine.LevelEditor.Behaviours;
using System;
using UnityEngine;
using UnityEngine.Rendering;

namespace Assets.Scripts.Engine.LevelEditor.Factories
{
    //public class RotationHandleFactory
    //{
    //    public static GameObject CreateRotationHandle(
    //        Vector3 position,
    //        Action<EditorHandleHighlightBehaviour> OnMouseEnterCallback,
    //        Action<EditorHandleHighlightBehaviour> OnMouseExitCallback,
    //        Action<EditorHandleHighlightBehaviour, Vector3, HandleAxis> OnAxisDrag,
    //        Vector3 up, Vector3 right, Vector3 forward
    //    )
    //    {
    //        var rotationHandle = new GameObject();
    //        rotationHandle.transform.position = position;
    //        rotationHandle.layer = Layers.levelEditorLayerIndex;

    //        var xRing = GameObject.CreatePrimitive(PrimitiveType.Cylinder);

    //        xRing.AddComponent<MeshCollider>().convex = true;

    //        xRing.transform.SetParent(rotationHandle.transform, false);
    //        xRing.transform.localScale = new Vector3(10f, 0.1f, 10f);
    //        xRing.transform.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.right);
    //        xRing.transform.localPosition = new Vector3(5f, 0f, 0f);
    //        xRing.layer = Layers.levelEditorLayerIndex;
    //        xRing.GetComponent<Renderer>().material.color = new Color(1f, 0f, 0f, 0.5f);
    //        var xHighlight = xRing.AddComponent<EditorHandleHighlightBehaviour>();

    //        xHighlight.OnMouseEnterCallback = OnMouseEnterCallback;
    //        xHighlight.OnMouseExitCallback = OnMouseExitCallback;
    //        xHighlight.OnAxisDrag = (element, previousCursorPosition) =>
    //            OnAxisDrag.Invoke(element, previousCursorPosition, HandleAxis.X);







    //        var yRing = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
    //        yRing.transform.SetParent(rotationHandle.transform, false);
    //        yRing.transform.localScale = new Vector3(10f, 0.1f, 10f);
    //        yRing.transform.rotation = Quaternion.LookRotation(Vector3.right, Vector3.up);
    //        yRing.transform.localPosition = new Vector3(0f, 5f, 0f);
    //        yRing.GetComponent<Renderer>().material.color = new Color(0f, 1f, 0f, 0.5f);
    //        yRing.layer = Layers.levelEditorLayerIndex;
    //        var yHighlight = yRing.AddComponent<EditorHandleHighlightBehaviour>();
    //        yRing.AddComponent<MeshCollider>().convex = true;

    //        yHighlight.OnMouseEnterCallback = OnMouseEnterCallback;
    //        yHighlight.OnMouseExitCallback = OnMouseExitCallback;
    //        yHighlight.OnAxisDrag = (element, previousCursorPosition) => OnAxisDrag.Invoke(element, previousCursorPosition, HandleAxis.Y);










    //        var zRing = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
    //        zRing.transform.SetParent(rotationHandle.transform, false);
    //        zRing.transform.localScale = new Vector3(10f, 0.1f, 10f);
    //        zRing.transform.rotation = Quaternion.LookRotation(Vector3.up, Vector3.forward);
    //        zRing.transform.localPosition = new Vector3(0f, 0f, 5f);
    //        zRing.layer = Layers.levelEditorLayerIndex;
    //        zRing.GetComponent<Renderer>().material.color = new Color(0f, 0f, 1f, 0.5f);
    //        var zHighlight = zRing.AddComponent<EditorHandleHighlightBehaviour>();
    //        zRing.AddComponent<MeshCollider>().convex = true;

    //        zHighlight.OnMouseEnterCallback = OnMouseEnterCallback;
    //        zHighlight.OnMouseExitCallback = OnMouseExitCallback;
    //        zHighlight.OnAxisDrag = (element, previousCursorPosition) => OnAxisDrag.Invoke(element, previousCursorPosition, HandleAxis.Z);

    //        return rotationHandle;
    //    }
    //}
}
