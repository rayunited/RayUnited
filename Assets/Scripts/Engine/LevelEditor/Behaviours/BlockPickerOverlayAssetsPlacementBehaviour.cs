﻿using Assets.Scripts.Common;
using Assets.Scripts.PlayerMechanics.Camera;
using System;
using UnityEngine;

namespace Assets.Scripts.Engine.LevelEditor
{
    //public class BlockPickerOverlayAssetsPlacementBehaviour : MonoBehaviour
    //{
    //    protected GameObject activeAssetGameObject;

    //    public Action<InSceneEditorAsset> OnAssetPlacedInScene;

    //    public EditorHandleKind editorHandleKind = EditorHandleKind.POSITION;
    //    public Action<EditorHandleKind> OnEditorHandleKindChoice;

    //    private void Start()
    //    {
    //        FindObjectOfType<EditorMenuPropagator>(includeInactive: true).OnMenuFinishedPropagation = () =>
    //        {
    //            var blockPickerOverlay = FindObjectOfType<BlockPickerOverlay>();

    //            blockPickerOverlay.OnAssetElementDragStart = (editorAsset, eventData) =>
    //            {
    //                var newGameObject = Instantiate(editorAsset.gameObject);
    //                this.activeAssetGameObject = newGameObject;

    //                var cameraScreenPointToRay =
    //                   FindObjectOfType<CameraActorHandle>()
    //                   .GetComponent<UnityEngine.Camera>()
    //                   .ScreenPointToRay(UnityEngine.Input.mousePosition);
    //                RaycastHit hitt;
    //                bool hit = Physics.Raycast(cameraScreenPointToRay, out hitt, 100000f, Layers.environmentLayerMask);

    //                if (hit)
    //                {
    //                    this.activeAssetGameObject.transform.position = hitt.point;
    //                }
    //                else
    //                {
    //                    Destroy(this.activeAssetGameObject);
    //                    this.activeAssetGameObject = null;
    //                }
    //            };

    //            blockPickerOverlay.OnAssetElementDrag = (editorAsset, eventData) =>
    //            {
    //                var cameraScreenPointToRay =
    //                   FindObjectOfType<CameraActorHandle>()
    //                   .GetComponent<UnityEngine.Camera>()
    //                   .ScreenPointToRay(UnityEngine.Input.mousePosition);
    //                RaycastHit hitt;
    //                bool hit = Physics.Raycast(cameraScreenPointToRay, out hitt, 100000f, Layers.environmentLayerMask);

    //                if (hit && this.activeAssetGameObject != null)
    //                {
    //                    this.activeAssetGameObject.transform.position = hitt.point;
    //                }
    //            };

    //            blockPickerOverlay.OnAssetDiscard = (editorAsset, eventData) =>
    //            {
    //                if (this.activeAssetGameObject != null)
    //                {
    //                    Destroy(this.activeAssetGameObject);
    //                    this.activeAssetGameObject = null;
    //                }
    //            };

    //            blockPickerOverlay.OnAssetPlacement = (editorAsset, eventData) =>
    //            {
    //                if (this.activeAssetGameObject != null)
    //                {
    //                    this.activeAssetGameObject.AddComponent<MeshCollider>();
    //                    this.activeAssetGameObject.layer = Layers.environmentLayerIndex;
    //                    this.activeAssetGameObject.AddComponent<InSceneEditorAsset>();
    //                    this.OnAssetPlacedInScene?.Invoke(this.activeAssetGameObject.GetComponent<InSceneEditorAsset>());
    //                    this.activeAssetGameObject = null;
    //                }
    //            };

    //            var editorToolboxVerticalBar = FindObjectOfType<EditorToolboxVerticalBar>();

    //            editorToolboxVerticalBar.OnEditorHandleKindChoice = (editorHandleKind) =>
    //            {
    //                this.editorHandleKind = editorHandleKind;
    //                this.OnEditorHandleKindChoice?.Invoke(editorHandleKind);
    //            };
    //        };
    //    }
    //}
}
