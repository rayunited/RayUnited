﻿using Assets.Scripts.Common;
using Assets.Scripts.Engine.LevelEditor.Factories;
using Assets.Scripts.HUD.EditorGUI;
using Assets.Scripts.PlayerMechanics.Camera;
using Assets.Scripts.Utils;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Assets.Scripts.Engine.LevelEditor.Behaviours
{
    //public class InSceneAssetSelectionBehaviour : MonoBehaviour
    //{
    //    protected List<InSceneEditorAsset> inSceneEditorAssets = new List<InSceneEditorAsset>();
    //    protected InSceneEditorAsset selectedAsset;

    //    protected GameObject positionHandle;
    //    protected GameObject rotationHandle;
    //    protected GameObject scaleHandle;

    //    protected bool isMouseInsideAsset = false;

    //    protected bool mouseDownForUnselection = false;

    //    //protected BlockPickerOverlayAssetsPlacementBehaviour placementBehaviour;

    //    private void LateUpdate()
    //    {
    //        if (!this.isMouseInsideAsset && UnityEngine.Input.GetMouseButtonDown(0) && this.selectedAsset != null)
    //        {
    //            this.selectedAsset.GetComponent<Renderer>().material.color = this.selectedAsset.baseBaseColor;
    //            this.selectedAsset = null;
    //            Destroy(this.positionHandle);
    //            Destroy(this.rotationHandle);
    //            Destroy(this.scaleHandle);
    //            this.positionHandle = null;
    //            this.mouseDownForUnselection = true;
    //        } else if (!UnityEngine.Input.GetMouseButtonDown(0))
    //        {
    //            this.mouseDownForUnselection = false;
    //        }

    //        if (this.selectedAsset != null && UnityEngine.Input.GetKey(KeyCode.Delete))
    //        {
    //            this.inSceneEditorAssets.Remove(this.selectedAsset);
    //            Destroy(this.selectedAsset.gameObject); 
    //            Destroy(this.positionHandle);
    //            Destroy(this.rotationHandle);
    //            Destroy(this.scaleHandle);
    //            this.selectedAsset = null;
    //        }
    //    }

    //    private void Update()
    //    {
    //        if (this.positionHandle != null && this.selectedAsset != null)
    //        {
    //            var camera = FindObjectOfType<CameraActorHandle>()
    //                .GetComponent<UnityEngine.Camera>();

    //            Ray screenPositionRay =
    //                camera.ScreenPointToRay(camera.WorldToScreenPoint(this.selectedAsset.transform.position));

    //            this.positionHandle.transform.position = screenPositionRay.GetPoint(30);
    //        }

    //        if (this.rotationHandle != null && this.selectedAsset != null)
    //        {
    //            var camera = FindObjectOfType<CameraActorHandle>()
    //                .GetComponent<UnityEngine.Camera>();

    //            Ray screenPositionRay =
    //                camera.ScreenPointToRay(camera.WorldToScreenPoint(this.selectedAsset.transform.position));

    //            this.rotationHandle.transform.position = screenPositionRay.GetPoint(30);
    //        }

    //        if (this.scaleHandle != null && this.selectedAsset != null)
    //        {
    //            var camera = FindObjectOfType<CameraActorHandle>()
    //                .GetComponent<UnityEngine.Camera>();

    //            Ray screenPositionRay =
    //                camera.ScreenPointToRay(camera.WorldToScreenPoint(this.selectedAsset.transform.position));

    //            this.scaleHandle.transform.position = screenPositionRay.GetPoint(30);
    //        }
    //    }

    //    private void Start()
    //    {
    //        //this.placementBehaviour = FindObjectOfType<BlockPickerOverlayAssetsPlacementBehaviour>();
    //        //this.placementBehaviour.OnAssetPlacedInScene = (inSceneEditorAsset) => {
    //        //    this.inSceneEditorAssets.Add(inSceneEditorAsset);
    //        //    inSceneEditorAsset.OnAssetFocus = (asset) =>
    //        //    {
    //        //        this.isMouseInsideAsset = true;
    //        //        if (this.selectedAsset != asset)
    //        //        {
    //        //            asset.GetComponent<Renderer>().material.color = Color.green;
    //        //        }
    //        //        Cursor.SetCursor(FindObjectOfType<CursorsHolder>().focusCursor, new Vector2(0, 0), CursorMode.ForceSoftware);
    //        //    };

    //        //    inSceneEditorAsset.OnAssetUnfocus = (asset) =>
    //        //    {
    //        //        if (!UnityEngine.Input.GetMouseButtonDown(0))
    //        //        {
    //        //            this.isMouseInsideAsset = false;
    //        //        }
                    
    //        //        if (this.selectedAsset != asset)
    //        //        {
    //        //            asset.GetComponent<Renderer>().material.color = asset.baseBaseColor;
    //        //        }
    //        //        Cursor.SetCursor(FindObjectOfType<CursorsHolder>().primaryCursor, new Vector2(0, 0), CursorMode.ForceSoftware);
    //        //    };

    //        //    inSceneEditorAsset.OnAssetClick = (asset) =>
    //        //    {
    //        //        this.isMouseInsideAsset = true;
    //        //        this.selectedAsset = asset;
    //        //        asset.GetComponent<Renderer>().material.color = Color.magenta;

    //        //        this.inSceneEditorAssets.ForEach(x => x.OnAssetUnfocus.Invoke(x));

    //        //        if (this.placementBehaviour.editorHandleKind == EditorMenuOverlay.Components.EditorHandleKind.POSITION)
    //        //        {
    //        //            if (this.rotationHandle != null)
    //        //            {
    //        //                Destroy(this.rotationHandle);
    //        //                this.rotationHandle = null;
    //        //            }
    //        //            if (this.scaleHandle != null)
    //        //            {
    //        //                Destroy(this.scaleHandle);
    //        //                this.scaleHandle = null;
    //        //            }

    //        //            if (this.positionHandle == null)
    //        //            {
    //        //                this.positionHandle = PositionHandleFactory.CreatePositionHandle(
    //        //                    position: asset.transform.position,
    //        //                    OnMouseEnterCallback: (element) =>
    //        //                    {
    //        //                        this.isMouseInsideAsset = true;
    //        //                    },
    //        //                    OnMouseExitCallback: (element) =>
    //        //                    {
    //        //                        this.isMouseInsideAsset = false;
    //        //                    },
    //        //                    OnAxisDrag: (element, previousCursorPosition, axis) =>
    //        //                    {
    //        //                        this.isMouseInsideAsset = true;
    //        //                        var camera = FindObjectOfType<CameraActorHandle>()
    //        //                                .GetComponent<UnityEngine.Camera>();

    //        //                        Ray worldPositionStartRay =
    //        //                            camera.ScreenPointToRay(previousCursorPosition);
    //        //                        Ray worldPositionEndRay =
    //        //                            camera.ScreenPointToRay(UnityEngine.Input.mousePosition);

    //        //                        float distance = Vector3.Distance(camera.transform.position, asset.transform.position);

    //        //                        var direction = Vector3.zero;
    //        //                        if (axis == HandleAxis.X)
    //        //                        {
    //        //                            direction = asset.transform.right;
    //        //                        }
    //        //                        else if (axis == HandleAxis.Y)
    //        //                        {
    //        //                            direction = asset.transform.up;
    //        //                        }
    //        //                        else
    //        //                        {
    //        //                            direction = asset.transform.forward;
    //        //                        }

    //        //                        Vector3 assetScreenPosition = camera.WorldToScreenPoint(asset.transform.position);

    //        //                        Vector3 worldTranslation = worldPositionEndRay.GetPoint(distance) - worldPositionStartRay.GetPoint(distance);
    //        //                        worldTranslation = Vector3ProjectionHelper.ProjectConsideringDirection(worldTranslation, direction).normalized * (UnityEngine.Input.mousePosition - previousCursorPosition).magnitude * 0.075f;

    //        //                        var directionRay = new Ray(asset.transform.position, direction);

    //        //                        Vector3 axisProjectedTranslation = Vector3ProjectionHelper.ProjectConsideringDirection(
    //        //                            worldTranslation, direction);

    //        //                        this.positionHandle.transform.position += axisProjectedTranslation;
    //        //                        this.selectedAsset.transform.position += axisProjectedTranslation;
    //        //                    },
    //        //                    up: asset.transform.up,
    //        //                    forward: asset.transform.forward,
    //        //                    right: asset.transform.right
    //        //                );
    //        //            }
    //        //            else
    //        //            {
    //        //                this.positionHandle.transform.position = asset.transform.position;
    //        //            }
    //        //        } else if (this.placementBehaviour.editorHandleKind == EditorMenuOverlay.Components.EditorHandleKind.ROTATION)
    //        //        {
    //        //            if (this.rotationHandle != null)
    //        //            {
    //        //                Destroy(this.rotationHandle);
    //        //                this.rotationHandle = null;
    //        //            }
    //        //            if (this.scaleHandle != null)
    //        //            {
    //        //                Destroy(this.scaleHandle);
    //        //                this.scaleHandle = null;
    //        //            }

    //        //            if (this.rotationHandle == null)
    //        //            {
    //        //                this.rotationHandle = RotationHandleFactory.CreateRotationHandle(
    //        //                    position: asset.transform.position,
    //        //                    OnMouseEnterCallback: (element) =>
    //        //                    {
    //        //                        this.isMouseInsideAsset = true;
    //        //                    },
    //        //                    OnMouseExitCallback: (element) =>
    //        //                    {
    //        //                        this.isMouseInsideAsset = false;
    //        //                    },
    //        //                    OnAxisDrag: (element, previousCursorPosition, axis) =>
    //        //                    {
    //        //                        this.isMouseInsideAsset = true;
    //        //                        var camera = FindObjectOfType<CameraActorHandle>()
    //        //                                .GetComponent<UnityEngine.Camera>();

    //        //                        Ray worldPositionStartRay =
    //        //                            camera.ScreenPointToRay(previousCursorPosition);
    //        //                        Ray worldPositionEndRay =
    //        //                            camera.ScreenPointToRay(UnityEngine.Input.mousePosition);

    //        //                        float distance = Vector3.Distance(camera.transform.position, asset.transform.position);

    //        //                        var direction = Vector3.zero;
    //        //                        if (axis == HandleAxis.X)
    //        //                        {
    //        //                            direction = asset.transform.right;
    //        //                        }
    //        //                        else if (axis == HandleAxis.Y)
    //        //                        {
    //        //                            direction = asset.transform.up;
    //        //                        }
    //        //                        else
    //        //                        {
    //        //                            direction = asset.transform.forward;
    //        //                        }

    //        //                        Vector3 assetScreenPosition = camera.WorldToScreenPoint(asset.transform.position);

    //        //                        Vector3 worldTranslation = worldPositionEndRay.GetPoint(distance) - worldPositionStartRay.GetPoint(distance);
    //        //                        worldTranslation = Vector3ProjectionHelper.ProjectConsideringDirection(worldTranslation, direction).normalized * (UnityEngine.Input.mousePosition - previousCursorPosition).magnitude * 0.075f;

    //        //                        var directionRay = new Ray(asset.transform.position, direction);

    //        //                        Vector3 axisProjectedTranslation = Vector3ProjectionHelper.ProjectConsideringDirection(
    //        //                            worldTranslation, direction);

    //        //                        float angle = axisProjectedTranslation.magnitude * Mathf.Sign(Vector3.Dot(worldTranslation, direction)) * 0.25f;

    //        //                        this.selectedAsset.transform.Rotate(direction, angle);
    //        //                    },
    //        //                    up: asset.transform.up,
    //        //                    forward: asset.transform.forward,
    //        //                    right: asset.transform.right
    //        //                );
    //        //            }
    //        //            else
    //        //            {
    //        //                this.rotationHandle.transform.position = asset.transform.position;
    //        //            }
    //        //        } else if (this.placementBehaviour.editorHandleKind == EditorMenuOverlay.Components.EditorHandleKind.SCALE)
    //        //        {
    //        //            if (this.rotationHandle != null)
    //        //            {
    //        //                Destroy(this.rotationHandle);
    //        //                this.rotationHandle = null;
    //        //            }
    //        //            if (this.positionHandle != null)
    //        //            {
    //        //                Destroy(this.positionHandle);
    //        //                this.positionHandle = null;
    //        //            }

    //        //            if (this.scaleHandle == null)
    //        //            {
    //        //                this.scaleHandle = ScaleHandleFactory.CreateScaleHandle(
    //        //                    position: asset.transform.position,
    //        //                    OnMouseEnterCallback: (element) =>
    //        //                    {
    //        //                        this.isMouseInsideAsset = true;
    //        //                    },
    //        //                    OnMouseExitCallback: (element) =>
    //        //                    {
    //        //                        this.isMouseInsideAsset = false;
    //        //                    },
    //        //                    OnAxisDrag: (element, previousCursorPosition, axis) =>
    //        //                    {
    //        //                        this.isMouseInsideAsset = true;
    //        //                        var camera = FindObjectOfType<CameraActorHandle>()
    //        //                                .GetComponent<UnityEngine.Camera>();

    //        //                        Ray worldPositionStartRay =
    //        //                            camera.ScreenPointToRay(previousCursorPosition);
    //        //                        Ray worldPositionEndRay =
    //        //                            camera.ScreenPointToRay(UnityEngine.Input.mousePosition);

    //        //                        float distance = Vector3.Distance(camera.transform.position, asset.transform.position);

    //        //                        var direction = Vector3.zero;
    //        //                        if (axis == HandleAxis.X)
    //        //                        {
    //        //                            direction = asset.transform.right;
    //        //                        }
    //        //                        else if (axis == HandleAxis.Y)
    //        //                        {
    //        //                            direction = asset.transform.up;
    //        //                        }
    //        //                        else
    //        //                        {
    //        //                            direction = asset.transform.forward;
    //        //                        }

    //        //                        Vector3 assetScreenPosition = camera.WorldToScreenPoint(asset.transform.position);

    //        //                        Vector3 worldTranslation = worldPositionEndRay.GetPoint(distance) - worldPositionStartRay.GetPoint(distance);
    //        //                        worldTranslation = Vector3ProjectionHelper.ProjectConsideringDirection(worldTranslation, direction).normalized * (UnityEngine.Input.mousePosition - previousCursorPosition).magnitude * 0.075f;

    //        //                        var directionRay = new Ray(asset.transform.position, direction);

    //        //                        Vector3 axisProjectedTranslation = Vector3ProjectionHelper.ProjectConsideringDirection(
    //        //                            worldTranslation, direction);

    //        //                        float angle = axisProjectedTranslation.magnitude * Mathf.Sign(Vector3.Dot(worldTranslation, direction)) * 0.25f;

    //        //                        this.selectedAsset.transform.localScale = this.selectedAsset.transform.localScale + axisProjectedTranslation * 0.25f;
    //        //                    },
    //        //                    up: asset.transform.up,
    //        //                    forward: asset.transform.forward,
    //        //                    right: asset.transform.right
    //        //                );
    //        //            }
    //        //            else
    //        //            {
    //        //                this.scaleHandle.transform.position = asset.transform.position;
    //        //            }
    //        //        }
    //        //    };
    //        //};

    //        //this.placementBehaviour.OnEditorHandleKindChoice = (editorHandleKind) =>
    //        //{

    //        //};
    //    }
    //}
}
