﻿using Assets.Scripts.HUD.EditorGUI;
using System;
using UnityEngine;

namespace Assets.Scripts.Engine.LevelEditor.Behaviours
{
    public class EditorHandleHighlightBehaviour : MonoBehaviour
    {
        protected Color baseColor;

        public Action<EditorHandleHighlightBehaviour, Vector3> OnAxisDrag;
        public Action<EditorHandleHighlightBehaviour> OnAxisDragEnd;
        public Action<EditorHandleHighlightBehaviour> OnMouseEnterCallback;
        public Action<EditorHandleHighlightBehaviour> OnMouseExitCallback;

        protected Vector3 previousCursorPosition;

        private void Start()
        {
            this.baseColor = GetComponent<Renderer>().material.color;
            this.previousCursorPosition = UnityEngine.Input.mousePosition;
        }

        private void OnMouseOver()
        {
            GetComponent<Renderer>().material.color = new Color(this.baseColor.r * 2, this.baseColor.g * 2, this.baseColor.b * 2, this.baseColor.a * 2);
            Cursor.SetCursor(FindObjectOfType<CursorsHolder>().focusCursor, new Vector2(), CursorMode.ForceSoftware);
            this.OnMouseEnterCallback?.Invoke(this);
        }

        private void OnMouseExit()
        {
            GetComponent<Renderer>().material.color = this.baseColor;
            Cursor.SetCursor(FindObjectOfType<CursorsHolder>().primaryCursor, new Vector2(), CursorMode.ForceSoftware);
            this.OnMouseExitCallback?.Invoke(this);
        }

        private void OnMouseDrag()
        {
            if ((UnityEngine.Input.mousePosition - this.previousCursorPosition).magnitude > 0.01f)
            {
                this.OnAxisDrag?.Invoke(this, this.previousCursorPosition);
                this.previousCursorPosition = UnityEngine.Input.mousePosition;
            }
        }

        private void OnMouseDown()
        {
            this.previousCursorPosition = UnityEngine.Input.mousePosition;
        }

        private void OnMouseUp()
        {
            this.OnAxisDragEnd?.Invoke(this);
        }
    }
}
