﻿using System;
using UnityEngine;

namespace Assets.Scripts.Engine.LevelEditor
{
    public class InSceneEditorAsset : MonoBehaviour
    {
        public Color baseBaseColor;

        public Action<InSceneEditorAsset> OnAssetFocus;
        public Action<InSceneEditorAsset> OnAssetUnfocus;
        public Action<InSceneEditorAsset> OnAssetClick;

        protected bool mouseIsAlreadyDown = false;

        private void Start()
        {
            this.baseBaseColor = GetComponent<Renderer>().material.color;
        }


        private void OnMouseOver()
        {
            this.OnAssetFocus?.Invoke(this);
            //GetComponent<Renderer>().material.color = Color.green;
            //Cursor.SetCursor(FindObjectOfType<CursorsHolder>().focusCursor, new Vector2(0, 0), CursorMode.ForceSoftware);
        }

        private void OnMouseExit()
        {
            this.OnAssetUnfocus?.Invoke(this);
            //GetComponent<Renderer>().material.color = this.baseBaseColor;
            //Cursor.SetCursor(FindObjectOfType<CursorsHolder>().primaryCursor, new Vector2(0, 0), CursorMode.ForceSoftware);
        }

        private void OnMouseDown()
        {
            if (!this.mouseIsAlreadyDown)
            {
                this.OnAssetClick?.Invoke(this);
            }          
            this.mouseIsAlreadyDown = true;
        }

        private void OnMouseUp()
        {
            this.mouseIsAlreadyDown = false;
        }
    }
}
