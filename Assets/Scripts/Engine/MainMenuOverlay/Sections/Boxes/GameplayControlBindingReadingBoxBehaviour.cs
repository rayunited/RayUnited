﻿using Assets.Scripts.Engine.Input;
using Assets.Scripts.Engine.Input.Bindings;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding;
using System;
using UnityEngine.InputSystem;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Boxes
{
    public abstract class GameplayControlBindingReadingBoxBehaviour : FocusBoxSectionBehaviour
    {
        protected Action<ControlBindingInfo> onApplyNewBindingHandler;
        protected Action onBindingCancelHandler;

        protected string bindingCancelKeyboardEscapeButtonPathPart = "escape";

        protected PlayerInputHub playerInputHub;

        public void SetBindingValueHandler(
            Action<ControlBindingInfo> onApplyNewBindingHandler)
        {
            this.onApplyNewBindingHandler =
                onApplyNewBindingHandler;
        }

        public void SetBindingCancelHandler(
            Action onBindingCancelHandler
            )
        {
            this.onBindingCancelHandler = onBindingCancelHandler;
        }

        protected abstract string GetInputListenerId();
        protected abstract void RegisterPlayerInputHubListener(
            string listenerId, Action<InputAction.CallbackContext> listener);
        protected abstract void UnregisterPlayerInputHubListener(string listenerId);

        protected void OnControlInput(InputAction.CallbackContext context)
        {
            UnregisterAsInputListener();

            if (context.control.path.Contains(
                    this.bindingCancelKeyboardEscapeButtonPathPart))
            {
                this.onBindingCancelHandler();
                return;
            }
            else
            {
                this.onApplyNewBindingHandler(
                    ControlBindingInfo.FromInputSystemInfo(
                        displayName: DeviceInputPathDisplayHelper.GetDisplayBindingName(
                            context.control.path),
                        inputBindingPath: context.control.path
                    ));
            }
        }

        protected void UnregisterAsInputListener()
        {
            if (this.playerInputHub == null)
            {
                throw new InvalidOperationException(
                    "Trying to unregister device input listener though " +
                    "playerInputHub reference is null. " +
                    "Did you forget to first register the listener?");
            }
            UnregisterPlayerInputHubListener(GetInputListenerId());
        }

        public void RegisterAsInputListener()
        {
            this.playerInputHub = FindObjectOfType<GameStateInfo>().GetComponent<PlayerInputHub>();
            RegisterPlayerInputHubListener(GetInputListenerId(), OnControlInput);
        }
    }
}
