﻿using System;
using UnityEngine.InputSystem;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Boxes
{
    public class GamepadGameplayControlBindingReadingFocusBoxBehaviour :
        GameplayControlBindingReadingBoxBehaviour
    {
        protected const string GAMEPAD_BINDING_FOCUS_BOX_GAMEPAD_INPUT_LISTENER_ID =
            "GAMEPAD_BINDING_FOCUS_BOX_GAMEPAD_INPUT_LISTENER_ID";

        protected override string GetInputListenerId()
        {
            return GAMEPAD_BINDING_FOCUS_BOX_GAMEPAD_INPUT_LISTENER_ID;
        }

        protected override void RegisterPlayerInputHubListener(
            string listenerId, Action<InputAction.CallbackContext> listener)
        {
            this.playerInputHub
                .RegisterAnyGameplaySignificantGamepadInputListener(listenerId, listener);
        }

        protected override void UnregisterPlayerInputHubListener(string listenerId)
        {
            this.playerInputHub
                .UnregisterAnyGameplaySignificantGamepadInputListener(listenerId);
        }
    }
}
