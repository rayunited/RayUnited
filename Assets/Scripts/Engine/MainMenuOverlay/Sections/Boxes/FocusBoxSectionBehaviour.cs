﻿using Assets.Scripts.Engine.BuildMainMenuOverlay;
using Assets.Scripts.Engine.BuildMainMenuOverlay.Ui;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Boxes
{
    public class FocusBoxSectionBehaviour : MonoBehaviour
    {
        public string focusBoxId { get; protected set; }

        protected int baseFontSize;
        protected Text messageText;
        protected Image focusBoxBackgroundImage;

        protected GameObject focusBoxesContainer;

        protected float messageAndButtonsSpacing;
        protected float messageMargin;
        protected Font font;
        protected Color textColor;
        protected Color backgroundColor;

        public void ResetState()
        {

        }

        public void SetMessage(string message)
        {
            RepropagateFocusBox(message);
        }

        public void ReinitUiState(
            string focusBoxId,
            GameObject focusBoxesContainer,
            float messageMargin,
            int baseFontSize,
            Font font,
            Color textColor,
            Color backgroundColor)
        {
            this.focusBoxId = focusBoxId;
            this.baseFontSize = baseFontSize;
            this.focusBoxesContainer = focusBoxesContainer;
            this.messageMargin = messageMargin;
            this.font = font;
            this.textColor = textColor;
            this.backgroundColor = backgroundColor;
        }

        public void RepropagateFocusBox(string message)
        {
            foreach (Transform child in this.transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            Text messageText = UnityUiTextFactory.GetCenteredTextLabel(
                FocusBoxParts.message, this.font, this.baseFontSize, message, this.textColor);
            float preferredBoxWidthForMessage =
                TextLabelDimensionHelper.GetPreferredWidthFor(messageText);
            float preferredBoxHeightForMessage =
                TextLabelDimensionHelper.GetPreferredHeightFor(messageText);

            Image focusBoxBackgroundImage =
                UnityUiImageFactory.GetSimpleImagePanel(
                    FocusBoxParts.focusBoxBackgroundBox, this.backgroundColor,
                    preferredBoxWidthForMessage + 2 * this.messageMargin,
                    preferredBoxHeightForMessage
                    + 2 * this.messageMargin);

            focusBoxBackgroundImage.gameObject
                .AddComponent<VerticalLayoutGroup>();

            messageText.transform.SetParent(
                focusBoxBackgroundImage.transform, worldPositionStays: false);

            this.messageText = messageText;

            this.focusBoxBackgroundImage = focusBoxBackgroundImage;

            focusBoxBackgroundImage.transform.SetParent(
                this.gameObject.transform, worldPositionStays: false);
        }
    }
}
