﻿using System;
using UnityEngine.InputSystem;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Boxes
{
    public class KeyboardMouseGameplayControlBindingReadingFocusBoxBehaviour
        : GameplayControlBindingReadingBoxBehaviour
    {
        protected const string MOUSE_KEYBOARD_BINDING_FOCUS_BOX_KEYBOARD_INPUT_LISTENER_ID =
            "MOUSE_KEYBOARD_BINDING_FOCUS_BOX_KEYBOARD_INPUT_LISTENER_ID";

        protected override string GetInputListenerId()
        {
            return MOUSE_KEYBOARD_BINDING_FOCUS_BOX_KEYBOARD_INPUT_LISTENER_ID;
        }

        protected override void RegisterPlayerInputHubListener(
            string listenerId, Action<InputAction.CallbackContext> listener)
        {
            this.playerInputHub.RegisterAnyMouseKeyboardInputListener(listenerId, listener);
        }

        protected override void UnregisterPlayerInputHubListener(string listenerId)
        {
            this.playerInputHub.UnregisterAnyMouseKeyboardInputListener(listenerId);
        }
    }
}
