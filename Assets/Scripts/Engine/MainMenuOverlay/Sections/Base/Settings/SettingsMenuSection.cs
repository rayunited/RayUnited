﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Base.Settings;
using System.Collections.Generic;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Base
{
    public static class SettingsMenuSectionDialogs
    {
        public const string SETTING_CONTROL_VALUE_APPLY_DIALOG =
            "SETTING_CONTROL_VALUE_APPLY_DIALOG";
    }

    public class ConstructedSettingsCategoryInfo : SettingsCategoryInfo
    {
        public List<SettingControlInfo> injectedSettingControlsInfo;

        public ConstructedSettingsCategoryInfo(
            string categoryName,
            SettingsSingleCategoryUiInfo settingsSingleCategoryUiInfo,
            List<SettingControlInfo> injectedSettingControlsInfo) :
            base(categoryName, settingsSingleCategoryUiInfo)
        {
            this.injectedSettingControlsInfo = injectedSettingControlsInfo;
        }

        protected override List<SettingControlInfo> GetSettingsControls()
        {
            return this.injectedSettingControlsInfo;
        }
    }

    public abstract class SettingsMenuSection : SettingsMenuSectionBase
    {
        protected abstract List<SettingControlInfo> GetSettingsControls();

        protected abstract string GetSettingsCategoryName();

        protected override List<SettingsCategoryInfo> GetSettingsCategories()
        {
            List<SettingControlInfo> settingControlInfos =
                GetSettingsControls();

            return new List<SettingsCategoryInfo>()
            {
                new ConstructedSettingsCategoryInfo(
                    GetSettingsCategoryName(),
                    this.settingsSingleCategoryUiInfo,
                    settingControlInfos)
            };
        }
    }
}
