﻿using Assets.Scripts.Engine.MainMenuOverlay.Sections.Dialogs;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Base
{
    public class MenuDialogDefinition
    {
        public string message;
        public string confirmCaption;
        public string rejectCaption;
        public string dialogId;

        public Color backgroundColor;
        public Color textColor;
        public Font font;
        public int fontSize;

        public DialogClass dialogClass;

        public MenuDialogDefinition(
            string message, string confirmCaption, string rejectCaption,
            string dialogId,
            DialogClass dialogClass,
            Color backgroundColor, Color textColor, Font font, int fontSize)
        {
            this.message = message;
            this.confirmCaption = confirmCaption;
            this.rejectCaption = rejectCaption;
            this.dialogId = dialogId;

            this.dialogClass = dialogClass;

            this.backgroundColor = backgroundColor;
            this.textColor = textColor;
            this.font = font;
            this.fontSize = fontSize;
        }
    }
}
