﻿using Assets.Scripts.Engine.MainMenuOverlay.Sections.Boxes;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Base
{
    public class MenuFocusBoxDefinition
    {
        public string message;
        public string focusBoxId;

        public Color backgroundColor;
        public Color textColor;
        public Font font;
        public int fontSize;

        public FocusBoxClass focusBoxClass;

        public MenuFocusBoxDefinition(
            string message,
            string focusBoxId,
            FocusBoxClass focusBoxClass,
            Color backgroundColor, Color textColor, Font font,
            int fontSize)
        {
            this.message = message;
            this.focusBoxId = focusBoxId;

            this.focusBoxClass = focusBoxClass;

            this.backgroundColor = backgroundColor;
            this.textColor = textColor;
            this.font = font;
            this.fontSize = fontSize;
        }
    }
}
