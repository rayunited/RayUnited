﻿using Assets.Scripts.Engine.BuildMainMenuOverlay;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Base
{
    public class SimpleMenuOptionInfo
    {
        public string caption;
        public int optionValue;

        public SimpleMenuOptionInfo(
            string caption, int optionValue)
        {
            this.caption = caption;
            this.optionValue = optionValue;
        }
    }

    public abstract class SimpleOptionsMenuSection : MenuSectionBehaviour
    {
        protected List<Text> optionsLabelsObjects =
            new List<Text>();
        protected List<SimpleMenuOptionInfo> options =
            new List<SimpleMenuOptionInfo>();

        protected int selectedOptionIndex = 0;
        protected int selectedOptionValue = -1;

        protected float selectedOptionOscillationSpeed = 6f;

        protected static T PropagateSimpleOptionsMenuPanel<T>(
            GameObject menuCanvasObject, Vector2 referenceResolution,
            string panelObjectName, Color backgroundColor) where T : SimpleOptionsMenuSection
        {
            Func<GameObject> panelObjectProvider = () => MenuPanelFactory
                    .CreatePanelObjectWithBoxBackgroundSizeReferenceResolution(
                        menuCanvasObject, referenceResolution,
                        panelObjectName,
                        0.333333f, 0.6f, backgroundColor);
            return PropagateMenuPanel<T>(menuCanvasObject, referenceResolution,
                panelObjectName, panelObjectProvider, backgroundColor);
        }

        public override void FirstInitState(Color backgroundColor)
        {
            base.FirstInitState(backgroundColor);
            this.optionsLabelsObjects =
                new List<Text>();
        }

        public override void ReinitState()
        {
            base.ReinitState();
            this.selectedOptionIndex = 0;
            this.selectedOptionValue = this.options[0].optionValue;
            this.optionsLabelsObjects.ForEach(x => x.rectTransform.localScale = Vector2.one);
        }

        protected override void PropagatePanel(GameObject panelObject)
        {
            this.options = GetOptions();
            GameObject boxBackgroundObject = MenuPanelFactory
                .GetBackgroundBoxObject(panelObject);
            this.sectionContainer = boxBackgroundObject;
            boxBackgroundObject.AddComponent<VerticalLayoutGroup>();
            foreach (SimpleMenuOptionInfo opt in this.options)
            {
                this.optionsLabelsObjects.Add(SimpleOptionsMenuSectionBuildingHelper
                    .AddSimpleMenuLabelOptionAdjustingBackgroundBox(
                        boxBackgroundObject, opt, this.font, this.textColor, this.baseFontSize));
            }
            this.selectedOptionIndex = 0;
            this.selectedOptionValue = this.options[0].optionValue;
        }

        protected abstract List<SimpleMenuOptionInfo> GetOptions();

        //public override void SelectOptionLeft()
        //{
        //    if (currentActiveDialog != null)
        //    {
        //        currentActiveDialog.SelectOptionLeft();
        //    }
        //}

        //public override void SelectOptionRight()
        //{
        //    if (currentActiveDialog != null)
        //    {
        //        currentActiveDialog.SelectOptionRight();
        //    }
        //}

        private void SelectOptionDownHandler(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            this.optionsLabelsObjects[this.selectedOptionIndex]
                .rectTransform.localScale = new Vector3(1f, 1f);
            //if (currentActiveDialog == null)
            //{
            if (this.selectedOptionIndex < this.optionsLabelsObjects.Count() - 1)
            {
                this.selectedOptionIndex++;
            }
            else
            {
                this.selectedOptionIndex = 0;
            }
            this.selectedOptionValue = this.options[this.selectedOptionIndex].optionValue;
            //}
        }

        private void SelectOptionUpHandler(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            //if (currentActiveDialog == null)
            //{
            this.optionsLabelsObjects[this.selectedOptionIndex]
                .rectTransform.localScale = new Vector3(1f, 1f);

            if (this.selectedOptionIndex > 0)
            {
                this.selectedOptionIndex--;
            }
            else
            {
                this.selectedOptionIndex = this.optionsLabelsObjects.Count() - 1;
            }

            this.selectedOptionValue = this.options[this.selectedOptionIndex].optionValue;
            //}
        }

        public override bool SelectOptionDown(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (base.SelectOptionDown(gameplayMenuBehaviour))
            {
                SelectOptionDownHandler(gameplayMenuBehaviour);
                return true;
            }
            return false;
        }

        public override bool SelectOptionUp(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (base.SelectOptionUp(gameplayMenuBehaviour))
            {
                SelectOptionUpHandler(gameplayMenuBehaviour);
                return true;
            }
            return false;
        }

        protected override void Update()
        {
            base.Update();
            this.optionsLabelsObjects[this.selectedOptionIndex].rectTransform.localScale =
                new Vector3(1f, 1f) * (float)(1f * MathHelper.SinusInRange(1.25f, 1.5f,
                    this.timeProgress * this.selectedOptionOscillationSpeed));
        }

        //public override void Draw()
        //{
        //    if (currentActiveDialog == null)
        //    {
        //        simpleOptionsMenuSectionUi
        //            .Draw(selectedOptionIndex, options);
        //    } else
        //    {
        //        currentActiveDialog.Draw();
        //    }            
        //}

        //protected override void InitState()
        //{
        //    selectedOptionIndex = 0;
        //    options = GetOptions();
        //    selectedOptionValue = options[0].optionValue;
        //    currentActiveDialog = null;
        //}
    }
}
