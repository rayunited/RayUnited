﻿using UnityEngine.SceneManagement;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Dialogs
{
    public class MainMenuDialogBehaviour : DialogSectionBehaviour
    {
        protected const int MAIN_MENU_SCENE_BUILD_INDEX = 0;

        protected override void OnDialogConfirm(GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            SceneManager.LoadScene(MAIN_MENU_SCENE_BUILD_INDEX);
        }
    }
}
