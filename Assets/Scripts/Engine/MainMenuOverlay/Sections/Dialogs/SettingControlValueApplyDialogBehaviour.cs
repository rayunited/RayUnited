﻿using System;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Dialogs
{
    public class SettingControlValueApplyDialogBehaviour : DialogSectionBehaviour
    {
        private Action<GameplayMenuBehaviour> confirmHandler;
        private Action<GameplayMenuBehaviour> rejectHandler;

        public void SetConfirmHandler(Action<GameplayMenuBehaviour> confirmHandler)
        {
            this.confirmHandler = confirmHandler;
        }

        public void SetRejectHandler(
            Action<GameplayMenuBehaviour> rejectHandler)
        {
            this.rejectHandler = rejectHandler;
        }

        public override void OnExitSection(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            gameplayMenuBehaviour.SetActiveDialog(null);
            OnDialogCancel(gameplayMenuBehaviour);
        }

        protected override void OnDialogConfirm(GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (this.confirmHandler != null)
            {
                this.confirmHandler(gameplayMenuBehaviour);
            }
            else
            {
                throw new InvalidOperationException(
                    "Confirm handler in this dialog is null!");
            }
        }

        protected override void OnDialogCancel(GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (this.rejectHandler != null)
            {
                this.rejectHandler(gameplayMenuBehaviour);
            }
            else
            {
                throw new InvalidOperationException(
                    "Reject handler in this dialog is null!");
            }
        }

        public void SetLabels(
            string message,
            string confirmLabel,
            string rejectLabel)
        {
            RepropagateDialog(message, confirmLabel, rejectLabel);
        }
    }
}
