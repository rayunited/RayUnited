﻿using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Dialogs
{
    public class ExitGameDialogBehaviour : DialogSectionBehaviour
    {
        protected override void OnDialogCancel(GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            gameplayMenuBehaviour.ClearActiveDialog();
        }

        protected override void OnDialogConfirm(GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            Application.Quit();
        }
    }
}
