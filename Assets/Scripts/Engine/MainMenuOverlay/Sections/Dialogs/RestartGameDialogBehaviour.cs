﻿using UnityEngine.SceneManagement;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Dialogs
{
    public class RestartGameDialogBehaviour : DialogSectionBehaviour
    {
        protected override void OnDialogConfirm(GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
