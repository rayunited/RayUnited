﻿namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Dialogs
{
    public enum DialogClass
    {
        EXIT_GAME_DIALOG,
        RESTART_SCENE_DIALOG,
        MAIN_MENU_DIALOG,

        SETTING_CONTROL_VALUE_APPLY_DIALOG
    }
}
