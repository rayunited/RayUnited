﻿namespace Assets.Scripts.Engine.MainMenuOverlay.Sections
{
    public enum MenuMainSectionEnum
    {
        SETTINGS_CHOICE_SECTION,
        MAIN_MENU_SECTION,
        SOUNDS_SETTINGS_SECTION,
        GRAPHICS_SETTINGS_SECTION,
        CONTROLS_SETTINGS_SECTION
    }
}
