﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Graphics;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Graphics.QualityLevel;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Base.Settings;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Settings
{
    //public class GraphicsSettingsSection : SettingsMenuSection
    public static class GraphicsSettingsSectionControls
    {
        public const string RESOLUTION_CONTROL = "RESOLUTION_CONTROL";
        public const string SCREEN_MODE_CONTROL = "SCREEN_MODE_CONTROL";
        public const string VSYNC_COUNT_CONTROL = "VSYNC_COUNT_CONTROL";
        public const string TARGET_FRAMERATE_CONTROL = "TARGET_FRAMERATE_CONTROL";
        public const string QUALITY_LEVEL_CONTROL = "QUALITY_LEVEL_CONTROL";
    }

    public class GraphicsSettingsSection : SettingsMenuSectionBase
    {
        public static GraphicsSettingsSection
            PropagatePanelUnderMenuCanvas(
                GameObject menuCanvasObject, Vector2 referenceResolution,
                Color backgroundColor)
        {
            return PropagateSettingsSectionMenuPanel<GraphicsSettingsSection>(
                menuCanvasObject, referenceResolution, "GraphicsSettingsSectionPanel",
                backgroundColor);
        }

        protected override bool ExitSectionHandler(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (base.ExitSectionHandler(gameplayMenuBehaviour))
            {
                gameplayMenuBehaviour
                    .SetActiveMenuMainSection(
                        MenuMainSectionEnum.SETTINGS_CHOICE_SECTION);
                return true;
            }
            else
            {
                return false;
            }
        }

        protected override List<SettingsCategoryInfo> GetSettingsCategories()
        {
            return new List<SettingsCategoryInfo>()
            {
                new ConstructedSettingsCategoryInfo(
                    "Display",
                    this.settingsSingleCategoryUiInfo,
                    new List<SettingControlInfo>()
                    {
                        new ScreenResolutionSettingControlInfo(
                            "Resolution", GraphicsSettingsSectionControls.RESOLUTION_CONTROL),
                        new FullscreenSettingControlInfo(
                            "Screen mode", GraphicsSettingsSectionControls.SCREEN_MODE_CONTROL),
                        new VSyncCountSettingControlInfo("VSync count",
                            "The number of vertical syncs that should pass between each frame.",
                            GraphicsSettingsSectionControls.VSYNC_COUNT_CONTROL),
                        new TargetFramerateSettingControlInfo("Target framerate",
                            "Applicable only when VSync count is OFF",
                            GraphicsSettingsSectionControls.TARGET_FRAMERATE_CONTROL,
                            10, 300),
                        new QualityLevelSettingControlInfo("Quality",
                            "Graphics quality level - low/medium/high",
                            GraphicsSettingsSectionControls.QUALITY_LEVEL_CONTROL),
                    })
            };
        }
    }
}
