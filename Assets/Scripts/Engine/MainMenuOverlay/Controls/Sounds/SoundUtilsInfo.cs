public class SoundUtilsInfo
{
    public const string SOUND_ENABLED =
        "SOUND_ENABLED";

    public const string MUSIC_VOLUME_PLAYER_PREFS_KEY =
        "MUSIC_VOLUME_PLAYER_PREFS_KEY";

    public const int DEFAULT_MUSIC_VOLUME = 50;
}
