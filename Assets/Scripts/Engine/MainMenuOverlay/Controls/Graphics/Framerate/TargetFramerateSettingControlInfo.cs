﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.IntRange;
using Assets.Scripts.ScenePreparation;
using Assets.Scripts.Settings;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Graphics
{
    public static class TargetFramerateSettingControlSpecialValues
    {
        public static List<AdditionalIntValueInfo> GetAdditionalValues()
        {
            return new List<AdditionalIntValueInfo>()
            {
                new AdditionalIntValueInfo("DEFAULT", -1)
            };
        }
    }

    public class TargetFramerateSettingControlInfo : ConfirmableIntFromRangeValueControl
    {
        public TargetFramerateSettingControlInfo(
            string label, string description, string controlId, int rangeStart, int rangeEnd) :
            base(label, description, controlId, rangeStart, rangeEnd,
                TargetFramerateSettingControlSpecialValues.GetAdditionalValues())
        { }

        //public override void ApplyOptionValue()
        //{
        //    Application.targetFrameRate = currentValue;
        //}

        public override void InitStateValue()
        {
            //base.InitState();
            this.currentValue = Application.targetFrameRate;
            UpdateTextValueToCurrentNumberValue();
        }

        protected override void ConfirmedApplySelectedValue()
        {
            PlayerPrefs.SetInt(
                PlayerPreferences.TARGET_FRAMERATE_PLAYER_PREFS_KEY, this.currentValue);
            TargetFramerateApplier.ApplyTargetFramerate();
        }

        protected override Tuple<string, string, string>
            GetOptionApplyConfirmDialogTexts()
        {
            return Tuple.Create(
                "You've changed target framerate setting value.\nDo you want to apply it?",
                "Yes", "No");
        }
    }
}
