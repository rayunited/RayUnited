﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.SelectValue;
using Assets.Scripts.ScenePreparation;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Graphics.QualityLevel
{
    public static class QualityLevels
    {
        public const int lowQualityLevelIndex = 0;
        public const int mediumQualityLevelIndex = 1;
        public const int highQualityLevelIndex = 2;
    }

    public class QualityLevelSettingControlInfo : ConfirmableSelectValueControl
    {
        public QualityLevelSettingControlInfo(
            string label, string description, string controlId) :
            base(label, controlId, description)
        { }

        public override void InitStateValue()
        {
            this.selectedOptionValue = QualitySettings.GetQualityLevel();
            this.selectedOptionIndex = this.optionValues.FindIndex(x =>
            {
                return this.selectedOptionValue == x.value;
            }); ;
            SetValueText(this.optionValues[this.selectedOptionIndex].label);
        }

        protected override void ConfirmedApplySelectedOption()
        {
            QualitySettings.SetQualityLevel(this.selectedOptionValue);
            GraphicsQualityApplier.ApplyCurrentGraphicsEngineQualitySettings();
        }

        protected override Tuple<string, string, string> GetOptionApplyConfirmDialogTexts()
        {
            return Tuple.Create(
               "You've changed Quality level setting value.\nDo you want to apply it?",
               "Yes", "No");
        }

        protected override List<ValueSelectionInfo> GetOptionValues()
        {
            return new List<ValueSelectionInfo>()
            {
                new ValueSelectionInfo(
                    "Low", QualityLevels.lowQualityLevelIndex, null),
                new ValueSelectionInfo(
                    "Medium", QualityLevels.mediumQualityLevelIndex, null),
                new ValueSelectionInfo(
                    "High", QualityLevels.highQualityLevelIndex, null)
            };
        }
    }
}
