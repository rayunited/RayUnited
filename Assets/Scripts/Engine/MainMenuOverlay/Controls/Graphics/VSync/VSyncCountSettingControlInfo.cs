﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.SelectValue;
using Assets.Scripts.ScenePreparation;
using Assets.Scripts.Settings;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Graphics
{
    internal class VSyncCountSettingControlInfo : ConfirmableSelectValueControl
    {
        public VSyncCountSettingControlInfo(string label, string description, string controlId) :
            base(label, controlId, description)
        { }

        protected override Tuple<string, string, string>
            GetOptionApplyConfirmDialogTexts()
        {
            return Tuple.Create(
                "You've changed VSync count setting value.\nDo you want to apply it?",
                "Yes", "No");
        }

        protected override void ConfirmedApplySelectedOption()
        {
            PlayerPrefs.SetInt(
                PlayerPreferences.VSYNC_COUNT_PLAYER_PREFS_KEY, this.selectedOptionValue);
            VSyncCountApplier.ApplyVSyncCount();
        }

        protected override List<ValueSelectionInfo> GetOptionValues()
        {
            return new List<ValueSelectionInfo>()
            {
                new ValueSelectionInfo("OFF", 0, null),
                new ValueSelectionInfo("1", 1, null),
                new ValueSelectionInfo("2", 2, null),
                new ValueSelectionInfo("3", 3, null),
                new ValueSelectionInfo("4", 4, null)
            };
        }

        public override void InitStateValue()
        {
            this.selectedOptionValue = QualitySettings.vSyncCount;
            this.selectedOptionIndex = this.optionValues.FindIndex(x =>
            {
                return this.selectedOptionValue == x.value;
            }); ;
            SetValueText(this.optionValues[this.selectedOptionIndex].label);
        }
    }
}
