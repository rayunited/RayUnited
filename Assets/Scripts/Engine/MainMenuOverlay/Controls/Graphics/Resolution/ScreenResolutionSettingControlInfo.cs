﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.SelectValue;
using Assets.Scripts.Settings;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Graphics
{
    public struct ResolutionInfo
    {
        public int width;
        public int height;
        public int refreshRate;

        public ResolutionInfo(int width, int height, int refreshRate)
        {
            this.width = width;
            this.height = height;
            this.refreshRate = refreshRate;
        }
    }

    public class ScreenResolutionSettingControlInfo : ConfirmableSelectValueControl
    {
        public ScreenResolutionSettingControlInfo(string label, string controlId)
            : base(label, controlId) { }

        protected override Tuple<string, string, string>
            GetOptionApplyConfirmDialogTexts()
        {
            return Tuple.Create(
                "You've changed screen resolution setting value.\nDo you want to apply it?",
                "Yes", "No");
        }

        protected override void ConfirmedApplySelectedOption()
        {
            ResolutionInfo selectedResolution =
                (ResolutionInfo)this.optionValues[this.selectedOptionIndex].additionalData;
            Screen.SetResolution(
                selectedResolution.width,
                selectedResolution.height,
                PlayerPreferences.GetFullScreenMode());
        }

        protected override List<ValueSelectionInfo> GetOptionValues()
        {
            UnityEngine.Resolution[] resolutions = Screen.resolutions;
            List<ValueSelectionInfo> result = new List<ValueSelectionInfo>();

            for (int i = 0; i < resolutions.Length; i++)
            {
                result.Add(
                    new ValueSelectionInfo(
                        string.Format(
                            "{0}x{1}x{2}Hz",
                            resolutions[i].width,
                            resolutions[i].height,
                            resolutions[i].refreshRate), i,
                        new ResolutionInfo(
                            resolutions[i].width,
                            resolutions[i].height,
                            resolutions[i].refreshRate))
                    );
            }

            return result;
        }

        public override void InitStateValue()
        {
            UnityEngine.Resolution currentResolution =
                PlayerPreferences.GetCurrentResolution();

            this.selectedOptionIndex = this.optionValues.FindIndex(x =>
            {
                ResolutionInfo info = (ResolutionInfo)x.additionalData;
                return currentResolution.width == info.width &&
                    currentResolution.height == info.height &&
                    currentResolution.refreshRate == info.refreshRate;
            });

            this.selectedOptionIndex =
                this.selectedOptionIndex >= 0 ? this.selectedOptionIndex : 0;
            SetValueText(this.optionValues[this.selectedOptionIndex].label);
        }
    }
}
