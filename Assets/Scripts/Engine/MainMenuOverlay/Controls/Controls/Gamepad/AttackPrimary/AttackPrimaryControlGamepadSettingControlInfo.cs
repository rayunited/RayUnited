﻿using Assets.Scripts.Engine.Input.InputSystem;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Gamepad.Attack
{
    public class AttackPrimaryControlGamepadSettingControlInfo :
        ControlBindingGamepadControl
    {
        public AttackPrimaryControlGamepadSettingControlInfo(
            string label, string controlId, string description = "")
            : base(label, controlId, description)
        {
        }

        public override void InitStateValue()
        {
            this.controlBindingInfo =
                GameplayGamepadControlsBindingsHelper
                    .GetCurrentGamepadAttackPrimaryBinding();
            SetValueText(this.controlBindingInfo.displayName);
        }

        protected override void OnApplyNewControlBinding(
            ControlBindingInfo newGamepadBindingInfo)
        {
            this.controlBindingInfo =
                GameplayGamepadControlsBindingsHelper
                    .SetNewGamepadAttackPrimaryBinding(newGamepadBindingInfo);
            SetValueText(this.controlBindingInfo.displayName);
        }
    }
}
