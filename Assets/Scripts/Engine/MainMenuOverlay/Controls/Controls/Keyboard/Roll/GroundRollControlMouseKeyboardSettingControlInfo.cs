﻿using Assets.Scripts.Engine.Input;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Keyboard.Roll
{
    public class GroundRollControlMouseKeyboardSettingControlInfo
        : ControlBindingMouseKeyboardControl
    {
        public GroundRollControlMouseKeyboardSettingControlInfo(
            string label, string controlId, string description = "")
            : base(label, controlId, description)
        {
        }

        public override void InitStateValue()
        {
            this.controlBindingInfo =
                GameplayMouseKeyboardControlsBindingsHelper
                    .GetCurrentMouseKeyboardGroundRollBinding();
            SetValueText(this.controlBindingInfo.displayName);
        }

        protected override void OnApplyNewControlBinding(
            ControlBindingInfo newMouseKeyboardBindingInfo)
        {
            this.controlBindingInfo =
                GameplayMouseKeyboardControlsBindingsHelper
                    .SetNewMouseKeyboardGroundRollBinding(newMouseKeyboardBindingInfo);
            SetValueText(this.controlBindingInfo.displayName);
        }
    }
}
