﻿using Assets.Scripts.Engine.Input;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Keyboard.Left
{
    public class MoveInputLeftControlMouseKeyboardSettingControlInfo
        : ControlBindingMouseKeyboardControl
    {
        public MoveInputLeftControlMouseKeyboardSettingControlInfo(
            string label, string controlId, string description = "")
            : base(label, controlId, description)
        {
        }

        public override void InitStateValue()
        {
            this.controlBindingInfo =
                GameplayMouseKeyboardControlsBindingsHelper
                    .GetCurrentMouseKeyboardLeftBinding();
            SetValueText(this.controlBindingInfo.displayName);
        }

        protected override void OnApplyNewControlBinding(
            ControlBindingInfo newMouseKeyboardBindingInfo)
        {
            this.controlBindingInfo =
                GameplayMouseKeyboardControlsBindingsHelper
                    .SetNewMouseKeyboardLeftBinding(newMouseKeyboardBindingInfo);
            SetValueText(this.controlBindingInfo.displayName);
        }
    }
}
