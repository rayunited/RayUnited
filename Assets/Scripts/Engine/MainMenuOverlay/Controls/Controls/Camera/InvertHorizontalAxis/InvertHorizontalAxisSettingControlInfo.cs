﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.SelectValue;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Camera.Sensitivity
{
    public class InvertHorizontalAxisSettingControlInfo :
        InstantYesNoValueControl
    {
        public InvertHorizontalAxisSettingControlInfo(
            string label, string controlId,
            string description = "") :
            base(label, controlId, description)
        { }

        protected override bool GetInitialTrueFalseControlValue()
        {
            int value = PlayerPrefs.GetInt(
                CameraSettingsUtilsInfo.INVERT_CAMERA_HORIZONTAL_AXIS_PLAYER_PREFS_KEY, 0);

            return value == 1;
        }

        protected override void OnNoValue()
        {
            PlayerPrefs.SetInt(
                CameraSettingsUtilsInfo.INVERT_CAMERA_HORIZONTAL_AXIS_PLAYER_PREFS_KEY,
                0);
        }

        protected override void OnYesValue()
        {
            PlayerPrefs.SetInt(
                CameraSettingsUtilsInfo.INVERT_CAMERA_HORIZONTAL_AXIS_PLAYER_PREFS_KEY,
                1);
        }
    }
}
