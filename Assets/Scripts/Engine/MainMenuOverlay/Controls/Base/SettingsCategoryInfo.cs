﻿using Assets.Scripts.Engine.BuildMainMenuOverlay;
using Assets.Scripts.Engine.BuildMainMenuOverlay.Settings;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.Scrolling;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Base.Settings;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base
{
    public static class SettingsCategoryParts
    {
        public const string settingsCategoryContainerPanel =
            "settingsCategoryContainerPanel";
    }

    public abstract class SettingsCategoryInfo
    {
        public List<SettingControlInfo> settingControlsInfos =
            new List<SettingControlInfo>();

        public SettingsScrollViewportBehaviour
            settingsScrollViewportHandle;

        public int selectedSettingControlIndex = 0;

        protected GameObject sectionContainer;
        protected GameObject settingsCategoryContainerPanel;

        protected int maximalAmountOfControlsVisibleAtOnce;
        protected Font font;
        protected int baseFontSize;
        protected Color textColor;
        protected Color scrollActiveColor;
        protected Color scrollInactiveColor;

        protected int descriptionFontSize;
        protected int settingControlFontSize;

        protected string categoryName;

        public SettingsCategoryInfo(
            string categoryName,
            SettingsSingleCategoryUiInfo settingsSingleCategoryUiInfo)
        {
            this.maximalAmountOfControlsVisibleAtOnce =
                settingsSingleCategoryUiInfo.maximalAmountOfControlsVisibleAtOnce;
            this.descriptionFontSize = settingsSingleCategoryUiInfo.descriptionFontSize;
            this.settingControlFontSize = settingsSingleCategoryUiInfo.settingControlFontSize;
            this.font = settingsSingleCategoryUiInfo.font;
            this.baseFontSize = settingsSingleCategoryUiInfo.baseFontSize;
            this.textColor = settingsSingleCategoryUiInfo.textColor;
            this.categoryName = categoryName;

            this.scrollActiveColor =
                settingsSingleCategoryUiInfo.scrollActiveColor;
            this.scrollInactiveColor =
                settingsSingleCategoryUiInfo.scrollInactiveColor;
        }

        public void PropagatePanel(GameObject panelObject)
        {
            this.settingControlsInfos = GetSettingsControls();
            GameObject boxBackgroundObject = MenuPanelFactory
                .GetBackgroundBoxObject(panelObject);
            this.sectionContainer = boxBackgroundObject;

            GameObject settingsCategoryContainerPanel =
                new GameObject(SettingsCategoryParts.settingsCategoryContainerPanel,
                    typeof(RectTransform));
            RectTransform containerRectTransform =
                boxBackgroundObject.GetComponent<RectTransform>();

            RectTransform settingsCategoryContainerPanelRectTransform
                = settingsCategoryContainerPanel.GetComponent<RectTransform>();
            settingsCategoryContainerPanelRectTransform.sizeDelta =
                new Vector2(
                    containerRectTransform.sizeDelta.x,
                    containerRectTransform.sizeDelta.y);

            Tuple<GameObject,
                SettingsScrollViewportBehaviour,
                float> settingControlsContainerInfo =
                SettingsSectPanelsFactory
                    .GetSettingsControlsContentContainerUnder(
                        settingsCategoryContainerPanel,
                        this.maximalAmountOfControlsVisibleAtOnce,
                        200, this.font, this.baseFontSize, this.textColor,
                        this.scrollActiveColor, this.scrollInactiveColor);

            this.settingsScrollViewportHandle = settingControlsContainerInfo.Item2;

            int controlInfoIndex = 0;
            foreach (SettingControlInfo controlInfo in this.settingControlsInfos)
            {
                this.settingControlsInfos[controlInfoIndex].settingControlBehaviour =
                    SettingsMenuSectionBuildingHelper
                        .AddSettingControlUnderSettingControlsContainer(
                            settingControlsContainerInfo.Item1, controlInfo, this.font, this.textColor,
                            this.settingControlFontSize, this.descriptionFontSize,
                            settingControlsContainerInfo.Item3);

                controlInfoIndex++;
            }

            this.settingsScrollViewportHandle.itemsTotal = this.settingControlsInfos.Count;
            this.settingsScrollViewportHandle.itemsInViewportAtOnce =
                this.maximalAmountOfControlsVisibleAtOnce;
            this.settingsScrollViewportHandle.ScrollToElementIndex(0);

            settingsCategoryContainerPanel.transform.SetParent(
                boxBackgroundObject.transform, false);

            this.settingsCategoryContainerPanel = settingsCategoryContainerPanel;
        }

        public void ReinitStateAndUiState()
        {
            ClearUiState();
            ReinitState();
        }

        public void ReinitState()
        {
            this.selectedSettingControlIndex = 0;
            this.settingsScrollViewportHandle.ScrollToElementIndex(0);
            this.settingControlsInfos.ForEach(x => x.InitState());
            this.settingsScrollViewportHandle
                .HideScrollingPromptsCompletelyIfScrollingNotNeeded();
        }

        protected abstract List<SettingControlInfo> GetSettingsControls();
        public void SelectOptionDown(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            bool canLeaveSettingControl =
                this.settingControlsInfos[this.selectedSettingControlIndex]
                    .OnControlLeaving(gameplayMenuBehaviour);

            if (canLeaveSettingControl)
            {
                this.settingControlsInfos[this.selectedSettingControlIndex]
                .settingControlBehaviour
                .SetLabelTextLocalScale(new Vector3(1f, 1f));

                if (this.selectedSettingControlIndex < this.settingControlsInfos.Count - 1)
                {
                    this.selectedSettingControlIndex++;
                }
                else
                {
                    this.selectedSettingControlIndex = 0;
                }

                this.settingsScrollViewportHandle.ScrollToElementIndex(this.selectedSettingControlIndex);
            }
        }
        public void SelectOptionUp(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            bool canLeaveSettingControl =
                this.settingControlsInfos[this.selectedSettingControlIndex]
                    .OnControlLeaving(gameplayMenuBehaviour);

            if (canLeaveSettingControl)
            {
                this.settingControlsInfos[this.selectedSettingControlIndex]
                .settingControlBehaviour
                .SetLabelTextLocalScale(new Vector3(1f, 1f));

                if (this.selectedSettingControlIndex > 0)
                {
                    this.selectedSettingControlIndex--;
                }
                else
                {
                    this.selectedSettingControlIndex =
                        this.settingControlsInfos.Count - 1;
                }

                this.settingsScrollViewportHandle.ScrollToElementIndex(this.selectedSettingControlIndex);
            }
        }

        public void SelectOptionLeft(bool fromRepeatedInput = false)
        {
            this.settingControlsInfos[this.selectedSettingControlIndex]
                .SelectOptionLeft(fromRepeatedInput);
        }

        public void SelectOptionRight(bool fromRepeatedInput = false)
        {
            this.settingControlsInfos[this.selectedSettingControlIndex]
                .SelectOptionRight(fromRepeatedInput);
        }

        public void ApplySelectedOption(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            this.settingControlsInfos[this.selectedSettingControlIndex]
                .ApplySelectedOption(gameplayMenuBehaviour);
        }

        public bool ExitSectionHandler(GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            bool canLeaveSettingControl =
                this.settingControlsInfos[this.selectedSettingControlIndex]
                    .OnControlLeaving(gameplayMenuBehaviour);

            return canLeaveSettingControl;
        }

        public bool OnCategoryLeaving(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            bool canLeaveSettingControl =
                this.settingControlsInfos[this.selectedSettingControlIndex]
                    .OnControlLeaving(gameplayMenuBehaviour);

            return canLeaveSettingControl;
        }

        public void HideCategory()
        {
            this.settingsCategoryContainerPanel.SetActive(false);
        }

        public void ClearUiState()
        {
            //throw new NotImplementedException();
        }

        public void ShowCategory()
        {
            this.settingsCategoryContainerPanel.SetActive(true);
        }

        public string GetCategoryName()
        {
            return this.categoryName;
        }
    }
}
