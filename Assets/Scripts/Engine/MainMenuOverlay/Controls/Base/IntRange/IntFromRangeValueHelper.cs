﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.IntRange
{
    public class IntElement
    {
        public int value;
        public string? label;

        public IntElement(
            int value,
            string? label = null
            )
        {
            this.value = value;
            this.label = label;
        }
    }

    public static class IntFromRangeValueHelper
    {
        private static List<IntElement> GetElements(
            int rangeStart, int rangeEnd,
            List<AdditionalIntValueInfo> additionalValues)
        {
            List<AdditionalIntValueInfo> sortedAdditionalValuesSmallerThanRangeStart =
                additionalValues.Where(x => x.value < rangeStart)
                .OrderBy(x => x.value).ToList();

            List<AdditionalIntValueInfo> sortedAdditionalValuesBiggerThanRangeEnd =
                additionalValues.Where(x => x.value > rangeEnd)
                .OrderBy(x => x.value).ToList();

            List<IntElement> elements = new List<IntElement>();
            sortedAdditionalValuesSmallerThanRangeStart
                .ForEach(x => elements.Add(new IntElement(x.value, x.label)));
            for (int i = rangeStart; i <= rangeEnd; i++)
            {
                elements.Add(new IntElement(i));
            }
            sortedAdditionalValuesBiggerThanRangeEnd
                .ForEach(x => elements.Add(new IntElement(x.value, x.label)));

            return elements;
        }

        public static int GetFirstLessOrEqualValueThan(
            int currentValue, int rangeStart, int rangeEnd,
            List<AdditionalIntValueInfo> additionalValues)
        {
            List<IntElement> elements =
                GetElements(rangeStart, rangeEnd, additionalValues);

            int currentValueIndex =
                elements.FindIndex(x => x.value == currentValue);

            if (currentValueIndex > 0)
            {
                return elements[currentValueIndex - 1].value;
            }
            else
            {
                return elements.Last().value;
            }
        }

        public static int GetFirstBiggerOrEqualValueThan(
            int currentValue, int rangeStart, int rangeEnd,
            List<AdditionalIntValueInfo> additionalValues)
        {
            List<IntElement> elements =
                 GetElements(rangeStart, rangeEnd, additionalValues);

            int currentValueIndex =
                elements.FindIndex(x => x.value == currentValue);

            if (currentValueIndex < elements.Count - 1)
            {
                return elements[currentValueIndex + 1].value;
            }
            else
            {
                return elements[0].value;
            }
        }
    }
}
