﻿using Assets.Scripts.Engine.MainMenuOverlay.Sections.Boxes;
using System;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding
{
    public abstract class ControlBindingControlBase : SettingControlInfo
    {
        protected bool isSelectedBindingApplied;
        protected bool hasCanceledRebindingProcess;

        protected ControlBindingInfo controlBindingInfo;
        protected string associatedFocusBoxId;

        public ControlBindingControlBase(
            string label, string controlId,
            string associatedFocusBoxId,
            string description = "") :
            base(label, controlId, description)
        {
            this.associatedFocusBoxId =
                associatedFocusBoxId;
        }

        protected override void PreinitStateHandler()
        {
            this.isSelectedBindingApplied = true;
            this.hasCanceledRebindingProcess = false;
        }

        public override void ApplySelectedOption(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (this.isSelectedBindingApplied)
            {
                string readingFocusBoxMessage = GetBindingReadingFocusBoxMessage();
                this.isSelectedBindingApplied = false;
                ShowBindingReadingInfoBox(
                    gameplayMenuBehaviour,
                    this.associatedFocusBoxId,
                    readingFocusBoxMessage);
            }
        }

        protected abstract string GetBindingReadingFocusBoxMessage();

        protected void ShowBindingReadingInfoBox(
            GameplayMenuBehaviour gameplayMenuBehaviour,
            string focusBoxId,
            string message)
        {
            gameplayMenuBehaviour
                .SetActiveFocusBox(
                    focusBoxId);

            GameplayControlBindingReadingBoxBehaviour focusBoxBehaviour =
                GetBindingFocusBoxBehaviour();

            focusBoxBehaviour.RegisterAsInputListener();
            focusBoxBehaviour.SetMessage(message);
            focusBoxBehaviour.SetBindingValueHandler(
                (newControlBindingInfo) =>
                {
                    gameplayMenuBehaviour.SetActiveFocusBox(null);
                    OnApplyNewControlBinding(newControlBindingInfo);
                    this.isSelectedBindingApplied = true;
                });
            focusBoxBehaviour.SetBindingCancelHandler(
                () =>
                {
                    gameplayMenuBehaviour.SetActiveFocusBox(null);
                    this.hasCanceledRebindingProcess = true;
                    this.isSelectedBindingApplied = true;
                });
        }

        protected abstract GameplayControlBindingReadingBoxBehaviour
            GetBindingFocusBoxBehaviour();

        protected abstract void OnApplyNewControlBinding(
            ControlBindingInfo newControlBindingInfo);

        public override bool OnControlLeaving(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (!this.hasCanceledRebindingProcess)
            {
                return this.isSelectedBindingApplied;
            }
            else
            {
                this.hasCanceledRebindingProcess = false;
                return false;
            }
        }

        public override bool SelectOptionLeft(bool fromRepeatedInput = false)
        {
            return true;
        }

        public override bool SelectOptionRight(bool fromRepeatedInput = false)
        {
            return true;
        }

        protected override void OnSelectedValueChange()
        {
            throw new NotImplementedException();
        }
    }
}
