﻿using Assets.Scripts.Engine.MainMenuOverlay.Sections.Boxes;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Settings;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding
{
    public class ControlBindingInfo
    {
        public string inputBindingPath { get; protected set; }
        public string displayName { get; protected set; }

        public ControlBindingInfo(string inputBindingPath, string displayName)
        {
            this.inputBindingPath = inputBindingPath;
            this.displayName = displayName;
        }

        public static ControlBindingInfo FromInputSystemInfo(
            string displayName, string inputBindingPath)
        {
            return new ControlBindingInfo(
                inputBindingPath, displayName
            );
        }
    }

    public abstract class ControlBindingMouseKeyboardControl :
        ControlBindingControlBase
    {
        public ControlBindingMouseKeyboardControl(
            string label, string controlId, string description = "") :
            base(label, controlId,
                ControlsSettingsMenuSectionFocusBoxes
                    .MOUSE_KEYBOARD_BINDING_READING_FOCUS_BOX,
                description)
        {
        }

        protected override GameplayControlBindingReadingBoxBehaviour
            GetBindingFocusBoxBehaviour()
        {
            return UnityEngine.Object
                .FindObjectOfType<
                    KeyboardMouseGameplayControlBindingReadingFocusBoxBehaviour>();
        }

        protected override string GetBindingReadingFocusBoxMessage()
        {
            return
                "Press key on the keyboard or mouse button to assign it to the "
                    + this.label + " control.";
        }
    }
}
