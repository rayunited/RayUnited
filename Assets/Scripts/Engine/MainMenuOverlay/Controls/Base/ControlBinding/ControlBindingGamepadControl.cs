﻿using Assets.Scripts.Engine.MainMenuOverlay.Sections.Boxes;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Settings;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding
{
    public abstract class ControlBindingGamepadControl :
        ControlBindingControlBase
    {
        public ControlBindingGamepadControl(
            string label, string controlId, string description = "") :
            base(label, controlId,
                ControlsSettingsMenuSectionFocusBoxes
                    .GAMEPAD_BINDING_READING_FOCUS_BOX,
                description)
        {
        }

        protected override string GetBindingReadingFocusBoxMessage()
        {
            return
                "Press button on the gamepad to assign it to the "
                    + this.label + " control.";
        }

        protected override GameplayControlBindingReadingBoxBehaviour
            GetBindingFocusBoxBehaviour()
        {
            return UnityEngine.Object
                .FindObjectOfType<
                    GamepadGameplayControlBindingReadingFocusBoxBehaviour>();
        }
    }
}
