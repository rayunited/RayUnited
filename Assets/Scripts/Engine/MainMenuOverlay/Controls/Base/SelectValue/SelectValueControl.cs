﻿using System.Collections.Generic;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base
{
    public class ValueSelectionInfo
    {
        public string label;
        public int value;
        public object additionalData;

        public ValueSelectionInfo(
            string label, int value, object additionalData)
        {
            this.label = label;
            this.value = value;
            this.additionalData = additionalData;
        }
    }

    public abstract class SelectValueControl : SettingControlInfo
    {
        protected int selectedOptionIndex;
        protected int selectedOptionValue;

        protected List<ValueSelectionInfo> optionValues;

        public SelectValueControl(string label, string controlId, string description = "") :
            base(label, controlId, description)
        {
            this.optionValues = GetOptionValues();
        }

        protected void UpdateTextValueToCurrentOptionIndex()
        {
            SetValueText(this.optionValues[this.selectedOptionIndex].label);
        }

        protected abstract List<ValueSelectionInfo> GetOptionValues();

        public override bool SelectOptionLeft(bool fromRepeatedInput = false)
        {
            if (this.selectedOptionIndex > 0)
            {
                this.selectedOptionIndex--;
            }
            else
            {
                this.selectedOptionIndex = this.optionValues.Count - 1;
            }
            this.selectedOptionValue = this.optionValues[this.selectedOptionIndex].value;
            UpdateTextValueToCurrentOptionIndex();
            OnSelectedValueChange();
            return true;
        }

        public override bool SelectOptionRight(bool fromRepeatedInput = false)
        {
            if (this.selectedOptionIndex < this.optionValues.Count - 1)
            {
                this.selectedOptionIndex++;
            }
            else
            {
                this.selectedOptionIndex = 0;
            }
            this.selectedOptionValue = this.optionValues[this.selectedOptionIndex].value;
            UpdateTextValueToCurrentOptionIndex();
            OnSelectedValueChange();
            return true;
        }
    }
}
