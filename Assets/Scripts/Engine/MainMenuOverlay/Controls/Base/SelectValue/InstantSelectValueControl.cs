﻿namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.SelectValue
{
    public abstract class InstantSelectValueControl : SelectValueControl
    {
        public InstantSelectValueControl(
            string label, string controlId,
            string description = "") :
            base(label, controlId, description)
        { }

        public override bool OnControlLeaving(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            return true;
        }
    }
}
