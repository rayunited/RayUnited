﻿using Assets.Scripts.Engine.MainMenuOverlay.Sections.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Dialogs;
using System;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.SelectValue
{
    public abstract class ConfirmableSelectValueControl : SelectValueControl
    {
        protected bool isSelectedOptionApplied;

        public ConfirmableSelectValueControl(
            string label, string controlId,
            string description = "") :
            base(label, controlId, description)
        { }

        protected override void PreinitStateHandler()
        {
            this.isSelectedOptionApplied = true;
        }

        public override bool OnControlLeaving(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (this.isSelectedOptionApplied)
            {
                return true;
            }
            else
            {
                Tuple<string, string, string>
                    dialogCaptions = GetOptionApplyConfirmDialogTexts();
                ShowDialogForConfirmingOptionApplying(
                    gameplayMenuBehaviour,
                    dialogCaptions.Item1, dialogCaptions.Item2, dialogCaptions.Item3);
                return false;
            }
        }

        private void ShowDialogForConfirmingOptionApplying(
            GameplayMenuBehaviour gameplayMenuBehaviour,
            string message, string confirmLabel, string rejectLabel)
        {
            gameplayMenuBehaviour
                .SetActiveDialog(
                    SettingsMenuSectionDialogs.SETTING_CONTROL_VALUE_APPLY_DIALOG);

            // this FindObjectOfType thing is a quick solution for now
            // as at the moment of writing this comment we have only
            // one settings section so far (graphics setting section)
            // in the future we need to ensure we always fetch
            // proper behaviour component for such setting value applying
            // dialog that is appropriate for the given menu settings section
            // in question
            SettingControlValueApplyDialogBehaviour dialogBehaviour =
                UnityEngine.Object
                    .FindObjectOfType<SettingControlValueApplyDialogBehaviour>();

            dialogBehaviour.SetLabels(message, confirmLabel, rejectLabel);
            dialogBehaviour.SetConfirmHandler(ApplySelectedOption);
            dialogBehaviour.SetRejectHandler(ValueApplyRejectHandler);
        }

        protected abstract Tuple<string, string, string>
            GetOptionApplyConfirmDialogTexts();

        protected abstract void ConfirmedApplySelectedOption();

        public override void ApplySelectedOption(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            ConfirmedApplySelectedOption();
            UpdateTextValueToCurrentOptionIndex();
            this.isSelectedOptionApplied = true;
            gameplayMenuBehaviour.SetActiveDialog(null);
            InitStateUi();
        }

        protected void ValueApplyRejectHandler(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            gameplayMenuBehaviour.SetActiveDialog(null);
            InitState();
        }

        protected override void OnSelectedValueChange()
        {
            this.isSelectedOptionApplied = false;
        }
    }
}
