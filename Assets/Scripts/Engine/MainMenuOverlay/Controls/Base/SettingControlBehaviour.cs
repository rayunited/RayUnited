﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base
{
    public class SettingControlBehaviour : MonoBehaviour
    {
        public Text labelText;
        public Text valueText;

        public void SetLabelTextLocalScale(Vector3 scaleFactors)
        {
            this.labelText.rectTransform.localScale = scaleFactors;
        }
    }
}
