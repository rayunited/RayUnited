﻿using Assets.Scripts.Engine.BuildMainMenuOverlay.Ui;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.Categories
{
    public static class SelectedCategoryPromptParts
    {
        public const string text = "text";
    }

    public class SelectedCategoryPromptBehaviour : MonoBehaviour
    {
        protected Font font;
        protected int fontSize;
        protected Color textColor;

        protected Text textHandle;

        protected float desiredPanelWidth;
        protected float desiredPanelHeight;

        public void RepropagatePanel(string settingsCategoryName)
        {
            foreach (Transform child in this.transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            Text text = UnityUiTextFactory.GetLeftAlignedTextLabel(
                SelectedCategoryPromptParts.text, this.font, this.fontSize,
                settingsCategoryName, this.textColor, this.desiredPanelWidth);
            text.transform.SetParent(this.gameObject.transform, false);
            text.gameObject.AddComponent<Outline>();
            text.gameObject.AddComponent<Outline>();
            text.gameObject.AddComponent<Outline>();

            RectTransform rectTransform =
                this.gameObject.GetComponent<RectTransform>();
            rectTransform.sizeDelta =
                new Vector2(this.desiredPanelWidth, this.desiredPanelHeight);
            this.textHandle = text;
        }

        public void ReinitUiState(
            Font font, int fontSize, Color textColor,
            float panelWidth, float panelHeight)
        {
            this.font = font;
            this.fontSize = fontSize;
            this.textColor = textColor;
            this.desiredPanelWidth = panelWidth;
            this.desiredPanelHeight = panelHeight;
        }

        public void ChangeCategoryTitle(string categoryName)
        {
            RepropagatePanel(categoryName);
        }
    }
}
