﻿using Assets.Scripts.Engine.BuildMainMenuOverlay.Ui;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.Categories
{
    public class PreviousCategoryPromptBehaviour : CategorySwitchingPromptBehaviourBase
    {
        protected override string GetCategoryNamePromptText()
        {
            return "Previous Settings Category";
        }

        protected override string GetInputPromptGamepadText()
        {
            return "L1";
        }

        protected override string GetInputPromptKeyboardText()
        {
            return "LCtrl";
        }

        protected override Text InputPromptTextUiProvider(float desiredPanelWidth, string keyInputPrompt)
        {
            return UnityUiTextFactory.GetCenteredTextLabel(
                CategorySwitchPromptParts.keyInputPromptTextPanel,
                this.font, this.inputPromptFontSize, keyInputPrompt,
                this.categoryChangePromptKeyTextColor, desiredPanelWidth);
        }
    }
}
