﻿using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.Scrolling
{
    public class SettingsScrollViewportBehaviour : MonoBehaviour
    {
        public ScrollingPromptBehaviour upScrollPromptHandle;
        public ScrollingPromptBehaviour downScrollPromptHandle;

        public RectTransform scrollableContent;

        public int itemsInViewportAtOnce;
        public int itemsTotal;

        public float elementHeight;

        protected int currentScrollViewElementBeginningIndex = 0;

        public void ScrollToElementIndex(
            int elementIndexToScrollTo)
        {
            int viewEndingElementIndex =
                this.currentScrollViewElementBeginningIndex + this.itemsInViewportAtOnce - 1;

            if (elementIndexToScrollTo > viewEndingElementIndex)
            {
                // scrolling down
                int elementsAmountDifference =
                    elementIndexToScrollTo - viewEndingElementIndex;
                this.currentScrollViewElementBeginningIndex += elementsAmountDifference;
                //viewEndingElementIndex = 
                //    currentScrollViewElementBeginningIndex + itemsInViewportAtOnce - 1;
                this.scrollableContent.anchoredPosition =
                    new Vector2(
                        this.scrollableContent.anchoredPosition.x,
                        this.scrollableContent.anchoredPosition.y +
                            (elementsAmountDifference * this.elementHeight));

            }
            else if (elementIndexToScrollTo < this.currentScrollViewElementBeginningIndex)
            {
                // scrolling up
                int elementsAmountDifference =
                    Mathf.Abs(elementIndexToScrollTo - this.currentScrollViewElementBeginningIndex);

                this.currentScrollViewElementBeginningIndex -= elementsAmountDifference;

                this.scrollableContent.anchoredPosition =
                    new Vector2(
                        this.scrollableContent.anchoredPosition.x,
                        this.scrollableContent.anchoredPosition.y -
                            (elementsAmountDifference * this.elementHeight));

            }

            if (this.currentScrollViewElementBeginningIndex > 0)
            {
                //upScrollPromptHandle.GetComponent<Text>().enabled = true;
                this.upScrollPromptHandle.ShowAsActive();
            }
            else
            {
                this.upScrollPromptHandle.ShowAsInactive();
                //upScrollPromptHandle.GetComponent<Text>().enabled = false;
            }


            if (this.currentScrollViewElementBeginningIndex <
                    this.itemsTotal - this.itemsInViewportAtOnce)
            {
                this.downScrollPromptHandle.ShowAsActive();
                //downScrollPromptHandle.GetComponent<Text>().enabled = true;
            }
            else
            {
                this.downScrollPromptHandle.ShowAsInactive();
                //downScrollPromptHandle.GetComponent<Text>().enabled = false;
            }
        }

        public void HideScrollingPromptsCompletelyIfScrollingNotNeeded()
        {
            if (this.itemsTotal <= this.itemsInViewportAtOnce)
            {
                this.upScrollPromptHandle.gameObject.SetActive(false);
                this.downScrollPromptHandle.gameObject.SetActive(false);
            }
        }
    }
}
