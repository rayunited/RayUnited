﻿using Assets.Scripts.Engine.Quality.Levels;

namespace Assets.Scripts.Engine.Quality
{
    public static class FrameSettingsProvider
    {
        public static FrameSettingsSet GetLowQualitySettings()
        {
            return new LowFrameSettingsSet();
        }

        public static FrameSettingsSet GetMediumQualitySettings()
        {
            return new MediumFrameSettingsSet();
        }

        public static FrameSettingsSet GetHighQualitySettings()
        {
            return new HighFrameSettingsSet();
        }
    }
}
