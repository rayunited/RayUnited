﻿namespace Assets.Scripts.Engine.Quality.Levels
{
    public class HighFrameSettingsSet : FrameSettingsSet
    {
        public HighFrameSettingsSet() : base()
        {
            //this.frameSettingsValues =
            //    new Dictionary<FrameSettingsField, bool>()
            //    {
            //        { FrameSettingsField.AfterPostprocess, true },
            //        { FrameSettingsField.AlphaToMask, true },
            //        { FrameSettingsField.Antialiasing, true },
            //        { FrameSettingsField.AsyncCompute, true },
            //        { FrameSettingsField.AtmosphericScattering, true },
            //        { FrameSettingsField.BigTilePrepass, true },
            //        { FrameSettingsField.Bloom, true },
            //        { FrameSettingsField.ChromaticAberration, true },
            //        { FrameSettingsField.ClearGBuffers, true },
            //        { FrameSettingsField.ColorGrading, true },
            //        { FrameSettingsField.ComputeLightEvaluation, true },
            //        { FrameSettingsField.ComputeLightVariants, true },
            //        { FrameSettingsField.ComputeMaterialVariants, true },
            //        { FrameSettingsField.ContactShadows, true },
            //        { FrameSettingsField.ContactShadowsAsync, true },
            //        { FrameSettingsField.CustomPass, true },
            //        { FrameSettingsField.CustomPostProcess, true },
            //        { FrameSettingsField.DecalLayers, true },
            //        { FrameSettingsField.Decals, true },
            //        { FrameSettingsField.DeferredTile, true },
            //        { FrameSettingsField.DepthOfField, true },
            //        { FrameSettingsField.DepthPrepassWithDeferredRendering, true },
            //        { FrameSettingsField.DirectSpecularLighting, true },
            //        { FrameSettingsField.Distortion, true },
            //        { FrameSettingsField.Dithering, true },
            //        { FrameSettingsField.ExposureControl, true },
            //        { FrameSettingsField.FilmGrain, true },
            //        { FrameSettingsField.FPTLForForwardOpaque, true },
            //        { FrameSettingsField.FullResolutionCloudsForSky, true },
            //        { FrameSettingsField.LensDistortion, true },
            //        { FrameSettingsField.LensFlareDataDriven, true },
            //        { FrameSettingsField.LightLayers, true },
            //        { FrameSettingsField.LightListAsync, true },
            //        { FrameSettingsField.LitShaderMode, true },
            //        { FrameSettingsField.LODBias, true },
            //        { FrameSettingsField.LODBiasMode, true },
            //        { FrameSettingsField.LODBiasQualityLevel, true },
            //        { FrameSettingsField.LowResTransparent, true },
            //        { FrameSettingsField.MaterialQualityLevel, true },
            //        { FrameSettingsField.MaximumLODLevel, true },
            //        { FrameSettingsField.MaximumLODLevelMode, true },
            //        { FrameSettingsField.MaximumLODLevelQualityLevel, true },
            //        { FrameSettingsField.MotionBlur, false },
            //        { FrameSettingsField.MotionVectors, true },
            //        { FrameSettingsField.MSAAMode, true },
            //        { FrameSettingsField.None, true },
            //        { FrameSettingsField.NormalizeReflectionProbeWithProbeVolume, true },
            //        { FrameSettingsField.ObjectMotionVectors, true },
            //        { FrameSettingsField.OpaqueObjects, true },
            //        { FrameSettingsField.PaniniProjection, true },
            //        { FrameSettingsField.PlanarProbe, true },
            //        { FrameSettingsField.Postprocess, true },
            //        { FrameSettingsField.ProbeVolume, true },
            //        { FrameSettingsField.RayTracing, true },
            //        { FrameSettingsField.ReflectionProbe, true },
            //        { FrameSettingsField.Refraction, true },
            //        { FrameSettingsField.ReplaceDiffuseForIndirect, true },
            //        { FrameSettingsField.ReprojectionForVolumetrics, true },
            //        { FrameSettingsField.RoughDistortion, true },
            //        { FrameSettingsField.ScreenSpaceShadows, true },
            //        { FrameSettingsField.ShadowMaps, true },
            //        { FrameSettingsField.Shadowmask, true },
            //        { FrameSettingsField.SkyReflection, true },
            //        { FrameSettingsField.SSAO, true },
            //        { FrameSettingsField.SSAOAsync, true },
            //        { FrameSettingsField.SSGI, true },
            //        { FrameSettingsField.SSR, true },
            //        { FrameSettingsField.SSRAsync, true },
            //        { FrameSettingsField.SssCustomSampleBudget, true },
            //        { FrameSettingsField.SssQualityLevel, true },
            //        { FrameSettingsField.SssQualityMode, true },
            //        { FrameSettingsField.StopNaN, true },
            //        { FrameSettingsField.SubsurfaceScattering, true },
            //        { FrameSettingsField.Tonemapping, true },
            //        { FrameSettingsField.Transmission, true },
            //        { FrameSettingsField.TransparentObjects, true },
            //        { FrameSettingsField.TransparentPostpass, true },
            //        { FrameSettingsField.TransparentPrepass, true },
            //        { FrameSettingsField.TransparentSSR, true },
            //        { FrameSettingsField.TransparentsWriteMotionVector, true },
            //        { FrameSettingsField.Vignette, true },
            //        { FrameSettingsField.VirtualTexturing, true },
            //        { FrameSettingsField.VolumetricClouds, true },
            //        { FrameSettingsField.Volumetrics, true },
            //        { FrameSettingsField.VolumeVoxelizationsAsync, true },
            //        { FrameSettingsField.ZTestAfterPostProcessTAA, true },
            //    };
        }
    }
}
