﻿namespace Assets.Scripts.Engine.Quality
{
    public static class FrameSettingsHelper
    {
        public static void ApplyLowQualityValues()
        {
            FrameSettingsApplier.ApplySettings(
                FrameSettingsProvider.GetLowQualitySettings());
        }

        public static void ApplyMediumQualityValues()
        {
            FrameSettingsApplier.ApplySettings(
                FrameSettingsProvider.GetMediumQualitySettings());
        }

        public static void ApplyHighQualityValues()
        {
            FrameSettingsApplier.ApplySettings(
                FrameSettingsProvider.GetHighQualitySettings());
        }
    }
}
