﻿using UnityEngine;

namespace Assets.Scripts.Engine.Debug
{
    public static class PlayerWallCollisionDebugFlags
    {
        public static bool debugInitialWallHitCheck = false;
        public static Color debugInitialWallHitCheckColor = Color.white;

        public static bool debugAveragingWallCollisionNormal = false;
        public static Color debugAveragingWallCollisionNormalColor = Color.white;

        public static bool debugHittingWallInGivenDirectionAtCorner = false;
        public static Color debugHittingWallInGivenDirectionAtCornerColor = Color.white;

        public static bool debugResultVelocity = true;
        public static float debugVelocityLengthMultiplier = 15f;
        public static Color debugResultVelocityColor = Color.white;

        public static bool debugResultWallNormal = false;
        public static Color debugResultWallNormalColor = Color.white;

        public static bool debugEntityForwardDirection = true;
        public static float debugEntityForwardDirectionLengthMultiplier = 5;
        public static Color debugEntityForwardDirectionColor = Color.red;
    }
}
