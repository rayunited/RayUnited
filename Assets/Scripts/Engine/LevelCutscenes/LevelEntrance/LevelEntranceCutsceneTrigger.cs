﻿using Assets.Scripts.Animations;
using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.Engine.Waypoints;
using Assets.Scripts.HUD;
using Assets.Scripts.HUD.Draw;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Camera;
using Assets.Scripts.PlayerMechanics.Rules.Common;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Engine.LevelCutscenes
{
    public static class WaypointsPathsFetcher
    {
        public static List<PlayerWaypoint>
            GetLevelEntranceCutsceneWaypoints(GameObject pathWaypointsContainerGameObject)
        {
            return pathWaypointsContainerGameObject
                .GetComponentsInChildren<PlayerWaypoint>().ToList();
        }

        public static List<SimpleWaypoint> GetLevelEntranceCutsceneCameraWaypoints(
            GameObject cameraWaypointsContainerGameObject)
        {
            return cameraWaypointsContainerGameObject
                .GetComponentsInChildren<SimpleWaypoint>().ToList();
        }
    }

    public class LevelEntranceCutsceneTrigger : GameplayOnlyMonoBehaviour
    {
        public GameObject levelEntranceWaypointsGameObjectContainer;
        public GameObject levelEntranceCameraWaypointsGameObjectContainer;

        public PlayerActorHandle playerActor;
        public CameraActorHandle cameraActor;

        public DrawCutsceneVignette cutsceneVignette;
        public DrawPlayerScoreCounter drawPlayerScoreCounter;
        public DrawHudTargetShootSight drawHudTargetShootSight;

        private List<PlayerWaypoint> levelEntranceWaypoints
            = new List<PlayerWaypoint>();
        private List<SimpleWaypoint> levelEntranceCameraWaypoints
            = new List<SimpleWaypoint>();

        protected bool hasEnded = false;

        protected override void BehaviourStart()
        {
            this.levelEntranceWaypoints =
                WaypointsPathsFetcher
                    .GetLevelEntranceCutsceneWaypoints(this.levelEntranceWaypointsGameObjectContainer);
            this.hasEnded = false;

            this.levelEntranceCameraWaypoints =
                WaypointsPathsFetcher
                    .GetLevelEntranceCutsceneCameraWaypoints(this.levelEntranceCameraWaypointsGameObjectContainer);

            this.cutsceneVignette.enabled = true;
            this.drawPlayerScoreCounter.enabled = false;
            this.drawHudTargetShootSight.SetEnabledDrawing(false);

            this.cameraActor.DisableGameplayCameraRules();
            this.cameraActor.SetCameraPosition(this.levelEntranceCameraWaypoints[0].transform.position);
            this.playerActor.DisablePlayerControl();
            this.playerActor.FollowWaypointPath(this.levelEntranceWaypoints);
        }

        protected override void GameplayFixedUpdate()
        {
            if (this.playerActor.HasEndedCurrentWaypointPath() && !this.hasEnded)
            {
                this.playerActor.EnablePlayerControl();
                this.cameraActor.EnableGameplayCameraRules();
                this.cutsceneVignette.enabled = false;
                this.drawPlayerScoreCounter.enabled = true;
                this.drawHudTargetShootSight.SetEnabledDrawing(true);
                this.hasEnded = true;
            }
            else if (!this.hasEnded)
            {
                this.cameraActor.LookAt(this.playerActor.transform.position);
            }
        }
    }
}
