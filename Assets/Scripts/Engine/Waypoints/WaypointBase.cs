﻿using UnityEngine;

namespace Assets.Scripts.Engine.Waypoints
{
    public class WaypointBase<T> : MonoBehaviour
    {
        public T nextWaypoint;
    }
}
