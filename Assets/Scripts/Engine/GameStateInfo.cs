using UnityEngine;

public enum GameState
{
    GAMEPLAY_STATE,
    GAMEPLAY_MENU_STATE
}

public class GameStateInfo : MonoBehaviour
{
    private GameState gameState;

    // Start is called before the first frame update
    private void Start()
    {
        this.gameState = GameState.GAMEPLAY_STATE;
    }

    // Update is called once per frame
    public bool IsGameplayState()
    {
        return this.gameState == GameState.GAMEPLAY_STATE;
    }

    public bool IsGameplayMenuState()
    {
        return this.gameState == GameState.GAMEPLAY_MENU_STATE;
    }

    public void SetGameState(GameState newGameState)
    {
        this.gameState = newGameState;
    }
}
