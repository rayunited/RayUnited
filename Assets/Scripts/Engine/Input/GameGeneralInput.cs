using Assets.Scripts.Engine.Input;
using UnityEngine;

public class GameGeneralInput : MonoBehaviour
{
    private OneTimeInput menuKey;
    private OneTimeInput menuBackKey;
    private OneTimeInput menuKeyUp;
    private OneTimeInput menuKeyDown;
    private OneTimeInput menuKeyLeft;
    private OneTimeInput menuKeyRight;
    private OneTimeInput menuOptionApplyKey;
    private OneTimeInput menuPreviousCategoryKey;
    private OneTimeInput menuNextCategoryKey;

    protected PlayerInputHub playerInputHub;

    // Start is called before the first frame update
    private void Start()
    {
        this.playerInputHub = FindObjectOfType<GameStateInfo>().GetComponent<PlayerInputHub>();

        this.menuKey = new OneTimeInput();
        this.menuKey.Init(() => this.playerInputHub.menuKey);

        this.menuBackKey = new OneTimeInput();
        this.menuBackKey.Init(() => this.playerInputHub.menuBackKey);

        this.menuKeyUp = new OneTimeInput();
        this.menuKeyUp.Init(() => this.playerInputHub.menuMoveInputRaw.y > 0.1f);

        this.menuKeyDown = new OneTimeInput();
        this.menuKeyDown.Init(() => this.playerInputHub.menuMoveInputRaw.y < -0.1f);

        this.menuKeyLeft = new OneTimeInput();
        this.menuKeyLeft.Init(() => this.playerInputHub.menuMoveInputRaw.x < -0.1f);

        this.menuKeyRight = new OneTimeInput();
        this.menuKeyRight.Init(() => this.playerInputHub.menuMoveInputRaw.x > 0.1f);

        this.menuOptionApplyKey = new OneTimeInput();
        this.menuOptionApplyKey.Init(() => this.playerInputHub.menuOptionApplyKey);

        this.menuPreviousCategoryKey = new OneTimeInput();
        this.menuPreviousCategoryKey.Init(() => this.playerInputHub.menuPreviousCategoryKey);

        this.menuNextCategoryKey = new OneTimeInput();
        this.menuNextCategoryKey.Init(() => this.playerInputHub.menuNextCategoryKey);
    }

    // Update is called once per frame
    private void Update()
    {
        float deltaTime = Time.deltaTime;
        this.menuKey.Update(deltaTime);
        this.menuBackKey.Update(deltaTime);
        this.menuKeyUp.Update(deltaTime);
        this.menuKeyDown.Update(deltaTime);
        this.menuKeyLeft.Update(deltaTime);
        this.menuKeyRight.Update(deltaTime);
        this.menuOptionApplyKey.Update(deltaTime);

        this.menuPreviousCategoryKey.Update(deltaTime);
        this.menuNextCategoryKey.Update(deltaTime);
    }

    public bool IsPressingMenuOptionRightKeyRepeatedly()
    {
        return this.menuKeyRight.IsPressingKeyRepeatedly();
    }

    public bool IsPressingDownMenuOptionRightKey()
    {
        return this.menuKeyRight.IsPressingDownKey();
    }

    public bool IsPressingMenuOptionLeftKeyRepeatedly()
    {
        return this.menuKeyLeft.IsPressingKeyRepeatedly();
    }

    public bool IsPressingDownMenuOptionLeftKey()
    {
        return this.menuKeyLeft.IsPressingDownKey();
    }

    public bool IsPressingDownMenuOptionApplyKey()
    {
        return this.menuOptionApplyKey.IsPressingDownKey();
    }

    public bool IsPressingDownMenuKey()
    {
        return this.menuKey.IsPressingDownKey();
    }

    public bool IsPressingDownMenuBackKey()
    {
        return this.menuBackKey.IsPressingDownKey();
    }

    public bool IsPressingMenuOptionUpKeyRepeatedly()
    {
        return this.menuKeyUp.IsPressingKeyRepeatedly();
    }

    public bool IsPressingDownMenuOptionUpKey()
    {
        return this.menuKeyUp.IsPressingDownKey();
    }

    public bool IsPressingMenuOptionDownKeyRepeatedly()
    {
        return this.menuKeyDown.IsPressingKeyRepeatedly();
    }

    public bool IsPressingDownMenuOptionDownKey()
    {
        return this.menuKeyDown.IsPressingDownKey();
    }

    public bool IsPressingDownMenuPreviousCategoryKey()
    {
        return this.menuPreviousCategoryKey.IsPressingDownKey();
    }

    public bool IsPressingDownMenuNextCategoryKey()
    {
        return this.menuNextCategoryKey.IsPressingDownKey();
    }
}
