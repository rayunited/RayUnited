﻿using Assets.Scripts.Engine.Input.InputSystem;
using Assets.Scripts.Engine.Input.Keyboard;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding;
using System.Collections.Generic;

namespace Assets.Scripts.Engine.Input
{
    public static class GameplayMouseKeyboardControlsBindingsHelper
    {
        public const string KEYBOARD_BINDING_PATH_ELEMENT = "Keyboard";
        public const string MOUSE_BINDING_PATH_ELEMENT = "Mouse";

        public static ControlBindingInfo GetCurrentMouseKeyboardUpBinding()
        {
            return ControlsBindingsHelper.GetCompositePartControlInfo(
                GameplayInputActions.MOVE,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT },
                MoveActionMouseKeyboardBindings.UP
                );
        }

        public static ControlBindingInfo SetNewMouseKeyboardUpBinding(
            ControlBindingInfo newMouseKeyboardBindingInfo)
        {
            return ControlsBindingsHelper.SetCompositePartControlBinding(
                GameplayInputActions.MOVE,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT },
                MoveActionMouseKeyboardBindings.UP,
                newMouseKeyboardBindingInfo,
                GetCurrentMouseKeyboardUpBinding
                );
        }

        public static ControlBindingInfo GetCurrentMouseKeyboardAttackBinding()
        {
            return ControlsBindingsHelper.GetRegularButtonControlInfo(
                GameplayInputActions.FIRE,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT }
                );
        }

        public static ControlBindingInfo SetNewMouseKeyboardAttackBinding(
            ControlBindingInfo newMouseKeyboardBindingInfo)
        {
            return ControlsBindingsHelper.SetRegularButtonControlBinding(
                GameplayInputActions.FIRE,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT },
                newMouseKeyboardBindingInfo,
                GetCurrentMouseKeyboardAttackBinding
                );
        }

        public static ControlBindingInfo GetCurrentMouseKeyboardDownBinding()
        {
            return ControlsBindingsHelper.GetCompositePartControlInfo(
                GameplayInputActions.MOVE,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT },
                MoveActionMouseKeyboardBindings.DOWN
                );
        }

        public static ControlBindingInfo SetNewMouseKeyboardDownBinding(
            ControlBindingInfo newMouseKeyboardBindingInfo)
        {
            return ControlsBindingsHelper.SetCompositePartControlBinding(
                GameplayInputActions.MOVE,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT },
                MoveActionMouseKeyboardBindings.DOWN,
                newMouseKeyboardBindingInfo,
                GetCurrentMouseKeyboardDownBinding
                );
        }

        public static ControlBindingInfo GetCurrentMouseKeyboardJumpBinding()
        {
            return ControlsBindingsHelper.GetRegularButtonControlInfo(
                GameplayInputActions.JUMP,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT }
                );
        }

        public static ControlBindingInfo SetNewMouseKeyboardJumpBinding(
            ControlBindingInfo newMouseKeyboardBindingInfo)
        {
            return ControlsBindingsHelper.SetRegularButtonControlBinding(
                GameplayInputActions.JUMP,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT },
                newMouseKeyboardBindingInfo,
                GetCurrentMouseKeyboardJumpBinding
                );
        }

        public static ControlBindingInfo GetCurrentMouseKeyboardWalkingBinding()
        {
            return ControlsBindingsHelper.GetRegularButtonControlInfo(
                GameplayInputActions.WALKING,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT }
                );
        }

        public static ControlBindingInfo SetNewMouseKeyboardWalkingBinding(
            ControlBindingInfo newMouseKeyboardBindingInfo)
        {
            return ControlsBindingsHelper.SetRegularButtonControlBinding(
                GameplayInputActions.WALKING,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT },
                newMouseKeyboardBindingInfo,
                GetCurrentMouseKeyboardWalkingBinding
                );
        }

        public static ControlBindingInfo GetCurrentMouseKeyboardLeftBinding()
        {
            return ControlsBindingsHelper.GetCompositePartControlInfo(
                GameplayInputActions.MOVE,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT },
                MoveActionMouseKeyboardBindings.LEFT
                );
        }

        public static ControlBindingInfo SetNewMouseKeyboardLeftBinding(
            ControlBindingInfo newMouseKeyboardBindingInfo)
        {
            return ControlsBindingsHelper.SetCompositePartControlBinding(
                GameplayInputActions.MOVE,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT },
                MoveActionMouseKeyboardBindings.LEFT,
                newMouseKeyboardBindingInfo,
                GetCurrentMouseKeyboardLeftBinding
                );
        }

        public static ControlBindingInfo GetCurrentMouseKeyboardStrafingBinding()
        {
            return ControlsBindingsHelper.GetRegularButtonControlInfo(
                GameplayInputActions.STRAFING,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT }
                );
        }

        public static ControlBindingInfo SetNewMouseKeyboardStrafingBinding(
            ControlBindingInfo newMouseKeyboardBindingInfo)
        {
            return ControlsBindingsHelper.SetRegularButtonControlBinding(
                GameplayInputActions.STRAFING,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT },
                newMouseKeyboardBindingInfo,
                GetCurrentMouseKeyboardStrafingBinding
                );
        }

        public static ControlBindingInfo GetCurrentMouseKeyboardGroundRollBinding()
        {
            return ControlsBindingsHelper.GetRegularButtonControlInfo(
                GameplayInputActions.GROUND_ROLL,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT }
                );
        }

        public static ControlBindingInfo SetNewMouseKeyboardGroundRollBinding(
            ControlBindingInfo newMouseKeyboardBindingInfo)
        {
            return ControlsBindingsHelper.SetRegularButtonControlBinding(
                GameplayInputActions.GROUND_ROLL,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT },
                newMouseKeyboardBindingInfo,
                GetCurrentMouseKeyboardGroundRollBinding
                );
        }

        public static ControlBindingInfo GetCurrentMouseKeyboardRightBinding()
        {
            return ControlsBindingsHelper.GetCompositePartControlInfo(
                GameplayInputActions.MOVE,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT },
                MoveActionMouseKeyboardBindings.RIGHT
                );
        }

        public static ControlBindingInfo SetNewMouseKeyboardRightBinding(
            ControlBindingInfo newMouseKeyboardBindingInfo)
        {
            return ControlsBindingsHelper.SetCompositePartControlBinding(
                GameplayInputActions.MOVE,
                new List<string>() {
                    KEYBOARD_BINDING_PATH_ELEMENT, MOUSE_BINDING_PATH_ELEMENT },
                MoveActionMouseKeyboardBindings.RIGHT,
                newMouseKeyboardBindingInfo,
                GetCurrentMouseKeyboardRightBinding
                );
        }
    }
}
