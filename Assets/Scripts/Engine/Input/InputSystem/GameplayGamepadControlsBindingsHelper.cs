﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding;
using System.Collections.Generic;

namespace Assets.Scripts.Engine.Input.InputSystem
{
    public static class GameplayGamepadControlsBindingsHelper
    {
        public const string GAMEPAD_BINDING_PATH_ELEMENT = "Gamepad";
        public const int GAMEPAD_FIRE_PRIMARY_BINDING_INDEX = 0;
        public const int GAMEPAD_FIRE_SECONDARY_BINDING_INDEX = 1;

        public static ControlBindingInfo GetCurrentGamepadJumpBinding()
        {
            return ControlsBindingsHelper.GetRegularButtonControlInfo(
                GameplayInputActions.JUMP,
                new List<string>() { GAMEPAD_BINDING_PATH_ELEMENT }
                );
        }

        public static ControlBindingInfo SetNewGamepadJumpBinding(
            ControlBindingInfo newGamepadBindingInfo)
        {
            return ControlsBindingsHelper.SetRegularButtonControlBinding(
                GameplayInputActions.JUMP,
                new List<string>() { GAMEPAD_BINDING_PATH_ELEMENT },
                newGamepadBindingInfo,
                GetCurrentGamepadJumpBinding
                );
        }

        public static ControlBindingInfo
            GetCurrentGamepadAttackPrimaryBinding()
        {
            return ControlsBindingsHelper.GetRegularButtonControlInfo(
                GameplayInputActions.FIRE,
                new List<string>() { GAMEPAD_BINDING_PATH_ELEMENT }
                );
        }

        public static ControlBindingInfo
            SetNewGamepadAttackPrimaryBinding(
            ControlBindingInfo newGamepadBindingInfo)
        {
            return ControlsBindingsHelper.SetRegularButtonControlBinding(
                GameplayInputActions.FIRE,
                new List<string>() { GAMEPAD_BINDING_PATH_ELEMENT },
                newGamepadBindingInfo,
                GetCurrentGamepadAttackPrimaryBinding
                );
        }

        public static ControlBindingInfo
            GetCurrentGamepadAttackSecondaryBinding()
        {
            return ControlsBindingsHelper.GetRegularButtonControlInfo(
                GameplayInputActions.FIRE_2,
                new List<string>() { GAMEPAD_BINDING_PATH_ELEMENT }
                );
        }

        public static ControlBindingInfo
            SetNewGamepadAttackSecondaryBinding(
            ControlBindingInfo newGamepadBindingInfo)
        {
            return ControlsBindingsHelper.SetRegularButtonControlBinding(
                GameplayInputActions.FIRE_2,
                new List<string>() { GAMEPAD_BINDING_PATH_ELEMENT },
                newGamepadBindingInfo,
                GetCurrentGamepadAttackSecondaryBinding
                );
        }

        public static ControlBindingInfo
            GetCurrentGamepadGroundRollBinding()
        {
            return ControlsBindingsHelper.GetRegularButtonControlInfo(
                GameplayInputActions.GROUND_ROLL,
                new List<string>() { GAMEPAD_BINDING_PATH_ELEMENT }
                );
        }

        public static ControlBindingInfo
            SetNewGamepadGroundRollBinding(
            ControlBindingInfo newGamepadBindingInfo)
        {
            return ControlsBindingsHelper.SetRegularButtonControlBinding(
                GameplayInputActions.GROUND_ROLL,
                new List<string>() { GAMEPAD_BINDING_PATH_ELEMENT },
                newGamepadBindingInfo,
                GetCurrentGamepadGroundRollBinding
                );
        }

        public static ControlBindingInfo
            GetCurrentGamepadStrafingBinding()
        {
            return ControlsBindingsHelper.GetRegularButtonControlInfo(
                GameplayInputActions.STRAFING,
                new List<string>() { GAMEPAD_BINDING_PATH_ELEMENT }
                );
        }

        public static ControlBindingInfo
            SetNewGamepadStrafingBinding(
            ControlBindingInfo newGamepadBindingInfo)
        {
            return ControlsBindingsHelper.SetRegularButtonControlBinding(
                GameplayInputActions.STRAFING,
                new List<string>() { GAMEPAD_BINDING_PATH_ELEMENT },
                newGamepadBindingInfo,
                GetCurrentGamepadStrafingBinding
                );
        }
    }
}
