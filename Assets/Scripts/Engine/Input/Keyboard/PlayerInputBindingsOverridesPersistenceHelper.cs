﻿using UnityEngine;

namespace Assets.Scripts.Engine.Input
{
    public static class PlayerInputBindingsOverridesPersistenceHelper
    {
        public const string PLAYER_INPUT_BINDINGS_OVERRIDES =
            "PLAYER_INPUT_BINDINGS_OVERRIDES";

        public static void SaveBindingsOverridesAsJson(
            string inputBindingsOverridesJson
            )
        {
            PlayerPrefs.SetString(
                PLAYER_INPUT_BINDINGS_OVERRIDES,
                inputBindingsOverridesJson);
        }

        public static string? LoadBindingsOverridesAsJson()
        {
            return PlayerPrefs.GetString(
                PLAYER_INPUT_BINDINGS_OVERRIDES, null);
        }
    }
}
