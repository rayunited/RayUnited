﻿using UnityEngine;

namespace Assets.Scripts.Engine.Behaviours
{
    public abstract class GameplayOnlyMonoBehaviour : MonoBehaviour
    {
        protected GameStateInfo gameStateInfo;

        private void Start()
        {
            this.gameStateInfo = FindObjectOfType<GameStateInfo>();
            BehaviourStart();
        }

        private void Update()
        {
            if (this.gameStateInfo.IsGameplayState())
            {
                GameplayUpdate(Time.deltaTime);
            }
        }

        private void FixedUpdate()
        {
            if (this.gameStateInfo.IsGameplayState())
            {
                GameplayFixedUpdate();
            }
        }

        private void OnGUI()
        {
            if (this.gameStateInfo.IsGameplayState())
            {
                GameplayOnGUI();
            }
        }

        protected virtual void BehaviourStart() { }

        protected virtual void GameplayOnGUI() { }
        protected virtual void GameplayUpdate(float deltaTime) { }
        protected virtual void GameplayFixedUpdate() { }
    }
}
