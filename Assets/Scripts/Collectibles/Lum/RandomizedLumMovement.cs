﻿using Assets.Scripts.Common;
using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.Movement;
using UnityEngine;

namespace Assets.Scripts.Collectibles.Lum
{
    public class RandomizedLumMovement : GameplayOnlyMonoBehaviour
    {
        public float movementAreaRadius = 1f;

        private float currentFloatingProgression = 0f;
        private float floatingProgressionStep = 0.1f;

        private Vector3 originalPosition;
        private Vector3 positionOffset;

        private Vector3 currentPositionWithoutUpDown;
        private Vector3 currentTargetPosition;

        private Vector3 currentVelocity;
        private float maxVelocity = 0.05f;

        private GameObject playerObject;

        private float movementDirectionRotationAngularSpeed = Mathf.Deg2Rad * 5f;

        private bool isBeingPulledTowardsThePlayer = false;

        protected override void BehaviourStart()
        {
            this.originalPosition = this.transform.position;
            this.currentFloatingProgression = Mathf.Deg2Rad * 360f * UnityEngine.Random.Range(0f, 1f);
            this.positionOffset = Vector3.zero;
            this.currentVelocity = Vector3.zero;
            this.currentTargetPosition = this.originalPosition;
            this.playerObject = FindObjectOfType<PlayerActorHandle>().gameObject;
            this.currentPositionWithoutUpDown = this.originalPosition;
            this.isBeingPulledTowardsThePlayer = false;
            InvokeRepeating("DrawNextTargetPosition",
                UnityEngine.Random.Range(0f, 5f), UnityEngine.Random.Range(1f, 7f));
        }

        public void BeginBeingPulledTowardsThePlayer()
        {
            this.isBeingPulledTowardsThePlayer = true;
        }

        public void StopBeingPulledTowardsThePlayer()
        {
            this.isBeingPulledTowardsThePlayer = false;
        }

        private void DrawNextTargetPosition()
        {
            Vector3 randomizedPositionOffset;

            while (true)
            {
                randomizedPositionOffset = new Vector3(
                    x: this.movementAreaRadius * UnityEngine.Random.Range(-1f, 1f),
                    y: this.movementAreaRadius * UnityEngine.Random.Range(-1f, 1f),
                    z: this.movementAreaRadius * UnityEngine.Random.Range(-1, 1f)
                );
                bool willHitTheWall = PhysicsRaycaster.Raycast(this.originalPosition,
                    randomizedPositionOffset.normalized,
                    randomizedPositionOffset.magnitude * 1.6f,
                    Layers.generalEnvironmentLayersMask);
                if (!willHitTheWall)
                {
                    this.currentTargetPosition = this.originalPosition + randomizedPositionOffset;
                    return;
                }
            }
        }

        protected override void GameplayFixedUpdate()
        {
            if (this.gameObject)
            {
                this.transform.position = this.currentPositionWithoutUpDown + Vector3.up * 0.25f *
                    Mathf.Sin(this.currentFloatingProgression);
                this.currentFloatingProgression += this.floatingProgressionStep;

                Vector3 translationToCurrentDestination;

                if (!this.isBeingPulledTowardsThePlayer)
                {
                    translationToCurrentDestination = this.currentTargetPosition - this.currentPositionWithoutUpDown;
                }
                else
                {
                    translationToCurrentDestination =
                        this.playerObject.transform.position + Vector3.up - this.currentPositionWithoutUpDown;
                }

                bool isGettingCloserToPlayer = translationToCurrentDestination.magnitude
                    < this.currentVelocity.magnitude;

                this.currentVelocity =
                    RandomizedFairyLikeMovementUtils.EvaluateVelocityForFairyLikeMovement(
                        this.currentVelocity,
                        translationToCurrentDestination,
                        this.movementDirectionRotationAngularSpeed,
                        this.maxVelocity
                        );

                //currentVelocity = Vector3.RotateTowards(
                //    currentVelocity, translationToCurrentDestination,
                //    !isBeingPulledTowardsThePlayer ? movementDirectionRotationAngularSpeed : 360f * Mathf.Deg2Rad, 
                //    !isBeingPulledTowardsThePlayer ? maxVelocity / 5 : maxVelocity * 5f);
                //currentVelocity = Vector3.ClampMagnitude(currentVelocity,
                //    !isBeingPulledTowardsThePlayer ? maxVelocity : maxVelocity * 5f);

                this.currentPositionWithoutUpDown = this.currentPositionWithoutUpDown + this.currentVelocity;
            }
        }
    }
}
