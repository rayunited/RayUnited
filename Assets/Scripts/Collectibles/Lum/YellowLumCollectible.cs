﻿using Assets.Scripts.Engine.Behaviours;
using UnityEngine;

namespace Assets.Scripts.Collectibles
{
    public class YellowLumCollectible : GameplayOnlyMonoBehaviour, ICollectible
    {
        private bool isToBeDestroyed = false;
        private bool canBeCollected = true;

        private AudioSource collectSound;

        protected override void BehaviourStart()
        {
            this.isToBeDestroyed = false;
            this.canBeCollected = true;
            this.collectSound = GetComponent<AudioSource>();
        }

        public bool IsCanBeCollected()
        {
            return this.canBeCollected;
        }

        public void InvokeCollectedBehaviour(Transform playerTransform)
        {
            if (this.canBeCollected)
            {
                this.isToBeDestroyed = true;
                this.canBeCollected = false;
                this.collectSound.Play();
                this.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
            }
        }

        protected override void GameplayFixedUpdate()
        {
            if (this.isToBeDestroyed && this.gameObject && !this.collectSound.isPlaying)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
