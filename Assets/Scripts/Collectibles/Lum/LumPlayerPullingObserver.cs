﻿using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.PlayerMechanics;
using UnityEngine;

namespace Assets.Scripts.Collectibles.Lum
{
    public class LumPlayerPullingObserver : GameplayOnlyMonoBehaviour
    {
        private GameObject playerObject;
        private float pullingTriggerProximityRadius = 6f;
        private RandomizedLumMovement lumMovementBehaviour;

        protected override void BehaviourStart()
        {
            this.playerObject = FindObjectOfType<PlayerActorHandle>().gameObject;
            this.lumMovementBehaviour = GetComponent<RandomizedLumMovement>();
        }

        protected override void GameplayFixedUpdate()
        {
            if (Vector3.Distance(this.playerObject.transform.position, this.transform.position)
                <= this.pullingTriggerProximityRadius)
            {
                this.lumMovementBehaviour.BeginBeingPulledTowardsThePlayer();
            }
            else
            {
                this.lumMovementBehaviour.StopBeingPulledTowardsThePlayer();
            }
        }
    }
}
