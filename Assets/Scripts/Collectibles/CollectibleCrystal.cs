using Assets.Scripts.Collectibles;
using Assets.Scripts.Engine.Behaviours;
using UnityEngine;

public class CollectibleCrystal : GameplayOnlyMonoBehaviour, ICollectible
{
    public bool canBeCollected = true;
    public float rotationSpeed = 1.0f;

    private bool isInMiddleOfBeingCollected = false;
    private Transform playerTransform;

    private AudioSource collectedSound;

    private float currentCollectingRotationAngle = 0f;
    private float collectingRotationSpeed = 20f;
    private float collectingAscendingSpeed = 0.6f;

    private float crystalDestroyHeightThreshold = 100f;

    private float currentHeight;

    public void InvokeCollectedBehaviour(Transform playerTransform)
    {
        if (this.canBeCollected)
        {
            this.canBeCollected = false;
            this.isInMiddleOfBeingCollected = true;
            this.playerTransform = playerTransform;
            this.currentHeight = playerTransform.position.y + 1.0f;
            //this.collectedSound.volume = 0.1f;
            this.collectedSound.Play();
        }
    }

    public bool IsCanBeCollected()
    {
        return this.canBeCollected;
    }

    private void Awake()
    {
        this.collectedSound = GetComponent<AudioSource>();
    }

    protected override void GameplayFixedUpdate()
    {
        if (this.isInMiddleOfBeingCollected)
        {
            this.transform.position = this.playerTransform.position +
                Quaternion.AngleAxis(this.currentCollectingRotationAngle, Vector3.up) * Vector3.forward;
            this.transform.position = new Vector3(this.transform.position.x, this.currentHeight, this.transform.position.z);

            if (this.currentCollectingRotationAngle > 360f)
            {
                this.transform.position = new Vector3(
                    this.playerTransform.position.x, this.currentHeight,
                    this.playerTransform.position.z);
                this.currentHeight += this.collectingAscendingSpeed;
            }
            else
            {
                this.currentCollectingRotationAngle += this.collectingRotationSpeed;
            }

            if (this.currentHeight > this.playerTransform.position.y + this.crystalDestroyHeightThreshold)
            {
                Debug.Log("Crystal destroyed!");
                Destroy(this.gameObject);
            }
        }
        else
        {
            this.transform.Rotate(new Vector3(0f, this.rotationSpeed, 0f));
        }
    }
}
