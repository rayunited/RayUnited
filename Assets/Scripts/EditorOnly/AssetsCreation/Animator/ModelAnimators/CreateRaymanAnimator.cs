﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.Animations.Models.Rayman;
using Assets.Scripts.EditorOnly.AssetsCreation.Animator;
using System.Collections.Generic;
using System.Linq;
#if (UNITY_EDITOR)
#endif
using UnityEngine;

public class CreateRaymanAnimator : MonoBehaviour
{
#if (UNITY_EDITOR)
    private AnimatorFactory animatorFactory = new AnimatorFactory();

    private void Start()
    {
        List<string> loopedAnimations = new List<string>(
            RaymanAnimations.animations.Values.Where(x => x.isLooped).Select(x => x.animationName));

        List<Assets.Scripts.Animations.Models.Model.AnimationClipConstructingRecipe> additionalDerivedAnimationsConstructingRecipes =
            RaymanDerivedAnimationStates.animationClipConstructingRecipes;

        Animator animator = GetComponent<Animator>();

        Transform actualProperArmatureRootBone =
            GetComponentsInChildren<Transform>().Where(x => x.name.Equals("ROOT_CHANNEL")).First();
        List<Transform> animatorProperBones = actualProperArmatureRootBone
            .GetComponentsInChildren<Transform>()
            .Where(x => !x.name.Contains("_end")).ToList();

        Debug.Log("Creating Rayman animator");
        this.animatorFactory.CreateAnimatorFor(
            this.gameObject,
            "Assets/Models/RaymanTest/Animations/",
            "Assets/Models/RaymanTest/AdditionalAnims/",
            "Assets/Models/RaymanTest/raymanAnimator.controller",
            "Assets/Models/RaymanTest/rayman3_with_helicopter_all_clips",
            RaymanAnimations.animations,
            additionalDerivedAnimationsConstructingRecipes,
            animator.transform,
            animatorProperBones);
    }
#endif
}
