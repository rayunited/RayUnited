﻿using UnityEngine;

namespace Assets.Scripts.HUD.EditorGUI
{
    public class CursorsHolder : MonoBehaviour
    {
        public Texture2D primaryCursor;
        public Texture2D focusCursor;
    }
}
