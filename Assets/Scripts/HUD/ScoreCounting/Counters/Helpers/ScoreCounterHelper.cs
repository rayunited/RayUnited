﻿using Assets.Scripts.HUD.ScoreCounting.Counters.Helpers;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.HUD.ScoreCounting
{
    public class ScoreCounterHelper : ScoreCounterHelperBase
    {
        public ScoreCounterHelper() : base(new Rect(0, 0.6752577f, 0.8917526f, 0.3247423f)) { }

        public override void DrawScoreCounter(
            float x, float y, float width, float height,
            List<float> digitsScrollsFrom0To10,
            Texture2D scoreCounterHUDTexture)
        {
            Rect scoreCounterFrameRect = new Rect(
                x, y, 226, 86);

            this.scoreDigitsWithNumberHelper.DrawNumberFromScrolledDigits(
                scoreCounterFrameRect.x + 17,
                scoreCounterFrameRect.y + 15,
                32,
                48,
                4,
                digitsScrollsFrom0To10,
                5,
                scoreCounterHUDTexture
            );

            // draw score counter frame on top of everything
            GUI.DrawTextureWithTexCoords(scoreCounterFrameRect, scoreCounterHUDTexture,
               this.scoreCounterFrameTextCoords);
        }
    }
}
