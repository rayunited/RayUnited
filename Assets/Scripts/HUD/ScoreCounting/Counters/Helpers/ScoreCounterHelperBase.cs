﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.HUD.ScoreCounting.Counters.Helpers
{
    public abstract class ScoreCounterHelperBase
    {
        protected Rect scoreCounterFrameTextCoords;
        protected ScoreDigitsWithNumberHelper scoreDigitsWithNumberHelper;
        protected ScoreCounterBase scoreCounter;

        public ScoreCounterHelperBase(Rect scoreCounterFrameTextCoords)
        {
            this.scoreCounterFrameTextCoords = scoreCounterFrameTextCoords;
            this.scoreDigitsWithNumberHelper = new ScoreDigitsWithNumberHelper();
        }

        public virtual void SetScoreCounter(ScoreCounterBase scoreCounter)
        {
            this.scoreCounter = scoreCounter;
        }

        public abstract void DrawScoreCounter(
            float x, float y, float width, float height,
            List<float> digitsScrollsFrom0To10,
            Texture2D scoreCounterHUDTexture);
    }
}
