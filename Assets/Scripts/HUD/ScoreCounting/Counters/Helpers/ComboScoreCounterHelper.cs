﻿using Assets.Scripts.HUD.ScoreCounting.Counters;
using Assets.Scripts.HUD.ScoreCounting.Counters.Helpers;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.HUD.ScoreCounting
{
    public class ComboScoreCounterHelper : ScoreCounterHelperBase
    {
        protected ComboScoreCounter comboScoreCounter;

        public ComboScoreCounterHelper() :
            base(new Rect(0, 0.5128866f, 0.8350515f, 0.1546392f))
        { }

        public override void SetScoreCounter(ScoreCounterBase scoreCounter)
        {
            base.SetScoreCounter(scoreCounter);
            this.comboScoreCounter = scoreCounter as ComboScoreCounter;
        }

        public override void DrawScoreCounter(
            float x, float y, float width, float height,
            List<float> digitsScrollsFrom0To10,
            Texture2D scoreCounterHUDTexture)
        {
            Rect comboScoreCounterFrameRect = new Rect(
                x, y, 208, 39);

            if (this.comboScoreCounter.IsComboOngoing())
            {
                this.scoreDigitsWithNumberHelper.DrawNumberFromScrolledDigits(
                    comboScoreCounterFrameRect.x + 99,
                    comboScoreCounterFrameRect.y + 85,
                    22,
                    31,
                    4,
                    digitsScrollsFrom0To10,
                    4,
                    scoreCounterHUDTexture
                );

                // draw combo score counter frame
                GUI.DrawTextureWithTexCoords(
                    new Rect(
                        comboScoreCounterFrameRect.x,
                        comboScoreCounterFrameRect.y + 80,
                        comboScoreCounterFrameRect.width,
                        comboScoreCounterFrameRect.height), scoreCounterHUDTexture,
                   this.scoreCounterFrameTextCoords);
            }
        }
    }
}
