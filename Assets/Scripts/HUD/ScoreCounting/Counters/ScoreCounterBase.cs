﻿using Assets.Scripts.HUD.ScoreCounting.Counters.Helpers;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.HUD.ScoreCounting.Counters
{
    public abstract class ScoreCounterBase
    {
        protected float currentScore;
        protected ScoreCounterHelperBase scoreCounterHelper;

        protected List<float> digitsScrollsFrom0To10;
        protected List<float> digitsScrollsOnBeginningOfCurrentCounterNormalization;

        protected bool counterNormalizationOngoing;

        protected float lastScoreAccumulationTime;
        protected int counterNormalizationWaitTimeThresholdMilliseconds;

        protected int normalizationTimeMilliseconds;

        protected float animatedNormalizationMaxTime;
        protected float normalizationStartTime;

        protected int digitsAmount;

        protected ScoreAccumulatorDeccumulator scoreAccumulatorDeccumulator;

        public ScoreCounterBase(
            ScoreCounterHelperBase scoreCounterHelper,
            int counterNormalizationWaitTimeThresholdMilliseconds,
            int normalizationTimeMilliseconds,
            float animatedNormalizationMaxTime,
            int digitsAmount,
            ScoreAccumulatorDeccumulator scoreAccumulatorDeccumulator,
            float initialScore = 0f)
        {
            this.currentScore = initialScore;
            this.scoreCounterHelper = scoreCounterHelper;
            this.lastScoreAccumulationTime = Time.time;
            this.normalizationStartTime = Time.time;
            this.counterNormalizationWaitTimeThresholdMilliseconds =
                counterNormalizationWaitTimeThresholdMilliseconds;
            this.normalizationTimeMilliseconds = normalizationTimeMilliseconds;

            this.animatedNormalizationMaxTime = animatedNormalizationMaxTime;

            this.digitsAmount = digitsAmount;

            this.digitsScrollsFrom0To10 = new List<float>();
            this.digitsScrollsOnBeginningOfCurrentCounterNormalization = new List<float>();

            this.scoreAccumulatorDeccumulator = scoreAccumulatorDeccumulator;

            for (int i = 0; i < digitsAmount; i++)
            {
                this.digitsScrollsFrom0To10.Add(0f);
                this.digitsScrollsOnBeginningOfCurrentCounterNormalization.Add(0f);
            }

            //this.digitsScrollsFrom0To10 =
            //    new List<float>() { 0f, 0f, 0f, 0f, 0f };
            //this.digitsScrollsOnBeginningOfCurrentCounterNormalization =
            //    new List<float>() { 0f, 0f, 0f, 0f, 0f };
            this.counterNormalizationOngoing = false;
        }

        public virtual void AccumulateScore(float accumulation)
        {
            this.currentScore += accumulation;
            this.lastScoreAccumulationTime = Time.time;
            DigitsScrollsHelper
                .AccumulateScrollsWithScoreNumberStep(
                    this.digitsScrollsFrom0To10, accumulation);
        }

        public virtual void DeccumulateScore(float deccumulation)
        {
            this.currentScore -= deccumulation;
            this.lastScoreAccumulationTime = Time.time;
            DigitsScrollsHelper
                .DecummulateScrollsWithScoreNumberStep(
                    this.digitsScrollsFrom0To10, deccumulation);
        }

        public virtual void Draw(
            float x, float y,
            float width, float height,
            Texture2D playerScoreCounterTexture)
        {
            this.scoreCounterHelper.DrawScoreCounter(
                x, y,
                width, height,
                this.digitsScrollsFrom0To10,
                playerScoreCounterTexture
                );
        }

        public virtual void FixedUpdate()
        {
            float currentTime = Time.time;
            if (currentTime - this.lastScoreAccumulationTime >
                (this.counterNormalizationWaitTimeThresholdMilliseconds / 1000f) || this.counterNormalizationOngoing)
            {
                if (!this.counterNormalizationOngoing)
                {
                    //Debug.Log("Starting counter normalization..., time difference " +
                    //    (currentTime - lastScoreAccumulationTime));
                    // we start normalization procedure -
                    // this if is accessed only once during each start of normalization
                    this.digitsScrollsOnBeginningOfCurrentCounterNormalization =
                        new List<float>(this.digitsScrollsFrom0To10);

                    this.normalizationStartTime = Time.time;
                }

                this.counterNormalizationOngoing = true;

                if (currentTime - this.normalizationStartTime < this.animatedNormalizationMaxTime)
                {
                    NormalizeCounterTowardsNumber(this.currentScore);
                }
                else
                {
                    // this is taking too long, just force the counter to particular number
                    SetCounterToDisplayNumber(this.currentScore);
                    this.counterNormalizationOngoing = false;
                }
            }
            else
            {
                this.counterNormalizationOngoing = false;
            }
        }

        protected void SetCounterToDisplayNumber(float number)
        {
            DigitsScrollsHelper.SetDigitsScrollsToReflectNumber(
                this.digitsScrollsFrom0To10, number);
        }

        protected void NormalizeCounterTowardsNumber(float number)
        {
            for (int digitIndex = 0;
                digitIndex < this.digitsScrollsFrom0To10.Count;
                digitIndex++)
            {
                int properDigitIndex = this.digitsScrollsFrom0To10.Count - digitIndex - 1;
                this.digitsScrollsFrom0To10[properDigitIndex] +=
                    DigitsScrollsHelper
                        .GetDigitScrollNormalizationInTimeStep(
                            number, properDigitIndex,
                            this.digitsScrollsFrom0To10[properDigitIndex],
                            this.digitsScrollsOnBeginningOfCurrentCounterNormalization[properDigitIndex],
                            this.normalizationTimeMilliseconds);
                this.digitsScrollsFrom0To10[properDigitIndex] = this.digitsScrollsFrom0To10[properDigitIndex] % 10f;
            }
        }
    }
}
