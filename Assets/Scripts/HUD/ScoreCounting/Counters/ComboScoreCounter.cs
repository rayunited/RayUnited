﻿using Assets.Scripts.HUD.ScoreCounting.Counters;
using UnityEngine;

namespace Assets.Scripts.HUD.ScoreCounting
{
    public class ComboScoreCounter : ScoreCounterBase
    {
        //private ScoreCounter mainScoreCounter;
        //private float offsetY;
        private bool isComboOngoing;
        private bool isComboDeccumulationOngoing;

        private float comboDeactivationMaxTimeWindowSeconds;

        private float lastComboScoreAccumulationTime;

        //private float comboActivatedYOffsetThreshold = 10f;

        public ComboScoreCounter(
            ScoreAccumulatorDeccumulator scoreAccumulatorDeccumulator) :
           base(new ComboScoreCounterHelper(), 50, 250, 0.5f, 5, scoreAccumulatorDeccumulator)
        {
            this.scoreCounterHelper.SetScoreCounter(this);
            //this.mainScoreCounter = mainScoreCounter;
            this.lastComboScoreAccumulationTime = Time.time;
            //this.offsetY = 0;
            this.isComboOngoing = false;
            this.comboDeactivationMaxTimeWindowSeconds = 1f;
        }

        public override void AccumulateScore(float accumulation)
        {
            base.AccumulateScore(accumulation);
            this.isComboOngoing = true;
            this.lastComboScoreAccumulationTime = Time.time;
            this.scoreAccumulatorDeccumulator.CancelCurrentComboDeccumulationAltogether();
            this.isComboDeccumulationOngoing = false;
        }

        public override void DeccumulateScore(float deccumulation)
        {
            base.DeccumulateScore(deccumulation);
            this.lastComboScoreAccumulationTime = Time.time;
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();
            float currentTime = Time.time;
            if (currentTime - this.lastComboScoreAccumulationTime > this.comboDeactivationMaxTimeWindowSeconds)
            {
                if (this.currentScore > 0.1f && !this.isComboDeccumulationOngoing)
                {
                    this.scoreAccumulatorDeccumulator.DeccumulateComboPointsWithMainPointsScoringInTime(this.currentScore, 1000);
                    //scoreAccumulatorDeccumulator.ScorePointsWithoutComboInTime(currentScore, 1000);
                    this.isComboDeccumulationOngoing = true;
                }
                else if (this.isComboDeccumulationOngoing)
                {
                    this.isComboDeccumulationOngoing = false;
                    // give player yet another brief moment after current deccumulation to have him have
                    // a chance to lengthen the combo
                    this.lastComboScoreAccumulationTime = Time.time;
                    //isComboOngoing = false;
                }
                else
                {
                    this.isComboOngoing = false;
                }
            }
        }

        public bool IsComboOngoing()
        {
            return this.isComboOngoing;
        }
    }
}
