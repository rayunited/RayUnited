﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.HUD.ScoreCounting
{
    public static class DigitsHelper
    {
        public static float
            GetDigitStartTextureCoordinateY(
            List<float> digitsTexCoordsYThresholds,
            float digitScrollFrom0To10)
        {
            int periodIndex = ((int)digitScrollFrom0To10) % 10;
            float segment = 0f;
            if (periodIndex < 9)
            {
                segment = digitsTexCoordsYThresholds[periodIndex + 1] - digitsTexCoordsYThresholds[periodIndex];
            }
            else
            {
                segment = digitsTexCoordsYThresholds[0] + 1f - digitsTexCoordsYThresholds[periodIndex];
            }

            float startThreshold = digitsTexCoordsYThresholds[periodIndex];
            return startThreshold + (digitScrollFrom0To10 - periodIndex) * segment;
        }
    }

    public class ScoreDigitsWithNumberHelper
    {
        private Rect baseDigitTexFrameRect;
        private List<float> digitsTexCoordsYThresholds;

        public ScoreDigitsWithNumberHelper()
        {
            this.baseDigitTexFrameRect = new Rect(
                0.9136598f,
                0.06314433f,
                0.07731956f,
                0.09793814f
                );

            this.digitsTexCoordsYThresholds = new List<float>
            {

                // zero
                0.07421875f,

                // one
                0.1640625f,

                // two
                0.25390625f,

                // three
                0.3515625f,

                // four
                0.4453125f,

                // five
                0.5390625f,

                // six
                0.6328125f,

                // seven
                0.71875f,

                // eight
                0.81640625f,

                // nine
                0.9140625f
            };
        }

        public void DrawNumberFromScrolledDigits(
            float x,
            float y,
            float digitCellWidth,
            float digitCellHeight,
            float digitCellsInterval,
            List<float> digitsScrollsFrom0To10,
            int digitCellsAmount,
            Texture2D scoreCounterTexture)
        {
            for (int digitIndex = 0; digitIndex < digitCellsAmount; digitIndex++)
            {
                float digitCellX = x + ((digitCellWidth + digitCellsInterval) * digitIndex);
                float digitCellY = y;

                DrawDigitCell(
                    digitCellX,
                    digitCellY,
                    digitCellWidth,
                    digitCellHeight,
                    digitsScrollsFrom0To10[digitCellsAmount - digitIndex - 1],
                    scoreCounterTexture);
            }
        }

        private void DrawDigitCell(
            float x,
            float y,
            float digitCellWidth,
            float digitCellHeight,
            float digitScrollFrom0To10,
            Texture2D scoreCounterTexture)
        {
            float digitStartTextureCoordinateY = DigitsHelper.GetDigitStartTextureCoordinateY(
                this.digitsTexCoordsYThresholds,
                digitScrollFrom0To10
                );

            Rect digitCellRect = new Rect(x, y, digitCellWidth, digitCellHeight);
            Rect digitTextureCoordinatesRect = new Rect(
                this.baseDigitTexFrameRect.x, digitStartTextureCoordinateY,
                this.baseDigitTexFrameRect.width, this.baseDigitTexFrameRect.height);

            GUI.DrawTextureWithTexCoords(digitCellRect, scoreCounterTexture,
                digitTextureCoordinatesRect);
        }
    }
}
