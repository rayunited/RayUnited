﻿using Assets.Scripts.Engine.Behaviours;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.HUD.ScoreCounting
{
    public class ScoreAccumulatorDeccumulator : GameplayOnlyMonoBehaviour
    {
        private ScoreCounter scoreCounter;
        private float lastScoringTime;
        private float comboMaxTimeWindowActivationThresholdSeconds;

        protected override void BehaviourStart()
        {
            this.scheduledScorings = new List<ScheduledScoring>();
            this.scheduledComboScorings = new List<ScheduledScoring>();
            this.scheduledFromComboToMainScorings = new List<ScheduledScoring>();
            this.lastScoringTime = Time.time;
            this.comboMaxTimeWindowActivationThresholdSeconds = 1f; // 1 second
        }

        public void SetScoreCounter(ScoreCounter scoreCounter)
        {
            this.scoreCounter = scoreCounter;
        }

        private int latestScoringId = 0;
        private int latestFromComboToMainScoringId = 0;
        private List<ScheduledScoring> scheduledScorings;
        private List<ScheduledScoring> scheduledComboScorings;

        private List<ScheduledScoring> scheduledFromComboToMainScorings;

        public void ScorePointsInTime(float amount, int milliseconds)
        {
            int ticksPerSecond = (int)(1f / Time.fixedDeltaTime);
            int ticksToPerformScoringIn = (int)((milliseconds / 1000f) * ticksPerSecond);
            float scoreStepInTick = amount / ticksToPerformScoringIn;

            this.scheduledScorings.Add(
                new ScheduledScoring(this.latestScoringId, scoreStepInTick,
                    ticksToPerformScoringIn));
            this.latestScoringId++;
            float currentTime = Time.time;
            if (currentTime - this.lastScoringTime <
                this.comboMaxTimeWindowActivationThresholdSeconds ||
                this.scoreCounter.IsComboOngoing())
            {
                this.scheduledComboScorings.Add(
                    new ScheduledScoring(this.latestScoringId, scoreStepInTick,
                        ticksToPerformScoringIn));
            }

            this.lastScoringTime = Time.time;
        }

        public void CancelCurrentComboDeccumulationAltogether()
        {
            this.scheduledFromComboToMainScorings.Clear();
            this.latestFromComboToMainScoringId = 0;
        }

        public void DeccumulateComboPointsWithMainPointsScoringInTime(float amount, int milliseconds)
        {
            int ticksPerSecond = (int)(1f / Time.fixedDeltaTime);
            int ticksToPerformScoringIn = (int)((milliseconds / 1000f) * ticksPerSecond);
            float scoreStepInTick = amount / ticksToPerformScoringIn;

            this.scheduledFromComboToMainScorings.Add(
                new ScheduledScoring(this.latestFromComboToMainScoringId, scoreStepInTick,
                    ticksToPerformScoringIn));
            this.latestFromComboToMainScoringId++;
        }

        protected override void GameplayFixedUpdate()
        {
            this.scoreCounter.FixedUpdate();
            List<int> scoringsToDelete = new List<int>();
            List<int> comboScoringsToDelete = new List<int>();
            List<int> fromComboScoringsToMainToDelete = new List<int>();

            if (this.scheduledScorings.Count > 0)
            {
                for (int i = 0; i < this.scheduledScorings.Count; i++)
                {
                    ScheduledScoring scheduledScoring = this.scheduledScorings[i];
                    if (scheduledScoring.fixedUpdatesLeft <= 0)
                    {
                        scoringsToDelete.Add(scheduledScoring.id);
                    }
                    else
                    {
                        this.scoreCounter.AccumulateScore(scheduledScoring.scoreStepInTick);
                        scheduledScoring.fixedUpdatesLeft--;
                    }
                }
            }

            if (this.scheduledComboScorings.Count > 0)
            {
                for (int i = 0; i < this.scheduledComboScorings.Count; i++)
                {
                    ScheduledScoring scheduledComboScoring = this.scheduledComboScorings[i];
                    if (scheduledComboScoring.fixedUpdatesLeft <= 0)
                    {
                        comboScoringsToDelete.Add(scheduledComboScoring.id);
                    }
                    else
                    {
                        this.scoreCounter.AccumulateComboScore(scheduledComboScoring.scoreStepInTick);
                        scheduledComboScoring.fixedUpdatesLeft--;
                    }
                }
            }


            if (this.scheduledFromComboToMainScorings.Count > 0)
            {
                for (int i = 0; i < this.scheduledFromComboToMainScorings.Count; i++)
                {
                    ScheduledScoring scheduledFromComboToMainScoring = this.scheduledFromComboToMainScorings[i];
                    if (scheduledFromComboToMainScoring.fixedUpdatesLeft <= 0)
                    {
                        fromComboScoringsToMainToDelete.Add(scheduledFromComboToMainScoring.id);
                    }
                    else
                    {
                        this.scoreCounter.AccumulateScore(scheduledFromComboToMainScoring.scoreStepInTick);
                        this.scoreCounter.DeccumulateComboScore(scheduledFromComboToMainScoring.scoreStepInTick);
                        scheduledFromComboToMainScoring.fixedUpdatesLeft--;
                    }
                }
            }

            this.scheduledComboScorings.RemoveAll(x => comboScoringsToDelete.Contains(x.id));
            this.scheduledScorings.RemoveAll(x => scoringsToDelete.Contains(x.id));
            this.scheduledFromComboToMainScorings.RemoveAll(x => fromComboScoringsToMainToDelete.Contains(x.id));
        }
    }
}
