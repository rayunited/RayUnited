﻿using Assets.Scripts.Engine.Behaviours;
using UnityEngine;

namespace Assets.Scripts.HUD.Draw
{
    public class DrawCutsceneVignette : GameplayOnlyMonoBehaviour
    {
        private Texture2D casetteFill;

        protected override void BehaviourStart()
        {
            this.casetteFill = new Texture2D(1, 1);
            this.casetteFill.SetPixel(0, 0, Color.black);
            this.casetteFill.Apply();
        }

        protected override void GameplayOnGUI()
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, 200), this.casetteFill);
            GUI.DrawTexture(new Rect(0, Screen.height - 200, Screen.width, 200), this.casetteFill);
        }
    }
}
