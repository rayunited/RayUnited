﻿using Assets.Scripts.Engine.FOV;
using Assets.Scripts.GameMechanics.Entities.Entity.Physics;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Aspects;
using Assets.Scripts.GameMechanics.Entities.Watcher;
using Assets.Scripts.GameMechanics.Entities.Watcher.Handles;
using Assets.Scripts.PlayerMechanics;
using UnityEngine;
using UnityEngine.Splines;
using System.Linq;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Body;
using JigglePhysics;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Dying;
using System.Collections.Generic;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Handles;
using System;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher
{
    public class NewWatcherController : 
        NewEntityController<NewWatcherMovementRule, NewWatcherContext>
    {
        public NewWatcherParams watcherParams = new();

        protected override void BehaviourStart()
        {
            base.BehaviourStart();
            var kinematics = new Kinematics();

            var parent = this.transform.parent.gameObject;

            Color baseWatcherBodyColor =
                WatcherBodyPartsFetcher
                  .GetWatcherBodyLowTransform(this.transform)
                  .GetComponent<SkinnedMeshRenderer>().material
                  .GetColor(WatcherBodyColorHandle.EMISSION_COLOR_PROPERTY);
            Color baseWatcherFlowerPartColor =
                WatcherBodyPartsFetcher
                  .GetWatcherFlowerPartsTransforms(this.transform)
                  .Select(x => x.GetComponent<SkinnedMeshRenderer>())
                  .First()
                    .material.GetColor(WatcherBodyColorHandle.EMISSION_COLOR_PROPERTY);
            float baseEyeLightIntensity =
                WatcherBodyPartsFetcher
                  .GetEyeLight(this.transform)
                  .GetComponent<Light>().intensity;

            this.controllerContext = new()
            {
                subject = this.transform,
                parent = parent,
                kinematics = kinematics,
                patrollingPathsSplinesContainer =
                    parent.GetComponentInChildren<SplineContainer>(),
                playerObject =
                    FindObjectOfType<PlayerActorHandle>().transform,
                playerObjectFovPositionOffset = Vector3.up,
                watcherRulesTransitions =
                    new(this),
                watcherParams = this.watcherParams,
                watcherAudioHandles = 
                    (new WatcherAudioHandles())
                        .PropagateWithAudioSourcesRefs(this.transform),
                watcherBodyHandles =
                    new WatcherBodyHandles(
                        watcherEyeHandle:
                            new WatcherEyeHandle(
                                eyeMeshRenderer:
                                    WatcherBodyPartsFetcher
                                        .GetEyeTransform(this.transform)
                                        .GetComponent<MeshRenderer>(),
                                watcherBodyLowMeshRenderer:
                                    WatcherBodyPartsFetcher
                                        .GetWatcherBodyLowTransform(this.transform)
                                        .GetComponent<SkinnedMeshRenderer>(),
                                eyeLight: 
                                    WatcherBodyPartsFetcher
                                        .GetEyeLight(this.transform)
                                        .GetComponent<Light>()
                            ),
                        watcherBodyColorHandle: new WatcherBodyColorHandle(
                                watcherBodyLowMeshRenderer:
                                    WatcherBodyPartsFetcher
                                        .GetWatcherBodyLowTransform(this.transform)
                                        .GetComponent<SkinnedMeshRenderer>(),
                                watcherFlowerPartsRenderers:
                                    WatcherBodyPartsFetcher
                                        .GetWatcherFlowerPartsTransforms(this.transform)
                                        .Select(x => x.GetComponent<SkinnedMeshRenderer>())
                                        .ToList()
                            )
                ),
                baseWatcherForwardDirection = this.transform.forward,
                baseWatcherPosition = this.transform.position,
                ghostModeAspect = new NewWatcherGhostModeInactiveAspect(this),
                baseWatcherBodyColor = baseWatcherBodyColor,
                baseWatcherFlowerPartColor = baseWatcherFlowerPartColor,
                baseEyeLightIntensity = baseEyeLightIntensity,
                currentHealthPoints = this.watcherParams.generalParams.healthPoints,
                watcherJiggleRigBuilderHandle = 
                    GetComponentInChildren<JiggleRigBuilder>(),
                ragdollRigidbodies = 
                    (new List<Rigidbody>() 
                        { GetComponent<Rigidbody>() }
                        .Concat(
                            GetComponentsInChildren<Rigidbody>(
                                includeInactive: true))
                        .ToList()),
                ragdollColliders =
                   (new List<Collider>()
                        { GetComponent<Collider>() }
                        .Concat(
                            GetComponentsInChildren<Collider>(
                                includeInactive: true))
                        .ToList()),
                headRigidbody = GetComponent<Rigidbody>()
            };
            if (this.watcherParams.ragdollDebugParams.startInRagdollMode)
            {
                WatcherRagdollTransformer.EnsureItIsInRagdollState(
                    this.controllerContext.watcherJiggleRigBuilderHandle,
                    this.controllerContext.ragdollRigidbodies,
                    this.controllerContext.ragdollColliders,
                    this.controllerContext.watcherParams.ragdollDebugParams.useGravity);
                this.controllerContext.watcherRulesTransitions.EnterRagdollDebug();
            } else
            {
                WatcherRagdollTransformer.EnsureItIsNotInRagdollState(
                    this.controllerContext.watcherJiggleRigBuilderHandle,
                    this.controllerContext.ragdollRigidbodies,
                    this.controllerContext.ragdollColliders);
                this.controllerContext.watcherRulesTransitions.EnterIdlePatrolling();
            }
        }

        protected override void GameplayFixedUpdate()
        {
            base.GameplayFixedUpdate();
            this.controllerContext.kinematics
                .GameplayFixedUpdateSubject(this.transform);
        }

        private void OnDrawGizmos()
        {
            if (this.watcherParams.fieldOfViewParams.visualize)
            {
                FieldOfViewVisualizationHelper.
                    DrawDebugFieldOfViewConeConsistingOfRays(
                        originTransform: this.transform,
                        origin: this.transform.position,
                        forwardDirection: this.transform.forward,
                        rightDirection: this.transform.right,
                        upDirection: this.transform.up,
                        maxDistance: this.watcherParams.fieldOfViewParams.maxDistance,
                        horizontalAngleDegrees: 
                            this.watcherParams.fieldOfViewParams
                                .horizontalAngleDegrees,
                        verticalAngleDegrees: this.watcherParams
                            .fieldOfViewParams.verticalAngleDegrees,
                        numberOfRaysLayers:
                            this.watcherParams.fieldOfViewParams
                                .fieldOfViewVisualizationParams
                                    .numberOfRaysLayers,
                        numberOfRaysInALayer:
                            this.watcherParams.fieldOfViewParams
                                .fieldOfViewVisualizationParams
                                    .numberOfRaysInALayer,
                        raysOpacity: this.watcherParams.fieldOfViewParams
                            .fieldOfViewVisualizationParams
                                .raysOpacity,
                        shadeRaysLuminocity: true,
                        visualizeAllFOVRays:
                            this.watcherParams.fieldOfViewParams
                                .fieldOfViewVisualizationParams
                                    .visualizeAllFOVRays,
                        fieldOfViewTargetsDebugConfigs:
                            this.watcherParams.fieldOfViewParams
                                .fieldOfViewVisualizationParams
                                .debugWatcherFieldOfViewTargets
                    );
            }
        }
    }
}
