﻿using Assets.Scripts.GameMechanics.Entities.Watcher.Rules.Utils;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.Splines;

namespace Assets.Scripts.GameMechanics.Entities.Watcher.Splines
{
    public static class SplineFetcher
    {
        public static (BezierCurveDirectional bezierCurve, int internalBezierCurveIndex) 
            FetchBezierCurveFromSplinePathInfo(
              SplineContainer splineContainer,
              SplinePathInfo splinePathInfo)
        {
            if (splinePathInfo.ConcernsOneSpline())
            {
                TraversingDirection traversingDirection =
                    SplineTraversingDirectionHelper
                   .GetTraversingDirectionForSplinePathInfo(
                        splineContainer,
                        splinePathInfo);

                Spline spline = splineContainer.Splines[
                    splinePathInfo.startSplineKnotIndex.Spline];

               if (traversingDirection == TraversingDirection.FORWARD)
               {
                    return 
                      (new BezierCurveDirectional(
                         curve: spline.GetCurve(splinePathInfo.startSplineKnotIndex.Knot),
                         direction: traversingDirection
                      ),
                      splinePathInfo.startSplineKnotIndex.Knot);
               } else
               {
                    return 
                      (new BezierCurveDirectional(
                        curve: spline.GetCurve(splinePathInfo.endSplineKnotIndex.Knot),
                        direction: traversingDirection
                      ),
                      splinePathInfo.endSplineKnotIndex.Knot);
               }
            }

            throw new InvalidOperationException(
                "You are not supposed to fetch BezierCurve from such" +
                " formed SplinePathInfo concerning different splines");
        }

        public static SplinePathInfo GetNextSplinePathInfoFor(
            SplineContainer splineContainer,
            SplinePathInfo splinePathInfo)
        {
            bool isBranchingEndKnot = 
                SplinesPathsTraversingHelper
                    .IsBranchingKnot(
                        splineContainer.KnotLinkCollection,
                        splinePathInfo.endSplineKnotIndex);

            Func<SplinePathInfo> traverseOneSplineRegularly = () =>
            {
                TraversingDirection traversingDirection =
                    SplineTraversingDirectionHelper
                   .GetTraversingDirectionForSplinePathInfo(
                        splineContainer,
                        splinePathInfo);

                if (traversingDirection == TraversingDirection.FORWARD)
                {
                    return new SplinePathInfo(
                        startSplineKnotIndex: splinePathInfo.endSplineKnotIndex,
                        endSplineKnotIndex:
                            new SplineKnotIndex(
                                spline: splinePathInfo.endSplineKnotIndex.Spline,
                                knot: SplineKnotIndexingHelper.GetNextKnotIndexInSpline(
                                    spline:
                                        splineContainer.Splines[
                                        splinePathInfo.endSplineKnotIndex.Spline],
                                    knotIndex: splinePathInfo.endSplineKnotIndex.Knot
                                ))
                        );
                }
                else
                {
                    return new SplinePathInfo(
                        startSplineKnotIndex: splinePathInfo.endSplineKnotIndex,
                        endSplineKnotIndex:
                            new SplineKnotIndex(
                                spline: splinePathInfo.endSplineKnotIndex.Spline,
                                knot: SplineKnotIndexingHelper.GetPreviousKnotIndexInSpline(
                                    spline:
                                        splineContainer.Splines[
                                            splinePathInfo.endSplineKnotIndex.Spline],
                                    knotIndex: splinePathInfo.endSplineKnotIndex.Knot))
                        );
                }
            };

            if (splinePathInfo.ConcernsOneSpline()
                && !isBranchingEndKnot)
            {
                TraversingDirection traversingDirection =
                    SplineTraversingDirectionHelper
                   .GetTraversingDirectionForSplinePathInfo(
                        splineContainer,
                        splinePathInfo);

                return traverseOneSplineRegularly();
            } else if (isBranchingEndKnot)
            {
                IReadOnlyList<SplineKnotIndex> links =
                    splineContainer.KnotLinkCollection
                    .GetKnotLinks(splinePathInfo.endSplineKnotIndex);

                int splineIndexToGoTo = 
                    links.Select(x => x.Spline).ToList()[
                        new Random().Next(links.Count)];

                SplineKnotIndex newStartSplineKnotIndex =
                    links.Where(x => x.Spline == splineIndexToGoTo).First();

                if (splineIndexToGoTo != splinePathInfo.endSplineKnotIndex.Spline)
                {
                    var directions = Enum.GetValues(
                        typeof(TraversingDirection));

                    TraversingDirection randomDirection =
                        (TraversingDirection)directions.GetValue(
                            new Random().Next(directions.Length));

                    Func<int> indexSupplierToCall =
                        randomDirection == TraversingDirection.FORWARD ? 
                            () => SplineKnotIndexingHelper.GetNextKnotIndexInSpline(
                                    spline:
                                    splineContainer.Splines[
                                        newStartSplineKnotIndex.Spline],
                                        knotIndex: newStartSplineKnotIndex.Knot
                                )
                            :
                            () => SplineKnotIndexingHelper.GetPreviousKnotIndexInSpline(
                                    spline:
                                    splineContainer.Splines[
                                        newStartSplineKnotIndex.Spline],
                                        knotIndex: newStartSplineKnotIndex.Knot
                                );

                    return new SplinePathInfo(
                        startSplineKnotIndex: newStartSplineKnotIndex,
                        endSplineKnotIndex:
                            new SplineKnotIndex(
                                spline: newStartSplineKnotIndex.Spline,
                                knot: indexSupplierToCall())
                     );
                } else
                {
                    return traverseOneSplineRegularly();
                }                
            } else
            {
                throw new InvalidOperationException(
                    "The path info concerns different splines," +
                    " but the knot in question is not a branching one!");
            }
        }

        public static Spline FetchActualSplineFromSplinePathInfo(
            SplineContainer splineContainer,
            SplinePathInfo splinePathInfo)
        {
            if (!splinePathInfo.ConcernsOneSpline())
            {
                throw new InvalidOperationException(
                    "Cannot fetch Spline object from such constructed " +
                    " SplinePathInfo that concerns different Splines!");
            }

            return splineContainer.Splines[splinePathInfo.startSplineKnotIndex.Spline];
        }
    }
}
