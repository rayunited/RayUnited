﻿using Assets.Scripts.GameMechanics.Entities.Watcher.Rules;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Splines;
using Assets.Scripts.GameMechanics.Entities.Watcher.Rules.Utils;
using System.Linq;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules;

namespace Assets.Scripts.GameMechanics.Entities.Watcher.Splines
{
    public static class SplinesPathsTraversingInitializationHelper
    {
        public static int GetClosestKnotIndex(
            Vector3 position, Transform splinePathTransform,
            BezierKnot[] bezierPathKnots)
        {
            int result = NewWatcherMovementIdlePatrollingRule.NO_PATROLLING_KNOT_INDEX;
            float candidateValue = float.PositiveInfinity;
            for (int i = 0; i < bezierPathKnots.Length; i++)
            {
                float currentDistance =
                    Vector3.Distance(
                        splinePathTransform
                            .TransformPoint(
                                bezierPathKnots[i].Position), position);

                if (currentDistance < candidateValue)
                {
                    result = i;
                    candidateValue = currentDistance;
                }
            }

            return result;
        }
        public static int GetClosestSplineIndex(
           Vector3 position,
           Transform patrollingSplinePathTransformContainerTransform,
           IReadOnlyList<Spline> patrollingBezierPaths)
        {
            int result = NewWatcherMovementIdlePatrollingRule.NO_PATROLLING_SPLINE_INDEX;
            float candidateValue = float.PositiveInfinity;
            for (int i = 0; i < patrollingBezierPaths.Count; i++)
            {
                float3 nearestPointOnConsideredSpline;
                float t;
                SplineUtility.GetNearestPoint(
                    patrollingBezierPaths[i],
                    position,
                    out nearestPointOnConsideredSpline,
                    out t);

                float currentDistance =
                    Vector3.Distance(
                        patrollingSplinePathTransformContainerTransform
                            .TransformPoint(
                                nearestPointOnConsideredSpline), position);

                if (currentDistance < candidateValue)
                {
                    result = i;
                    candidateValue = currentDistance;
                }
            }

            return result;
        }

        public static SplinePathInfo GetAdjustedFinalEntireSplinePathInfoForInitialization(
            SplineContainer splineContainer,
            SplineKnotIndex startSplineKnotIndex)
        {
            if (!SplinesPathsTraversingHelper
                 .IsBranchingKnot(
                     splineContainer.KnotLinkCollection, startSplineKnotIndex))
            {
                return
                    new SplinePathInfo(
                        startSplineKnotIndex: startSplineKnotIndex,
                        endSplineKnotIndex: new SplineKnotIndex(
                            spline: startSplineKnotIndex.Spline, 
                            knot: (startSplineKnotIndex.Knot + 1) 
                                % splineContainer.Splines[startSplineKnotIndex.Spline].Count));
            } else
            {
                IReadOnlyList<SplineKnotIndex> links =
                    splineContainer.KnotLinkCollection
                    .GetKnotLinks(startSplineKnotIndex);

                int splineIndexToGoTo =
                    links.Select(x => x.Spline)
                        .Where(
                            splineIndex => splineContainer
                                .Splines[splineIndex].Count > 1)
                        .DefaultIfEmpty(-1)
                        .First();

                if (splineIndexToGoTo != -1)
                {
                    Spline targetSpline =
                        splineContainer.Splines[splineIndexToGoTo];

                    SplineKnotIndex newStartSplineKnotIndex =
                        links.Where(x => x.Spline == splineIndexToGoTo)
                            .First();

                    int startKnotIndex = newStartSplineKnotIndex.Knot;

                    SplineKnotIndex endSplineKnotIndex =
                       new SplineKnotIndex(
                          spline: splineIndexToGoTo, knot: (startKnotIndex + 1) % targetSpline.Count);

                    return new SplinePathInfo(
                       startSplineKnotIndex: newStartSplineKnotIndex,
                       endSplineKnotIndex: endSplineKnotIndex
                    );
                }  
                return null;            
            }
        }
    }
}
