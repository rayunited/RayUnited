﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Splines;

namespace Assets.Scripts.GameMechanics.Entities.Watcher.Splines
{
    public static class SplinePathHashHelper
    {
        public static int HashForBezierKnot(BezierKnot bezierKnot)
        {
            int hashValue = 0;
            hashValue = HashCode.Combine(bezierKnot.Position.GetHashCode(), hashValue);
            hashValue = HashCode.Combine(bezierKnot.Rotation.GetHashCode(), hashValue);
            hashValue = HashCode.Combine(bezierKnot.TangentIn.GetHashCode(), hashValue);
            hashValue = HashCode.Combine(bezierKnot.TangentOut.GetHashCode(), hashValue);
            return hashValue;
        }

        public static int CalculateHashForSplinePath(
            BezierKnot[] bezierPathKnots)
        {
            int hashValue = 0;
            for (int i = 0; i < bezierPathKnots.Length; i++)
            {
                hashValue = HashCode.Combine(
                    HashForBezierKnot(bezierPathKnots[i]), hashValue);
            }

            return hashValue;
        }

        public static int CalculateHashForSplines(
            IReadOnlyList<Spline> patrollingBezierPaths)
        {
            int hashValue = 0;
            for (int i = 0; i < patrollingBezierPaths.Count; i++)
            {
                hashValue = HashCode.Combine(
                    CalculateHashForSplinePath(
                        patrollingBezierPaths[i].ToArray()), hashValue);
            }

            return hashValue;
        }
    }
}
