﻿using System;
using UnityEngine;
using UnityEngine.Splines;
using Assets.Scripts.GameMechanics.Entities.Watcher.Splines;
using Assets.Scripts.Utils;

namespace Assets.Scripts.GameMechanics.Entities.Watcher.Rules.Utils
{
    public static class SplinesPathsTraversingHelper
    {
        public static Vector3 GetVelocityTowardsTargetPathKnot(
            Vector3 currentPosition,
            Vector3 currentVelocity,
            Transform splinePathTransform,
            BezierCurveDirectional currentBezierCurve,
            float currentBezierPathProgress,
            float velocityChangeInterpolation,
            float maxVelocity)
        {
            Vector3 onCurveWorldSpacePosition =
                splinePathTransform.TransformPoint(
                    CurveUtility.EvaluatePosition(
                        currentBezierCurve.curve, 
                        currentBezierCurve.GetProgressFromNormalized(currentBezierPathProgress)));

            Vector3 distanceToCurrentTarget = onCurveWorldSpacePosition - currentPosition;

            return Vector3.Lerp(
                currentVelocity,
                Vector3.ClampMagnitude(distanceToCurrentTarget, maxVelocity),
                velocityChangeInterpolation);
        }

        public static bool HasCompletedPath(
            float bezierCurveProgress,
            BezierCurveDirectional currentBezierCurve)
        {
            return bezierCurveProgress >= 1f;
        }

        public static bool 
            IsBranchingKnot(
            KnotLinkCollection knotLinkCollection,
            SplineKnotIndex splineKnotIndex)
        {
            return knotLinkCollection
                .GetKnotLinks(splineKnotIndex).Count > 1;
        }
    }
}
