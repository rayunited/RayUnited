﻿using Assets.Scripts.Engine.FOV;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;
using Assets.Scripts.GameMechanics.Entities.Watcher.Rules.Utils;
using Assets.Scripts.GameMechanics.Entities.Watcher.Rules.Utils.IdlePatrolling;
using Assets.Scripts.GameMechanics.Entities.Watcher.Splines;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Splines;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules
{
    public static class NewWatcherMovementRuleIdlePatrollingEffects
    {
        public const string SPLINE_PATH_CHANGED_EFFECT = "SPLINE_PATH_CHANGED_EFFECT";
        public const string INIT_EFFECT = "INIT_EFFECT";
    }

    public static class NewWatcherMovementRuleIdlePatrollingState
    {
        public const string CURRENT_BEZIER_PATH_PROGRESS_STATE =
            "CURRENT_BEZIER_PATH_PROGRESS_STATE";
        public const string SPLINES_PATHS_HASH_STATE = "SPLINE_PATH_HASH_STATE";
        public const string TIME_PROGRESS_STATE = "TIME_PROGRESS_STATE";

        public const string SPLINE_PATH_INFO_STATE = "SPLINE_PATH_INFO_STATE";
        public const string TAIL_TIME_PROGRESS_PERIODS = "TAIL_TIME_PROGRESS_PERIODS";
    }

    public class NewWatcherMovementIdlePatrollingRule : NewWatcherMovementRule
    {
        public const string RULE_ID = "IDLE_PATROLLING_PLACEHOLDER";
        protected BodyEmissionColorAndStrengthContext bodyEmissionColorAndStrengthContext;

        public const int NO_PATROLLING_KNOT_INDEX = -1;
        public const int NO_PATROLLING_SPLINE_INDEX = -1;

        public NewWatcherMovementIdlePatrollingRule(object controller) 
            : base(RULE_ID, controller) {
            this.bodyEmissionColorAndStrengthContext = new()
            {
                bodyEmissionColorProvider = () => UseEntityContext().watcherParams.idlePatrollingParams.bodyEmissionColor,
                flowerPartEmissionColorProvider = () => UseEntityContext().watcherParams.idlePatrollingParams.flowerPartEmissionColor,
                bodyEmissionStrengthProvider = () => UseEntityContext().watcherParams.idlePatrollingParams.bodyEmissionStrength,
                flowerPartEmissionStrengthProvider = () => UseEntityContext().watcherParams.idlePatrollingParams.flowerPartEmissionStrength
            };
        }

        public override BodyEmissionColorAndStrengthContext GetBodyEmissionColorAndStrengthContext()
        {
            return this.bodyEmissionColorAndStrengthContext;
        }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<IdlePatrollingContext>();

            args.ghostModeAspect =
                args.ghostModeAspect.EvaluateFixedUpdate().nextGhostModeAspect;

            IReadOnlyList<Spline> patrollingBezierPaths =
                args.patrollingPathsSplinesContainer.Splines;

            int obtainedSplinesPathsHash =
                SplinePathHashHelper
                   .CalculateHashForSplines(
                       patrollingBezierPaths);

            (float timeProgress, Action<float> SetTimeProgress) =
                UseState(
                    NewWatcherMovementRuleIdlePatrollingState.TIME_PROGRESS_STATE, 0f);
            (int tailTimeProgressPeriods, Action<int> SetTailTimeProgressPeriods)
                = UseState(NewWatcherMovementRuleIdlePatrollingState.TAIL_TIME_PROGRESS_PERIODS, 0);

            (int splinesPathsHashState, Action<int> SetSplinesPathsHashState) =
                UseState(
                    NewWatcherMovementRuleIdlePatrollingState.SPLINES_PATHS_HASH_STATE,
                    obtainedSplinesPathsHash
                );

            (SplinePathInfo splinePathInfoState,
                Action<SplinePathInfo> SetSplinePathInfoState)
                = UseState(
                    NewWatcherMovementRuleIdlePatrollingState.SPLINE_PATH_INFO_STATE,
                    context.initialSplinePathInfoState
                  );

            (float currentBezierPathProgressState, Action<float> SetCurrentBezierPathProgressState)
                = UseState
                (
                    NewWatcherMovementRuleIdlePatrollingState
                        .CURRENT_BEZIER_PATH_PROGRESS_STATE,
                    context.patrollingBezierPathProgress
                );

            Action reinitSplinePath = () =>
            {
                if (!SplinePathInfoValidator.IsEntireSplinePathInfoValid(
                      args.patrollingPathsSplinesContainer,
                      splinePathInfoState))
                {
                    SplinePathInfo newSplinePathInfo =
                    WatcherIdlePatrollingBehaviours.ReinitSplinePathTargeting(
                       subjectPosition: args.subject.position,
                       splineContainer: args.patrollingPathsSplinesContainer,
                       patrollingSplinePathTransformContainer:
                          args.patrollingPathsSplinesContainer.transform,
                       patrollingBezierPaths: patrollingBezierPaths
                    );
                    SetCurrentBezierPathProgressState(0f);
                    SetSplinesPathsHashState(obtainedSplinesPathsHash);
                    SetSplinePathInfoState(newSplinePathInfo);
                }
            };

            SplinePathInfoValidator
                .OnInvalidSplinePathInfo(
                    args.patrollingPathsSplinesContainer,
                    splinePathInfoState,
                    () => reinitSplinePath());

            UseEffect(
                NewWatcherMovementRuleIdlePatrollingEffects.INIT_EFFECT,
                () =>
                {
                    args.watcherParams.soundParams
                        .idlePatrollingSounds.ambientSounds
                            .Play(
                                GetController(),
                                roarAudioSource: 
                                    args.watcherAudioHandles.ambientAudioHandles.roarAudioSource,
                                ambientAudioSource: 
                                    args.watcherAudioHandles.ambientAudioHandles.ambientAudioSource
                            );
                }, Tuple.Create(0));

            UseEffect(
                NewWatcherMovementRuleIdlePatrollingEffects.SPLINE_PATH_CHANGED_EFFECT,
                () => reinitSplinePath(),
                Tuple.Create(obtainedSplinesPathsHash)
            );

            SplinePathInfoValidator.OnValidSplinePathInfo(
                args.patrollingPathsSplinesContainer,
                splinePathInfoState,
                () =>
                {
                    (BezierCurveDirectional currentBezierCurve, int internalBezierCurveIndex) =
                        SplineFetcher.FetchBezierCurveFromSplinePathInfo(
                            args.patrollingPathsSplinesContainer,
                            splinePathInfoState);

                    Spline currentActualSpline =
                        SplineFetcher.FetchActualSplineFromSplinePathInfo(
                            args.patrollingPathsSplinesContainer,
                            splinePathInfoState
                        );

                    Vector3 newVelocity =
                    SplinesPathsTraversingHelper.GetVelocityTowardsTargetPathKnot(
                        args.subject.position,
                        args.kinematics.velocity,
                        args.patrollingPathsSplinesContainer.transform,
                        currentBezierCurve,
                        currentBezierPathProgressState,
                        args.watcherParams.idlePatrollingParams.velocityChangeInterpolation,
                        args.watcherParams.idlePatrollingParams.velocity
                    );

                    Vector3 onCurveWorldSpacePosition =
                        args.patrollingPathsSplinesContainer
                            .transform.TransformPoint(
                                CurveUtility.EvaluatePosition(
                                    currentBezierCurve.curve,
                                    currentBezierCurve
                                        .GetProgressFromNormalized(
                                            currentBezierPathProgressState)));

                    float distanceToCurrentTarget =
                        (onCurveWorldSpacePosition - args.subject.position).magnitude;

                    float curveLengthWorldSpace =
                        args.patrollingPathsSplinesContainer.transform
                            .TransformDirection(Vector3.forward *
                            CurveUtility.CalculateLength(currentBezierCurve.curve)).magnitude;

                    const float targetShiftingDistanceThreshold = 1f;
                    const float headDirectionTurnInterpolationRate = 0.1f;

                    if (distanceToCurrentTarget <= targetShiftingDistanceThreshold)
                    {
                        SetCurrentBezierPathProgressState(
                            currentBezierPathProgressState +
                            (args.watcherParams.idlePatrollingParams.velocity
                                / curveLengthWorldSpace));

                        Vector3 baseForwardLookingDirection =
                            currentBezierCurve.EvaluateTangent(
                               args.patrollingPathsSplinesContainer.transform,
                               currentBezierPathProgressState);

                        args.baseWatcherForwardDirection =
                            baseForwardLookingDirection;
                       
                        Vector3 evaluatedUpVector =
                            currentBezierCurve.EvaluateUpVector(
                                currentActualSpline,
                                internalBezierCurveIndex,
                                currentBezierPathProgressState
                            );

                        args.subject.forward =
                            Vector3.Lerp(
                                args.subject.forward,
                                Quaternion.AngleAxis(
                                    Mathf.Sin(timeProgress) *
                                        args.watcherParams.idlePatrollingParams
                                            .patrollingHeadDeflectionAngleDegrees,
                                        evaluatedUpVector) * baseForwardLookingDirection,
                                headDirectionTurnInterpolationRate
                            ).normalized;
                    }
                    else
                    {
                        args.subject.forward =
                            Vector3.Lerp(
                                args.subject.forward,
                                newVelocity.normalized,
                                headDirectionTurnInterpolationRate
                            ).normalized;

                        args.baseWatcherForwardDirection =
                            newVelocity.normalized;
                    }
                    args.kinematics.velocity = newVelocity;

                    if (SplinesPathsTraversingHelper
                          .HasCompletedPath(
                            currentBezierPathProgressState,
                            currentBezierCurve))
                    {
                        SplinePathInfo nextSplinePathInfo =
                            SplineFetcher.GetNextSplinePathInfoFor(
                                args.patrollingPathsSplinesContainer,
                                splinePathInfoState);

                        SetSplinePathInfoState(nextSplinePathInfo);
                        SetCurrentBezierPathProgressState(0f);
                    }

                    Tuple<FieldOfViewDetectionState, Vector3>
                        playerFieldOfViewDetectionInfo =
                            FieldOfViewTargetDetectionHelper.Inspect(
                                args.subject.position,
                                args.subject,
                                args.watcherParams.fieldOfViewParams.maxDistance,
                                args.watcherParams.fieldOfViewParams.horizontalAngleDegrees,
                                args.watcherParams.fieldOfViewParams.verticalAngleDegrees,
                                args.playerObject,
                                args.playerObjectFovPositionOffset
                            );

                    if (playerFieldOfViewDetectionInfo.Item1 ==
                        FieldOfViewDetectionState.DETECTED)
                    {
                        SetContext(
                            new IdlePatrollingContext
                            {
                                initialSplinePathInfoState = splinePathInfoState,
                                patrollingBezierPathProgress = currentBezierPathProgressState
                            }
                        );

                        args.watcherParams.soundParams
                            .idlePatrollingSounds.ambientSounds.ambientSound
                                .StopPlayingWithAudioSource(
                                    args.watcherAudioHandles.ambientAudioHandles.ambientAudioSource);
                        args.watcherParams.soundParams
                            .idlePatrollingSounds.ambientSounds.roarSound
                                .StopPlayingWithAudioSource(
                                    args.watcherAudioHandles.ambientAudioHandles.roarAudioSource);
                        args.watcherRulesTransitions
                            .EnterAlerted();
                    }
                });

            SetTimeProgress(timeProgress +
                args.watcherParams.idlePatrollingParams
                    .patrollingHeadWigglingSpeed);

            if (((int)Mathf.Floor(timeProgress / (1.5f * (float)Math.PI))) 
                    > tailTimeProgressPeriods)
            {
                int newTimeProgressPeriods = tailTimeProgressPeriods + 1;
                if (newTimeProgressPeriods % 2 == 1)
                {
                    args.watcherParams.soundParams
                        .idlePatrollingSounds.tailWavingSounds
                        .tailWavingSecondDirection
                            .PlayOnceWithAudioSource(
                            args.watcherAudioHandles.tailAudioHandles.tailWavingAudioSource);
                } else
                {
                    args.watcherParams.soundParams
                        .idlePatrollingSounds.tailWavingSounds
                        .tailWavingFirstDirection
                            .PlayOnceWithAudioSource(
                            args.watcherAudioHandles.tailAudioHandles.tailWavingAudioSource);
                }

                SetTailTimeProgressPeriods(newTimeProgressPeriods);
            }

            args.watcherBodyHandles.watcherEyeHandle
                .SetEyeSize(
                    Mathf.Lerp(
                        args.watcherBodyHandles.watcherEyeHandle.GetEyeSize(),
                        args.watcherParams.idlePatrollingParams.eyeSize,
                        args.watcherParams.idlePatrollingParams.eyeSizeCooldownInterpolation));
            args.watcherBodyHandles.watcherEyeHandle
                .SetEyeClosedBlendshapeValue(
                    Mathf.Lerp(
                        args.watcherBodyHandles
                            .watcherEyeHandle.GetEyeClosedBlendshapeValue(),
                        0f,
                        args.watcherParams
                            .idlePatrollingParams.eyeSizeCooldownInterpolation));
            args.watcherBodyHandles.watcherEyeHandle
                .SetEyeLightIntensity(
                    Mathf.Lerp(
                        args.watcherBodyHandles.watcherEyeHandle.GetEyeLightIntensity(),
                        args.watcherParams.idlePatrollingParams.eyeLightIntensity,
                        args.watcherParams
                            .idlePatrollingParams.eyeSizeCooldownInterpolation));

            args.baseWatcherPosition = args.subject.position;
        }

        public override Func<float> GetEyeLightIntensityParamValueProvider()
        {
            return () => UseEntityContext().watcherParams.idlePatrollingParams.eyeLightIntensity;
        }

        public override void StopRuleAmbientSounds()
        {
            var args = UseEntityContext();
            args.watcherParams.soundParams.idlePatrollingSounds.ambientSounds.Stop(
                roarAudioSource: args.watcherAudioHandles.ambientAudioHandles.roarAudioSource,
                ambientAudioSource: args.watcherAudioHandles.ambientAudioHandles.ambientAudioSource
            );
            args.watcherParams.soundParams.idlePatrollingSounds.tailWavingSounds.Stop(
                tailWavingAudioSource: args.watcherAudioHandles.tailAudioHandles.tailWavingAudioSource
            );
        }
    }
}
