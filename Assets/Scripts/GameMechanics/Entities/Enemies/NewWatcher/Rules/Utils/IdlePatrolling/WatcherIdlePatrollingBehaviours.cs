﻿using Assets.Scripts.GameMechanics.Entities.Entity.Physics;
using Assets.Scripts.GameMechanics.Entities.Watcher.Splines;
using Assets.Scripts.GameMechanics.Entities.Watcher.Splines.Validation;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Splines;

namespace Assets.Scripts.GameMechanics.Entities.Watcher.Rules.Utils.IdlePatrolling
{
    public static class WatcherIdlePatrollingBehaviours
    {
        public static SplinePathInfo ReinitSplinePathTargeting(
            Vector3 subjectPosition,
            SplineContainer splineContainer,
            Transform patrollingSplinePathTransformContainer,
            IReadOnlyList<Spline> patrollingBezierPaths
            )
        {
            int closestSplineIndex =
                SplinesPathsTraversingInitializationHelper
                    .GetClosestSplineIndex(
                        subjectPosition,
                        patrollingSplinePathTransformContainer,
                        patrollingBezierPaths
                    );

            BezierKnot[] bezierPathKnots =
                closestSplineIndex >= 0 &&
                closestSplineIndex < patrollingBezierPaths.Count ?
                patrollingBezierPaths[closestSplineIndex].ToArray() :
                Array.Empty<BezierKnot>();

            int closestKnotIndex =
               SplinesPathsTraversingInitializationHelper
                   .GetClosestKnotIndex(
                       subjectPosition,
                       patrollingSplinePathTransformContainer,
                       bezierPathKnots);

            SplineKnotIndex startSplineKnotIndex = 
                new SplineKnotIndex(spline: closestSplineIndex, knot: closestKnotIndex);
            SplinePathInfo adjustedFinalSplinePathInfo =
                SplinesPathsTraversingInitializationHelper
                    .GetAdjustedFinalEntireSplinePathInfoForInitialization(
                        splineContainer,
                        startSplineKnotIndex);

            return adjustedFinalSplinePathInfo;        
        }
    }
}
