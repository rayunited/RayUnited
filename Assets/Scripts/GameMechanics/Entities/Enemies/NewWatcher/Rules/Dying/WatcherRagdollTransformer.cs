﻿
using JigglePhysics;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Dying
{
    public static class WatcherRagdollTransformer
    {
        public static void EnsureItIsNotInRagdollState(
            JiggleRigBuilder watcherJiggleRigBuilderHandle,
            List<Rigidbody> ragdollRigidbodies,
            List<Collider> ragdollColliders)
        {
            watcherJiggleRigBuilderHandle.enabled = true;
            ragdollRigidbodies.ForEach(x =>
            {
                x.isKinematic = true;
                x.useGravity = false;
            });
            ragdollColliders.ForEach(x =>
            {
                x.enabled = false;
            });
        }

        public static void EnsureItIsInRagdollState(
            JiggleRigBuilder watcherJiggleRigBuilderHandle,
            List<Rigidbody> ragdollRigidbodies,
            List<Collider> ragdollColliders,
            bool useGravity)
        {
            watcherJiggleRigBuilderHandle.enabled = false;
            ragdollRigidbodies.ForEach(x =>
            {
                x.isKinematic = false;
                x.useGravity = useGravity;
            });
            ragdollColliders.ForEach(x =>
            {
                x.enabled = true;
            });
        }
    }
}
