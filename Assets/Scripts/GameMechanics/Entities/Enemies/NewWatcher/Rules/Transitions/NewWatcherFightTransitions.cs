﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;
using System;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Transitions
{
    public class NewWatcherFightTransitions
    {
        public static FightContext GetFightContext(
            float initialTimeProgress)
        {
            return new() { 
                initialTimeProgress = initialTimeProgress,
            };
        }
    }
}
