﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;
using Assets.Scripts.GameMechanics.Entities.Watcher.Splines;
using System;
using UnityEngine.Splines;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Transitions
{
    public static class NewWatcherIdlePatrollingTransitions
    {
        public static IdlePatrollingContext GetIdlePatrollingContext(
            SplinePathInfo initialSplinePathInfoState = null,
            float patrollingBezierPathProgress = 0f
            )
        {
            return new()
            {
                initialSplinePathInfoState = initialSplinePathInfoState,
                patrollingBezierPathProgress = patrollingBezierPathProgress,
            };
        }
    }
}
