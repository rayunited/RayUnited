﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Transitions
{
    public static class NewWatcherDamageTakingTransitions
    {
        public static DamageTakingContext GetDamageTakingContext(
            DamageTakingContext previousContext,
            string damageTakingEntranceRuleClassName,
            Func<float> entryRuleDamageTakingSequenceEyeLightIntensityProvider,
            float hitProjectileStrength,
            float maxHitProjectileStrength,
            Vector3 hitProjectileDirection,
            Vector3 hitProjectileUpVector
            )
        {
            string entryRuleDamageTakingSequenceClassName;

            if (damageTakingEntranceRuleClassName.Equals(
                typeof(NewWatcherMovementDamageTakingRule).Name))
            {
                entryRuleDamageTakingSequenceClassName = 
                    previousContext.entryRuleDamageTakingSequenceClassName;
            } else
            {
                entryRuleDamageTakingSequenceClassName = damageTakingEntranceRuleClassName;
            }

            return new() {
                entryRuleDamageTakingSequenceClassName = entryRuleDamageTakingSequenceClassName,
                entryRuleDamageTakingSequenceEyeLightIntensityProvider = entryRuleDamageTakingSequenceEyeLightIntensityProvider,
                hitProjectileStrength = hitProjectileStrength,
                maxHitProjectileStrength = maxHitProjectileStrength,
                hitProjectileDirection = hitProjectileDirection,
                hitProjectileUpVector = hitProjectileUpVector
            };
        }
    }
}
