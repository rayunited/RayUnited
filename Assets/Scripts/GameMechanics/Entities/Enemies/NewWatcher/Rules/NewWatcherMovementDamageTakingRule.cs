﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Aspects;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules
{
    public static class NewWatcherMovementRuleDamageTakingState
    {
        public const string NORMALIZED_RULE_PROGRESS = 
            "NORMALIZED_RULE_PROGRESS";
        public const string HIT_FEEDBACK_DEFLECTION_ANGLE_DEGREES = 
            "HIT_FEEDBACK_DEFLECTION_ANGLE_DEGREES";
        public const string HAS_PASSED_HALF_RULE_NORMALIZED_PROGRESS =
            "HAS_PASSED_HALF_RULE_NORMALIZED_PROGRESS";
    }

    public static class NewWatcherMovementRuleDamageEffects
    {
        public const string INIT_EFFECT =
            "INIT_EFFECT";
    }

    public class NewWatcherMovementDamageTakingRule : NewWatcherMovementRule
    {
        public const string RULE_ID = "DAMAGE_TAKING_PLACEHOLDER";
        protected BodyEmissionColorAndStrengthContext bodyEmissionColorAndStrengthContext;

        public NewWatcherMovementDamageTakingRule(object controller)
            : base(RULE_ID, controller) {
            this.bodyEmissionColorAndStrengthContext = new()
            {
                bodyEmissionColorProvider = () => UseEntityContext().watcherParams.damageTakingParams.bodyEmissionColor,
                flowerPartEmissionColorProvider = () => UseEntityContext().watcherParams.damageTakingParams.flowerPartEmissionColor,
                bodyEmissionStrengthProvider = () => UseEntityContext().watcherParams.damageTakingParams.bodyEmissionStrength,
                flowerPartEmissionStrengthProvider = () => UseEntityContext().watcherParams.damageTakingParams.flowerPartEmissionStrength
            };
        }

        public override BodyEmissionColorAndStrengthContext GetBodyEmissionColorAndStrengthContext()
        {
            return this.bodyEmissionColorAndStrengthContext;
        }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<DamageTakingContext>();

            args.ghostModeAspect =
                args.ghostModeAspect.EvaluateFixedUpdate().nextGhostModeAspect;

            UseEffect(NewWatcherMovementRuleDamageEffects.INIT_EFFECT, 
            () =>
            {
                args.ghostModeAspect = new NewWatcherGhostModeAspect(
                    this.GetController());

                args.watcherAudioHandles
                    .ambientAudioHandles.ambientAudioSource.Stop();
                args.watcherAudioHandles.alertedAudioHandles
                    .alertedAudioSource.Stop();

                args.currentHealthPoints -= context.hitProjectileStrength;

                if (args.currentHealthPoints <= 0)
                {
                    args.watcherRulesTransitions.EnterDying(
                        hitProjectileStrength: context.hitProjectileStrength,
                        maxHitProjectileStrength: context.maxHitProjectileStrength,
                        hitProjectileDirection: context.hitProjectileDirection
                        );
                } else
                {
                    args.watcherBodyHandles.watcherEyeHandle
                        .SetEyeLightIntensity(
                            args.watcherParams.damageTakingParams.peakEyeLightIntensity
                        );
                    args.watcherParams.soundParams.damageTakingSounds
                        .punchHitSound.PlayOnceInPosition(args.subject.position);
                    args.watcherParams.soundParams.damageTakingSounds
                        .hitMoanSound.PlayOnceWithAudioSource(
                        args.watcherAudioHandles.damageTakingAudioHandles.hitMoanAudioSource);
                    args.watcherParams.soundParams.damageTakingSounds.hitBodyDeflectionSound
                        .PlayOnceWithAudioSource(
                        args.watcherAudioHandles.damageTakingAudioHandles
                            .bodyDeflectionAudioSource);
                    args.watcherParams.soundParams
                        .damageTakingSounds.ambientSounds
                        .Play(
                            GetController(),
                            roarAudioSource:
                                args.watcherAudioHandles.ambientAudioHandles.roarAudioSource,
                            ambientAudioSource:
                                args.watcherAudioHandles.ambientAudioHandles.ambientAudioSource
                        );
                }
            }, Tuple.Create(0));

            Vector3 baseWatcherForwardDirection = 
                args.baseWatcherForwardDirection;

            Vector3 hitProjectileDirection =
                context.hitProjectileDirection;

            float damageTakingProgressSpeed = 
                args.watcherParams.damageTakingParams.damageTakingProgressSpeed;
            float damageTakingCooldownSpeed =
                args.watcherParams.damageTakingParams.damageTakingCooldownSpeed;
            float maxHitFeedbackDeflectionDistance =
                args.watcherParams.damageTakingParams
                    .maxHitFeedbackDeflectionDistance;
            float hitProjectileStrength =
                context.hitProjectileStrength;
            float maxHitProjectileStrength =
                context.maxHitProjectileStrength;
            float maxHitFeedbackDeflectionAngleDegrees =
                args.watcherParams.damageTakingParams
                .maxHitFeedbackDeflectionAngleDegrees;
            Vector3 hitProjectileUpVector = 
                context.hitProjectileUpVector;

            (float normalizedRuleProgress, Action<float> SetNormalizedRuleProgress) =
                UseState<float>(
                    NewWatcherMovementRuleDamageTakingState.NORMALIZED_RULE_PROGRESS, 0f);
            (float hitFeedbackDeflectionAngleDegrees, Action<float> SetHitFeedbackDeflectionAngleDegress) =
                UseState<float>
                    (NewWatcherMovementRuleDamageTakingState.HIT_FEEDBACK_DEFLECTION_ANGLE_DEGREES, 0f);
            (bool hasPassedHalfRuleNormalizedProgress, Action<bool> SetHasPassedHalfRuleNormalizedProgress) =
                UseState(
                    NewWatcherMovementRuleDamageTakingState.HAS_PASSED_HALF_RULE_NORMALIZED_PROGRESS,
                    false);

            float deflectionAngleForGivenHit =
                //maxHitFeedbackDeflectionAngleDegrees;
                (hitProjectileStrength / maxHitProjectileStrength) 
                    * maxHitFeedbackDeflectionAngleDegrees;

            float deflectionDistanceForGivenHit =
                (hitProjectileStrength / maxHitProjectileStrength)
                    * maxHitFeedbackDeflectionDistance;

            if (normalizedRuleProgress <= 0.5f)
            {
                SetNormalizedRuleProgress(normalizedRuleProgress +
                    damageTakingProgressSpeed);
            } else
            {
                SetNormalizedRuleProgress(normalizedRuleProgress +
                    damageTakingCooldownSpeed);
            }
            
            SetHitFeedbackDeflectionAngleDegress(
                Mathf.Sin(normalizedRuleProgress * 180f * Mathf.Deg2Rad) * deflectionAngleForGivenHit
            );
            
            float angleDeflectionDirectionFactor =
                Vector3.SignedAngle(
                    hitProjectileDirection, 
                        baseWatcherForwardDirection,
                        hitProjectileUpVector) > 0f ?
                -1 : 1;

            if (normalizedRuleProgress <= 1f)
            {
                if (normalizedRuleProgress >= 0.5f && !hasPassedHalfRuleNormalizedProgress)
                {
                    args.watcherParams.soundParams.damageTakingSounds
                        .hitBodyGoingToNormalAfterDeflectionSound
                        .PlayOnceWithAudioSource(
                            args.watcherAudioHandles.damageTakingAudioHandles
                                .bodyDeflectionAudioSource);
                    SetHasPassedHalfRuleNormalizedProgress(true);
                }

                args.subject.forward = Vector3.Lerp(
                    args.subject.forward,
                    Quaternion.AngleAxis(
                        angleDeflectionDirectionFactor * hitFeedbackDeflectionAngleDegrees,
                        hitProjectileUpVector) * baseWatcherForwardDirection,
                    args.watcherParams.damageTakingParams
                    .forwardDirectionInterpolation
                );
                args.subject.position = Vector3.Lerp(
                    args.subject.position,
                    args.baseWatcherPosition + args.subject.right * 
                        Mathf.Sin(normalizedRuleProgress * 180f * Mathf.Deg2Rad) *
                            angleDeflectionDirectionFactor * deflectionDistanceForGivenHit,
                    args.watcherParams.damageTakingParams
                        .forwardDirectionInterpolation
                    );
                args.watcherBodyHandles.watcherEyeHandle
                    .SetEyeClosedBlendshapeValue(
                        Mathf.Lerp(
                            args.watcherBodyHandles.watcherEyeHandle.GetEyeClosedBlendshapeValue(),
                            Mathf.Sin(normalizedRuleProgress * 180f * Mathf.Deg2Rad) * 100f,
                            0.5f));
                args.watcherBodyHandles.watcherEyeHandle
                    .SetEyeLightIntensity(
                        Mathf.Lerp(
                            args.watcherBodyHandles.watcherEyeHandle.GetEyeLightIntensity(),
                            (Mathf.Cos(normalizedRuleProgress * 360f * Mathf.Deg2Rad) + 1f) * 
                                context.entryRuleDamageTakingSequenceEyeLightIntensityProvider.Invoke(),
                            0.5f)
                    );             
            } else
            {
                args.watcherRulesTransitions.LeaveDamageTaking(
                    context.entryRuleDamageTakingSequenceClassName
                    );
            }   
        }

        public override Func<float> GetEyeLightIntensityParamValueProvider()
        {
            return () => UseEntityContext().watcherParams.damageTakingParams.peakEyeLightIntensity;
        }

        public override void StopRuleAmbientSounds()
        {
            var args = UseEntityContext();
            args.watcherParams.soundParams.damageTakingSounds.ambientSounds.Stop(
                roarAudioSource: args.watcherAudioHandles.ambientAudioHandles.roarAudioSource,
                ambientAudioSource: args.watcherAudioHandles.ambientAudioHandles.ambientAudioSource
            );
        }
    }
}
