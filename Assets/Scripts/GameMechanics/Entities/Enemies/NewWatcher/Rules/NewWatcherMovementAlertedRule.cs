﻿using Assets.Scripts.Engine.FOV;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules
{
    public static class NewWatcherMovementRuleAlertedState
    {
        public const string TIME_PROGRESS_STATE = "TIME_PROGRESS_STATE";
        public const string TIME_PROGRESS_ELAPSED_TIME =
            "TIME_PROGRESS_ELAPSED_TIME";
        public const string TAIL_TIME_PROGRESS_PERIODS = "TAIL_TIME_PROGRESS_PERIODS";
    }

    public static class NewWatcherMovementRuleAlertedEffects {
        public const string INIT_EFFECT = "INIT_EFFECT";
    }

    public class NewWatcherMovementAlertedRule : NewWatcherMovementRule
    {
        public const string RULE_ID = "ALERTED_PLACEHOLDER";
        protected BodyEmissionColorAndStrengthContext bodyEmissionColorAndStrengthContext;

        public NewWatcherMovementAlertedRule(object controller)
            : base(RULE_ID, controller) {

            this.bodyEmissionColorAndStrengthContext = new()
            {
                bodyEmissionColorProvider = () => UseEntityContext().watcherParams.alertedParams.bodyEmissionColor,
                flowerPartEmissionColorProvider = () => UseEntityContext().watcherParams.alertedParams.flowerPartEmissionColor,
                bodyEmissionStrengthProvider = () => UseEntityContext().watcherParams.alertedParams.bodyEmissionStrength,
                flowerPartEmissionStrengthProvider = () => UseEntityContext().watcherParams.alertedParams.flowerPartEmissionStrength
            };
        }

        public override BodyEmissionColorAndStrengthContext GetBodyEmissionColorAndStrengthContext()
        {
            return this.bodyEmissionColorAndStrengthContext;
        }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<AlertedContext>();

            args.ghostModeAspect = 
                args.ghostModeAspect.EvaluateFixedUpdate().nextGhostModeAspect;

            const float velocityFadeInterpolation = 0.3f;
            const float lookingDirectionInterpolation = 0.3f;
            const float headDeflectionAngleDegrees = 30f;
            const float timeProgressFixedStep = 0.15f;

            (float timeProgressElapsedTime, Action<float> SetTimeProgressElapsedTime) =
                UseState<float>(
                    NewWatcherMovementRuleAlertedState.TIME_PROGRESS_ELAPSED_TIME, 0f);

            (float timeProgress, Action<float> SetTimeProgress) =
                UseState<float>(
                    NewWatcherMovementRuleAlertedState.TIME_PROGRESS_STATE, 0f);
            (int tailTimeProgressPeriods, Action<int> SetTailTimeProgressPeriods)
               = UseState(NewWatcherMovementRuleAlertedState.TAIL_TIME_PROGRESS_PERIODS, 0);

            UseEffect(NewWatcherMovementRuleAlertedEffects.INIT_EFFECT, () =>
            {
                args.watcherParams.soundParams.alertedSounds
                    .playerSpottedSound.PlayOnceWithAudioSourceAndThen(
                        (MonoBehaviour)this.entityController,
                        args.watcherAudioHandles.alertedAudioHandles.alertedAudioSource,
                        () =>
                        {
                            args.watcherParams.soundParams.alertedSounds
                                .loopedAlertedSoundAfterSpotted.PlayLoopedWithAudioSource(
                                    args.watcherAudioHandles.alertedAudioHandles.alertedAudioSource
                                );
                        }
                    );
                args.watcherParams.soundParams
                    .alertedSounds.ambientSounds
                    .Play(
                        GetController(),
                           roarAudioSource:
                              args.watcherAudioHandles.ambientAudioHandles.roarAudioSource,
                           ambientAudioSource:
                              args.watcherAudioHandles.ambientAudioHandles.ambientAudioSource
                    );
            }, Tuple.Create(0));

            args.kinematics.velocity =
                Vector3.Lerp(
                    args.kinematics.velocity,
                    Vector3.zero,
                    velocityFadeInterpolation
                    );

            Vector3 playerPosition = args.playerObject.position +
                args.playerObjectFovPositionOffset;

            Vector3 localUpVector =
                args.subject
                    .InverseTransformDirection(Vector3.up);

            args.subject.forward =
                Vector3.Lerp(
                    args.subject.forward,
                    (Quaternion.AngleAxis(
                        Mathf.Sin(timeProgress) * headDeflectionAngleDegrees, localUpVector) *
                        (playerPosition - args.subject.position)).normalized
                    , lookingDirectionInterpolation
                    );

            args.baseWatcherForwardDirection = 
                (playerPosition - args.subject.position).normalized;
            args.baseWatcherPosition = args.subject.position;

            Tuple<FieldOfViewDetectionState, Vector3>
                playerFieldOfViewDetectionInfo =
                    FieldOfViewTargetDetectionHelper.Inspect(
                        args.subject.position,
                        args.subject,
                        args.watcherParams.fieldOfViewParams.maxDistance,
                        args.watcherParams.fieldOfViewParams.horizontalAngleDegrees,
                        args.watcherParams.fieldOfViewParams.verticalAngleDegrees,
                        args.playerObject,
                        args.playerObjectFovPositionOffset
                    );

            if (playerFieldOfViewDetectionInfo.Item1 !=
                FieldOfViewDetectionState.DETECTED)
            {
                args.watcherParams.soundParams.alertedSounds
                   .loopedAlertedSoundAfterSpotted.StopPlayingWithAudioSource(
                       args.watcherAudioHandles.alertedAudioHandles.alertedAudioSource
                   );
                args.watcherRulesTransitions.EnterIdlePatrolling();
            }

            SetTimeProgress(timeProgress + timeProgressFixedStep);
            SetTimeProgressElapsedTime(timeProgressElapsedTime + Time.fixedDeltaTime);

            if (((int)Mathf.Floor(timeProgress / (1 * (float)Math.PI)))
                    > tailTimeProgressPeriods)
            {
                int newTimeProgressPeriods = tailTimeProgressPeriods + 1;
                if (newTimeProgressPeriods % 2 == 1)
                {
                    args.watcherParams.soundParams
                        .alertedSounds.tailWavingSounds
                        .tailWavingSecondDirection
                            .PlayOnceWithAudioSource(
                            args.watcherAudioHandles.tailAudioHandles.tailWavingAudioSource);
                }
                else
                {
                    args.watcherParams.soundParams
                        .alertedSounds.tailWavingSounds
                        .tailWavingFirstDirection
                            .PlayOnceWithAudioSource(
                            args.watcherAudioHandles.tailAudioHandles.tailWavingAudioSource);
                }

                SetTailTimeProgressPeriods(newTimeProgressPeriods);
            }

            if (timeProgressElapsedTime > 
                args.watcherParams.alertedParams
                    .alertedTimeToTransitionToFightSeconds)
            {
                args.watcherParams.soundParams.alertedSounds
                   .loopedAlertedSoundAfterSpotted.StopPlayingWithAudioSource(
                       args.watcherAudioHandles.alertedAudioHandles.alertedAudioSource
                   );
                args.watcherRulesTransitions.EnterFight(timeProgress);
            }

            args.watcherBodyHandles.watcherEyeHandle
                .SetEyeSize(
                    Mathf.Lerp(
                        args.watcherBodyHandles.watcherEyeHandle.GetEyeSize(),
                        args.watcherParams
                            .alertedParams.alertedEyeSize,
                        args.watcherParams
                            .alertedParams.eyeSizeAlertedInterpolation));
            args.watcherBodyHandles.watcherEyeHandle
                .SetEyeClosedBlendshapeValue(
                    Mathf.Lerp(
                        args.watcherBodyHandles
                            .watcherEyeHandle.GetEyeClosedBlendshapeValue(),
                        0f,
                        args.watcherParams
                            .alertedParams.eyeSizeAlertedInterpolation));
            args.watcherBodyHandles.watcherEyeHandle
                .SetEyeLightIntensity(
                    Mathf.Lerp(
                        args.watcherBodyHandles.watcherEyeHandle.GetEyeLightIntensity(),
                        args.watcherParams.alertedParams.eyeLightIntensity,
                        args.watcherParams
                            .alertedParams.eyeSizeAlertedInterpolation));
        }

        public override Func<float> GetEyeLightIntensityParamValueProvider()
        {
            return () => UseEntityContext().watcherParams.alertedParams.eyeLightIntensity;
        }

        public override void StopRuleAmbientSounds()
        {
            var args = UseEntityContext();
            args.watcherParams.soundParams.alertedSounds.ambientSounds.Stop(
                roarAudioSource: args.watcherAudioHandles.ambientAudioHandles.roarAudioSource,
                ambientAudioSource: args.watcherAudioHandles.ambientAudioHandles.ambientAudioSource
            );
            args.watcherParams.soundParams.alertedSounds.tailWavingSounds.Stop(
                tailWavingAudioSource: args.watcherAudioHandles.tailAudioHandles.tailWavingAudioSource
            );
        }
    }
}
