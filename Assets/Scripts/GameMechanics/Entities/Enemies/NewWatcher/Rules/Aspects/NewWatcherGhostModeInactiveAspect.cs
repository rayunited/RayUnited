﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Aspects
{
    public class NewWatcherGhostModeInactiveAspect : NewWatcherMovementRule, INewWatcherGhostModeAspect
    {
        public const string ASPECT_ID = "EMPTY_ASPECT";

        public NewWatcherGhostModeInactiveAspect(object controller) : base(ASPECT_ID, controller) { }

        public (NewWatcherFightParams, NewWatcherFightSounds, INewWatcherGhostModeAspect) EvaluateFixedUpdate()
        {
            var args = UseEntityContext();

            args.watcherBodyHandles.watcherBodyColorHandle.SetBodyEmissionColor(
                Color.Lerp(
                    args.watcherBodyHandles.watcherBodyColorHandle.GetBodyEmissionColor(),
                    GetController().GetCurrentRule().GetBodyEmissionColorAndStrengthContext().bodyEmissionColorProvider.Invoke(),
                    0.5f)
            );
            args.watcherBodyHandles.watcherBodyColorHandle.SetFlowerPartsEmissionColor(
                Color.Lerp(
                    args.watcherBodyHandles.watcherBodyColorHandle.GetFlowerPartsEmissionColor(),
                    GetController().GetCurrentRule().GetBodyEmissionColorAndStrengthContext().flowerPartEmissionColorProvider.Invoke(),
                    0.5f)
            );
            args.watcherBodyHandles.watcherBodyColorHandle.SetBodyEmissionStrength(
                Mathf.Lerp(
                    args.watcherBodyHandles.watcherBodyColorHandle.GetBodyEmissionStrength(),
                    GetController().GetCurrentRule().GetBodyEmissionColorAndStrengthContext().bodyEmissionStrengthProvider.Invoke(),
                    0.5f)
            );
            args.watcherBodyHandles.watcherBodyColorHandle.SetFlowerPartsEmissionStrength(
                Mathf.Lerp(
                    args.watcherBodyHandles.watcherBodyColorHandle.GetFlowerPartsEmissionStrength(),
                    GetController().GetCurrentRule().GetBodyEmissionColorAndStrengthContext().flowerPartEmissionStrengthProvider.Invoke(),
                    0.5f)
            );

            return (args.watcherParams.normalFightParams,
                    args.watcherParams.soundParams.normalFightSounds, this);
        }

        public override void GameplayFixedUpdate()
        {
            throw new InvalidOperationException();
        }

        public override void GameplayUpdate(float deltaTime)
        {
            throw new InvalidOperationException();
        }

        public override BodyEmissionColorAndStrengthContext GetBodyEmissionColorAndStrengthContext()
        {
            throw new InvalidOperationException();
        }

        public override Func<float> GetEyeLightIntensityParamValueProvider()
        {
            throw new InvalidOperationException();
        }

        public bool IsGhostModeActive()
        {
            return false;
        }
    }
}
