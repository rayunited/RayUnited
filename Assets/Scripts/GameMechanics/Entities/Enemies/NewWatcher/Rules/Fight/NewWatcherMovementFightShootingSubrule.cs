﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Handles;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound;
using Assets.Scripts.GameMechanics.Entities.Watcher.Handles;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Fight
{
    public static class ShootingEyeHelper
    {
        public static void PerformEyeClosingShootingAndEyeOpeningSequence(
            NewWatcherFightSounds fightSounds,
            WatcherAudioHandles watcherAudioHandles,
            WatcherBodyHandles watcherBodyHandles,
            float baseEyeLightIntensity,
            float sequenceProgress,
            Action<float> SetSequenceProgress,
            bool passedProjectileEmission,
            Action<bool> SetPassedProjectileEmission,
            bool sequenceEnded,
            Action<bool> SetSequenceEnded,
            float eyeClosingInterpolation,
            float eyeOpeningInterpolation,
            Action onProjectileEmissionMoment,
            Action onEnd
            )
        {
            if (!passedProjectileEmission && sequenceProgress <= 0.499f)
            {
                watcherBodyHandles.watcherEyeHandle
                    .SetEyeClosedBlendshapeValue(
                        sequenceProgress * 2f * 100f
                    );
                watcherBodyHandles.watcherEyeHandle
                    .SetEyeLightIntensity(
                        baseEyeLightIntensity - (sequenceProgress * 2f * baseEyeLightIntensity));

                SetSequenceProgress(
                    Mathf.Lerp(
                        sequenceProgress,
                        0.5f,
                        eyeClosingInterpolation));

            } else if (!passedProjectileEmission)
            {
                SetPassedProjectileEmission(true);
                fightSounds.shootingSounds.eyeOpeningSound
                    .PlayOnceWithAudioSource(
                    watcherAudioHandles.eyeAudioHandles.blinkingAudioSource);
                fightSounds.shootingSounds
                    .projectileShootingSound.PlayOnceWithAudioSource(
                        watcherAudioHandles.eyeAudioHandles.shootingAudioSource);
                onProjectileEmissionMoment.Invoke();
            } else if (sequenceProgress <= 0.999f)
            {
                watcherBodyHandles.watcherEyeHandle
                    .SetEyeClosedBlendshapeValue(
                        100f - ((sequenceProgress - 0.5f) * 2f * 100f)
                    );
                watcherBodyHandles.watcherEyeHandle
                    .SetEyeLightIntensity(
                        (sequenceProgress - 0.5f) * 2f * baseEyeLightIntensity);

                SetSequenceProgress(
                    Mathf.Lerp(
                        sequenceProgress,
                        1f,
                        eyeOpeningInterpolation));
            } else if (!sequenceEnded)
            {
                SetSequenceEnded(true);
                onEnd.Invoke();
            } else
            {
                throw new InvalidOperationException();
            }            
        }
    }

    public static class NewWatcherMovementFightShootingSubruleState
    {
        public const string SEQUENCE_PROGRESS_STATE = 
            "SEQUENCE_PROGRESS_STATE";
        public const string PASSED_PROJECTILE_EMISSION_STATE =
            "PASSED_PROJECTILE_EMISSION_STATE";
        public const string SEQUENCE_ENDED_STATE =
            "SEQUENCE_ENDED_STATE";
    }

    public class NewWatcherMovementFightShootingSubrule : NewWatcherMovementRule
    {
        public const string RULE_ID = "FIGHT_SHOOTING_SUBRULE_PLACEHOLDER";

        public NewWatcherMovementFightShootingSubrule(object controller)
            : base(RULE_ID, controller) { }

        public void EvaluateFixedUpdate(
            NewWatcherFightSounds fightSounds,
            float eyeClosingInterpolation,
            float eyeOpeningInterpolation,
            float eyeLightIntensity,
            Action onShootingEnded,
            Action onProjectileEmission)
        {
            var args = UseEntityContext();

            (float sequenceProgress, Action<float> SetSequenceProgress) =
                UseState(
                    NewWatcherMovementFightShootingSubruleState
                        .SEQUENCE_PROGRESS_STATE,
                    0f);
            (bool passedProjectileEmission, Action<bool> SetPassedProjectileEmission) =
                UseState(
                    NewWatcherMovementFightShootingSubruleState
                        .PASSED_PROJECTILE_EMISSION_STATE,
                    false);
            (bool sequenceEnded, Action<bool> SetSequenceEnded) =
                UseState(
                    NewWatcherMovementFightShootingSubruleState
                        .SEQUENCE_ENDED_STATE,
                    false);

            ShootingEyeHelper.PerformEyeClosingShootingAndEyeOpeningSequence(
                fightSounds,
                args.watcherAudioHandles,
                args.watcherBodyHandles,
                eyeLightIntensity,
                sequenceProgress,
                SetSequenceProgress,
                passedProjectileEmission,
                SetPassedProjectileEmission,
                sequenceEnded,
                SetSequenceEnded,
                eyeClosingInterpolation, 
                eyeOpeningInterpolation,
                onProjectileEmissionMoment: onProjectileEmission,
                onEnd: () => {
                    SetPassedProjectileEmission(false);
                    SetSequenceEnded(false);
                    SetSequenceProgress(0f);
                    onShootingEnded.Invoke();
                }
            );
        }

        public override void GameplayFixedUpdate()
        {
            throw new InvalidOperationException();
        }

        public override void GameplayUpdate(float deltaTime)
        {
            throw new InvalidOperationException();
        }

        public override BodyEmissionColorAndStrengthContext GetBodyEmissionColorAndStrengthContext()
        {
            throw new InvalidOperationException();
        }

        public override Func<float> GetEyeLightIntensityParamValueProvider()
        {
            throw new InvalidOperationException();
        }
    }
}
