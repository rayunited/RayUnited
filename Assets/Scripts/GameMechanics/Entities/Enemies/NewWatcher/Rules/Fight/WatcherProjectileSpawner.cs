﻿using Assets.Scripts.GameMechanics.Entities.Explosion;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params;
using Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile;
using Assets.Scripts.Utils.Audio;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Fight
{
    public static class WatcherProjectileSpawner
    {
        public static void SpawnProjectile(
            Vector3 position,
            Vector3 target,
            float projectileSpeed,
            GameObject projectilePrefab,
            GameObject projectileExplosionPrefab,
            ExplosionDescriptiveParams explosionParams,
            AudioClipWithAudioSourceParams flyingAudioClipWithAudioSourceParams,
            AudioClipWithAudioSourceParams explosionAudioClipWithAudioSourceParams)
        {
            GameObject projectileInstance = 
                UnityEngine.Object.Instantiate(
                    projectilePrefab, position, Quaternion.identity);

            var controller = 
                projectileInstance.AddComponent<NewWatcherProjectileController>();
            controller.watcherProjectileParams.flyingParams.destination = target;
            controller.watcherProjectileParams.flyingParams.startPosition = position;
            controller.watcherProjectileParams.flyingParams.maxSpeed = projectileSpeed;
            controller.watcherProjectileParams.flyingParams.explosionPrefab = projectileExplosionPrefab;
            controller.watcherProjectileParams.flyingParams.explosionParams = explosionParams;
            controller.watcherProjectileParams.flyingParams
                .flyingAudioClipWithAudioSourceParams = flyingAudioClipWithAudioSourceParams;
            controller.watcherProjectileParams.flyingParams
                .explosionAudioClipWithAudioSourceParams = explosionAudioClipWithAudioSourceParams;
        }
    }
}
