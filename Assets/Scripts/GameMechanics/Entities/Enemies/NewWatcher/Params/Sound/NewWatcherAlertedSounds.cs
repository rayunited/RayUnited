﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Common;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Fight;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Sound;
using System;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound
{
    [Serializable]
    public class NewWatcherAlertedSounds
    {
        public SoundWithVariants playerSpottedSound = new();
        public SoundWithVariants loopedAlertedSoundAfterSpotted = new();
        public TailWavingSounds tailWavingSounds = new();
        public AmbientSounds ambientSounds = new();
    }
}
