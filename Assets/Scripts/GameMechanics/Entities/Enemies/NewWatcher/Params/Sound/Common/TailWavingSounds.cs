﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Sound;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Fight
{
    [Serializable]
    public class TailWavingSounds
    {
        public SimpleSound tailWavingFirstDirection = new();
        public SimpleSound tailWavingSecondDirection = new();

        public void Stop(
            AudioSource tailWavingAudioSource)
        {
            this.tailWavingFirstDirection.Stop(tailWavingAudioSource);
            this.tailWavingSecondDirection.Stop(tailWavingAudioSource);
        }
    }
}
