﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Common;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Fight;
using System;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound
{
    [Serializable]
    public class NewWatcherIdlePatrollingSounds
    {
        public TailWavingSounds tailWavingSounds = new();
        public AmbientSounds ambientSounds = new();
    }
}
