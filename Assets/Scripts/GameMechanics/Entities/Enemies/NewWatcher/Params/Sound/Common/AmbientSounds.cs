﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Sound;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Common
{
    [Serializable]
    public class AmbientSounds
    {
        public OptionalSoundWithVariantsModulatedWithRandomIntervals roarSound = new();
        public ContinuousLoopedSoundModulated ambientSound = new();

        public void Stop(
            AudioSource roarAudioSource, AudioSource ambientAudioSource)
        {
            this.roarSound.StopPlayingWithAudioSource(roarAudioSource);
            this.ambientSound.StopPlayingWithAudioSource(ambientAudioSource);
        }

        public void Play(MonoBehaviour coroutinesHost, 
            AudioSource roarAudioSource, AudioSource ambientAudioSource)
        {
            this.roarSound
                .PlayRandomlyRepeatedWithChangingModulationEachTimeWithAudioSource(
                    coroutinesHost, roarAudioSource
                );
            this.ambientSound.PlayLoopedWithChangingModulationEachTimeWithAudioSource(
                coroutinesHost, ambientAudioSource);
        }
    }
}
