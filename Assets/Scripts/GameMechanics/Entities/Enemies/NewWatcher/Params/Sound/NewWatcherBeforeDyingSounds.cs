﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Common;
using System;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound
{
    [Serializable]
    public class NewWatcherBeforeDyingSounds
    {
        public SoundWithVariants beforeDeathExplosionBoilingSound = new();
        public SoundWithVariants finalHitSound = new();
        public SoundWithVariants headHitEnvironmentSound = new();
        public AmbientSounds ambientSounds = new();
    }
}
