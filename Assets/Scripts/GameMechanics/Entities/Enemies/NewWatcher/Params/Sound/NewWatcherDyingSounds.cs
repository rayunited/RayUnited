﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Common;
using System;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound
{
    [Serializable]
    public class NewWatcherDyingSounds
    {
        public SoundWithVariants dyingExplosionSound = new();
        public SoundWithVariants dyingMoanSound = new();
    }
}
