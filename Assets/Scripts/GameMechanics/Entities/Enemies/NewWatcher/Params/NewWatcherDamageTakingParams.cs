﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params
{
    [Serializable]
    public class NewWatcherDamageTakingParams
    {
        [Range(0.0001f, 1f)]
        public float damageTakingProgressSpeed = 0.1f;

        [Range(0.0001f, 1f)]
        public float damageTakingCooldownSpeed = 0.1f;

        [Range(0.0001f, 90f)]
        public float maxHitFeedbackDeflectionAngleDegrees = 30f;

        [Range(0.0001f, 1f)]
        public float forwardDirectionInterpolation = 0.5f;

        [Range(0f, 10f)]
        public float maxHitFeedbackDeflectionDistance = 3f;

        public float peakEyeLightIntensity = 80000f;

        public Color bodyEmissionColor = NewWatcherContext.DEFAULT_BODY_EMISSION_COLOR;
        public Color flowerPartEmissionColor = NewWatcherContext.DEFAULT_FLOWER_PART_EMISSION_COLOR;

        public float bodyEmissionStrength = NewWatcherContext.DEFAULT_BODY_EMISSION_STRENGTH;
        public float flowerPartEmissionStrength = NewWatcherContext.DEFAULT_FLOWER_PART_EMISSION_STRENGTH;
    }
}
