﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params
{
    [Serializable]
    public class NewWatcherDyingParams
    {
        [Range(0, 1f)]
        public float eyeClosingInterpolation = 0.1f;

        public float maximumHitForceNewtons = 30f;

        [Range(0f, 15f)]
        public float timeToExplosionSeconds = 5f;

        public float peakEyeLightIntensity = 80000f;

        public Color bodyEmissionColor = NewWatcherContext.DEFAULT_BODY_EMISSION_COLOR;
        public Color flowerPartEmissionColor = NewWatcherContext.DEFAULT_FLOWER_PART_EMISSION_COLOR;

        public float bodyEmissionStrength = NewWatcherContext.DEFAULT_BODY_EMISSION_STRENGTH;
        public float flowerPartEmissionStrength = NewWatcherContext.DEFAULT_FLOWER_PART_EMISSION_STRENGTH;

        public GameObject explosionPrefab;
    }
}
