﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params
{
    [Serializable]
    public class NewWatcherIdlePatrollingParams
    {
        [Range(0.01f, 1f)]
        public float velocityChangeInterpolation = 0.2f;

        [Range(0.0f, 10f)]
        public float velocity = 0.3f;

        [Range(0.0f, 1f)]
        public float patrollingHeadWigglingSpeed = 0.1f;

        [Range(0.0f, 90f)]
        public float patrollingHeadDeflectionAngleDegrees = 30f;

        [Range(0.0f, 1f)]
        public float eyeSizeCooldownInterpolation = 0.05f;

        [Range(0.0f, 2f)]
        public float eyeSize = 0.27f;

        public float eyeLightIntensity = 80000f;

        public Color bodyEmissionColor = NewWatcherContext.DEFAULT_BODY_EMISSION_COLOR;
        public Color flowerPartEmissionColor = NewWatcherContext.DEFAULT_FLOWER_PART_EMISSION_COLOR;

        public float bodyEmissionStrength = NewWatcherContext.DEFAULT_BODY_EMISSION_STRENGTH;
        public float flowerPartEmissionStrength = NewWatcherContext.DEFAULT_FLOWER_PART_EMISSION_STRENGTH;
    }
}
