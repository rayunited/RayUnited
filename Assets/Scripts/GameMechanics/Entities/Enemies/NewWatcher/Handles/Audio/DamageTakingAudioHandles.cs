﻿using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Handles.Audio
{
    public static class DamageTakingAudioSources
    {
        public const string PUNCH_HIT_AUDIO_SOURCE = "punch_hit_audio_source";
        public const string HIT_MOAN_AUDIO_SOURCE = "hit_moan_audio_source";
        public const string BODY_DEFLECTION_AUDIO_SOURCE = "body_deflection_audio_source";
    }

    public class  DamageTakingAudioHandles
    {
        public AudioSource punchHitAudioSource;
        public AudioSource hitMoanAudioSource;
        public AudioSource bodyDeflectionAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform watcherSubject)
        {
            this.punchHitAudioSource =
                TransformHelper.RecursiveFindChild(
                    watcherSubject, DamageTakingAudioSources.PUNCH_HIT_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
            this.hitMoanAudioSource =
                TransformHelper.RecursiveFindChild(
                    watcherSubject, DamageTakingAudioSources.HIT_MOAN_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
            this.bodyDeflectionAudioSource =
                TransformHelper.RecursiveFindChild(
                    watcherSubject, DamageTakingAudioSources.BODY_DEFLECTION_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
        }
    }
}
