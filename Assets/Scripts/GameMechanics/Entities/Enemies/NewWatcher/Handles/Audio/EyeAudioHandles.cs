﻿using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Handles.Audio
{
    public static class EyeAudioSources
    {
        public const string SHOOTING_AUDIO_SOURCE = "shooting_audio_source";
        public const string BLINKING_AUDIO_SOURCE = "blinking_audio_source";
    }

    public class EyeAudioHandles
    {
        public AudioSource shootingAudioSource;
        public AudioSource blinkingAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform watcherSubject)
        {
            this.shootingAudioSource = 
                TransformHelper.RecursiveFindChild(
                    watcherSubject, EyeAudioSources.SHOOTING_AUDIO_SOURCE).GetComponent<AudioSource>();
            this.blinkingAudioSource =
                TransformHelper.RecursiveFindChild(
                    watcherSubject, EyeAudioSources.BLINKING_AUDIO_SOURCE).GetComponent<AudioSource>();
        }
    }
}
