﻿using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Handles.Audio
{
    public static class DyingAudioSources
    {
        public const string EXPLOSION_AUDIO_SOURCE = "explosion_audio_source";
        public const string MOAN_AUDIO_SOURCE = "moan_audio_source";
    }

    public class DyingAudioHandles
    {
        public AudioSource explosionAudioSource;
        public AudioSource moanAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform watcherSubject)
        {
            this.explosionAudioSource =
                TransformHelper.RecursiveFindChild(
                    watcherSubject, DyingAudioSources.EXPLOSION_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
            this.moanAudioSource =
                TransformHelper.RecursiveFindChild(
                    watcherSubject, DyingAudioSources.MOAN_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
        }
    }
}
