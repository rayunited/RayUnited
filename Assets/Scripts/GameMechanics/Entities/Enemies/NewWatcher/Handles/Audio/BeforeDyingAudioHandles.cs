﻿using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Handles.Audio
{
    public static class BeforeDyingAudioSources
    {
        public const string BOILING_AUDIO_SOURCE = "boiling_audio_source";
        public const string HEAD_HIT_ENV_AUDIO_SOURCE = "head_hit_env_audio_source";
    }

    public class BeforeDyingAudioHandles
    {
        public AudioSource boilingAudioSource;
        public AudioSource headHitEnvAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform watcherSubject)
        {
            this.boilingAudioSource =
                TransformHelper.RecursiveFindChild(
                    watcherSubject, BeforeDyingAudioSources.BOILING_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
            this.headHitEnvAudioSource =
                TransformHelper.RecursiveFindChild(
                    watcherSubject, BeforeDyingAudioSources.HEAD_HIT_ENV_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
        }
    }
}
