﻿using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Handles.Audio
{
    public static class TailAudioSources
    {
        public const string TAIL_WAVING_AUDIO_SOURCE = "tail_waving_audio_source";
    }

    public class TailAudioHandles
    {
        public AudioSource tailWavingAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform watcherSubject)
        {
            this.tailWavingAudioSource =
                TransformHelper.RecursiveFindChild(
                    watcherSubject, TailAudioSources.TAIL_WAVING_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
        }
    }
}
