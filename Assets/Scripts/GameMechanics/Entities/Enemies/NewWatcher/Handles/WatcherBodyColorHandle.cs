﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Watcher.Handles
{
    public class WatcherBodyColorHandle
    {
        protected SkinnedMeshRenderer watcherBodyLowMeshRenderer;
        protected List<SkinnedMeshRenderer> watcherFlowerPartsRenderers;

        public const string EMISSION_COLOR_PROPERTY = "_Emission_Color";
        public const string EMISSION_STRENGTH_PROPERTY = "_Emission_Strenght";

        public WatcherBodyColorHandle(
            SkinnedMeshRenderer watcherBodyLowMeshRenderer,
            List<SkinnedMeshRenderer> watcherFlowerPartsRenderers)
        {
            this.watcherBodyLowMeshRenderer = watcherBodyLowMeshRenderer;
            this.watcherFlowerPartsRenderers = watcherFlowerPartsRenderers;
        }

        public Color GetBodyEmissionColor()
        {
            return this.watcherBodyLowMeshRenderer
                .material.GetColor(EMISSION_COLOR_PROPERTY);
        }

        public Color GetFlowerPartsEmissionColor()
        {
            return this.watcherFlowerPartsRenderers[0]
                .material.GetColor(EMISSION_COLOR_PROPERTY);
        }

        public void SetFlowerPartsEmissionColor(Color color)
        {
            this.watcherFlowerPartsRenderers.ForEach(
                x => x.material.SetColor(EMISSION_COLOR_PROPERTY, color));
        }

        public void SetBodyEmissionColor(Color color)
        {
            this.watcherBodyLowMeshRenderer.material.SetColor(
                EMISSION_COLOR_PROPERTY, color);
        }

        public float GetBodyEmissionStrength()
        {
            return this.watcherBodyLowMeshRenderer.material.GetFloat(
                EMISSION_STRENGTH_PROPERTY
            );
        }

        public void SetBodyEmissionStrength(float strength)
        {
            this.watcherBodyLowMeshRenderer.material.SetFloat(
                EMISSION_STRENGTH_PROPERTY, strength
            );
        }

        public float GetFlowerPartsEmissionStrength()
        {
            return this.watcherFlowerPartsRenderers[0]
                .material.GetFloat(EMISSION_STRENGTH_PROPERTY);
        }

        public void SetFlowerPartsEmissionStrength(float strength)
        {
            this.watcherFlowerPartsRenderers.ForEach(
                x => x.material.SetFloat(EMISSION_STRENGTH_PROPERTY, strength));
        }
    }
}
