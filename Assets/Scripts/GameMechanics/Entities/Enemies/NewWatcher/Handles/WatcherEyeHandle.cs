﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Watcher.Handles
{
    public class WatcherEyeHandle
    {
        protected MeshRenderer eyeMeshRenderer;
        protected SkinnedMeshRenderer watcherBodyLowMeshRenderer;
        protected Light eyeLight;
        protected const string EYE_SIZE_PROPERTY = "_EyeSize";

        protected const int eyeClosedBlenshapeIndex = 0;

        public WatcherEyeHandle(
            MeshRenderer eyeMeshRenderer,
            SkinnedMeshRenderer watcherBodyLowMeshRenderer,
            Light eyeLight)
        {
            this.eyeMeshRenderer = eyeMeshRenderer;
            this.watcherBodyLowMeshRenderer = watcherBodyLowMeshRenderer;
            this.eyeLight = eyeLight;
        }

        public float GetEyeSize()
        {
            return this.eyeMeshRenderer
                .material.GetFloat(EYE_SIZE_PROPERTY);
        }

        public void SetEyeSize(float size)
        {
            this.eyeMeshRenderer.material
                .SetFloat(
                    EYE_SIZE_PROPERTY, size);
        }

        public float GetEyeLightIntensity()
        {
            return this.eyeLight.intensity;
        }

        public void SetEyeLightIntensity(float intensity)
        {
            this.eyeLight.intensity = intensity;
        }

        public float GetEyeClosedBlendshapeValue()
        {
            return this.watcherBodyLowMeshRenderer
                .GetBlendShapeWeight(eyeClosedBlenshapeIndex);
        }

        public void SetEyeClosedBlendshapeValue(float value)
        {
            this.watcherBodyLowMeshRenderer
                .SetBlendShapeWeight(eyeClosedBlenshapeIndex, value);
        }

        //public float GetEyeBottomEyelidRotation()
        //{
        //    throw new NotImplementedException();
        //}

        //public void SetEyeBottomEyelidRotation(float rotation)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
