﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts
{
    public class DyingContext : Context
    {
        public Vector3 hitProjectileDirection;
        public float hitProjectileStrength;
        public float maxHitProjectileStrength;
    }
}
