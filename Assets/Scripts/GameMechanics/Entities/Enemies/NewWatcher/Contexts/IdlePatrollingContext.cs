﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using Assets.Scripts.GameMechanics.Entities.Watcher.Splines;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts
{
    public class IdlePatrollingContext : Context
    {
        public SplinePathInfo initialSplinePathInfoState;
        public float patrollingBezierPathProgress;
    }
}
