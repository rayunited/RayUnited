﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts
{
    public class BodyEmissionColorAndStrengthContext
    {
        public Func<Color> bodyEmissionColorProvider;
        public Func<Color> flowerPartEmissionColorProvider;

        public Func<float> bodyEmissionStrengthProvider;
        public Func<float> flowerPartEmissionStrengthProvider;
    }
}
