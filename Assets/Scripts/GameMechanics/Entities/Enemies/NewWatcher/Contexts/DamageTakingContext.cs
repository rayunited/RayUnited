﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts
{
    public class DamageTakingContext : Context
    {
        public Vector3 hitProjectileDirection;
        public Vector3 hitProjectileUpVector;
        public float hitProjectileStrength;
        public float maxHitProjectileStrength;
        
        public string entryRuleDamageTakingSequenceClassName;
        public Func<float> entryRuleDamageTakingSequenceEyeLightIntensityProvider;
    }
}
