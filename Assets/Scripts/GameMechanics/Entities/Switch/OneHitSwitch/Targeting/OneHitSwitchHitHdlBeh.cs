﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Aspects;
using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Rules;
using Assets.Scripts.PlayerMechanics.Camera;
using Assets.Scripts.PlayerMechanics.Shooting;
using Assets.Scripts.PlayerMechanics.Targeting;
using Assets.Scripts.Sound;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Targeting
{
    public class OneHitSwitchHitHdlBeh : TargetHitHandlingBeh
    {
        protected OneHitSwitchController switchController;

        protected override void BehaviourStart()
        {
            this.switchController = GetComponent<OneHitSwitchController>();
        }

        public override void OnTargetHit(
           HandProjectileBehaviour handProjectileBehaviour)
        {
            string currentRule =
                this.switchController.GetCurrentRuleClassName();

            if (currentRule.Equals(typeof(OneHitSwitchOffRule).Name))
            {
                this.switchController.switchParams.offParams.offBehaviour?.SwitchStateLeave();
                this.switchController.controllerContext.switchRulesTransitions.EnterOn(OneHitSwitchOnRule.RULE_ID);
                AudioSource.PlayClipAtPoint(FindObjectOfType<BasicSoundsMap>().switchHit, FindObjectOfType<CameraActorHandle>().transform.position);
            }
        }
    }
}
