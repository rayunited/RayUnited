﻿using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Contexts;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Rules
{
    public static class OneHitTimeoutSwitchOnRuleEffects
    {
        public const string INIT_EFFECT = "INIT_EFFECT";
    }

    public static class OneHitTimeoutSwitchOnRuleState
    {
        public const string TIME_ELAPSED = "TIME_ELAPSED";
    }

    public class OneHitSwitchOnRule : OneHitSwitchBehaviourRule
    {
        public const string RULE_ID = "ON_PLACEHOLDER";

        public OneHitSwitchOnRule(object controller)
            : base(RULE_ID, controller)
        {}

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<OnContext>();

            (float timeElapsed, Action<float> SetTimeElapsed) = 
                UseState(OneHitTimeoutSwitchOnRuleState.TIME_ELAPSED, 0f);

            UseEffect(OneHitTimeoutSwitchOnRuleEffects.INIT_EFFECT, () =>
            {
                args.switchLookHandles.switchTexture.SetOnLook();
                args.switchParams.onParams.onBehaviour?.SwitchStateEnter();
                args.targetableProperties.drawShootSight = false;
            }, Tuple.Create(0));

            SetTimeElapsed(timeElapsed + Time.fixedDeltaTime);
            args.switchParams.onParams.onBehaviour?.SwitchStateFixedUpdate();

            if (args.switchParams.onParams.hasTimeout && timeElapsed > args.switchParams.onParams.stateTimeoutSeconds)
            {
                args.switchParams.onParams.onBehaviour?.SwitchStateLeave();
                args.switchRulesTransitions.EnterOff();
            }
        }
    }
}
