﻿using Assets.Scripts.GameMechanics.Entities.NewEntity;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Rules
{
    public class OneHitSwitchBehaviourRule : NewEntityRule<OneHitSwitchContext>
    {
        public OneHitSwitchBehaviourRule(string ruleId, object controller)
            : base(ruleId, controller) { }

        protected override OneHitSwitchContext UseEntityContext()
        {
            return GetController().controllerContext;
        }

        protected OneHitSwitchController GetController()
        {
            return GetCast<OneHitSwitchController>(this.entityController);
        }
    }
}
