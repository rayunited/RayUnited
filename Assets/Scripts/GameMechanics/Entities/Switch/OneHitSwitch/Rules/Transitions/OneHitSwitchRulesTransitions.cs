﻿using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Contexts;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Rules.Transitions
{
    public class OneHitSwitchRulesTransitions
    {
        protected OneHitSwitchController switchController;

        public OneHitSwitchRulesTransitions(OneHitSwitchController switchController)
        {
            this.switchController = switchController;
        }

        public void EnterOff()
        {
            var savedContext = this.switchController
                .controllerContext.contextsHolder.UseContext<OffContext>(
                OneHitSwitchOffRule.RULE_ID);

            this.switchController.controllerContext.contextsHolder.
                SetContext(OneHitSwitchOffRule.RULE_ID,
                    OneHitSwitchOffTransitions.GetOffContext(savedContext));
            this.switchController.SetCurrentRule(
                new OneHitSwitchOffRule(this.switchController));
        }

        public void EnterOn(string ruleId)
        {
            var savedContext = ruleId == 
                OneHitSwitchOnRule.RULE_ID ? 
                    this.switchController
                        .controllerContext.contextsHolder.UseContext<OnContext>(
                        OneHitSwitchOnRule.RULE_ID)
                    :
                    this.switchController
                        .controllerContext.contextsHolder.UseContext<OnContext>(
                        OneHitSwitchOnRule.RULE_ID);

            if (ruleId == OneHitSwitchOnRule.RULE_ID)
            {
                this.switchController.controllerContext.contextsHolder.
                    SetContext(OneHitSwitchOnRule.RULE_ID,
                        OneHitSwitchOnTransitions.GetOnContext(savedContext));
                this.switchController.SetCurrentRule(
                    new OneHitSwitchOnRule(this.switchController));
            } else if (ruleId == OneHitSwitchOnRule.RULE_ID)
            {
                this.switchController.controllerContext.contextsHolder.
                    SetContext(OneHitSwitchOnRule.RULE_ID,
                        OneHitSwitchOnTransitions.GetOnContext(savedContext));
                this.switchController.SetCurrentRule(
                    new OneHitSwitchOnRule(this.switchController));
            }           
        }
    }
}
