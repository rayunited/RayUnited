﻿using System;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Params
{
    [Serializable]
    public class OneHitSwitchParams
    {
        public OneHitSwitchOffParams offParams = new();
        public OneHitSwitchOnParams onParams = new();
    }
}
