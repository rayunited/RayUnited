﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Params
{
    [Serializable]
    public class OneHitSwitchOffParams
    {
        public Texture switchOffLookTexture;
        public SwitchStateBehaviour offBehaviour;
    }
}
