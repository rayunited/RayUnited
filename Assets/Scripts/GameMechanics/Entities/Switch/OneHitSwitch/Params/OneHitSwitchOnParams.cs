﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Params
{
    [Serializable]
    public class OneHitSwitchOnParams
    {
        public float stateTimeoutSeconds = 1f;
        public bool hasTimeout = false;
        public Texture switchOnLookTexture;
        public SwitchStateBehaviour onBehaviour;
    }
}
