﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Handles
{
    public class OneHitSwitchTextureHandle
    {
        protected Material material;

        protected OneHitSwitchController switchController;

        public OneHitSwitchTextureHandle(
            OneHitSwitchController switchController,
            Material material)
        {
            this.material = material;
            this.switchController = switchController;
        }

        public void SetOnLook()
        {
            if (this.switchController.switchParams.onParams.switchOnLookTexture != null)
            {
                this.material.SetTexture("_BaseMap", this.switchController.switchParams.onParams.switchOnLookTexture);
            }
        }

        public void SetOffLook()
        {
            if (this.switchController.switchParams.offParams.switchOffLookTexture != null)
            {
                this.material.SetTexture("_BaseMap", this.switchController.switchParams.offParams.switchOffLookTexture);
            }
        }
    }
}
