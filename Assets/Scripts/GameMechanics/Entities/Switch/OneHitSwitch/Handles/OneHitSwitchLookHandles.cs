﻿namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Handles
{
    public class OneHitSwitchLookHandles
    {
        public OneHitSwitchTextureHandle switchTexture;

        public OneHitSwitchLookHandles(
            OneHitSwitchTextureHandle switchTexture)
        {
            this.switchTexture = switchTexture;
        }
    }
}
