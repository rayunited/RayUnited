﻿using Assets.Scripts.GameMechanics.Entities.Explosion;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params;
using Assets.Scripts.Utils.Audio;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile.Params
{
    [Serializable]
    public class NewWatcherProjectileFlyingParams
    {
        public Vector3 destination;
        public Vector3 startPosition;
        public float maxSpeed = 1f;
        public GameObject explosionPrefab;

        public ExplosionDescriptiveParams explosionParams;

        public AudioClipWithAudioSourceParams flyingAudioClipWithAudioSourceParams;
        public AudioClipWithAudioSourceParams explosionAudioClipWithAudioSourceParams;
    }
}
