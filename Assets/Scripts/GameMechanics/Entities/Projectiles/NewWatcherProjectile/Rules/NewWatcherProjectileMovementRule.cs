﻿using Assets.Scripts.GameMechanics.Entities.NewEntity;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile.Rules
{
    public class NewWatcherProjectileMovementRule : NewEntityRule<NewWatcherProjectileContext>
    {
        public NewWatcherProjectileMovementRule(string ruleId, object controller)
            : base(ruleId, controller) { }

        //common logic between WatcherMovementRules

        #region ContextRegion

        protected override NewWatcherProjectileContext UseEntityContext()
        {
            return GetController().controllerContext;
        }

        protected NewWatcherProjectileController GetController()
        {
            return GetCast<NewWatcherProjectileController>(this.entityController);
        }

        #endregion
    }
}
