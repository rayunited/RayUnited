﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile.Rules.Transitions
{
    public static class NewWatcherProjectileFlyingTransitions
    {
        public static FlyingContext GetFlyingContext()
        {
            return new();
        }
    }
}
