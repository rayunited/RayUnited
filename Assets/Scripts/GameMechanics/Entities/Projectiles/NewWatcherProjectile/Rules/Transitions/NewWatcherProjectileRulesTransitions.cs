﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile.Rules.Transitions
{
    public class NewWatcherProjectileRulesTransitions
    {
        protected NewWatcherProjectileController watcherProjectileController;

        public NewWatcherProjectileRulesTransitions(
            NewWatcherProjectileController watcherProjectileController)
        {
            this.watcherProjectileController = watcherProjectileController;
        }

        public void PrepareTransition()
        {
            this.watcherProjectileController.controllerContext.kinematics.velocity
                = Vector3.zero;
        }

        public void EnterFlying()
        {
            PrepareTransition();

            this.watcherProjectileController.controllerContext.contextsHolder
                .SetContext(NewWatcherProjectileMovementFlyingRule.RULE_ID,
                NewWatcherProjectileFlyingTransitions.GetFlyingContext());

            this.watcherProjectileController.SetCurrentRule(
                new NewWatcherProjectileMovementFlyingRule(
                    this.watcherProjectileController));
        }
    }
}
