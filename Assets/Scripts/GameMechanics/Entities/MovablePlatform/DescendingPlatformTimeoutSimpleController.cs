﻿using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.MovablePlatform
{
    public class DescendingPlatformTimeoutSimpleController : SwitchStateBehaviour
    {
        protected float startHeight;

        public float descendingSpeed = 0.5f;

        private void Start()
        {
            this.startHeight = this.transform.position.y; 
        }

        public override void SwitchStateFixedUpdate()
        {
            this.transform.position = new Vector3(
                this.transform.position.x,
                Mathf.Clamp(this.transform.position.y - this.descendingSpeed, this.startHeight, this.transform.position.y),
                this.transform.position.z
            );
        }
    }
}
