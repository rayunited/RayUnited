﻿using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.MovablePlatform
{
    public class AscendingPlatformTimeoutSimpleController : SwitchStateBehaviour
    {
        protected float startHeight;
        public float ascendingSpeed = 0.5f;
        public float ascendingMaxDistance = 10f;

        private void Start()
        {
            this.startHeight = this.transform.position.y; 
        }

        public override void SwitchStateFixedUpdate() {

            this.transform.position = new Vector3(
                this.transform.position.x,
                Mathf.Clamp(this.transform.position.y + this.ascendingSpeed, this.startHeight, this.startHeight + this.ascendingMaxDistance),
                this.transform.position.z
            );
        }
    }
}
