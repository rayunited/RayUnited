﻿using Assets.Scripts.Engine.Behaviours;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.MovablePlatform
{
    public class MovablePlatformSimpleController : GameplayOnlyMonoBehaviour
    {
        private float baseHeight;
        private float baseX;
        private float baseZ;

        private Vector3 basePosition;

        public float maxOffset = 10f;

        public bool moveVertically = false;
        public bool startPositionIsMin = false;
        public bool invertPhase = false;

        protected override void BehaviourStart()
        {
            this.baseHeight = this.transform.position.y;
            this.baseX = this.transform.position.x;
            this.baseZ = this.transform.position.z;

            this.basePosition = this.transform.position;
        }

        protected override void GameplayFixedUpdate()
        {
            float sinOffset = !this.startPositionIsMin ? 0f : this.maxOffset;
            float divider = !this.startPositionIsMin ? 1f : 2f;
            float phaseMultiplier = !this.invertPhase ? 1f : -1f;
            float phasePart = !this.invertPhase ? 0f : sinOffset;

            this.transform.position = 
                this.moveVertically ?
                    this.basePosition + phasePart * this.transform.up
                        + phaseMultiplier * ((Mathf.Sin(Time.realtimeSinceStartup * 1f) * this.maxOffset + sinOffset) / divider) * this.transform.up
                    :
                    this.basePosition + phasePart * this.transform.right
                        + phaseMultiplier * ((Mathf.Sin(Time.realtimeSinceStartup * 1f) * this.maxOffset + sinOffset) / divider) * this.transform.right;        
        }
    }
}
