﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Params.Sound
{
    [Serializable]
    public class PlayerGroundSounds
    {
        public SoundWithVariants rollSound = new();
    }
}
