﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound;
using Assets.Scripts.GameMechanics.Entities.Player.Params.Sound.Air;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Params.Sound
{
    [Serializable]
    public class PlayerAirSounds
    {
        public JumpingSounds jumpingSounds = new();
    }
}
