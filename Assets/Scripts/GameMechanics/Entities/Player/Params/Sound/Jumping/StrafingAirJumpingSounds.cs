﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Params.Sound.Jumping
{
    [Serializable]
    public class StrafingAirJumpingSounds
    {
        public SoundWithVariants airFlipLeftStrafingJumpSound = new();
        public SoundWithVariants airFlipRightStrafingJumpSound = new();
        public SoundWithVariants airExtremumForwardStrafingJumpSound = new();
        public SoundWithVariants airFlipBackwardStrafingJumpSound = new();
        public SoundWithVariants jumpOffTheGroundSound = new();
    }
}
