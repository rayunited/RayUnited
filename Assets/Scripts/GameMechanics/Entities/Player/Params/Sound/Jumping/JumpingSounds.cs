﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Params.Sound.Air
{
    [Serializable]
    public class JumpingSounds
    {
        public SoundWithVariants airFlipRunningJumpSound = new();
        public SoundWithVariants runningJumpOffTheGroundSound = new();
        public SoundWithVariants standingJumpOffTheGroundSound = new();
    }
}
