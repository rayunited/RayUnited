﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Sound;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Params.Sound
{
    [Serializable]
    public class PlayerShootingSounds
    {
        public SoundWithVariants fistChargingCycleStartSound = new();
        public SoundWithVariants unchargedPlainFistShootingSound = new();

        public SoundWithVariants fistChargingCycleCancellationSound = new();

        public SoundWithVariants chargedFistShootingSound = new();
        public ContinuousLoopedSoundModulated chargingFistSound = new();
    }
}
