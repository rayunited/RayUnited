﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Params.Sound
{
    [Serializable]
    public class PlayerStrafingGroundSounds
    {
        public SoundWithVariants enteringStrafingSound = new();
        public SoundWithVariants leavingStrafingSound = new();

        public SoundWithVariants rollForwardSound = new();
        public SoundWithVariants rollBackwardSound = new();
        public SoundWithVariants rollSideSound = new();
    }
}
