﻿using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Params.Sound
{
    [Serializable]
    public class PlayerSoundParams
    {
        public PlayerHelicopterSounds helicopterSounds = new();
        public PlayerAirSounds airSounds = new();
        public PlayerGroundSounds groundSounds = new();
        public PlayerLedgeGrabbingSounds ledgeGrabbingSounds = new();
        public PlayerLumSwingingSounds lumSwingingSounds = new();
        public PlayerRoofHangingSounds roofHangingSounds = new();
        public PlayerStrafingAirSounds strafingAirSounds = new();
        public PlayerStrafingGroundSounds strafingGroundSounds = new();
        public PlayerWallClimbingSounds wallClimbingSounds = new();
        public PlayerShootingSounds shootingSounds = new();
    }
}
