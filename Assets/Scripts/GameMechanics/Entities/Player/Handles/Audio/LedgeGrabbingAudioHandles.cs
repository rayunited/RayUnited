﻿using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Handles.Audio
{
    public static class LedgeGrabbingAudioSources
    {
        public const string LEDGE_GRABBING_AUDIO_SOURCE = "ledge_grabbing_audio_source";
        public const string LEDGE_GRABBING_AMBIENT_AUDIO_SOURCE = "ledge_grabbing_ambient_audio_source";
    }

    public class  LedgeGrabbingAudioHandles
    {
        public AudioSource ledgeGrabbingAudioSource;
        public AudioSource ledgeGrabbingAmbientAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform playerSubject)
        {
            this.ledgeGrabbingAudioSource =
                TransformHelper.RecursiveFindChild(
                    playerSubject, LedgeGrabbingAudioSources.LEDGE_GRABBING_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
            this.ledgeGrabbingAmbientAudioSource =
                TransformHelper.RecursiveFindChild(
                    playerSubject, LedgeGrabbingAudioSources.LEDGE_GRABBING_AMBIENT_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
        }
    }
}
