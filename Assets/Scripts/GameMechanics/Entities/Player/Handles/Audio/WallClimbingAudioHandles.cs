﻿using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Handles.Audio
{
    public static class WallClimbingAudioSources
    {
        public const string WALL_GRABBING_AUDIO_SOURCE = "wall_grabbing_audio_source";
        public const string WALL_MOVEMENT_AUDIO_SOURCE = "wall_movement_audio_source";
    }

    public class WallClimbingAudioHandles
    {
        public AudioSource wallGrabbingAudioSource;
        public AudioSource wallMovementAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform playerSubject)
        {
            this.wallGrabbingAudioSource =
                TransformHelper.RecursiveFindChild(
                    playerSubject, WallClimbingAudioSources.WALL_GRABBING_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
            this.wallMovementAudioSource =
                TransformHelper.RecursiveFindChild(
                    playerSubject, WallClimbingAudioSources.WALL_MOVEMENT_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
        }
    }
}
