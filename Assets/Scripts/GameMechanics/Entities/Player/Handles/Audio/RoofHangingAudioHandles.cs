﻿using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Handles.Audio
{
    public static class RoofHangingAudioSources
    {
        public const string ROOF_CATCHING_LEAVING_AUDIO_SOURCE = "roof_catching_leaving_audio_source";
        public const string ROOF_MOVEMENT_AUDIO_SOURCE = "roof_movement_audio_source";
    }

    public class  RoofHangingAudioHandles
    {
        public AudioSource roofCatchingLeavingAudioSource;
        public AudioSource roofMovementAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform playerSubject)
        {
            this.roofCatchingLeavingAudioSource =
                TransformHelper.RecursiveFindChild(
                     playerSubject, RoofHangingAudioSources.ROOF_CATCHING_LEAVING_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
            this.roofMovementAudioSource =
                TransformHelper.RecursiveFindChild(
                     playerSubject, RoofHangingAudioSources.ROOF_MOVEMENT_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
        }
    }
}
