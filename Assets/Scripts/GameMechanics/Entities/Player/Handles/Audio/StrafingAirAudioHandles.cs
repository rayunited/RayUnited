﻿using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Handles.Audio
{
    public static class StrafingAirAudioSources
    {
        public const string AIR_FLIP_AUDIO_SOURCE = "air_flip_audio_source";
        public const string JUMP_AUDIO_SOURCE = "jump_audio_source";
    }

    public class  StrafingAirAudioHandles
    {
        public AudioSource airFlipAudioSource;
        public AudioSource jumpAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform playerSubject)
        {
            this.airFlipAudioSource =
                TransformHelper.RecursiveFindChild(
                    playerSubject, StrafingAirAudioSources.AIR_FLIP_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
            this.jumpAudioSource =
                TransformHelper.RecursiveFindChild(
                    playerSubject, StrafingAirAudioSources.JUMP_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
        }
    }
}
