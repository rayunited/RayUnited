﻿using Assets.Scripts.GameMechanics.Entities.Player.Handles.Audio;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Handles
{
    public class PlayerAudioHandles
    {
        public AirAudioHandles airAudioHandles = new();
        public GroundAudioHandles groundAudioHandles = new();
        public HelicopterAudioHandles helicopterAudioHandles = new();
        public LedgeGrabbingAudioHandles ledgeGrabbingAudioHandles = new();
        public LumSwingingAudioHandles lumSwingingAudioHandles = new();
        public RoofHangingAudioHandles roofHangingAudioHandles = new();
        public ShootingAudioHandles shootingAudioHandles = new();
        public StrafingAirAudioHandles strafingAirAudioHandles = new();
        public StrafingGroundAudioHandles strafingGroundAudioHandles = new();
        public WallClimbingAudioHandles wallClimbingAudioHandles = new();

        public PlayerAudioHandles 
            PropagateWithAudioSourcesRefs(Transform playerSubject)
        {
            this.airAudioHandles.PropagateWithAudioSourcesRefs(playerSubject);
            this.groundAudioHandles.PropagateWithAudioSourcesRefs(playerSubject);
            this.helicopterAudioHandles.PropagateWithAudioSourcesRefs(playerSubject);
            this.ledgeGrabbingAudioHandles.PropagateWithAudioSourcesRefs(playerSubject);
            this.lumSwingingAudioHandles.PropagateWithAudioSourcesRefs(playerSubject);
            this.roofHangingAudioHandles.PropagateWithAudioSourcesRefs(playerSubject);
            this.shootingAudioHandles.PropagateWithAudioSourcesRefs(playerSubject);
            this.strafingAirAudioHandles.PropagateWithAudioSourcesRefs(playerSubject);
            this.strafingGroundAudioHandles.PropagateWithAudioSourcesRefs(playerSubject);
            this.wallClimbingAudioHandles.PropagateWithAudioSourcesRefs(playerSubject);

            return this;
        }
    }
}
