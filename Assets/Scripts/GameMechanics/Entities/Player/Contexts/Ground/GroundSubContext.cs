﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;

namespace Assets.Scripts.GameMechanics.Entities.Player.Contexts.Ground
{
    public class GroundSubContext : Context
    {
        public bool landing;
        public bool landingWithHelicopter;
        public bool enteringFromGroundStrafing;
    }
}
