﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Contexts
{
    public class LumSwingingContext : Context
    {
        public GameObject lumForSwinging;
        public Vector3 baseSwingingGravityDirection;
        public Vector3 baseSwingingGravityForward;
        public bool caughtByRightHand;
        public GameObject handObject;

        public float minimalSwingingDeflectionAngleDegrees;
        public float maximalSwingingDeflectionAngleDegrees;
        public float maxSwingingRadius;
    }
}
