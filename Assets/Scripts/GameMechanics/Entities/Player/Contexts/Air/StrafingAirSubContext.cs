﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Contexts.Air
{
    public class StrafingAirSubContext : Context
    {
        public bool isHelicopter;
        public bool isJumping;
        public UnityEngine.Vector3 initialIntentionalMovementDirection;
        public UnityEngine.Vector3 initialVelocity;
    }
}
