﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Contexts.Air
{
    public class AirSubContext : Context
    {
        public bool isHelicopter;
        public bool isJumping;
        public Vector3 initialIntentionalMovementDirection;
        public Vector3 initialVelocity;
    }
}
