﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Contexts
{
    public class WallClimbingContext : Context
    {
        public RaycastHit wallClimbHit;
    }
}
