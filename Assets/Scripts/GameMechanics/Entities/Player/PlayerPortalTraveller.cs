﻿using Assets.Scripts.Common;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics.Utils;
using Assets.Scripts.GameMechanics.Entities.Portal;
using Assets.Scripts.GameMechanics.Portals;
using Assets.Scripts.NewPlayerMechanics;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.GameMechanics.Entities.Camera;

namespace Assets.Scripts.GameMechanics.Entities.Player
{
    public class PlayerPortalTraveller : PortalTraveller
    {
        protected PlayerController playerController;
        protected CameraController cameraController;
        public List<Tuple<PortalController, PortalController>> passedPortals { get; private set; }

        private void Awake()
        {
            this.playerController = GetComponent<PlayerController>();
            this.cameraController = FindObjectOfType<CameraController>();
            this.passedPortals = new List<Tuple<PortalController, PortalController>>();
        }

        protected override bool CheckPortalCollision( 
            out RaycastHit portalThresholdHit, Vector3 gravityDirection, Vector3 gravityForward)
        {
            return Raycaster.CylinderRaycasterSingle(
                   this.transform.position + (-gravityDirection), gravityForward, gravityDirection,
                    3f, 0.5f, 5, 100, out portalThresholdHit, Layers.portalsLayerMask, debug: true, debugColor: Color.yellow);
        }

        protected override void OnPortalTravel(Transform fromPortal, Transform toPortal, Vector3 gravityDirection, Vector3 gravityForward)
        {
            GetComponent<PlayerController>().OrientatePlayerAccordingToPortals(fromPortal, toPortal);

            Tuple<PortalController, PortalController> lastPortalsTransition =
                this.passedPortals.LastOrDefault();

            if (lastPortalsTransition != null &&
                lastPortalsTransition.Item2.name.Equals(fromPortal.name) &&
                lastPortalsTransition.Item1.name.Equals(toPortal.name))
            {
                this.passedPortals.RemoveAt(this.passedPortals.Count - 1);
            } else
            {
                var newPart = Tuple.Create(fromPortal.GetComponent<PortalController>(), toPortal.GetComponent<PortalController>());
                this.passedPortals.Add(newPart);
            }
        }

        public override Vector3 GetGravityDirection()
        {
            return this.playerController.controllerContext.gravityDirection;
        }

        public override Vector3 GetGravityForward()
        {
            return this.playerController.controllerContext.gravityForward;
        }
    }
}
