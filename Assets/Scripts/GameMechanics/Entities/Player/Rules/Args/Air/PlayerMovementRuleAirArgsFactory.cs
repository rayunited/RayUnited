﻿using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Args
{
    public static class PlayerMovementRuleAirArgsFactory
    {
        public static PlayerMovementRuleAirArgs GetFreeFallingInputArgs(
            Vector3 initialIntentionalMovementDirection
            )
        {
            return new PlayerMovementRuleAirArgsBuilder()
                .WithInitialVelocity(Vector3.zero)
                .WithInitialIntentionalMovementDirection(initialIntentionalMovementDirection)
                .WithIsJumping(false)
                .Build();
        }

        public static PlayerMovementRuleAirArgs GetJumpingAirRuleInputArgs(
            Vector3 jumpVelocity,
            Vector3 initialIntentionalMovementDirection
            )
        {
            return new PlayerMovementRuleAirArgsBuilder()
                .WithInitialVelocity(jumpVelocity)
                .WithInitialIntentionalMovementDirection(initialIntentionalMovementDirection)
                .WithIsJumping(true)
                .Build();
        }
    }
}
