﻿using Assets.Scripts.PlayerMechanics.Camera.Rules;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Args.LumSwinging
{
    public static class PlayerMovementRuleLumSwingingArgsFactory
    {
        public static PlayerMovementRuleLumSwingingArgs GetLumSwingingFromAnyInputArgs(
            GameObject lumForSwinging, Vector3 baseSwingingGravityDirection,
            Vector3 baseSwingingGravityForward,
            bool caughtByRightHand, GameObject handObject, float minimalSwingingDeflectionAngleDegrees,
            float maximalSwingingDeflectionAngleDegrees, float maxSwingingRadius)
        {
            return new PlayerMovementRuleLumSwingingArgsBuilder()
                .WithLumForSwinging(lumForSwinging)
                .WithBaseSwingingGravityDirection(baseSwingingGravityDirection)
                .WithBaseSwingingGravityForward(baseSwingingGravityForward)
                .WithCaughtByRightHand(caughtByRightHand)
                .WithHandObject(handObject)
                .WithMinimalSwingingDeflectionAngleDegrees(minimalSwingingDeflectionAngleDegrees)
                .WithMaximalSwingingDeflectionAngleDegrees(maximalSwingingDeflectionAngleDegrees)
                .WithMaxSwingingRadius(maxSwingingRadius)
                .Build();
        }
    }
}
