﻿using Assets.Scripts.PlayerMechanics.Camera.Rules;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Args.LumSwinging
{
    public class PlayerMovementRuleLumSwingingArgsBuilder
    {
        private GameObject lumForSwinging = null;
        private Vector3 baseSwingingGravityDirection = Vector3.zero;
        private Vector3 baseSwingingGravityForward = Vector3.zero;
        private bool caughtByRightHand = false;
        private GameObject handObject = null;
        private float minimalSwingingDeflectionAngleDegrees = 0f;
        private float maximalSwingingDeflectionAngleDegrees = 0f;
        private float maxSwingingRadius = 0f;

        public PlayerMovementRuleLumSwingingArgsBuilder
            WithLumForSwinging(GameObject lumForSwinging)
        {
            this.lumForSwinging = lumForSwinging;
            return this;
        }

        public PlayerMovementRuleLumSwingingArgsBuilder
            WithBaseSwingingGravityDirection(Vector3 baseSwingingGravityDirection)
        {
            this.baseSwingingGravityDirection = baseSwingingGravityDirection;
            return this;
        }

        public PlayerMovementRuleLumSwingingArgsBuilder
            WithBaseSwingingGravityForward(Vector3 baseSwingingGravityForward)
        {
            this.baseSwingingGravityForward = baseSwingingGravityForward;
            return this;
        }

        public PlayerMovementRuleLumSwingingArgsBuilder
            WithCaughtByRightHand(bool caughtByRightHand)
        {
            this.caughtByRightHand = caughtByRightHand;
            return this;
        }

        public PlayerMovementRuleLumSwingingArgsBuilder
            WithHandObject(GameObject handObject)
        {
            this.handObject = handObject;
            return this;
        }

        public PlayerMovementRuleLumSwingingArgsBuilder
            WithMinimalSwingingDeflectionAngleDegrees(float minimalSwingingDeflectionAngleDegrees)
        {
            this.minimalSwingingDeflectionAngleDegrees = minimalSwingingDeflectionAngleDegrees;
            return this;
        }

        public PlayerMovementRuleLumSwingingArgsBuilder
            WithMaximalSwingingDeflectionAngleDegrees(float maximalSwingingDeflectionAngleDegrees)
        {
            this.maximalSwingingDeflectionAngleDegrees = maximalSwingingDeflectionAngleDegrees;
            return this;
        }

        public PlayerMovementRuleLumSwingingArgsBuilder
            WithMaxSwingingRadius(float maxSwingingRadius)
        {
            this.maxSwingingRadius = maxSwingingRadius;
            return this;
        }

        public PlayerMovementRuleLumSwingingArgs Build()
        {
            return new PlayerMovementRuleLumSwingingArgs(
                this.lumForSwinging,
                this.baseSwingingGravityDirection,
                this.baseSwingingGravityForward,
                this.caughtByRightHand, this.handObject,
                this.minimalSwingingDeflectionAngleDegrees,
                this.maximalSwingingDeflectionAngleDegrees,
                this.maxSwingingRadius);
        }
    }

    public class PlayerMovementRuleLumSwingingArgs
    {
        public GameObject lumForSwinging { get; }
        public Vector3 baseSwingingGravityDirection { get; }
        public Vector3 baseSwingingGravityForward { get; }
        public bool caughtByRightHand { get; }
        public GameObject handObject { get; }
        public float minimalSwingingDeflectionAngleDegrees { get; }
        public float maximalSwingingDeflectionAngleDegress { get; }
        public float maxSwingingRadius { get; }


        public PlayerMovementRuleLumSwingingArgs(
            GameObject lumForSwinging,
            Vector3 baseSwingingGravityDirection,
            Vector3 baseSwingingGravityForward,
            bool caughtByRightHand,
            GameObject handObject,
            float minimalSwingingDeflectionAngleDegrees,
            float maximalSwingingDeflectionAngleDegress,
            float maxSwingingRadius)
        {
            this.baseSwingingGravityDirection = baseSwingingGravityDirection;
            this.baseSwingingGravityForward = baseSwingingGravityForward;
            this.lumForSwinging = lumForSwinging;
            this.caughtByRightHand = caughtByRightHand;
            this.handObject = handObject;
            this.minimalSwingingDeflectionAngleDegrees = minimalSwingingDeflectionAngleDegrees;
            this.maximalSwingingDeflectionAngleDegress = maximalSwingingDeflectionAngleDegress;
            this.maxSwingingRadius = maxSwingingRadius;
        }
    }
}
