﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Utils;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using Assets.Scripts.Sound;
using Assets.Scripts.Utils;
using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules
{
    public static class PlayerMovementRuleLedgeGrabbingEffects
    {
        public const string INIT_EFFECT = "INIT_EFFECT";
        public const string INIT_EFFECT_2 = "INIT_EFFECT_2";
    }

    public static class PlayerMovementRuleLedgeGrabbingStates
    {
        public const string CAN_CLIMB_THE_LEDGE = "CAN_CLIMB_THE_LEDGE";
        public const string IS_ALLOWED_TO_CLIMB = "IS_ALLOWED_TO_CLIMB";
        public const string LEDGE_GRABBING_LOCAL_POSITION = "LEDGE_GRABBING_LOCAL_POSITION";
    }

    public class PlayerMovementRuleLedgeGrabbing : PlayerMovementRule
    {
        public const string RULE_ID = "PLAYER_MOVEMENT_RULE_LEDGE_GRABBING";

        public PlayerMovementRuleLedgeGrabbing(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void OnPortalTravel()
        {
        }

        private IEnumerator AllowPlayerToClimbAfterMilliseconds(int milliseconds, RefObject<bool> isAllowedToClimb) 
        {
            yield return new WaitForSeconds(milliseconds / 1000f);
            isAllowedToClimb.current = true;
        }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            HandleGemsCollectiblesCollisions(args, args.gravityDirection, args.gravityForward);
            var context = UseContext<LedgeGrabbingContext>();

            RefObject<bool> canClimbTheLedge = 
               UseRef(PlayerMovementRuleLedgeGrabbingStates.CAN_CLIMB_THE_LEDGE, false);
            RefObject<bool> isAllowedToClimb =
               UseRef(PlayerMovementRuleLedgeGrabbingStates.IS_ALLOWED_TO_CLIMB, false);
            RefObject<Vector3> ledgeGrabbingLocalPosition =
               UseRef(PlayerMovementRuleLedgeGrabbingStates.LEDGE_GRABBING_LOCAL_POSITION, Vector3.zero);

            Action<int> DelayAllowingPlayerToClimbMilliseconds = (milliseconds) =>
            {
                args.playerController
                    .StartCoroutine(
                        AllowPlayerToClimbAfterMilliseconds(milliseconds, isAllowedToClimb));
            };

            UseEffect(PlayerMovementRuleLedgeGrabbingEffects.INIT_EFFECT, () =>
            {
                LedgeColliderInfo ledgeColliderInfo =
                    context.ledgeColliderHit.collider.gameObject.GetComponent<LedgeColliderInfo>();
                Vector3 grabNormal = ledgeColliderInfo.normal;
                Vector3 nearestPointOnEdge = LedgeGrabbingHelper.GetNearestPointOnEdgeFrom(
                    args.subject.transform.position,
                    ledgeColliderInfo.transform.TransformPoint(ledgeColliderInfo.edgePointALocal),
                    ledgeColliderInfo.transform.TransformPoint(ledgeColliderInfo.edgePointBLocal));

                ledgeGrabbingLocalPosition.current = context.ledgeColliderHit.transform.InverseTransformPoint(nearestPointOnEdge);
            }, Tuple.Create(0));

            UseEffect(PlayerMovementRuleLedgeGrabbingEffects.INIT_EFFECT_2, () =>
            {
                args.playerController.controllerContext.cameraController
                    .controllerContext.cameraRulesTransitions.EnterLedgeGrab();

                LedgeColliderInfo ledgeColliderInfo =
                    context.ledgeColliderHit.collider.gameObject.GetComponent<LedgeColliderInfo>();
                Vector3 grabNormal = ledgeColliderInfo.normal;

                args.playerAnimatedModel.forward = -grabNormal;

                PlayerOrientationHelper.OrientatePlayerAnimatedPart(
                    args.playerAnimatedModel.playerAnimatedModelTransform.transform,
                    -grabNormal,
                    args.playerController.controllerContext.gravityForward,
                    args.playerController.controllerContext.gravityRight,
                    args.playerController.controllerContext.gravityDirection
                    );

                args.legacyPlayerMovementMetrics.intentionalMovingDirection =
                    -grabNormal.normalized;
                args.subject.transform.position = ledgeColliderInfo.transform.TransformPoint(ledgeGrabbingLocalPosition.current);
                args.playerAnimatedModel.playerAnimatedModelTransform.transform.position =
                    ledgeColliderInfo.transform.TransformPoint(ledgeGrabbingLocalPosition.current) +
                    args.playerController.controllerContext.gravityDirection * 1.78f + grabNormal.normalized * 0.82f;

                DelayAllowingPlayerToClimbMilliseconds(300);

                args.playerAnimationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.StartingHangingOnLedgeFromFallingStateName(),
                    priority: 5);
            }, Tuple.Create(0));

            LedgeColliderInfo ledgeColliderInfoMov =
                context.ledgeColliderHit.collider.gameObject.GetComponent<LedgeColliderInfo>();

            args.subject.transform.position = ledgeColliderInfoMov.transform.TransformPoint(ledgeGrabbingLocalPosition.current);

            if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                RaymanAnimations.StartingHangingOnLedgeFromFallingStateName()) &&
                args.playerAnimationController.HasAnimationEnded())
            {
                args.playerAnimationController.ChangeAnimationState(
                    RaymanAnimations.HangingOnLedgeStateName());
                canClimbTheLedge.current = true;

                args.subject.transform.position = ledgeColliderInfoMov.transform.TransformPoint(ledgeGrabbingLocalPosition.current);
                return;
            }

            if (!args.legacyPlayerMovementInput.GetJumpButton())
            {
                args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
                args.subject.transform.position = ledgeColliderInfoMov.transform.TransformPoint(ledgeGrabbingLocalPosition.current);
            }

            LedgeColliderInfo ledgeColliderInfo =
                context.ledgeColliderHit.collider.gameObject.GetComponent<LedgeColliderInfo>();
            Vector3 grabNormal = ledgeColliderInfo.normal;

            if (args.legacyPlayerMovementInput.GetJumpButton() && args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState)
            {
                LegacyRuleLedgeGrabbingToNewAirTransitions.
                    EnterRuleAirJumpingFromRuleLedgeGrabbing(
                        args.subject.transform,
                        args.legacyRayCollider,
                        args.playerAnimatedModel.playerAnimatedModelTransform.gameObject,
                        args.playerAnimationController,
                        args.legacyPlayerMovementStateInfo,
                        args.legacyPlayerMovementMetrics,
                        ledgeColliderInfo,
                        grabNormal,
                        args.playerController
                    );
                AudioSource.PlayClipAtPoint(UnityEngine.Object.FindObjectOfType<BasicSoundsMap>().jump, args.subject.transform.position);
                return;
            }

            if (canClimbTheLedge.current)
            {
                float axesMagnitude =
                Mathf.Clamp01(new Vector2(
                    args.playerInputHub.moveInputRaw.x, args.playerInputHub.moveInputRaw.y).magnitude);

                Vector3 translation = args.subject.transform.TransformDirection(args.legacyPlayerMovementInput.currentTranslationFromUpdate);
                       //args.subject.transform.TransformDirection((args.playerInputHub.moveInputRaw.y * args.playerController.controllerContext.gravityForward).normalized +
                       //(args.playerInputHub.moveInputRaw.x * args.playerController.controllerContext.gravityRight).normalized).normalized
                       //* axesMagnitude;

                args.subject.transform.position = ledgeColliderInfoMov.transform.TransformPoint(ledgeGrabbingLocalPosition.current);

                if (
                    Vector3.Angle(translation, -grabNormal) < 80f
                    &&
                    translation.magnitude > 0.1f
                    //&&
                    //isAllowedToClimb.current
                    )
                {


                    //if deciding to climb the ledge
                    canClimbTheLedge.current = false;

                    args.playerAnimationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.ClimbingFromHangingOnLedgeStateName(),
                        priority: 5);

                    Vector3 nearestPointOnEdge = LedgeGrabbingHelper.GetNearestPointOnEdgeFrom(
                        args.subject.transform.position,
                        ledgeColliderInfo.transform.TransformPoint(ledgeColliderInfo.edgePointALocal),
                        ledgeColliderInfo.transform.TransformPoint(ledgeColliderInfo.edgePointBLocal));

                    Vector3 startingHangingPosition =
                        nearestPointOnEdge + args.playerController.controllerContext.gravityDirection * 1.78f + grabNormal.normalized * 0.82f;
                    Vector3 endingClimbingPosition = args.subject.transform.position + ((-grabNormal.normalized) * 1.18f);

                    args.subject.transform.position = ledgeColliderInfoMov.transform.TransformPoint(ledgeGrabbingLocalPosition.current);
                    return;
                }
                else if (!args.legacyPlayerMovementInput.GetJumpButton() && Vector3.Angle(translation, -grabNormal) > 100f &&
                    translation.magnitude > 0.1f)
                {
                    LegacyRuleLedgeGrabbingToNewAirTransitions.
                        EnterRuleAirFallingOffFromRuleLedgeGrabbing(
                            args.subject.transform,
                            args.legacyRayCollider,
                            args.playerAnimatedModel.playerAnimatedModelTransform.gameObject,
                            args.legacyPlayerMovementStateInfo,
                            args.legacyPlayerMovementMetrics,
                            ledgeColliderInfo,
                            grabNormal,
                            args.playerController
                        );
                    args.subject.transform.position = ledgeColliderInfoMov.transform.TransformPoint(ledgeGrabbingLocalPosition.current);
                    return;
                }
                args.subject.transform.position = ledgeColliderInfoMov.transform.TransformPoint(ledgeGrabbingLocalPosition.current);
                // otherwise ignore all Player's intentions, inputs etc for now :)
            }
            args.subject.transform.position = ledgeColliderInfoMov.transform.TransformPoint(ledgeGrabbingLocalPosition.current);
        }

        public override void GameplayUpdate(float deltaTime)
        {
            var args = UseEntityContext();
            var context = UseContext<LedgeGrabbingContext>();

            RefObject<Vector3> ledgeGrabbingLocalPosition =
               UseRef(PlayerMovementRuleLedgeGrabbingStates.LEDGE_GRABBING_LOCAL_POSITION, Vector3.zero);

            LedgeColliderInfo ledgeColliderInfoMov =
                context.ledgeColliderHit.collider.gameObject.GetComponent<LedgeColliderInfo>();

            Vector3 grabNormalMov = ledgeColliderInfoMov.normal;

            Vector3 nearestPointOnEdgeMov = LedgeGrabbingHelper.GetNearestPointOnEdgeFrom(
                args.subject.transform.position,
                ledgeColliderInfoMov.transform.TransformPoint(ledgeColliderInfoMov.edgePointALocal),
                ledgeColliderInfoMov.transform.TransformPoint(ledgeColliderInfoMov.edgePointBLocal));

            args.subject.transform.position = ledgeColliderInfoMov.transform.TransformPoint(ledgeGrabbingLocalPosition.current);

            if (args.playerAnimationController.GetCurrentAnimationState()
                .Equals(RaymanAnimations.ClimbingFromHangingOnLedgeStateName()))
            {
                args.subject.transform.position = ledgeColliderInfoMov.transform.TransformPoint(ledgeGrabbingLocalPosition.current);

                if (!args.playerAnimationController.HasAnimationEnded())
                {
                    Vector3 startingHangingPosition =
                        nearestPointOnEdgeMov + args.playerController.controllerContext.gravityDirection * 1.78f + grabNormalMov.normalized * 0.82f;
                    Vector3 endingClimbingPosition = nearestPointOnEdgeMov + ((-grabNormalMov.normalized) * 1.18f);
                    // still climbing, adjust Rayman's model position in-between
                    // since climbing animation has positioning offset...
                    float animationNormalizedTime = args.playerAnimationController.GetAnimationNormalizedTime();

                    args.subject.transform.position = ledgeColliderInfoMov.transform.TransformPoint(ledgeGrabbingLocalPosition.current);
                    args.playerAnimatedModel.playerAnimatedModelTransform.transform.position =
                        Vector3.Lerp(
                            startingHangingPosition,
                            endingClimbingPosition,
                            animationNormalizedTime);
                    return;
                }
                else
                {
                    Vector3 startingHangingPosition = nearestPointOnEdgeMov +
                        args.playerController.controllerContext.gravityDirection * 1.78f + grabNormalMov.normalized * 0.82f;
                    Vector3 endingClimbingPosition = args.subject.transform.position + ((-grabNormalMov.normalized) * 1.18f);

                    args.subject.transform.position = ledgeColliderInfoMov.transform.TransformPoint(ledgeGrabbingLocalPosition.current);
                    args.playerAnimatedModel.playerAnimatedModelTransform.transform.position = args.subject.transform.position;
                    args.legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_GROUND;
                    args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                    args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                    args.playerRulesTransitions.EnterGround();
                    // climbing ended, move to GROUND rule with 'clear/canonical' state
                    return;
                }
            }
            args.subject.transform.position = ledgeColliderInfoMov.transform.TransformPoint(ledgeGrabbingLocalPosition.current);
        }
    }
}
