﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts.Air;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Strafing;
using Assets.Scripts.PlayerMechanics.Camera;
using Assets.Scripts.Sound;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules
{
    public static class PlayerMovementRuleGroundState
    {
        public const string GROUND_SUBRULE = "GROUND_SUBRULE";
        public const string STRAFING_GROUND_SUBRULE = "STRAFING_GROUND_SUBRULE";
    }

    public static class PlayerMovementRuleGroundEffects
    {
        public const string INIT_EFFECT = "INIT_EFFECT";
    }

    public class PlayerMovementRuleGround : PlayerMovementRule
    {
        public const string RULE_ID = "PLAYER_MOVEMENT_RULE_GROUND";

        public PlayerMovementRuleGround(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void OnPortalTravel()
        {
        }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            HandleGemsCollectiblesCollisions(args, args.gravityDirection, args.gravityForward);
            var context = UseContext<GroundContext>();

            RefObject<GroundSubrule> groundSubrule =
                UseRef<GroundSubrule>(PlayerMovementRuleGroundState.GROUND_SUBRULE, null);
            RefObject<StrafingGroundSubrule> strafingGroundSubrule =
                UseRef<StrafingGroundSubrule>(PlayerMovementRuleGroundState.STRAFING_GROUND_SUBRULE, null);

            UseEffect(PlayerMovementRuleGroundEffects.INIT_EFFECT, () =>
            {
                groundSubrule.current = new GroundSubrule(args.playerController);
                strafingGroundSubrule.current = new StrafingGroundSubrule(args.playerController);
            }, Tuple.Create(context.isStrafing));

            if (context.isStrafing)
            {
                strafingGroundSubrule.current.GameplayFixedUpdate();
            }
            else
            {
                groundSubrule.current.GameplayFixedUpdate();
            }
        }
    }
}
