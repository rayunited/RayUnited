﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Utils;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.PlayerMechanics.Rules;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.Sound;
using Assets.Scripts.Utils;
using UnityEngine;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts.Air;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.PlayerMechanics.Camera;
using System;
using Unity.Mathematics;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Strafing
{
    public static class StrafingAirSubruleStates
    {
        public const string RANDOM = "RANDOM";
    }

    public static class StrafingAirSubruleEffects
    {
        public const string INIT_EFFECT = "INIT_EFFECT";
    }

    public class StrafingAirSubrule : PlayerMovementRule
    {
        public const string RULE_ID = "PLAYER_MOVEMENT_SUBRULE_STRAFING_AIR";

        public StrafingAirSubrule(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void OnPortalTravel()
        {
        }

        protected float CalculateAngleBetweenTranslationInWorldSpaceAndPlayerAnimatedPartForward(
            Vector3 translationInWorldSpace)
        {
            var args = UseEntityContext();
            return Vector3.Angle(translationInWorldSpace.normalized, -args.playerAnimatedModel.playerAnimatedModelTransform.forward.normalized);
        }

        protected float CalculateAngleBetweenTranslationInWorldSpaceAndPlayerAnimatedPartRight(
            Vector3 translationInWorldSpace)
        {
            var args = UseEntityContext();
            return Vector3.Angle(translationInWorldSpace.normalized, -args.playerAnimatedModel.playerAnimatedModelTransform.right.normalized);
        }

        public override void OnShooting(float fistShootingPowerNormalized)
        {
            var args = UseEntityContext();
            var context = UseContext<StrafingAirSubContext>(StrafingAirSubrule.RULE_ID);

            RefObject<bool> isHelicopter =
                UseRef<bool>(AirSubruleRefs.IS_HELICOPTER, context.isHelicopter);
            RefObject<System.Random> random = UseRef<System.Random>(StrafingAirSubruleStates.RANDOM, null);

            

            if (fistShootingPowerNormalized >= 0.9f)
            {
                if (args.legacyPlayerMovementStateInfo.isHelicopter)
                {
                    args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER].animationName,
                            priority: 20);
                    if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                    {
                        args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER].animationName, 0f, Time.fixedDeltaTime);
                    }
                }
                else
                {
                    args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName,
                            priority: 15);
                    if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                    {
                        args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName, 0f, Time.fixedDeltaTime);
                    }
                }
                var s = GameObject.FindObjectOfType<BasicSoundsMap>();
                AudioSource.PlayClipAtPoint(s.chargedShootsVariants[random.current.Next(0, s.chargedShootsVariants.Length)],
                    GameObject.FindObjectOfType<CameraActorHandle>().transform.position);
            }
        }

        public override void GameplayFixedUpdate()
        {

            //Debug.Log("STRAFING AIR ANIMMMM " + args.playerAnimationController.GetCurrentAnimationState());

            var args = UseEntityContext();
            var context = UseContext<StrafingAirSubContext>();

            float playerInAirStrafingSpeed = 0.13f;
            float playerJumpSpeed = 0.28f;

            var isMaxChargedShootWithHelicopterAnim =
                    args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER].animationName);
            var isMaxChargedShootAnim = args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName);

            var maxChargeHelicopterAnim = RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER].animationName;
            var maxChargeAnim = RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName;

            RefObject<System.Random> random = UseRef<System.Random>(StrafingAirSubruleStates.RANDOM, null);

            UseEffect(StrafingAirSubruleEffects.INIT_EFFECT, () =>
            {
                random.current = new System.Random();
            }, Tuple.Create(0));

            if (isMaxChargedShootWithHelicopterAnim && args.playerAnimationController.HasAnimationEnded())
            {
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(maxChargeHelicopterAnim, 0);

                if (!args.legacyPlayerMovementInput.GetJumpButton())
                {
                    args.playerController.GetComponentsInChildren<AudioSource>()[1].Stop();
                    args.legacyPlayerMovementStateInfo.isHelicopter = false;
                    args.playerAnimationController.ChangeAnimationState(RaymanAnimations.FallingAnimationStateName());
                }
                else
                {
                    args.playerAnimationController.ChangeAnimationState(RaymanAnimations.HelicopterStateName());
                }
            }

            if (isMaxChargedShootAnim && args.playerAnimationController.HasAnimationEnded())
            {
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(maxChargeAnim, 0);
                args.playerAnimationController.ChangeAnimationState(RaymanAnimations.FallingAnimationStateName());
            }



            if (!args.legacyPlayerMovementInput.GetStrafingButton())
            {
                // exit strafing air rule right away and go into regular air rule
                LegacyRuleStrafingAirToNewAirTransitions.
                    EnterRuleAirFromRuleStrafingAir(
                        args.legacyPlayerMovementStateInfo,
                        args.legacyPlayerMovementMetrics,
                        args.playerController
                    );
                return;
            }

            HandleShooting(args);
            Vector3 inputTranslation = args.legacyPlayerMovementInput.GetTranslation();
            Vector3 translation = args.legacyPlayerMovementInput.GetTranslation() * playerInAirStrafingSpeed;

            float helperAngle = Vector3.SignedAngle(Vector3.forward, translation.normalized, Vector3.up);

            Vector3 translationDirectionInWorldSpace =
                args.subject.transform.TransformDirection(translation).normalized;

            if (inputTranslation.magnitude < 0.1)
            {
                float movementSmoothingInterpolation = 0.6f;
                args.legacyPlayerMovementStateInfo.movementVelocity.x =
                    Mathf.Lerp(args.legacyPlayerMovementStateInfo.movementVelocity.x, 0, movementSmoothingInterpolation);
                args.legacyPlayerMovementStateInfo.movementVelocity.z =
                    Mathf.Lerp(args.legacyPlayerMovementStateInfo.movementVelocity.z, 0, movementSmoothingInterpolation);
            }
            else
            {
                //float movementSmoothingInterpolation = 0.25f;
                float movementSmoothingInterpolation = 0.25f;
                args.legacyPlayerMovementStateInfo.movementVelocity.x =
                    Mathf.Lerp(args.legacyPlayerMovementStateInfo.movementVelocity.x, translation.x, movementSmoothingInterpolation);
                args.legacyPlayerMovementStateInfo.movementVelocity.z =
                    Mathf.Lerp(args.legacyPlayerMovementStateInfo.movementVelocity.z, translation.z, movementSmoothingInterpolation);
                args.legacyPlayerMovementMetrics.intentionalMovingDirection =
                    Vector3.ProjectOnPlane(
                        args.subject.transform.TransformDirection(
                            args.legacyPlayerMovementStateInfo.movementVelocity),
                        -args.playerController.controllerContext.gravityDirection).normalized;
            }

            if (inputTranslation.magnitude > 0.1)
            {
                PlayerOrientationHelper.OrientatePlayerObject(
                    args.subject.transform,
                    PlayerPortalsHelper.TransformPortalsProjectedDirectionToWorldDirectionThroughChainOfPortalsTransitions(
                        args.playerController.controllerContext.playerPortalTraveller.passedPortals,
                        args.playerController.controllerContext.baseCameraOrientedForwardDirectionForPlayer.Invoke(),
                        args.playerController.controllerContext.gravityDirection
                    ),
                    args.playerController.controllerContext.gravityForward,
                    args.playerController.controllerContext.gravityRight,
                    args.playerController.controllerContext.gravityDirection
                );
            }

            if (args.legacyPlayerTargetingAspect.currentTarget)
            {
                Vector3 targetLookingDirection =
                    args.legacyPlayerTargetingAspect.GetPlayerLookingDirectionTowardsTarget(
                        args.playerAnimatedModel.playerAnimatedModelTransform.transform.position,
                        args.playerController.controllerContext.gravityDirection);

                PlayerOrientationHelper.OrientatePlayerAnimatedPart(
                    args.playerAnimatedModel.playerAnimatedModelTransform.transform,
                    targetLookingDirection,
                    args.playerController.controllerContext.gravityForward,
                    args.playerController.controllerContext.gravityRight,
                    args.playerController.controllerContext.gravityDirection
                );

                args.legacyPlayerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_TARGET_PLAYER_BACK_STRAFING;
                args.cameraController.controllerContext.cameraRulesTransitions.EnterTargetPlayerBackStrafing();
            }
            else
            {
                args.legacyPlayerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_STRAFING;
                args.cameraController.controllerContext.cameraRulesTransitions.EnterStrafing();
            }

            args.legacyPlayerMovementStateInfo.movementVelocity = args.subject.transform.InverseTransformDirection
                (PlayerWallCollisionHelper
                   .GetMovementVelocityConsideringWallCollisions(
                    args.subject.gameObject,
                    args.legacyRayCollider,
                    args.legacyPlayerMovementMetrics.intentionalMovingDirection.normalized,
                    args.subject.transform.TransformDirection(args.legacyPlayerMovementStateInfo.movementVelocity),
                    args.playerController.controllerContext.gravityDirection,
                    args.playerController.controllerContext.gravityForward));

            args.legacyPlayerMovementStateInfo.movementVelocity = args.subject.transform.InverseTransformDirection(
                args.legacyRayCollider.AdjustVelocityIfIsHittingTheCeiling(
                    args.subject.transform.TransformDirection(args.legacyPlayerMovementStateInfo.movementVelocity),
                    args.playerController.controllerContext.gravityDirection,
                    args.playerController.controllerContext.gravityForward));

            if (args.legacyPlayerMovementStateInfo.isJumping)
            {
                args.legacyPlayerMovementStateInfo.isJumping = false;
                //this.jumpSound.Play();
                args.legacyPlayerMovementStateInfo.movementVelocity.y = playerJumpSpeed;

                if (inputTranslation.magnitude > 0.1f)
                {
                    float angleBetweenPlayerForwardAndTranslation =
                        CalculateAngleBetweenTranslationInWorldSpaceAndPlayerAnimatedPartForward(
                            translationDirectionInWorldSpace);

                    float angleBetweenPlayerRightAndTranslation =
                        CalculateAngleBetweenTranslationInWorldSpaceAndPlayerAnimatedPartRight(
                            translationDirectionInWorldSpace
                            );

                    if (angleBetweenPlayerForwardAndTranslation < 45f)
                    {
                        // jumping forward
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.JumpStartBounceFromGroundForwardStrafingAirAnimationStateName(),
                            priority: 3);
                        args.playerController.playerParams.soundParams
                            .strafingAirSounds.jumpingSounds.jumpOffTheGroundSound
                            .PlayOnceWithAudioSource(
                                args.playerController.controllerContext.playerAudioHandles
                                .strafingAirAudioHandles.jumpAudioSource);
                    }
                    else if (angleBetweenPlayerForwardAndTranslation < 120f)
                    {
                        // jumping left or right
                        if (angleBetweenPlayerRightAndTranslation < 90f)
                        {
                            // jumping right
                            args.playerAnimationController.ChangeAnimationStateWithPriority(
                                RaymanAnimations.JumpStartBounceFromGroundRightStrafingAirAnimationStateName(),
                                priority: 3);
                            args.playerController.playerParams.soundParams
                                .strafingAirSounds.jumpingSounds.jumpOffTheGroundSound
                                .PlayOnceWithAudioSource(
                                    args.playerController.controllerContext.playerAudioHandles
                                    .strafingAirAudioHandles.jumpAudioSource);
                        }
                        else
                        {
                            // jumping left
                            args.playerAnimationController.ChangeAnimationStateWithPriority(
                                RaymanAnimations.JumpStartBounceFromGroundLeftStrafingAirAnimationStateName(),
                                priority: 3);
                            args.playerController.playerParams.soundParams
                                .strafingAirSounds.jumpingSounds.jumpOffTheGroundSound
                                .PlayOnceWithAudioSource(
                                    args.playerController.controllerContext.playerAudioHandles
                                    .strafingAirAudioHandles.jumpAudioSource);
                        }
                    }
                    else
                    {
                        // jumping backwards
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.JumpStartBounceFromGroundBackwardsStrafingAirAnimationStateName(),
                            priority: 3);
                        args.playerController.playerParams.soundParams
                            .strafingAirSounds.jumpingSounds.jumpOffTheGroundSound
                            .PlayOnceWithAudioSource(
                                args.playerController.controllerContext.playerAudioHandles
                                .strafingAirAudioHandles.jumpAudioSource);
                    }
                }
                else
                {
                    // jumping straight up
                    args.playerAnimationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(),
                        priority: 3);
                    args.playerController.playerParams.soundParams
                        .strafingAirSounds.jumpingSounds.jumpOffTheGroundSound
                        .PlayOnceWithAudioSource(
                            args.playerController.controllerContext.playerAudioHandles
                            .strafingAirAudioHandles.jumpAudioSource);
                }
            }

            if (args.legacyPlayerMovementStateInfo.movementVelocity.y > -PlayerMovementMetrics.limitFallSpeed)
            {
                if (!args.legacyPlayerMovementStateInfo.isHelicopter)
                {
                    args.legacyPlayerMovementStateInfo.movementVelocity.y -= PlayerMovementMetrics.gravityAcceleration;
                }
            }

            if (args.legacyPlayerMovementStateInfo.isHelicopter)
            {
                var a = args.subject.transform.GetComponentsInChildren<AudioSource>()[1];
                if (!a.isPlaying)
                {
                    a.clip = GameObject.FindObjectOfType<BasicSoundsMap>().helicopter;
                    a.volume = 0.4f;
                    a.loop = true;
                    a.Play();
                }


                args.legacyPlayerMovementStateInfo.movementVelocity.y = -PlayerMovementMetrics.limitFallSpeedHelicopter;
            }

            args.subject.transform.Translate(args.legacyPlayerMovementStateInfo.movementVelocity);

            if (
                (
                    args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundForwardStrafingAirAnimationStateName())
                    ||
                    args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundBackwardsStrafingAirAnimationStateName())
                    ||
                    args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundLeftStrafingAirAnimationStateName())
                    ||
                    args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundRightStrafingAirAnimationStateName())

                ) &&
                  args.playerAnimationController.HasAnimationEnded())
            {
                if (args.legacyPlayerMovementStateInfo.movementVelocity.y < 0.1f)
                {
                    //animationController.ChangeAnimationStateWithPriority(
                    //    RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName(),
                    //    RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateSpeed(), 2);


                    // change into respective roll/extremum of the animation sequence in the air

                    if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundForwardStrafingAirAnimationStateName()))
                    {
                        // extremum when strafing forward
                        args.playerAnimationController.ChangeAnimationState(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollForwardAnimationStateName());
                        args.playerController.playerParams.soundParams
                            .strafingAirSounds.jumpingSounds.airExtremumForwardStrafingJumpSound
                            .PlayOnceWithAudioSource(
                                args.playerController.controllerContext.playerAudioHandles
                                .strafingAirAudioHandles.airFlipAudioSource);
                        // AudioSource.PlayClipAtPoint(GameObject.FindObjectOfType<BasicSoundsMap>().airFlip, args.subject.transform.position);
                    }
                    else if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundBackwardsStrafingAirAnimationStateName()))
                    {
                        // backflip when strafing backwards
                        args.playerAnimationController.ChangeAnimationState(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollBackwardsAnimationStateName());
                        args.playerController.playerParams.soundParams
                            .strafingAirSounds.jumpingSounds.airFlipBackwardStrafingJumpSound
                            .PlayOnceWithAudioSource(
                                args.playerController.controllerContext.playerAudioHandles
                                .strafingAirAudioHandles.airFlipAudioSource);
                        AudioSource.PlayClipAtPoint(GameObject.FindObjectOfType<BasicSoundsMap>().airFlip, args.subject.transform.position);
                    }
                    else if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundLeftStrafingAirAnimationStateName()))
                    {
                        // extremum (air flip?) when strafing left
                        args.playerAnimationController.ChangeAnimationState(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollLeftAnimationStateName());
                        args.playerController.playerParams.soundParams
                            .strafingAirSounds.jumpingSounds.airFlipLeftStrafingJumpSound
                            .PlayOnceWithAudioSource(
                                args.playerController.controllerContext.playerAudioHandles
                                .strafingAirAudioHandles.airFlipAudioSource);
                        AudioSource.PlayClipAtPoint(GameObject.FindObjectOfType<BasicSoundsMap>().airFlip, args.subject.transform.position);
                    }
                    else
                    {
                        // extremum (air flip?) when strafing right
                        args.playerAnimationController.ChangeAnimationState(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollRightAnimationStateName());
                        args.playerController.playerParams.soundParams
                            .strafingAirSounds.jumpingSounds.airFlipRightStrafingJumpSound
                            .PlayOnceWithAudioSource(
                                args.playerController.controllerContext.playerAudioHandles
                                .strafingAirAudioHandles.airFlipAudioSource);
                    }
                }
            }
            else if (
                (
                    (
                        args.playerAnimationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollForwardAnimationStateName())

                       ||

                       args.playerAnimationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollBackwardsAnimationStateName())

                       ||

                       args.playerAnimationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollLeftAnimationStateName())

                       ||

                       args.playerAnimationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollRightAnimationStateName())
                    )
                    && args.playerAnimationController.HasAnimationEnded())
               )
            {

                // change into finishing falling animation appropriate for given strafing air jumping sequence

                if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.StrafingAirJumpExtremumAirRollForwardAnimationStateName()))
                {
                    // falling from extremum forward
                    args.playerAnimationController.ChangeAnimationState(
                        RaymanAnimations.StrafingAirFallingAfterExtremumAirRollForwardAnimationStateName());
                }
                else if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.StrafingAirJumpExtremumAirRollBackwardsAnimationStateName()))
                {
                    // falling from extremum backwards
                    args.playerAnimationController.ChangeAnimationState(
                        RaymanAnimations.StrafingAirFallingAfterExtremumAirRollBackwardsAnimationStateName());
                }
                else if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.StrafingAirJumpExtremumAirRollLeftAnimationStateName()))
                {
                    // falling from extremum left
                    args.playerAnimationController.ChangeAnimationState(
                        RaymanAnimations.StrafingAirFallingAfterExtremumAirRollLeftAnimationStateName());
                }
                else
                {
                    // falling from extremum right
                    args.playerAnimationController.ChangeAnimationState(
                        RaymanAnimations.StrafingAirFallingAfterExtremumAirRollRightAnimationStateName());
                }
            }

            else
                if ((args.legacyPlayerMovementStateInfo.movementVelocity.y < 0.1f &&
                    args.playerAnimationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName()))

                    ||

                    (!args.legacyPlayerMovementStateInfo.isHelicopter &&
                    !args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.FallingFromJumpFromRunningAnimationStateName()))

                        &&


                        (!args.legacyPlayerMovementStateInfo.isHelicopter &&
                            !args.playerAnimationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.StrafingAirFallingAfterExtremumAirRollForwardAnimationStateName()))

                            &&

                            (!args.legacyPlayerMovementStateInfo.isHelicopter &&
                                !args.playerAnimationController.GetCurrentAnimationState().Equals(
                                RaymanAnimations.StrafingAirFallingAfterExtremumAirRollBackwardsAnimationStateName()))

                            &&

                            (!args.legacyPlayerMovementStateInfo.isHelicopter &&
                                !args.playerAnimationController.GetCurrentAnimationState().Equals(
                                RaymanAnimations.StrafingAirFallingAfterExtremumAirRollLeftAnimationStateName()))


                                &&


                            (!args.legacyPlayerMovementStateInfo.isHelicopter &&
                               !args.playerAnimationController.GetCurrentAnimationState().Equals(
                               RaymanAnimations.StrafingAirFallingAfterExtremumAirRollRightAnimationStateName()))



                      &&


                    !args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.StrafingAirJumpExtremumAirRollForwardAnimationStateName())

                    &&

                    !args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.StrafingAirJumpExtremumAirRollBackwardsAnimationStateName())

                    &&

                    !args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.StrafingAirJumpExtremumAirRollLeftAnimationStateName())

                    &&
                    !args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.StrafingAirJumpExtremumAirRollRightAnimationStateName()))
            {
                args.playerAnimationController.ChangeAnimationState(
                    RaymanAnimations.FallingAnimationStateName());
            }

            if (args.legacyPlayerMovementInput.GetJumpButton() &&
                    !args.legacyPlayerMovementStateInfo.isHelicopter
                    && args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState)
            {
                args.legacyPlayerMovementStateInfo.isHelicopter = true;
                args.playerController.playerParams.soundParams
                    .helicopterSounds.helicopterSound.PlayLoopedWithAudioSource(
                        args.playerController.controllerContext
                            .playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );

                //this.helicopterSound.Play();
                args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                args.playerAnimationController.ChangeAnimationStateWithPriority(
                   RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName(), priority: 4);
            }

            if (args.legacyPlayerMovementStateInfo.isHelicopter
                    && !args.legacyPlayerMovementInput.GetJumpButton() &&
                    args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState
                    && (!args.playerAnimationController.GetCurrentAnimationState()
                        .Equals(RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER].animationName) || args.playerAnimationController.HasAnimationEnded()))
            {
                args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                args.legacyPlayerMovementStateInfo.isHelicopter = false;
                args.playerController.playerParams.soundParams
                    .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                        args.playerController.controllerContext
                            .playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );

                var a = args.subject.transform.GetComponentsInChildren<AudioSource>()[1];
                if (a.isPlaying)
                {
                    a.Stop();
                }

                //this.helicopterSound.Pause();
                args.playerAnimationController.ChangeAnimationState(
                   RaymanAnimations.FallingAnimationStateName());
            }

            if ((args.playerAnimationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName())
                || (args.playerAnimationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER].animationName)
                     )) &&
                  args.playerAnimationController.HasAnimationEnded())
            {
                args.playerAnimationController.ChangeAnimationState(
                   RaymanAnimations.HelicopterStateName());

                if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                {
                    args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.HelicopterStateName(), 0f, Time.fixedDeltaTime);
                }
            }

            RaycastHit ledgeGrabHit = new RaycastHit();
            RaycastHit wallClimbHit = new RaycastHit();
            RaycastHit roofHangingHit = new RaycastHit();
            if (args.legacyRayCollider.IsHittingGround(
                args.playerController.controllerContext.gravityDirection) && args.legacyPlayerMovementStateInfo.movementVelocity.y <= 0)
            {
                // allow falling animation to be interrupted
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.FallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(), newPriority: 1);
                args.legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_STRAFING_GROUND;
                args.legacyPlayerMovementStateInfo.isHelicopter = false;
                args.playerController.playerParams.soundParams
                    .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                        args.playerController.controllerContext
                            .playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );
                var a = args.subject.transform.GetComponentsInChildren<AudioSource>()[1];
                if (a.isPlaying)
                {
                    a.Stop();
                }
                //this.helicopterSound.Pause();
                args.legacyPlayerMovementStateInfo.previousLocalGroundContactPointOffset = null;
                args.playerRulesTransitions.EnterGroundStrafing();
                return;
            }
            else if (args.legacyRayCollider.IsCollidingWithLedgeGrabCollider(
                args.playerController.controllerContext.gravityDirection,
                args.playerController.controllerContext.gravityForward,
                out ledgeGrabHit))
            {
                args.legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_LEDGE_GRABBING;
                args.legacyPlayerMovementStateInfo.isHelicopter = false;
                args.playerController.playerParams.soundParams
                    .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                        args.playerController.controllerContext
                            .playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );
                //this.helicopterSound.Pause();
                //args.playerController.GetComponent<RuleLedgeGrabbing>().ClearRuleState(ledgeGrabHit);
                args.playerController.controllerContext.playerRulesTransitions.EnterLedgeGrabbing(ledgeGrabHit);

                var a = args.subject.transform.GetComponentsInChildren<AudioSource>()[1];
                if (a.isPlaying)
                {
                    a.Stop();
                }

                return;
            }
            else if (args.legacyRayCollider.IsCollidingWithWallClimbObject(
                -args.playerAnimatedModel.playerAnimatedModelTransform.transform.forward,
                args.playerAnimatedModel.playerAnimatedModelTransform.transform.up,
                -args.playerAnimatedModel.playerAnimatedModelTransform.transform.right,
                args.playerController.controllerContext.gravityDirection,
                args.playerController.controllerContext.gravityForward,
                out wallClimbHit))
            {
                args.legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_WALL_CLIMBING;
                //this.helicopterSound.Pause();
                args.legacyPlayerMovementStateInfo.isHelicopter = false;
                args.playerController.playerParams.soundParams
                    .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                        args.playerController.controllerContext
                            .playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );
                //args.playerController.GetComponent<RuleWallClimbing>().ClearRuleState(wallClimbHit);
                args.playerController.controllerContext.playerRulesTransitions.EnterWallClimbing(wallClimbHit);

                var a = args.subject.transform.GetComponentsInChildren<AudioSource>()[1];
                if (a.isPlaying)
                {
                    a.Stop();
                }
                return;
            }
            else if (args.legacyRayCollider.IsHittingRoofHanging(
                args.playerController.controllerContext.gravityDirection, out roofHangingHit))
            {
                args.legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_ROOF_HANGING;
                //this.helicopterSound.Pause();
                args.legacyPlayerMovementStateInfo.isHelicopter = false;
                args.playerController.playerParams.soundParams
                    .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                        args.playerController.controllerContext
                            .playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );
                //args.playerController.GetComponent<RuleRoofHanging>().ClearRuleState(roofHangingHit);
                args.playerController.controllerContext.playerRulesTransitions.EnterRoofHanging(roofHangingHit);

                var a = args.subject.transform.GetComponentsInChildren<AudioSource>()[1];
                if (a.isPlaying)
                {
                    a.Stop();
                }
                return;
            }
            else if (!args.legacyPlayerMovementInput.GetJumpButton())
            {
                args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = true;
            }
        }
    }
}
