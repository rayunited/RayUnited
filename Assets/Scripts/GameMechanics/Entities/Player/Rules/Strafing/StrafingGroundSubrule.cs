﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Utils;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.PlayerMechanics.Rules;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.Utils;
using UnityEngine;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts.Ground;
using Assets.Scripts.PlayerMechanics.Camera;
using Assets.Scripts.Sound;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Strafing
{
    public static class StrafingGroundSubruleState
    {
        public const string RANDOM = "RANDOM";
    }

    public static class StrafingGroundSubruleEffects
    {
        public const string INIT_EFFECT = "INIT_EFFECT";
    }

    public class StrafingGroundSubrule : PlayerMovementRule
    {
        public const string RULE_ID = "PLAYER_MOVEMENT_SUBRULE_STRAFING_GROUND";

        public StrafingGroundSubrule(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void OnPortalTravel()
        {
        }

        public override void OnShooting(float fistShootingPowerNormalized)
        {
            var args = UseEntityContext();
            var context = UseContext<StrafingGroundSubContext>();

            RefObject<System.Random> random = UseRef<System.Random>(StrafingGroundSubruleState.RANDOM, null);

            UseEffect(StrafingGroundSubruleEffects.INIT_EFFECT, () =>
            {
                random.current = new System.Random();
            }, Tuple.Create(0));

            if (fistShootingPowerNormalized >= 0.9f)
            {
                {
                    args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName,
                            priority: 15);
                    if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                    {
                        args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName, 0f, Time.fixedDeltaTime);
                    }
                }
                var s = GameObject.FindObjectOfType<BasicSoundsMap>();
                AudioSource.PlayClipAtPoint(s.chargedShootsVariants[random.current.Next(0, s.chargedShootsVariants.Length)],
                    GameObject.FindObjectOfType<CameraActorHandle>().transform.position);
            }
        }

        public override void GameplayFixedUpdate()
        {
            #region Rule
            var args = UseEntityContext();
            var context = UseContext<StrafingGroundSubContext>();

            float playerStrafingSpeed = 0.12f;

            if (!args.legacyPlayerMovementInput.GetStrafingButton() &&
                !args.legacyPlayerGroundRollingAspect.IsPerformingGroundRoll())
            {
                // exit strafing ground rule right away and go into regular ground rule
                //args.playerController.GetComponent<RuleGround>().SetTransitionData(fromGroundStrafing: true);
                args.legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_GROUND;
                args.legacyPlayerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_FOLLOW;
                args.playerController.controllerContext.cameraController.controllerContext.cameraRulesTransitions.EnterFollow();
                args.playerController.controllerContext.playerRulesTransitions.EnterGroundRegular(
                    enteringFromGroundStrafing: true
                    );
                return;
            }

            HandleShooting(args);
            // Debug.Log("ANIIIIM ground strafing " + args.playerAnimationController.GetCurrentAnimationState() + " ---- " + args.playerAnimationController.GetAnimationNormalizedTime());
            args.legacyPlayerMovementStateInfo.previousLocalGroundContactPointOffset =
                args.legacyRayCollider.AlignOnTopOfTheGround(
                    args.legacyPlayerMovementStateInfo.previousLocalGroundContactPointOffset,
                    args.subject.TransformDirection(args.legacyPlayerMovementStateInfo.movementVelocity),
                    args.playerController.controllerContext.gravityDirection);

            Vector3 inputTranslation = args.legacyPlayerMovementInput.GetTranslation();
            Vector3 translation = args.legacyPlayerMovementInput.GetTranslation() * playerStrafingSpeed;

            float helperAngle = Vector3.SignedAngle(Vector3.forward, translation.normalized, Vector3.up);

            Vector3 translationDirectionInWorldSpace =
                args.subject.TransformDirection(translation).normalized;

            args.legacyPlayerMovementStateInfo.movementVelocity.y = 0;

            if (inputTranslation.magnitude < 0.1)
            {
                float movementSmoothingInterpolation = 0.6f;
                args.legacyPlayerMovementStateInfo.movementVelocity.x =
                    Mathf.Lerp(args.legacyPlayerMovementStateInfo.movementVelocity.x, 0, movementSmoothingInterpolation);
                args.legacyPlayerMovementStateInfo.movementVelocity.z =
                    Mathf.Lerp(args.legacyPlayerMovementStateInfo.movementVelocity.z, 0, movementSmoothingInterpolation);
            }
            else
            {
                //float movementSmoothingInterpolation = 0.25f;
                float movementSmoothingInterpolation = 0.25f;
                args.legacyPlayerMovementStateInfo.movementVelocity.x =
                    Mathf.Lerp(args.legacyPlayerMovementStateInfo.movementVelocity.x, translation.x, movementSmoothingInterpolation);
                args.legacyPlayerMovementStateInfo.movementVelocity.z =
                    Mathf.Lerp(args.legacyPlayerMovementStateInfo.movementVelocity.z, translation.z, movementSmoothingInterpolation);

                args.legacyPlayerMovementMetrics.intentionalMovingDirection =
                    Vector3.ProjectOnPlane(
                        args.subject.TransformDirection(args.legacyPlayerMovementStateInfo.movementVelocity),
                            -args.playerController.controllerContext.gravityDirection).normalized;
            }

            if (inputTranslation.magnitude > 0.1)
            {
                PlayerOrientationHelper.OrientatePlayerObject(
                    args.subject,
                    PlayerPortalsHelper.TransformPortalsProjectedDirectionToWorldDirectionThroughChainOfPortalsTransitions(
                        args.playerController.controllerContext.playerPortalTraveller.passedPortals,
                        args.playerController.controllerContext.baseCameraOrientedForwardDirectionForPlayer.Invoke(),
                        args.playerController.controllerContext.gravityDirection
                    ),
                    args.playerController.controllerContext.gravityForward,
                    args.playerController.controllerContext.gravityRight,
                    args.playerController.controllerContext.gravityDirection
                );

                if (args.legacyPlayerMovementInput.GetGroundRollButton()
                && (!args.legacyPlayerGroundRollingAspect.IsPerformingGroundRoll() && !args.legacyPlayerGroundRollingAspect.HasEndedGroundRoll()))
                {
                    args.legacyPlayerGroundRollingAspect.StartPerformingStrafingGroundRoll(
                        -args.playerAnimatedModel.playerAnimatedModelTransform.transform.forward.normalized,
                        -args.playerAnimatedModel.playerAnimatedModelTransform.transform.right.normalized,
                        translationDirectionInWorldSpace.normalized);
                    return;
                }
            }

            if (args.legacyPlayerTargetingAspect.currentTarget)
            {
                Vector3 targetLookingDirection =
                    args.legacyPlayerTargetingAspect.GetPlayerLookingDirectionTowardsTarget(
                        args.playerAnimatedModel.playerAnimatedModelTransform.transform.position,
                        args.playerController.controllerContext.gravityDirection);

                PlayerOrientationHelper.OrientatePlayerAnimatedPart(
                    args.playerAnimatedModel.playerAnimatedModelTransform.transform,
                    targetLookingDirection,
                    args.playerController.controllerContext.gravityForward,
                    args.playerController.controllerContext.gravityRight,
                    args.playerController.controllerContext.gravityDirection
                );

                args.legacyPlayerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_TARGET_PLAYER_BACK_STRAFING;
                args.cameraController.controllerContext.cameraRulesTransitions.EnterTargetPlayerBackStrafing();
            }
            else
            {
                args.legacyPlayerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_STRAFING;
                args.cameraController.controllerContext.cameraRulesTransitions.EnterStrafing();
                //args.playerRulesTransitions.EnterAirStrafing(isJumping: true);
            }

            if (args.legacyPlayerGroundRollingAspect.IsPerformingGroundForwardRoll())
            {
                args.legacyPlayerMovementStateInfo.movementVelocity =
                    args.subject.InverseTransformDirection(
                        args.legacyPlayerGroundRollingAspect.GetMovementVelocityAppropriateForCurrentForwardRollCycle());
            }
            else if (args.legacyPlayerGroundRollingAspect.IsPerformingGroundLeftOrRightRoll())
            {
                args.legacyPlayerMovementStateInfo.movementVelocity =
                    args.subject.InverseTransformDirection(
                        args.legacyPlayerGroundRollingAspect.GetMovementVelocityAppropriateForCurrentLeftRightRollCycle());
            }
            else if (args.legacyPlayerGroundRollingAspect.IsPerformingGroundBackwardsRoll())
            {
                args.legacyPlayerMovementStateInfo.movementVelocity =
                    args.subject.InverseTransformDirection(
                        args.legacyPlayerGroundRollingAspect.GetMovementVelocityAppropriateForCurrentBackwardsRollCycle());
            }

            args.legacyPlayerMovementStateInfo.movementVelocity = args.subject.InverseTransformDirection
                (PlayerWallCollisionHelper
                   .GetMovementVelocityConsideringWallCollisions(
                    args.subject.gameObject,
                    args.legacyRayCollider,
                    args.legacyPlayerMovementMetrics.intentionalMovingDirection.normalized,
                    args.subject.TransformDirection(args.legacyPlayerMovementStateInfo.movementVelocity),
                    args.playerController.controllerContext.gravityDirection,
                    args.playerController.controllerContext.gravityForward));

            args.subject.Translate(args.legacyPlayerMovementStateInfo.movementVelocity);

            if (args.legacyPlayerMovementInput.GetJumpButton()
                && args.legacyRayCollider.IsHittingGround(args.playerController.controllerContext.gravityDirection)
                && args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState)
            {
                args.legacyPlayerMovementStateInfo.isJumping = true;
                args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                args.legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_STRAFING_AIR;
                args.legacyPlayerGroundRollingAspect.BreakTheRollCycle();
                args.playerRulesTransitions.EnterAirStrafing(isJumping: true);
                return;
            }

            if (!args.legacyRayCollider.IsHittingGround(args.playerController.controllerContext.gravityDirection))
            {
                args.legacyPlayerMovementStateInfo.isJumping = false;
                args.legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_STRAFING_AIR;
                args.legacyPlayerGroundRollingAspect.BreakTheRollCycle();
                args.playerRulesTransitions.EnterAirStrafing(isJumping: false);
                return;
            }

            if (!args.legacyPlayerMovementInput.GetJumpButton())
            {
                args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
                args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
            }

            #endregion
            #region Animation
            if (!args.legacyPlayerGroundRollingAspect.IsPerformingGroundRoll())
            {
                if (inputTranslation.magnitude > 0.1)
                {
                    float angleBetweenTranslationAndAnimatedPlayerPartForward =
                        Vector3.Angle(
                            -args.playerAnimatedModel.playerAnimatedModelTransform.transform.forward.normalized, translationDirectionInWorldSpace);

                    float angleBetweenTranslationAndAnimatedPlayerPartRight =
                        Vector3.Angle(
                            -args.playerAnimatedModel.playerAnimatedModelTransform.transform.right.normalized, translationDirectionInWorldSpace
                            );
                    if (angleBetweenTranslationAndAnimatedPlayerPartForward < 20f)
                    {
                        // going straight forward
                        args.playerAnimationController.ChangeAnimationState(
                            RaymanAnimations.StrafingForwardStateName());
                    }
                    else if (angleBetweenTranslationAndAnimatedPlayerPartForward < 80f)
                    {
                        // going forward left or forward right
                        if (angleBetweenTranslationAndAnimatedPlayerPartRight < 90f)
                        {
                            // going forward right
                            args.playerAnimationController.ChangeAnimationState(
                                RaymanAnimations.StrafingForwardRightStateName());
                        }
                        else
                        {
                            // going forward left
                            args.playerAnimationController.ChangeAnimationState(
                                RaymanAnimations.StrafingForwardLeftStateName());
                        }
                    }
                    else if (angleBetweenTranslationAndAnimatedPlayerPartForward < 90f)
                    {
                        // going left or right
                        if (angleBetweenTranslationAndAnimatedPlayerPartRight < 90f)
                        {
                            // going right
                            args.playerAnimationController.ChangeAnimationState(
                                RaymanAnimations.StrafingRightStateName());
                        }
                        else
                        {
                            // going left
                            args.playerAnimationController.ChangeAnimationState(
                                RaymanAnimations.StrafingLeftStateName());
                        }
                    }
                    else if (angleBetweenTranslationAndAnimatedPlayerPartForward < 160f)
                    {
                        // going backwards left or backwards right
                        if (angleBetweenTranslationAndAnimatedPlayerPartRight < 90f)
                        {
                            // going backwards right
                            args.playerAnimationController.ChangeAnimationState(
                                RaymanAnimations.StrafingBackwardsRightStateName());
                        }
                        else
                        {
                            // going backwards left
                            args.playerAnimationController.ChangeAnimationState(
                                RaymanAnimations.StrafingBackwardsLeftStateName());
                        }
                    }
                    else
                    {
                        // going straight backwards
                        args.playerAnimationController.ChangeAnimationState(
                            RaymanAnimations.StrafingBackwardsStateName());
                    }

                    //animationController.ChangeAnimationState(
                    //    RaymanAnimations.StrafingIdleStateName());
                }
                else if ((!args.playerAnimationController.GetCurrentAnimationState()
                    .Equals(RaymanAnimations.StrafingEnterFromIdleGroundStateName())
                    && !args.playerAnimationController.GetCurrentAnimationState().Equals(RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName))
                    || args.playerAnimationController.HasAnimationEnded())
                {
                    args.playerAnimationController.ChangeAnimationState(
                        RaymanAnimations.StrafingIdleStateName());

                    if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                    {
                        args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.StrafingIdleStateName(), 0f, Time.fixedDeltaTime);
                    }
                }
            }
            #endregion
        }
    }
}
