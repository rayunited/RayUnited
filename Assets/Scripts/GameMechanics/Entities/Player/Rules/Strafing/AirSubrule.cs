﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.Entity.Rules.Utils;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts.Air;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Utils;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Camera;
using Assets.Scripts.PlayerMechanics.Rules;
using Assets.Scripts.Sound;
using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Strafing
{
    public static class AirSubruleRefs
    {
        public const string IS_HELICOPTER = "IS_HELICOPTER";
        public const string RANDOM = "RANDOM";
    }

    public static class AirSubruleState
    {
        public const string IS_JUMPING = "IS_JUMPING";
        public const string INTENTIONAL_MOVEMENT_DIRECTION = "INTENTIONAL_MOVEMENT_DIRECTION";
        public const string RANDOM = "RANDOM";
    }

    public static class AirSubruleEffects
    {
        public const string INITIAL_VELOCITY_EFFECT = "INITIAL_VELOCITY_EFFECT";
    }

    public class AirSubrule : PlayerMovementRule
    {
        public const string RULE_ID = "PLAYER_MOVEMENT_SUBRULE_AIR";

        public AirSubrule(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void OnPortalTravel()
        {
        }

        public override void OnShooting(float fistShootingPowerNormalized)
        {
            var args = UseEntityContext();
            var context = UseContext<AirSubContext>();

            RefObject<bool> isHelicopter =
                UseRef<bool>(AirSubruleRefs.IS_HELICOPTER, context.isHelicopter);
            RefObject<System.Random> random = UseRef<System.Random>(AirSubruleState.RANDOM, null);

            if (fistShootingPowerNormalized >= 0.9f)
            {
                if (isHelicopter.current)
                {
                    args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER].animationName,
                            priority: 15);
                    if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                    {
                        args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER].animationName, 0f, Time.fixedDeltaTime);
                    }
                }
                else
                {
                    args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName,
                            priority: 15);
                    if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                    {
                        args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName, 0f, Time.fixedDeltaTime);
                    }
                }
                var s = GameObject.FindObjectOfType<BasicSoundsMap>();
                AudioSource.PlayClipAtPoint(s.chargedShootsVariants[random.current.Next(0, s.chargedShootsVariants.Length)],
                    GameObject.FindObjectOfType<CameraActorHandle>().transform.position);
            }
        }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<AirSubContext>();

            const float playerInAirSpeed = 0.15f;
            const float gravityAcceleration = 0.012f;
            const float limitFallSpeed = 0.3f;
            const float limitFallSpeedHelicopter = 0.03f;

            RefObject<bool> isHelicopter =
                UseRef<bool>(AirSubruleRefs.IS_HELICOPTER, context.isHelicopter);
            (bool isJumping, Action<bool> SetIsJumping) =
                UseState<bool>(AirSubruleState.IS_JUMPING,
                    context.isJumping);
            RefObject<System.Random> random = UseRef<System.Random>(GroundSubruleStates.RANDOM, null);

            var isMaxChargedShootWithHelicopterAnim =
                    args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER].animationName);
            var isMaxChargedShootAnim = args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName);

            var maxChargeHelicopterAnim = RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER].animationName;
            var maxChargeAnim = RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName;

            if (isMaxChargedShootWithHelicopterAnim && args.playerAnimationController.HasAnimationEnded())
            {
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(maxChargeHelicopterAnim, 0);

                if (!args.playerInputHub.jumpButton)
                {
                    args.subject.GetComponentsInChildren<AudioSource>()[1].Stop();
                    isHelicopter.current = false;
                    args.playerAnimationController.ChangeAnimationState(RaymanAnimations.FallingAnimationStateName());
                }
                else
                {
                    args.playerAnimationController.ChangeAnimationState(RaymanAnimations.HelicopterStateName());
                }

            }

            if (isMaxChargedShootAnim && args.playerAnimationController.HasAnimationEnded())
            {
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(maxChargeAnim, 0);
                args.playerAnimationController.ChangeAnimationState(RaymanAnimations.FallingAnimationStateName());
            }

            (Vector3 intentionalMovementDirection, Action<Vector3> SetIntentionalMovementDirection)
                = UseState<Vector3>(
                    AirSubruleState.INTENTIONAL_MOVEMENT_DIRECTION,
                    context.initialIntentionalMovementDirection);

            HandleShooting(args);

            UseEffect(AirSubruleEffects.INITIAL_VELOCITY_EFFECT, () =>
            {
                random.current = new System.Random();

                args.kinematics.velocity =
                    context.initialVelocity;
                args.platformerCollisionHub.GameplayFixedUpdate(
                    args.subject, args.kinematics, args.gravityDirection);
                args.playerInputHubStateHub.DismissStatesFixedUpdate();

                if (isJumping)
                {
                    SetIsJumping(false);

                    if (args.legacyPlayerMovementInput
                        .GetTranslation().magnitude > 0.7f)
                    {
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.JumpStartRunningBounceFromGroundAnimationStateName(),
                            priority: 3);
                        args.playerParams.soundParams.airSounds.jumpingSounds
                            .runningJumpOffTheGroundSound
                            .PlayOnceWithAudioSource(
                                args.playerAudioHandles.airAudioHandles.runningJumpOffTheGroundAudioSource);
                    }
                    else
                    {
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(),
                            priority: 3);
                        args.playerParams.soundParams.airSounds.jumpingSounds
                            .standingJumpOffTheGroundSound
                            .PlayOnceWithAudioSource(
                                args.playerAudioHandles.airAudioHandles.standingJumpOffTheGroundAudioSource);
                    }
                }
            }, Tuple.Create(0));

            args.playerInputEffects.OnJumpButtonDown(() =>
            {
                isHelicopter.current = true;

                var a = args.subject.GetComponentsInChildren<AudioSource>()[1];
                a.clip = GameObject.FindObjectOfType<BasicSoundsMap>().helicopter;
                a.volume = 0.4f;
                a.loop = true;
                a.Play();

                args.playerParams.soundParams
                    .helicopterSounds.helicopterSound.PlayLoopedWithAudioSource(
                        args.playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );

                args.playerAnimationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName(),
                    priority: 4);
            });

            args.playerInputEffects.OnJumpButtonUp(() =>
            {
                var isMaxChargedShootWithHelicopterAnim =
                    args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER].animationName);

                if (isHelicopter.current && !isMaxChargedShootWithHelicopterAnim)
                {
                    args.subject.GetComponentsInChildren<AudioSource>()[1].Stop();

                    // if exiting helicopter case...
                    args.playerParams.soundParams.helicopterSounds.helicopterSound
                        .StopPlayingWithAudioSource(
                            args.playerAudioHandles.helicopterAudioHandles.helicopterAudioSource);
                    args.playerAnimationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.FallingAnimationStateName(), priority: 10);
                    if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                    {
                        args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.FallingAnimationStateName(), 0f, Time.fixedDeltaTime);
                    }
                }
                else if (!isMaxChargedShootWithHelicopterAnim)
                {
                    args.playerAnimationController.ChangeAnimationState(
                        RaymanAnimations.FallingAnimationStateName());
                    if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                    {
                        args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.FallingAnimationStateName(), 0f, Time.fixedDeltaTime);
                    }
                }

                if (!isMaxChargedShootWithHelicopterAnim)
                {
                    isHelicopter.current = false;
                }

                //args.playerSounds.helicopterSound.Pause();               
            });

            args.playerInputWrapperEffects
                .OnMoveInputMagnitudeBiggerOrEqualThan(0.1f, (wrappedPlayerMoveVector3) =>
                {
                    var newVector = Vector3.ProjectOnPlane(
                        wrappedPlayerMoveVector3.normalized, -args.gravityDirection)
                        .normalized;

                    SetIntentionalMovementDirection(newVector);
                    if (newVector.magnitude > 0.001f && Vector3.Angle(newVector, -args.gravityDirection) > 80f &&
                        Vector3.Angle(newVector, args.gravityDirection) > 80f)
                    {
                        args.legacyPlayerMovementMetrics.intentionalMovingDirection = newVector;
                    }

                });
            Debug.DrawRay(args.subject.transform.position, args.legacyPlayerMovementMetrics.intentionalMovingDirection * 100f, Color.red);

            args.platformerCollisionEffects.OnCollisionWithSteadyGroundWithVelocity(
                (collisionNormal, collisionPoint) =>
                {
                    args.subject.GetComponentInChildren<AudioSource>().Stop();
                    args.subject.GetComponentsInChildren<AudioSource>()[1].Stop();
                    args.legacyPlayerMovementStateInfo.previousLocalGroundContactPointOffset = null;
                    args.playerRulesTransitions.EnterGround(landingWithHelicopter: isHelicopter.current, landing: true);
                });

            PlayerInputKinematicsUtils.FadeOutHorizontalVelocityOnNoInput(
                args);
            PlayerInputKinematicsUtils.FadeInHorizontalVelocityOnInput(
                args, !isHelicopter.current ? playerInAirSpeed : playerInAirSpeed / 2f);

            args.kinematics.velocity = PlayerWallCollisionHelper
                .GetMovementVelocityConsideringWallCollisions(
                    args.subject.gameObject,
                    args.legacyRayCollider,
                    intentionalMovementDirection,
                    args.kinematics.velocity,
                    args.gravityDirection,
                    args.gravityForward);

            args.kinematics.velocity = args.legacyRayCollider
                .AdjustVelocityIfIsHittingTheCeiling(
                   args.kinematics.velocity, args.gravityDirection, args.gravityForward);

            if (!isHelicopter.current)
            {
                if (!args.playerAnimationController.GetCurrentAnimationState()
                    .Equals(RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName()) ||
                    args.playerAnimationController.GetAnimationNormalizedTime() >= 0.5f)
                {
                    args.kinematics.velocity =
                        GravityUtils.AccelerateGravitationallyUpUntilLimitFallSpeed(
                            args.kinematics.velocity,
                            args.gravityDirection,
                            gravityAcceleration,
                            limitFallSpeed
                        );
                }
                else
                {
                    args.kinematics.velocity = Vector3.ProjectOnPlane(args.kinematics.velocity, -args.gravityDirection);
                }
                
            }
            else
            {
                args.kinematics.velocity =
                    GravityUtils.FallAtSpeed(
                        args.kinematics.velocity,
                        args.gravityDirection,
                        limitFallSpeedHelicopter
                    );
            }

            if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.JumpStartRunningBounceFromGroundAnimationStateName()) &&
                  args.playerAnimationController.HasAnimationEnded())
            {
                if (Vector3ProjectionHelper.ProjectConsideringDirection(args.kinematics.velocity, -args.gravityDirection).magnitude *
                    Mathf.Sign(Vector3.Dot(args.kinematics.velocity, -args.gravityDirection)) < 0.1f)
                {
                    args.playerParams.soundParams.airSounds.jumpingSounds.airFlipRunningJumpSound
                        .PlayOnceWithAudioSource(
                            args.playerAudioHandles.airAudioHandles.airFlipRunningJumpAudioSource);
                    args.playerAnimationController
                        .ChangeAnimationState(
                        RaymanAnimations
                            .AirRollFromJumpStartRunningBounceFromGroundAnimationStateName());
                    AudioSource.PlayClipAtPoint(GameObject.FindObjectOfType<BasicSoundsMap>().airFlip, args.subject.position);

                    if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                    {
                        args.playerAnimationController.SetAnimationFrameInManualUpdating(
                            RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName(), 0f, Time.fixedDeltaTime);
                    }
                }
            }
            else if ((args.playerAnimationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName())
                    && args.playerAnimationController.HasAnimationEnded())
               )
            {
                args.playerAnimationController.ChangeAnimationState(
                    RaymanAnimations.FallingFromJumpFromRunningAnimationStateName());
            }
            else
                if ((Vector3ProjectionHelper.ProjectConsideringDirection(args.kinematics.velocity, -args.gravityDirection).magnitude *
                        Mathf.Sign(Vector3.Dot(args.kinematics.velocity, -args.gravityDirection)) < 0.1f &&
                    args.playerAnimationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName()))

                    ||

                    (!isHelicopter.current &&
                    !args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.FallingFromJumpFromRunningAnimationStateName()))

                        &&

                    !args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName()))
            {
                args.playerAnimationController.ChangeAnimationState(
                    RaymanAnimations.FallingAnimationStateName());
            }

            if ((args.playerAnimationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName()) &&
                  args.playerAnimationController.HasAnimationEnded()) ||
                  (((args.playerAnimationController.GetCurrentAnimationState().Equals(RaymanAnimations.FallingAnimationStateName())
                  ||
                  (args.playerAnimationController.GetCurrentAnimationState().Equals(
                      RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER].animationName) && args.playerAnimationController.HasAnimationEnded())))
                  && isHelicopter.current))
            {
                args.playerParams.soundParams
                     .helicopterSounds.helicopterSound.PlayLoopedWithAudioSource(
                      args.playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );
                args.playerAnimationController.ChangeAnimationStateWithPriority(
                   RaymanAnimations.HelicopterStateName(), priority: 10);
                if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                {
                    args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.HelicopterStateName(), 0f, Time.fixedDeltaTime);
                }
            }

            // <----------- COMPLETELY LEGACY BELOW ----------------->


            // completely legacy rules transitions, copy pasted for now directly
            RaycastHit ledgeGrabHit = new RaycastHit();
            RaycastHit wallClimbHit = new RaycastHit();
            RaycastHit roofHangingHit = new RaycastHit();
            if (args.legacyRayCollider.IsHittingGround(args.gravityDirection)
                && Vector3ProjectionHelper.ProjectConsideringDirection(args.kinematics.velocity, -args.gravityDirection).magnitude *
                    Mathf.Sign(Vector3.Dot(args.kinematics.velocity, -args.gravityDirection)) <= 0)
            {
                args.playerParams.soundParams
                     .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                      args.playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );

                // allow falling animation to be interrupted
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.FallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.HelicopterStateName(), newPriority: 0);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(), newPriority: 1);
                args.legacyPlayerMovementStateInfo
                    .currentRule = PlayerMovementRuleEnum.RULE_GROUND;
                //args.subject.GetComponent<RuleGround>().SetTransitionData(
                //    landing: true,
                //    withHelicopterFlag: isHelicopter.current);

                args.kinematics.velocity = Vector3.zero;
                // args.playerController.SetCurrentRule(new PlayerMovementRuleGround(args.playerController));

                args.playerController.controllerContext.playerRulesTransitions.EnterGround(
                    landingWithHelicopter: context.isHelicopter,
                    landing: true
                );

                args.subject.GetComponentInChildren<AudioSource>().Stop();
                args.subject.GetComponentsInChildren<AudioSource>()[1].Stop();
                return;
            }
            else if (args.legacyRayCollider
                .IsCollidingWithLedgeGrabCollider(args.gravityDirection, args.gravityForward, out ledgeGrabHit))
            {
                args.playerParams.soundParams
                     .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                      args.playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );

                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.FallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.HelicopterStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(), newPriority: 1);

                args.legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_LEDGE_GRABBING;
                args.legacyPlayerMovementStateInfo.isHelicopter = false;
                //args.playerSounds.helicopterSound.Pause();
                //args.subject.GetComponent<RuleLedgeGrabbing>()
                //    .ClearRuleState(ledgeGrabHit);



                args.kinematics.velocity = Vector3.zero;
                //args.playerController.SetCurrentRule(new PlayerMovementRuleLedgeGrabbing(args.playerController));

                args.playerController.controllerContext.playerRulesTransitions.EnterLedgeGrabbing(ledgeGrabHit);

                args.subject.GetComponentInChildren<AudioSource>().Stop();
                args.subject.GetComponentsInChildren<AudioSource>()[1].Stop();
                return;
            }
            else if (args.legacyRayCollider
                .IsCollidingWithWallClimbObject(
                args.playerAnimatedModel.forward,
                args.playerAnimatedModel.up,
                args.playerAnimatedModel.right,
                args.gravityDirection,
                args.gravityForward,
                out wallClimbHit))
            {
                args.playerParams.soundParams
                     .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                      args.playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );

                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.FallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.HelicopterStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(), newPriority: 1);

                args.legacyPlayerMovementStateInfo
                    .currentRule = PlayerMovementRuleEnum.RULE_WALL_CLIMBING;
                //args.playerSounds.helicopterSound.Pause();
                args.legacyPlayerMovementStateInfo.isHelicopter = false;
                //args.subject.
                //    GetComponent<RuleWallClimbing>().ClearRuleState(wallClimbHit);

                args.kinematics.velocity = Vector3.zero;
                // args.playerController.SetCurrentRule(new PlayerMovementRuleWallClimbing(args.playerController));

                args.playerController.controllerContext.playerRulesTransitions.EnterWallClimbing(wallClimbHit);

                args.subject.GetComponentInChildren<AudioSource>().Stop();
                args.subject.GetComponentsInChildren<AudioSource>()[1].Stop();
                return;
            }
            else if (args.legacyRayCollider
                .IsHittingRoofHanging(args.gravityDirection, out roofHangingHit))
            {
                args.playerParams.soundParams
                     .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                      args.playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );

                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.FallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.HelicopterStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(), newPriority: 1);

                args.legacyPlayerMovementStateInfo
                    .currentRule = PlayerMovementRuleEnum.RULE_ROOF_HANGING;
                //args.playerSounds.helicopterSound.Pause();
                args.legacyPlayerMovementStateInfo.isHelicopter = false;
                //args.subject
                //    .GetComponent<RuleRoofHanging>().ClearRuleState(roofHangingHit);

                args.kinematics.velocity = Vector3.zero;
                // args.playerController.SetCurrentRule(new PlayerMovementRuleRoofHanging(args.playerController));

                args.playerController.controllerContext.playerRulesTransitions.EnterRoofHanging(roofHangingHit);

                args.subject.GetComponentInChildren<AudioSource>().Stop();
                args.subject.GetComponentsInChildren<AudioSource>()[1].Stop();
                return;
            }
            else if (args.playerInputHub.strafingButton)
            {
                args.playerParams.soundParams
                     .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                      args.playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );

                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.FallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.HelicopterStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(), newPriority: 1);

                args.legacyPlayerMovementStateInfo
                    .currentRule = PlayerMovementRuleEnum.RULE_STRAFING_AIR;
                //args.playerSounds.helicopterSound.Pause();
                args.legacyPlayerMovementStateInfo.isHelicopter = isHelicopter.current;
                //args.subject
                //    .GetComponent<RuleStrafingAir>().ClearRuleState(roofHangingHit);

                args.legacyPlayerMovementStateInfo.isJumping = false;
                args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = !isHelicopter.current;

                args.legacyPlayerMovementStateInfo.movementVelocity = args.subject.InverseTransformDirection(args.kinematics.velocity);
                args.kinematics.velocity = Vector3.zero;
                // args.playerController.SetCurrentRule(new EmptyNewPlayerMovementRule(args.playerController));
                args.playerController.controllerContext.playerRulesTransitions.EnterAirStrafing(context.isJumping);

                args.subject.GetComponentInChildren<AudioSource>().Stop();
                args.subject.GetComponentsInChildren<AudioSource>()[1].Stop();
            }
        }
    }
}
