﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Utils;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.PlayerMechanics.Camera;
using Assets.Scripts.PlayerMechanics.Rules;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.Sound;
using Assets.Scripts.Utils;
using UnityEngine;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using System;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts.Air;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts.Ground;
using System.Collections;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Strafing
{
    public static class GroundSubruleStates
    {
        public const string TIME_FROM_FIRE_PRESS_SECONDS = "TIME_FROM_FIRE_PRESS_SECONDS";
        public const string LANDING = "LANDING";
        public const string LANDING_WITH_HELICOPTER = "LANDING_WITH_HELICOPTER";
        public const string ENTERING_FROM_GROUND_STRAFING = "ENTERING_FROM_GROUND_STRAFING";
        public const string RANDOM = "RANDOM";
    }

    public static class GroundSubruleEffects
    {
        public const string INIT_EFFECT = "INIT_EFFECT";
    }

    public class GroundSubrule : PlayerMovementRule
    {
        public const string RULE_ID = "PLAYER_MOVEMENT_SUBRULE_GROUND";

        public GroundSubrule(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void OnPortalTravel()
        {
        }

        public override void OnShooting(float fistShootingPowerNormalized)
        {
            var args = UseEntityContext();
            var context = UseContext<GroundSubContext>();

            RefObject<System.Random> random = UseRef<System.Random>(GroundSubruleStates.RANDOM, null);

            if (fistShootingPowerNormalized >= 0.9f)
            {
                {
                    args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName,
                            priority: 15);
                    if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                    {
                        args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName, 0f, Time.fixedDeltaTime);
                    }
                }
                var s = GameObject.FindObjectOfType<BasicSoundsMap>();
                AudioSource.PlayClipAtPoint(s.chargedShootsVariants[random.current.Next(0, s.chargedShootsVariants.Length)],
                    GameObject.FindObjectOfType<CameraActorHandle>().transform.position);
            }
        }

        public override void GameplayFixedUpdate()
        {
            #region Rule

            var args = UseEntityContext();
            var context = UseContext<GroundSubContext>();

            float playerRunningSpeed = args.boostTimeSeconds > 0f ? 0.3f : 0.21f;

            if (args.boostTimeSeconds > 0f)
            {
                args.playerAnimationController.animator.speed = 1f;
                args.boostTimeSeconds -= Time.fixedDeltaTime;
            } else
            {
                args.playerAnimationController.animator.speed = 0f;
            }

            RefObject<float> timeFromFirePressSeconds = UseRef(GroundSubruleStates.TIME_FROM_FIRE_PRESS_SECONDS, Mathf.Infinity);
            RefObject<bool> landing = UseRef(GroundSubruleStates.LANDING, context.landing);
            RefObject<bool> landingWithHelicopter = UseRef(GroundSubruleStates.LANDING_WITH_HELICOPTER, context.landingWithHelicopter);
            RefObject<bool> enteringFromGroundStrafing = UseRef(GroundSubruleStates.ENTERING_FROM_GROUND_STRAFING, context.enteringFromGroundStrafing);
            RefObject<System.Random> random = UseRef<System.Random>(GroundSubruleStates.RANDOM, null);

            bool isSpeedBoost = args.isSpeedBoost;

            UseEffect(GroundSubruleEffects.INIT_EFFECT, () =>
            {
                random.current = new System.Random();
            }, Tuple.Create(0));

            if (args.legacyPlayerMovementInput.GetFireButton())
            {
                timeFromFirePressSeconds.current = 0f;
            }
            else
            {
                timeFromFirePressSeconds.current += Time.fixedDeltaTime;
            }

            if (args.legacyPlayerMovementInput.GetStrafingButton())
            {
                // exit ground rule right away and go into strafing ground rule
                args.legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_STRAFING_GROUND;

                args.legacyPlayerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_STRAFING;
                args.cameraController.controllerContext.cameraRulesTransitions.EnterStrafing();
                args.playerRulesTransitions.EnterGroundStrafing();

                args.playerController.playerParams.soundParams
                    .strafingGroundSounds.enteringStrafingSound.PlayOnceWithAudioSource(
                        args.playerController.controllerContext.playerAudioHandles
                        .strafingGroundAudioHandles.enteringLeavingStrafingAudioSource
                    );
                AudioSource.PlayClipAtPoint(GameObject.FindObjectOfType<BasicSoundsMap>().enterStrafing, args.subject.transform.position);
                return;
            }

            if (args.legacyPlayerMovementInput.GetGroundRollButton() &&
                (!args.legacyPlayerGroundRollingAspect.IsPerformingGroundRoll() && !args.legacyPlayerGroundRollingAspect.HasEndedGroundRoll()))
            {
                args.legacyPlayerGroundRollingAspect.StartPerformingForwardGroundRoll(
                    -args.playerAnimatedModel.playerAnimatedModelTransform.transform.forward);
                args.playerController.playerParams.soundParams.groundSounds.rollSound
                    .PlayOnceWithAudioSource(
                        args.playerController.controllerContext
                            .playerAudioHandles.groundAudioHandles.rollAudioSource);
                return;
            }

            HandleShooting(args);
            //Debug.Log("ANIIIIM ground " + this.playerAnimationController.GetCurrentAnimationState() + " ---- " + this.playerAnimationController.GetAnimationNormalizedTime());

            args.legacyPlayerMovementStateInfo.previousLocalGroundContactPointOffset =
                args.legacyRayCollider.AlignOnTopOfTheGround(
                    args.legacyPlayerMovementStateInfo.previousLocalGroundContactPointOffset,
                    args.subject.transform.TransformDirection(args.legacyPlayerMovementStateInfo.movementVelocity),
                    args.playerController.controllerContext.gravityDirection);

            Vector3 inputTranslation = args.legacyPlayerMovementInput.GetTranslation();
            Vector3 translation = args.legacyPlayerMovementInput.GetTranslation() * playerRunningSpeed;

            float helperAngle = Vector3.SignedAngle(Vector3.forward, translation.normalized, Vector3.up);

            Vector3 translationDirectionInWorldSpace =
                (Quaternion.AngleAxis(
                    args.legacyPlayerMovementMetrics.angleFromAbsoluteForward, -args.playerController.controllerContext.gravityDirection) *
                    (Quaternion.AngleAxis(helperAngle, -args.playerController.controllerContext.gravityDirection)
                    * args.playerController.controllerContext.gravityForward)).normalized;

            args.legacyPlayerMovementStateInfo.movementVelocity.y = 0;

            if (!args.legacyPlayerGroundRollingAspect.IsPerformingGroundRoll())
            {
                if (inputTranslation.magnitude < 0.1f)
                {
                    float movementSmoothingInterpolation = 0.6f;
                    args.legacyPlayerMovementStateInfo.movementVelocity.x =
                        Mathf.Lerp(args.legacyPlayerMovementStateInfo.movementVelocity.x, 0, movementSmoothingInterpolation);
                    args.legacyPlayerMovementStateInfo.movementVelocity.z =
                        Mathf.Lerp(args.legacyPlayerMovementStateInfo.movementVelocity.z, 0, movementSmoothingInterpolation);
                }
                else
                {
                    float movementSmoothingInterpolation = 0.25f;
                    args.legacyPlayerMovementStateInfo.movementVelocity.x =
                        Mathf.Lerp(args.legacyPlayerMovementStateInfo.movementVelocity.x, translation.x, movementSmoothingInterpolation);
                    args.legacyPlayerMovementStateInfo.movementVelocity.z =
                        Mathf.Lerp(args.legacyPlayerMovementStateInfo.movementVelocity.z, translation.z, movementSmoothingInterpolation);
                }

                if (inputTranslation.magnitude > 0.1f)
                {
                    PlayerOrientationHelper.OrientatePlayerObject(
                        args.subject.transform,
                        PlayerPortalsHelper.TransformPortalsProjectedDirectionToWorldDirectionThroughChainOfPortalsTransitions(
                            args.playerController.controllerContext.playerPortalTraveller.passedPortals,
                            args.playerController.controllerContext.baseCameraOrientedForwardDirectionForPlayer.Invoke(),
                            args.playerController.controllerContext.gravityDirection
                        ),
                        args.playerController.controllerContext.gravityForward,
                        args.playerController.controllerContext.gravityRight,
                        args.playerController.controllerContext.gravityDirection
                    );

                    PlayerOrientationHelper.OrientatePlayerAnimatedPart(
                        args.playerAnimatedModel.playerAnimatedModelTransform.transform,
                        translationDirectionInWorldSpace,
                        args.playerController.controllerContext.gravityForward,
                        args.playerController.controllerContext.gravityRight,
                        args.playerController.controllerContext.gravityDirection
                    );

                    args.legacyPlayerMovementMetrics.intentionalMovingDirection =
                        translationDirectionInWorldSpace.normalized;
                }
            }
            else
            {
                args.legacyPlayerMovementStateInfo.movementVelocity =
                    args.subject.transform.InverseTransformDirection(args.legacyPlayerGroundRollingAspect
                        .GetMovementVelocityAppropriateForCurrentForwardRollCycle());
            }

            args.legacyPlayerMovementStateInfo.movementVelocity = args.subject.transform.InverseTransformDirection
                (PlayerWallCollisionHelper
                   .GetMovementVelocityConsideringWallCollisions(
                    args.subject.gameObject,
                    args.legacyRayCollider,
                    args.legacyPlayerMovementMetrics.intentionalMovingDirection.normalized,
                    args.subject.transform.TransformDirection(args.legacyPlayerMovementStateInfo.movementVelocity),
                    args.playerController.controllerContext.gravityDirection,
                    args.playerController.controllerContext.gravityForward));

            Vector3 nonInterpolatedInputTranslation = args.legacyPlayerMovementInput.GetNonInterpolatedTranslation();

            string currentAnimationState = args.playerAnimationController.GetCurrentAnimationState();

            if (
                !RaymanAnimationsGroundHelper.IsUprisingWalkingRunningAnimation(currentAnimationState)
                &&
                !RaymanAnimationsGroundHelper.IsExitingWalkingRunningAnimation(currentAnimationState)
                &&
                !RaymanAnimationsGroundHelper.IsLandingAnimation(currentAnimationState))
            {
                args.subject.transform.Translate(args.legacyPlayerMovementStateInfo.movementVelocity);
            }
            else if (
                !RaymanAnimationsGroundHelper.IsExitingWalkingRunningAnimation(currentAnimationState))
            {
                args.subject.transform.Translate(args.legacyPlayerMovementStateInfo.movementVelocity / 2f);
            }
            else if (nonInterpolatedInputTranslation.magnitude <= 0.1f)
            {
                Vector3 fadingOutTranslation;

                if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.ExitingWalkingCycle2ToGroundIdleStateName()))
                {
                    fadingOutTranslation = (
                        -args.playerAnimatedModel.playerAnimatedModelTransform.transform.forward).normalized * playerRunningSpeed * 0.25f *
                        (1f - args.playerAnimationController.GetAnimationNormalizedTime());

                    if (args.playerAnimationController.GetAnimationNormalizedTime() >= 1.05f)
                    {
                        args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.IdleAnimationStateName(), 0f, Time.fixedDeltaTime);
                    }
                }
                else
                {
                    fadingOutTranslation = (
                        -args.playerAnimatedModel.playerAnimatedModelTransform.transform.forward).normalized * playerRunningSpeed * 0.6f *
                        (1f - args.playerAnimationController.GetAnimationNormalizedTime());

                    if (args.playerAnimationController.GetAnimationNormalizedTime() >= 1.05f)
                    {
                        args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.IdleAnimationStateName(), 0f, Time.fixedDeltaTime);
                    }
                }

                if (!WallCollisionHelper.IsGoingTowardsTheWall(
                    args.subject.transform.position + (-args.playerController.controllerContext.gravityDirection), fadingOutTranslation,
                    args.playerController.controllerContext.gravityDirection))
                {
                    args.subject.transform.Translate(args.subject.transform.InverseTransformDirection(fadingOutTranslation));
                }
            }

            if (args.legacyPlayerMovementInput.GetJumpButton()
                && args.legacyRayCollider.IsHittingGround(args.playerController.controllerContext.gravityDirection)
                && args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState
                && !args.legacyPlayerGroundRollingAspect.IsPerformingGroundRoll())
            {
                args.legacyPlayerMovementStateInfo.isJumping = true;
                args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                args.legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_AIR;

                args.playerRulesTransitions.EnterAirJumping(args.legacyPlayerMovementMetrics.intentionalMovingDirection);

                AudioSource.PlayClipAtPoint(UnityEngine.Object.FindObjectOfType<BasicSoundsMap>().jump, args.subject.transform.position, volume: 1f);

                return;
            }

            if (!args.legacyRayCollider.IsHittingGround(args.playerController.controllerContext.gravityDirection))
            {
                LegacyRuleGroundToNewAirTransitions.
                    EnterRuleAirFreeFallingFromRuleGround(
                        args.legacyPlayerGroundRollingAspect,
                        args.legacyPlayerMovementStateInfo,
                        args.legacyPlayerMovementMetrics,
                        args.playerController
                    );
                return;
            }

            RaycastHit boosterHit;

            if (args.legacyRayCollider.IsHittingBooster(
                args.playerController.controllerContext.gravityDirection, out boosterHit))
            {
                args.boostTimeSeconds = 1f;
                args.playerInputHub.moveInputRaw = args.playerInputHub.moveInputRaw.normalized;
            }

            if (!args.legacyPlayerMovementInput.GetJumpButton())
            {
                args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
                args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
            }

            #endregion
            #region Animation

            if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                 RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName)
                 && !args.playerAnimationController.HasAnimationEnded())
            {
                return;
            }
            else if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                 RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName)
                 && args.playerAnimationController.HasAnimationEnded())
            {
                // args.playerAnimationController.ChangeAnimationState(RaymanAnimations.RunningAnimationStateName());

                //if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                //{
                //    args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.RunningAnimationStateName(), 0f, Time.fixedDeltaTime);
                //}

                //return;   
            }

            if (enteringFromGroundStrafing.current)
            {
                args.playerAnimationController.ChangeAnimationState(
                    RaymanAnimations.StrafingExitIntoIdleGroundStateName());
                enteringFromGroundStrafing.current = false;
                return;
            }

            if (landingWithHelicopter.current)
            {
                if (nonInterpolatedInputTranslation.magnitude > 0.1f && nonInterpolatedInputTranslation.magnitude <= 0.8f)
                {
                    args.playerAnimationController.ChangeAnimationState(RaymanAnimations.LandingFromHelicopterToRunningCycle4StateName());
                }
                else if (nonInterpolatedInputTranslation.magnitude <= 0.1f)
                {
                    args.playerAnimationController.ChangeAnimationState(RaymanAnimations.LandingFromHelicopterToGroundIdleStateName());
                }
                else
                {
                    args.playerAnimationController.ChangeAnimationState(RaymanAnimations.LandingFromHelicopterToRunningCycle4StateName());
                }


                landingWithHelicopter.current = false;
                landing.current = false;
                return;
            }
            else if (landing.current)
            {
                if (nonInterpolatedInputTranslation.magnitude > 0.1f && nonInterpolatedInputTranslation.magnitude <= 0.8f)
                {
                    args.playerAnimationController.ChangeAnimationState(RaymanAnimations.LandingFromVerticalJumpToWalkingCycle2StateName());
                }
                else if (nonInterpolatedInputTranslation.magnitude <= 0.1f)
                {
                    args.playerAnimationController.ChangeAnimationState(RaymanAnimations.LandingFromVerticalJumpToGroundIdleStateName());
                }
                else
                {
                    args.playerAnimationController.ChangeAnimationState(
                        RaymanAnimations.LandingFromRunningJumpAfterAirFlipToRunningCycle4StateName());
                }


                landingWithHelicopter.current = false;
                landing.current = false;
                return;
            }



            if (nonInterpolatedInputTranslation.magnitude > 0.1f && nonInterpolatedInputTranslation.magnitude <= 0.8f
                && (args.playerAnimationController.GetCurrentAnimationState().Equals(RaymanAnimations.IdleAnimationStateName())
                ))
            {
                args.playerAnimationController.ChangeAnimationState(
                    RaymanAnimations.UprisingFromGroundIdleToWalkingCycle1StateName());
                return;
            }
            else if (nonInterpolatedInputTranslation.magnitude > 0.8f &&
                (args.playerAnimationController.GetCurrentAnimationState().Equals(RaymanAnimations.IdleAnimationStateName())
                ))
            {
                args.playerAnimationController.ChangeAnimationState(
                    RaymanAnimations.UprisingFromGroundIdleToRunningCycle4StateName());
                return;
            }
            else if (nonInterpolatedInputTranslation.magnitude <= 0.1f &&
                (args.playerAnimationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.RunningAnimationStateName())))
            {
                args.playerAnimationController.ChangeAnimationState(
                    RaymanAnimations.ExitingRunningCycle4ToGroundIdleStateName());
                return;
            }
            else if (nonInterpolatedInputTranslation.magnitude <= 0.1f &&
                (args.playerAnimationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.WalkingCycle2StateName())))
            {
                args.playerAnimationController.ChangeAnimationState(
                    RaymanAnimations.ExitingWalkingCycle2ToGroundIdleStateName());
                return;
            }
            else if (inputTranslation.magnitude > 0.8f &&
                args.playerAnimationController.GetCurrentAnimationState().Equals(
                 RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName)
                && args.playerAnimationController.HasAnimationEnded())
            {
                args.playerAnimationController.ChangeAnimationState(RaymanAnimations.RunningAnimationStateName());

                if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                {
                    args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.RunningAnimationStateName(), 0f, Time.fixedDeltaTime);
                }
                return;
            }

            if (nonInterpolatedInputTranslation.magnitude > 0.1f && nonInterpolatedInputTranslation.magnitude <= 0.8f &&

                (args.playerAnimationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.UprisingFromGroundIdleToWalkingCycle1StateName())

                ||
                RaymanAnimationsGroundHelper.IsLandingAnimation(args.playerAnimationController.GetCurrentAnimationState())
                )
                && !args.playerAnimationController.HasAnimationEnded())
            {
                return;
            }

            else if (nonInterpolatedInputTranslation.magnitude > 0.8f &&
                (args.playerAnimationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.UprisingFromGroundIdleToRunningCycle4StateName())
                ||
                RaymanAnimationsGroundHelper.IsLandingAnimation(args.playerAnimationController.GetCurrentAnimationState())
                || args.playerAnimationController.GetCurrentAnimationState().Equals(
                 RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName))
                && !args.playerAnimationController.HasAnimationEnded())
            {
                return;
            }

            else if (nonInterpolatedInputTranslation.magnitude < 0.1f &&
                (
                    args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.ExitingRunningCycle4ToGroundIdleStateName())
                    ||
                    args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.ExitingWalkingCycle2ToGroundIdleStateName()
                ))
                && !args.playerAnimationController.HasAnimationEnded())
            {
                if (args.legacyPlayerMovementInput.GetFireButton())
                {
                    args.playerAnimationController.ChangeAnimationState(RaymanAnimations.IdleAnimationStateName());
                }

                if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.3f)
                {
                    args.playerAnimationController.ChangeAnimationState(RaymanAnimations.IdleAnimationStateName());
                }

                return;
            }

            if (inputTranslation.magnitude > 0.1f && inputTranslation.magnitude < 0.4f)
            {
                args.playerAnimationController.ChangeAnimationState(
                    RaymanAnimations.WalkingCycle1StateName());

                if (args.legacyPlayerMovementInput.GetFireButton())
                {
                    args.playerAnimationController.ChangeAnimationState(RaymanAnimations.IdleAnimationStateName());
                }
            }
            else if (inputTranslation.magnitude > 0.1f && inputTranslation.magnitude <= 0.8f)
            {
                args.playerAnimationController.ChangeAnimationState(RaymanAnimations.WalkingCycle2StateName());

                if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f && timeFromFirePressSeconds.current < 0.1f)
                {
                    args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.WalkingCycle2StateName(), 0f, Time.fixedDeltaTime);
                }
            }
            else if (inputTranslation.magnitude > 0.8f)
            {
                if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f && timeFromFirePressSeconds.current < 0.1f)
                {
                    args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.RunningAnimationStateName(), 0f, Time.fixedDeltaTime);
                }

                args.playerAnimationController.ChangeAnimationState(
                    RaymanAnimations.RunningAnimationStateName());
            }
            else if ((!RaymanAnimationsGroundHelper
                .IsLandingAnimation(args.playerAnimationController.GetCurrentAnimationState())
                && !args.playerAnimationController.GetCurrentAnimationState()
                .Equals(RaymanAnimations.StrafingExitIntoIdleGroundStateName())) ||
                args.playerAnimationController.HasAnimationEnded() || (args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName) && args.playerAnimationController.HasAnimationEnded()))
            {

                if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.ExitingRunningCycle4ToGroundIdleStateName()) || (!args.legacyFistChargingAspect.IsInValidCycleCurrently()
                        &&
                        !args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName)
                        ))
                {
                    //args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(RaymanAnimations.FIST_CHARGING_STANDING_LEFT_HAND, 0);
                    //args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(RaymanAnimations.FIST_CHARGING_STANDING_RIGHT_HAND, 0);

                    if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_CHARGING_STANDING_RIGHT_HAND].animationName)
                        )
                    {
                        if (this.lastFistChargePowerNormalized < 0.9f)
                        {
                            args.playerAnimationController.ChangeAnimationStateWithPriority(
                                RaymanAnimations.animations[RaymanAnimations.FIST_CHARGING_ENDING_STANDING_RIGHT_HAND].animationName, priority: 1);
                            return;
                        }
                        else
                        {
                            var s = UnityEngine.Object.FindObjectOfType<BasicSoundsMap>();
                            AudioSource.PlayClipAtPoint(s.chargedShootsVariants[random.current.Next(0, s.chargedShootsVariants.Length)], UnityEngine.Object.FindObjectOfType<CameraActorHandle>().transform.position);

                            args.playerAnimationController.ChangeAnimationStateWithPriority(
                                RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName, priority: 1);

                            if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                            {
                                args.playerAnimationController.SetAnimationFrameInManualUpdating(
                                    RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName, 0f, Time.fixedDeltaTime);
                            }
                            return;
                        }
                    }
                    else if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_CHARGING_STANDING_LEFT_HAND].animationName))
                    {
                        if (this.lastFistChargePowerNormalized < 0.9f)
                        {
                            args.playerAnimationController.ChangeAnimationStateWithPriority(
                                RaymanAnimations.animations[RaymanAnimations.FIST_CHARGING_ENDING_STANDING_LEFT_HAND].animationName, priority: 1);
                            return;
                        }
                        else
                        {
                            var s = UnityEngine.Object.FindObjectOfType<BasicSoundsMap>();
                            AudioSource.PlayClipAtPoint(s.chargedShootsVariants[random.current.Next(0, s.chargedShootsVariants.Length)], UnityEngine.Object.FindObjectOfType<CameraActorHandle>().transform.position);
                            args.playerAnimationController.ChangeAnimationStateWithPriority(
                                RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName, priority: 1);

                            if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                            {
                                args.playerAnimationController.SetAnimationFrameInManualUpdating(
                                    RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName, 0f, Time.fixedDeltaTime);
                            }
                            return;
                        }
                    }
                    else
                    {
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.IdleAnimationStateName());
                        return;
                    }

                }
                else if (args.legacyFistChargingAspect.IsChargingLeftHandCurrently())
                {
                    if (!args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_CHARGING_STANDING_LEFT_HAND].animationName) &&
                            args.playerAnimationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.IdleAnimationStateName()))
                    {
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.animations[RaymanAnimations.FIST_CHARGING_START_STANDING_LEFT_HAND].animationName, priority: 1);
                        return;
                    }
                    else if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_CHARGING_START_STANDING_LEFT_HAND].animationName) &&
                        args.playerAnimationController.HasAnimationEnded())
                    {
                        // left hand
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.animations[RaymanAnimations.FIST_CHARGING_STANDING_LEFT_HAND].animationName, priority: 1);
                        return;
                    }

                    if (args.legacyPlayerShootingAspect.WantsToFinishChargingAndShootPeek())
                    {
                        if (this.lastFistChargePowerNormalized < 0.9f)
                        {
                            args.playerAnimationController.ChangeAnimationStateWithPriority(
                                RaymanAnimations.animations[RaymanAnimations.FIST_CHARGING_ENDING_STANDING_LEFT_HAND].animationName, priority: 1);
                            return;
                        }
                        else
                        {
                            var s = UnityEngine.Object.FindObjectOfType<BasicSoundsMap>();
                            AudioSource.PlayClipAtPoint(s.chargedShootsVariants[random.current.Next(0, s.chargedShootsVariants.Length)], UnityEngine.Object.FindObjectOfType<CameraActorHandle>().transform.position);
                            args.playerAnimationController.ChangeAnimationStateWithPriority(
                                RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName, priority: 1);

                            if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                            {
                                args.playerAnimationController.SetAnimationFrameInManualUpdating(
                                    RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName, 0f, Time.fixedDeltaTime);
                            }
                            return;
                        }
                    }

                    if ((args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_CHARGING_ENDING_STANDING_LEFT_HAND].animationName)
                        ||
                        args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName)) &&
                        args.playerAnimationController.HasAnimationEnded())
                    {
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.IdleAnimationStateName());
                        return;
                    }

                }
                else if (args.legacyFistChargingAspect.isChargingRightHandCurrently)
                {
                    // right hand 
                    if (!args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_CHARGING_STANDING_RIGHT_HAND].animationName) &&
                            args.playerAnimationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.IdleAnimationStateName()))
                    {
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.animations[RaymanAnimations.FIST_CHARGING_START_STANDING_RIGHT_HAND].animationName, priority: 1);
                        return;
                    }
                    else if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_CHARGING_START_STANDING_RIGHT_HAND].animationName) &&
                        args.playerAnimationController.HasAnimationEnded())
                    {
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.animations[RaymanAnimations.FIST_CHARGING_STANDING_RIGHT_HAND].animationName, priority: 1);
                        return;
                    }

                    if (args.legacyPlayerShootingAspect.WantsToFinishChargingAndShootPeek())
                    {
                        if (this.lastFistChargePowerNormalized < 0.9f)
                        {
                            args.playerAnimationController.ChangeAnimationStateWithPriority(
                                RaymanAnimations.animations[RaymanAnimations.FIST_CHARGING_ENDING_STANDING_RIGHT_HAND].animationName, priority: 1);
                            return;
                        }
                        else
                        {
                            var s = UnityEngine.Object.FindObjectOfType<BasicSoundsMap>();
                            AudioSource.PlayClipAtPoint(s.chargedShootsVariants[random.current.Next(0, s.chargedShootsVariants.Length)], UnityEngine.Object.FindObjectOfType<CameraActorHandle>().transform.position);
                            args.playerAnimationController.ChangeAnimationStateWithPriority(
                                RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName, priority: 1);

                            if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
                            {
                                args.playerAnimationController.SetAnimationFrameInManualUpdating(
                                    RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName, 0f, Time.fixedDeltaTime);
                            }
                            return;
                        }
                    }

                    if ((args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_CHARGING_ENDING_STANDING_RIGHT_HAND].animationName)
                        ||
                        args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName)) &&
                        args.playerAnimationController.HasAnimationEnded())
                    {
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.IdleAnimationStateName());
                        return;
                    }
                }
                else if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName) &&
                        args.playerAnimationController.HasAnimationEnded())
                {
                    args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.IdleAnimationStateName());
                }
            }


            #endregion
        }
    }
}
