﻿using Assets.Scripts.Engine.Input;
using Assets.Scripts.NewPlayerMechanics;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Input
{
    public class ButtonInputInfo
    {
        public bool currentButtonState = false;
        public bool buttonDown = false;
        public bool buttonUp = false;

        public void ApplyValues(ButtonInputInfo other)
        {
            this.currentButtonState = other.currentButtonState;
            this.buttonDown = other.buttonDown;
            this.buttonUp = other.buttonUp;
        }

        public void DismissUpAndDownStateInfo()
        {
            this.buttonDown = false;
            this.buttonUp = false;
        }

        public void UpdateInputState(
            bool currentState)
        {
            if (this.currentButtonState == false && currentState == true)
            {
                this.buttonDown = true;
            }
            else if (this.currentButtonState == true && currentState == false)
            {
                this.buttonUp = true;
            }
            this.currentButtonState = currentState;
        }
    }

    public class PlayerInputState
    {
        //public Vector2 moveInput { get; set; }
        public ButtonInputInfo walkingButton { get; set; }
            = new ButtonInputInfo();
        public ButtonInputInfo groundRollButton { get; set; }
            = new ButtonInputInfo();
        public ButtonInputInfo strafingButton { get; set; }
            = new ButtonInputInfo();
        public ButtonInputInfo fireButton { get; set; }
            = new ButtonInputInfo();

        public ButtonInputInfo jumpButton { get; set; }
            = new ButtonInputInfo();

        public void ApplyStateValues(
            PlayerInputState inputState)
        {
            //moveInput = inputState.moveInput;
            this.walkingButton.ApplyValues(inputState.walkingButton);
            this.groundRollButton.ApplyValues(inputState.groundRollButton);
            this.strafingButton.ApplyValues(inputState.strafingButton);
            this.fireButton.ApplyValues(inputState.fireButton);
            this.jumpButton.ApplyValues(inputState.jumpButton);
        }

        internal void DismissButtonStates()
        {
            this.walkingButton.DismissUpAndDownStateInfo();
            this.groundRollButton.DismissUpAndDownStateInfo();
            this.strafingButton.DismissUpAndDownStateInfo();
            this.fireButton.DismissUpAndDownStateInfo();
            this.jumpButton.DismissUpAndDownStateInfo();
        }

        public void UpdateInputState(
            PlayerInputHub playerInputHub)
        {
            this.walkingButton.UpdateInputState(playerInputHub.walkingButton);
            this.groundRollButton.UpdateInputState(playerInputHub.groundRollButton);
            this.strafingButton.UpdateInputState(playerInputHub.strafingButton);
            this.fireButton.UpdateInputState(playerInputHub.fireButton);
            this.jumpButton.UpdateInputState(playerInputHub.jumpButton);
        }
    }

    public class PlayerInputHubStateHub : MonoBehaviour
    {
        protected PlayerInputHub playerInputHub;

        protected PlayerInputState previousInputState { get; set; }
            = new PlayerInputState();
        protected PlayerInputState currentInputState { get; set; }
            = new PlayerInputState();

        public ButtonInputInfo jumpButton => this.currentInputState.jumpButton;

        private void Start()
        {
            if (!GetComponent<PlayerController>().playerParams.controlledProgrammatically)
            {
                this.playerInputHub = FindObjectOfType<GameStateInfo>().GetComponent<PlayerInputHub>();
            }
        }

        public void DismissStatesFixedUpdate()
        {
            this.currentInputState.DismissButtonStates();
            this.previousInputState.DismissButtonStates();
        }

        private void FixedUpdate()
        {
            if (this.playerInputHub == null && GetComponent<PlayerController>().playerParams.controlledProgrammatically)
            {
                this.playerInputHub = GetComponent<PlayerInputHub>();
            }

            this.previousInputState.ApplyStateValues(
                this.currentInputState);
            this.currentInputState.UpdateInputState(
                this.playerInputHub);
        }
    }
}
