﻿using Assets.Scripts.GameMechanics.Entities.Player.Rules.Args;
using Assets.Scripts.NewPlayerMechanics;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Factories
{
    public static class RuleAirFactory
    {
        public static PlayerMovementRuleAir GetFreeFallingAirRule(
            PlayerController playerController,
            Vector3 initialIntentionalMovementDirection
            )
        {
            playerController.controllerContext.playerRulesTransitions
                .EnterAirFromLegacy(PlayerMovementRuleAirArgsFactory.GetFreeFallingInputArgs(
                    initialIntentionalMovementDirection));
            return (PlayerMovementRuleAir)playerController.GetCurrentRule();
        }

        public static PlayerMovementRuleAir GetJumpingAirRule(
            PlayerController playerController,
            Vector3 initialIntentionalMovementDirection)
        {
            const float playerJumpSpeed = 0.28f;
            Vector3 jumpingVelocity = (-playerController.controllerContext.gravityDirection) * playerJumpSpeed;

            playerController.controllerContext.playerRulesTransitions
                .EnterAirFromLegacy(
                    PlayerMovementRuleAirArgsFactory.GetJumpingAirRuleInputArgs(
                        jumpingVelocity, initialIntentionalMovementDirection)
                );
            return (PlayerMovementRuleAir)playerController.GetCurrentRule();
        }
    }
}
