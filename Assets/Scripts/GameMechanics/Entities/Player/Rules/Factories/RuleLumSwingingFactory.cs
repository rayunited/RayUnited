﻿using Assets.Scripts.GameMechanics.Entities.Player.Rules.Args.LumSwinging;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Factories
{
    public static class RuleLumSwingingFactory
    {
        public static PlayerMovementRuleLumSwinging GetLumSwingingRule(
            PlayerController playerController,
            GameObject lumForSwinging,
            Vector3 baseSwingingGravityDirection,
            Vector3 baseSwingingGravityForward,
            bool caughtByRightHand,
            GameObject handObject,
            float minimalSwingingDeflectionAngleDegrees,
            float maximalSwingingDeflectionAngleDegrees,
            float maxSwingingRadius)
        {
            playerController.controllerContext.playerRulesTransitions
                .EnterLumSwingingFromAny(
                    PlayerMovementRuleLumSwingingArgsFactory.GetLumSwingingFromAnyInputArgs(
                        lumForSwinging, baseSwingingGravityDirection,
                        baseSwingingGravityForward,
                        caughtByRightHand, handObject,
                        minimalSwingingDeflectionAngleDegrees, maximalSwingingDeflectionAngleDegrees,
                        maxSwingingRadius
                    ));
            return (PlayerMovementRuleLumSwinging)playerController.GetCurrentRule();
        }
    }
}
