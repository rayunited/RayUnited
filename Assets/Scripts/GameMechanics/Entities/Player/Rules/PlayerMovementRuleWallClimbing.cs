﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Utils;
using Assets.Scripts.PlayerMechanics.Collisions;
using Assets.Scripts.Sound;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules
{
    public static class PlayerMovementRuleWallClimbingStates
    {
        public const string IS_ALREADY_MOVING = "IS_ALREADY_MOVING";
    }

    public static class PlayerMovementRuleWallClimbingEffects
    {
        public const string INIT_EFFECT = "INIT_EFFECT";
    }

    public class PlayerMovementRuleWallClimbing : PlayerMovementRule
    {
        public const string RULE_ID = "PLAYER_MOVEMENT_RULE_WALL_CLIMBING";

        public PlayerMovementRuleWallClimbing(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void OnPortalTravel()
        {
        }

        private bool WillStillCollideWithWallClimbAndDoesNotCollideWithWall(Vector3 translationInWorldSpace)
        {
            var args = UseEntityContext();
            HandleGemsCollectiblesCollisions(args, args.gravityDirection, args.gravityForward);
            var context = UseContext<WallClimbingContext>();

            bool willCollideWithWall = false;

            Vector3 wallClimbNormal = context.wallClimbHit.normal;
            float wallClimbCheckRayDepth = 1.5f;

            RaycastHit wallClimbCollideHit = new RaycastHit();

            bool willCollideWithWallClimb =
                PlayerCollidingRules.IsCollidingWithWallClimbObject(
                    args.subject.transform.position + translationInWorldSpace,
                    -args.playerAnimatedModel.playerAnimatedModelTransform.transform.forward,
                    args.playerAnimatedModel.playerAnimatedModelTransform.transform.up,
                    -args.playerAnimatedModel.playerAnimatedModelTransform.transform.right,
                    out wallClimbCollideHit,
                    (-args.playerController.controllerContext.gravityDirection) + args.playerAnimatedModel.playerAnimatedModelTransform.transform.forward,
                    wallClimbCheckRayDepth,
                    1.7f);

            return willCollideWithWallClimb && !willCollideWithWall;
        }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<WallClimbingContext>();

            UseEffect(PlayerMovementRuleWallClimbingEffects.INIT_EFFECT, () =>
            {
                Vector3 wallClimbNormal = context.wallClimbHit.normal;

                args.subject.transform.position = context.wallClimbHit.point - (-args.playerController.controllerContext.gravityDirection);

                PlayerOrientationHelper.OrientatePlayerObject(
                    args.subject.transform,
                    -wallClimbNormal,
                    args.playerController.controllerContext.gravityForward,
                    args.playerController.controllerContext.gravityRight,
                    args.playerController.controllerContext.gravityDirection
                );

                PlayerOrientationHelper.OrientatePlayerAnimatedPart(
                    args.playerAnimatedModel.playerAnimatedModelTransform.transform,
                    -wallClimbNormal,
                    args.playerController.controllerContext.gravityForward,
                    args.playerController.controllerContext.gravityRight,
                    args.playerController.controllerContext.gravityDirection
                );

                args.playerAnimatedModel.playerAnimatedModelTransform.transform.position =
                    args.playerAnimatedModel.playerAnimatedModelTransform.transform.position + wallClimbNormal * 0.8f + 
                    (-args.playerController.controllerContext.gravityDirection) * 0.5f;

                args.playerAnimationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.WallClimbingIdleStateName(), priority: 5);

                //args.playerController.playerParams.soundParams
                //   .wallClimbingSounds.grabbingWallSound
                //   .PlayOnceWithAudioSource(
                //        args.playerController.controllerContext.playerAudioHandles
                //        .wallClimbingAudioHandles.wallGrabbingAudioSource);
            }, Tuple.Create(0));

            RefObject<bool> isMovingAlready = UseRef(PlayerMovementRuleWallClimbingStates.IS_ALREADY_MOVING, false);

            if (!args.legacyPlayerMovementInput.GetJumpButton())
            {
                args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
            }

            float playerClimbingSpeed = 0.05f;

            Vector3 inputTranslation = args.legacyPlayerMovementInput.GetNonInterpolatedTranslationUpRight();
            Vector3 translation = args.legacyPlayerMovementInput.GetNonInterpolatedTranslationUpRight() * playerClimbingSpeed;

            Vector3 translationInWorldSpace = args.subject.transform.TransformDirection(translation);

            if (args.legacyPlayerMovementInput.GetJumpButton() &&
                args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState)
            {
                LegacyRuleWallClimbingToNewAirTransitions.
                    EnterRuleAirJumpingFromRuleWallClimbing(
                        args.legacyPlayerMovementStateInfo,
                        args.subject.transform,
                        args.playerAnimatedModel.playerAnimatedModelTransform.gameObject,
                        args.legacyRayCollider,
                        args.playerAnimationController,
                        args.legacyPlayerMovementMetrics,
                        args.playerController
                    );
                AudioSource.PlayClipAtPoint(
                    UnityEngine.Object.FindObjectOfType<BasicSoundsMap>().jump, args.subject.transform.position, volume: 1f);
                var a = args.playerController.GetComponentInChildren<AudioSource>();
                a.clip = UnityEngine.Object.FindObjectOfType<BasicSoundsMap>().wallClimbing;
                a.Stop();
                return;
            }

            if (inputTranslation.magnitude < 0.1f)
            {
                args.playerAnimationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.WallClimbingIdleStateName(),
                    priority: 5);
                isMovingAlready.current = false;
                //args.playerController.playerParams.soundParams
                //    .wallClimbingSounds.climbingUpSound
                //    .StopPlayingWithAudioSource(
                //         args.playerController.controllerContext.playerAudioHandles
                //            .wallClimbingAudioHandles.wallMovementAudioSource);
                var a = args.playerController.GetComponentInChildren<AudioSource>();
                a.Stop();
            }
            else
            {
                if (!isMovingAlready.current)
                {
                    var a = args.playerController.GetComponentInChildren<AudioSource>();
                    a.clip = UnityEngine.Object.FindObjectOfType<BasicSoundsMap>().wallClimbing;
                    a.Play();
                }

                args.legacyPlayerMovementMetrics.intentionalMovingDirection =
                    args.subject.transform.TransformDirection(translation).normalized;

                if (Vector3.Angle((Vector3.up), translation) < 20f &&
                    WillStillCollideWithWallClimbAndDoesNotCollideWithWall(translationInWorldSpace))
                {
                    // going straight up
                    args.playerAnimationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.WallClimbingUpStateName(),
                        priority: 5);

                    args.subject.transform.Translate(translation);
                    isMovingAlready.current = true;
                }
                else if (Vector3.Angle((Vector3.up), translation) < 80f &&
                    WillStillCollideWithWallClimbAndDoesNotCollideWithWall(translationInWorldSpace))
                {
                    // going diagonal up
                    if (Vector3.Angle(translation, Vector3.right) < 90f)
                    {
                        // going diagonal up right
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingUpRightStateName(),
                            priority: 5);
                    }
                    else
                    {
                        // going diagonal up left
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingUpLeftStateName(),
                            priority: 5);
                    }
                    isMovingAlready.current = true;
                    args.subject.transform.Translate(translation);
                }
                else if (Vector3.Angle((Vector3.up), translation) < 90f &&
                    WillStillCollideWithWallClimbAndDoesNotCollideWithWall(translationInWorldSpace))
                {
                    // going left or right
                    if (Vector3.Angle(translation, Vector3.right) < 90f)
                    {
                        // going right
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingRightStateName(),
                            priority: 5);
                        isMovingAlready.current = true;
                    }
                    else
                    {
                        // going left
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingLeftStateName(),
                            priority: 5);
                        isMovingAlready.current = true;
                    }
                    args.subject.transform.Translate(translation);
                }
                else if (Vector3.Angle((Vector3.up), translation) < 160f &&
                    WillStillCollideWithWallClimbAndDoesNotCollideWithWall(translationInWorldSpace))
                {
                    //going diagonal down
                    if (Vector3.Angle(translation, Vector3.right) < 90f)
                    {
                        // going diagonal down right
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingDownRightStateName(),
                            priority: 5);
                        isMovingAlready.current = true;
                    }
                    else
                    {
                        // going diagonal down left
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingDownLeftStateName(),
                            priority: 5);
                        isMovingAlready.current = true;
                    }

                    args.subject.transform.Translate(translation);
                }
                else if (WillStillCollideWithWallClimbAndDoesNotCollideWithWall(translationInWorldSpace))
                {
                    //going straight down
                    args.playerAnimationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.WallClimbingDownStateName(),
                        priority: 5);
                    isMovingAlready.current = true;
                    args.subject.transform.Translate(translation);
                }
                else
                {
                    args.playerAnimationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.WallClimbingIdleStateName(),
                        priority: 5);
                    args.playerController.playerParams.soundParams
                    .wallClimbingSounds.climbingUpSound
                    .StopPlayingWithAudioSource(
                         args.playerController.controllerContext.playerAudioHandles
                            .wallClimbingAudioHandles.wallMovementAudioSource);
                    isMovingAlready.current = false;
                    var a = args.playerController.GetComponentInChildren<AudioSource>();
                    a.clip = UnityEngine.Object.FindObjectOfType<BasicSoundsMap>().wallClimbing;
                    a.Stop();
                }
            }
        } 
    }
}
