﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts.Air;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Strafing;
using Assets.Scripts.GameMechanics.Entities.Portal;
using Assets.Scripts.PlayerMechanics.Camera;
using Assets.Scripts.Sound;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules
{
    public static class PlayerMovementRuleAirState
    {       
        public const string AIR_SUBRULE = "AIR_SUBRULE";
        public const string STRAFING_AIR_SUBRULE = "STRAFING_AIR_SUBRULE";
    }

    public static class PlayerMovementRuleAirEffects
    {
        public const string INIT_EFFECT = "INIT_EFFECT";
    }

    public class PlayerMovementRuleAir : PlayerMovementRule
    {
        public const string RULE_ID = "PLAYER_MOVEMENT_RULE_AIR";

        protected System.Random random = new System.Random();

        public PlayerMovementRuleAir(
            object controller) : base(RULE_ID, controller)
        {
        }

        //public override void OrientatePlayerAccordingToPortals(Transform fromPortal, Transform toPortal)
        //{
        //    var fromPortalRend = fromPortal.GetComponent<PortalRenderingBehaviour>();
        //    var toPortalRend = toPortal.GetComponent<PortalRenderingBehaviour>();

        //    var context = UseContext<AirContext>();
        //    var args = UseEntityContext();

        //    (Vector3 intentionalMovementDirection, Action<Vector3> SetIntentionalMovementDirection)
        //        = UseState<Vector3>(
        //            PlayerMovementRuleAirState.INTENTIONAL_MOVEMENT_DIRECTION,
        //            context.initialIntentionalMovementDirection);

        //    SetIntentionalMovementDirection(toPortalRend.TransformDirection(fromPortalRend.InverseTransformDirection(intentionalMovementDirection)));

        //    args.kinematics.velocity = toPortalRend.TransformDirection(fromPortalRend.InverseTransformDirection(args.kinematics.velocity));
        //}

        public override void OnPortalTravel()
        {
            // throw new NotImplementedException();
        }

        //public override void OnShooting(float fistShootingPowerNormalized)
        //{
        //    var args = UseEntityContext();
        //    var context = UseContext<AirContext>();

        //    if (fistShootingPowerNormalized >= 0.9f)
        //    {
        //        if (isHelicopter.current)
        //        {
        //            args.playerAnimationController.ChangeAnimationStateWithPriority(
        //                    RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER].animationName,
        //                    priority: 15);
        //            if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
        //            {
        //                args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_IN_AIR_HELICOPTER].animationName, 0f, Time.fixedDeltaTime);
        //            }
        //        }
        //        else
        //        {
        //            args.playerAnimationController.ChangeAnimationStateWithPriority(
        //                    RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName,
        //                    priority: 15);
        //            if (args.playerAnimationController.GetAnimationNormalizedTime() > 1.1f)
        //            {
        //                args.playerAnimationController.SetAnimationFrameInManualUpdating(RaymanAnimations.animations[RaymanAnimations.FIST_SHOOTING_MAX_FORCE_ON_LAND_THEN_STARTING_WALKING].animationName, 0f, Time.fixedDeltaTime);
        //            }
        //        }
        //        var s = GameObject.FindObjectOfType<BasicSoundsMap>();
        //        AudioSource.PlayClipAtPoint(s.chargedShootsVariants[this.random.Next(0, s.chargedShootsVariants.Length)],
        //            GameObject.FindObjectOfType<CameraActorHandle>().transform.position);
        //    }
        //}

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            HandleGemsCollectiblesCollisions(args, args.gravityDirection, args.gravityForward);

            var context = UseContext<AirContext>();

            RefObject<AirSubrule> airSubrule = 
                UseRef<AirSubrule>(PlayerMovementRuleAirState.AIR_SUBRULE, null);
            RefObject<StrafingAirSubrule> strafingAirSubrule = 
                UseRef<StrafingAirSubrule>(PlayerMovementRuleAirState.STRAFING_AIR_SUBRULE, null);

            UseEffect(PlayerMovementRuleAirEffects.INIT_EFFECT, () =>
            {
                var airSubruleContext = UseContext<AirSubContext>(AirSubrule.RULE_ID);
                airSubruleContext.isJumping = context.isJumping;
                airSubruleContext.initialVelocity = context.initialVelocity;

                airSubrule.current = new AirSubrule(args.playerController);
                strafingAirSubrule.current = new StrafingAirSubrule(args.playerController);


            }, Tuple.Create(context.isStrafing));

            if (context.isStrafing)
            {
                strafingAirSubrule.current.GameplayFixedUpdate();
            } else
            {
                airSubrule.current.GameplayFixedUpdate();
            }
        }
    }
}
