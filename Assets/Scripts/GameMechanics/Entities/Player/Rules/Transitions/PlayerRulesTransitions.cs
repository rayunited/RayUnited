﻿using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts.Ground;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Args;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Args.LumSwinging;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Factories;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Strafing;
using Assets.Scripts.NewPlayerMechanics;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions
{
    public static class EntityRulesTransitionsValidator
    {
        public static void ValidateRule<T, U>(
            NewEntityController<T,U> entityController, Type ruleType)
            where T : NewEntityRule<U>
            where U : NewEntityContext, new()
        {
            if (!entityController.GetCurrentRuleClassName()
                    .Equals(ruleType.Name))
            {
                throw new InvalidOperationException(
                    "Invalid rules transition access from " +
                        entityController.GetCurrentRuleClassName()
                        + " that is for " + ruleType.Name
                    );
            }
        }
    }

    public class PlayerRulesTransitions
    {
        protected PlayerController playerController;

        protected void ValidateRule(Type ruleType)
        {
            EntityRulesTransitionsValidator
                .ValidateRule(
                    this.playerController, ruleType);
        }

        public PlayerRulesTransitions(
            PlayerController playerController)
        {
            this.playerController = playerController;
        }

        public void EnterAirFreeFalling(Vector3 initialIntentionalMovementDirection)
        {
            PrepareTransition();

            this.playerController.controllerContext.contextsHolder
                .SetContext(PlayerMovementRuleAir.RULE_ID,                
                    RuleAirTransitions
                        .GetFreeFallingAirContext(
                            initialIntentionalMovementDirection));

            this.playerController.SetCurrentRule(
                new PlayerMovementRuleAir(this.playerController));
        }

        public void EnterAirJumping(Vector3 initialIntentionalMovementDirection)
        {
            PrepareTransition();

            this.playerController.SetCurrentRule(
                RuleAirFactory.GetJumpingAirRule(this.playerController, initialIntentionalMovementDirection));
        }

        public void EnterAirStrafing(bool isJumping)
        {
            PrepareTransition();

            this.playerController.controllerContext.contextsHolder
                .SetContext(PlayerMovementRuleAir.RULE_ID,
                    RuleAirTransitions
                        .GetStrafingAirContext(isJumping));

            this.playerController.SetCurrentRule(
                new PlayerMovementRuleAir(this.playerController));
        }

        public void EnterGround(
            bool landingWithHelicopter = false,
            bool landing = false
        )
        {
            PrepareTransition();

            this.playerController.controllerContext.contextsHolder
                .SetContext(PlayerMovementRuleGround.RULE_ID,
                RuleGroundTransitions.GetGroundContext(
                    this.playerController.controllerContext.contextsHolder.UseContext<GroundContext>(
                        PlayerMovementRuleGround.RULE_ID)));

            var groundSubContext = new GroundSubContext();
            groundSubContext.landingWithHelicopter = landingWithHelicopter;
            groundSubContext.landing = landing;

            this.playerController.controllerContext.contextsHolder
                .SetContext(GroundSubrule.RULE_ID, groundSubContext);

            this.playerController.SetCurrentRule(
                new PlayerMovementRuleGround(this.playerController));
        }

        public void EnterGroundStrafing()
        {
            PrepareTransition();

            var context = new GroundContext();
            context.isStrafing = true;

            this.playerController.controllerContext.contextsHolder
                .SetContext(PlayerMovementRuleGround.RULE_ID,
                RuleGroundTransitions.GetGroundContext(context));

            this.playerController.SetCurrentRule(
                new PlayerMovementRuleGround(this.playerController));
        }

        public void EnterGroundRegular(
            bool enteringFromGroundStrafing = false
            )
        {
            PrepareTransition();

            var context = new GroundContext();
            context.isStrafing = false;

            var groundSubContext = new GroundSubContext();
            groundSubContext.enteringFromGroundStrafing = enteringFromGroundStrafing;

            this.playerController.controllerContext.contextsHolder
                .SetContext(PlayerMovementRuleGround.RULE_ID,
                RuleGroundTransitions.GetGroundContext(context));

            this.playerController.controllerContext.contextsHolder
                .SetContext(GroundSubrule.RULE_ID,
                groundSubContext);

            this.playerController.SetCurrentRule(
                new PlayerMovementRuleGround(this.playerController));
        }

        public void PrepareTransition()
        {
            this.playerController.controllerContext.kinematics.velocity
                = Vector3.zero;
        }

        public void EnterAirFromLegacy(
            PlayerMovementRuleAirArgs playerMovementRuleAirArgs)
        {
            PrepareTransition();

            this.playerController.controllerContext.contextsHolder
                .SetContext(PlayerMovementRuleAir.RULE_ID,
                RuleAirTransitions
                    .GetAirContextFromLegacy(
                        playerMovementRuleAirArgs));

            this.playerController.SetCurrentRule(
                new PlayerMovementRuleAir(this.playerController));
        }

        public void EnterLumSwingingFromAny(
            PlayerMovementRuleLumSwingingArgs playerMovementRuleLumSwingingArgs)
        {
            PrepareTransition();

            this.playerController.controllerContext.contextsHolder
                .SetContext(PlayerMovementRuleLumSwinging.RULE_ID,
                RuleLumSwingingTransitions
                    .GetLumSwingingContextFromAny(
                        playerMovementRuleLumSwingingArgs));

            this.playerController.SetCurrentRule(
                new PlayerMovementRuleLumSwinging(this.playerController));
        }

        public void EnterLedgeGrabbing(RaycastHit ledgeGrabHit)
        {
            PrepareTransition();

            this.playerController.controllerContext.contextsHolder
                .SetContext(PlayerMovementRuleLedgeGrabbing.RULE_ID,
                RuleLedgeGrabbingTransitions
                    .GetLedgeGrabbingContext(ledgeGrabHit));

            this.playerController.SetCurrentRule(
                new PlayerMovementRuleLedgeGrabbing(this.playerController));
        }

        public void EnterWallClimbing(RaycastHit wallClimbHit)
        {
            PrepareTransition();

            this.playerController.controllerContext.contextsHolder
                .SetContext(PlayerMovementRuleWallClimbing.RULE_ID,
                RuleWallClimbingTransitions
                    .GetWallClimbingContext(wallClimbHit));

            this.playerController.SetCurrentRule(
                new PlayerMovementRuleWallClimbing(this.playerController));
        }

        public void EnterRoofHanging(RaycastHit roofHangingHit)
        {
            PrepareTransition();

            this.playerController.controllerContext.contextsHolder
                .SetContext(PlayerMovementRuleRoofHanging.RULE_ID,
                RuleRoofHangingTransitions
                    .GetRoofHangingContext(roofHangingHit));

            this.playerController.SetCurrentRule(
                new PlayerMovementRuleRoofHanging(this.playerController));
        }
    }
}
