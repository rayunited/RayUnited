﻿using Assets.Scripts.GameMechanics.Entities.Player.Rules.Factories;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy.LumSwinging
{
    public static class LegacySwingLumHitHdlBehToNewLumSwingingTransitions
    {
        public static void EnterLumSwingingFromSwingLumHitHdlBeh(
            PlayerMovementStateInfo legacyPlayerMovementStateInfo,
            PlayerController playerController,
            GameObject lumForSwinging,
            Vector3 baseSwingingGravityDirection,
            Vector3 baseSwingingGravityForward,
            bool caughtByRightHand,
            GameObject handObject,
            float minimalSwingingDeflectionAngleDegrees,
            float maximalSwingingDeflectionAngleDegrees,
            float maxSwingingRadius)
        {
            legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.LEGACY_EMPTY_RULE;
            legacyPlayerMovementStateInfo.isJumping = true;
            legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
            legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;

            playerController.SetCurrentRule(
                RuleLumSwingingFactory.GetLumSwingingRule(
                    playerController,
                    lumForSwinging,
                    baseSwingingGravityDirection,
                    baseSwingingGravityForward,
                    caughtByRightHand,
                    handObject,
                    minimalSwingingDeflectionAngleDegrees,
                    maximalSwingingDeflectionAngleDegrees,
                    maxSwingingRadius
                ));

            PlayerMovementMetrics playerMovementMetrics = playerController.GetComponent<PlayerMovementMetrics>();
            playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_LUM_SWINGING;
            playerController.controllerContext.cameraController.controllerContext.cameraRulesTransitions.EnterLumSwinging();
        }
    }
}
