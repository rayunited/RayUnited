﻿using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Factories;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy
{
    public static class
        LegacyRuleRoofHangingToNewAirTransitions
    {
        public static void EnterRuleAirFallingOffFromRuleRoofHanging(
            RayCollider rayCollider,
            AnimationController animationController,
            PlayerMovementStateInfo legacyPlayerMovementStateInfo,
            PlayerMovementMetrics legacyPlayerMovementMetrics,
            PlayerController playerController)
        {
            rayCollider.DisableRoofHangingCollisionDetectionForMilliseconds(400);
            legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.LEGACY_EMPTY_RULE;
            legacyPlayerMovementStateInfo.isJumping = false;
            legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
            legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
            legacyPlayerMovementStateInfo.movementVelocity.y = -0.01f;
            animationController.ChangeAnimationStateWithPriority(
                RaymanAnimations.RoofHangingLeavingStateName(),
                priority: 8);

            playerController.SetCurrentRule(
                RuleAirFactory.GetFreeFallingAirRule(
                    playerController,
                    legacyPlayerMovementMetrics.intentionalMovingDirection
                    ));
        }
    }
}
