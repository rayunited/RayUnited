﻿using Assets.Scripts.GameMechanics.Entities.Player.Rules.Factories;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Aspects;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy
{
    public static class LegacyRuleGroundToNewAirTransitions
    {
        public static void EnterRuleAirJumpingFromRuleGround(
            PlayerMovementStateInfo legacyPlayerMovementStateInfo,
            PlayerMovementMetrics legacyPlayerMovementMetrics,
            PlayerController playerController
            )
        {
            legacyPlayerMovementStateInfo.isJumping = true;
            legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
            legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
            legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.LEGACY_EMPTY_RULE;

            playerController.SetCurrentRule(RuleAirFactory.GetJumpingAirRule(
                playerController,
                legacyPlayerMovementMetrics.intentionalMovingDirection
                ));

            legacyPlayerMovementStateInfo.movementVelocity = Vector3.zero;
        }

        public static void EnterRuleAirFreeFallingFromRuleGround(
            PlayerGroundRollingAspect playerGroundRollingAspect,
            PlayerMovementStateInfo legacyPlayerMovementStateInfo,
            PlayerMovementMetrics legacyPlayerMovementMetrics,
            PlayerController playerController)
        {
            playerGroundRollingAspect.BreakTheRollCycle();
            legacyPlayerMovementStateInfo.currentRule =
                PlayerMovementRuleEnum.LEGACY_EMPTY_RULE;

            legacyPlayerMovementStateInfo.movementVelocity = Vector3.zero;

            playerController.SetCurrentRule(RuleAirFactory.GetFreeFallingAirRule(
                playerController,
                legacyPlayerMovementMetrics.intentionalMovingDirection
                ));
        }
    }
}
