﻿using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Factories;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy
{
    public static class
        LegacyRuleLedgeGrabbingToNewAirTransitions
    {
        public static void EnterRuleAirFallingOffFromRuleLedgeGrabbing(
            Transform transform,
            RayCollider rayCollider,
            GameObject animatedPlayerPart,
            PlayerMovementStateInfo legacyPlayerMovementStateInfo,
            PlayerMovementMetrics legacyPlayerMovementMetrics,
            LedgeColliderInfo ledgeColliderInfo,
            Vector3 grabNormal,
            PlayerController playerController)
        {
            //if deciding to leave the ledge and fall down
            Vector3 nearestPointOnEdge = LedgeGrabbingHelper.GetNearestPointOnEdgeFrom(
                transform.position, 
                ledgeColliderInfo.transform.TransformPoint(ledgeColliderInfo.edgePointALocal),
                ledgeColliderInfo.transform.TransformPoint(ledgeColliderInfo.edgePointBLocal));

            animatedPlayerPart.transform.position = transform.position;

            transform.position = nearestPointOnEdge + playerController.controllerContext.gravityDirection * 1.78f + grabNormal.normalized * 0.82f;
            rayCollider.DisableLedgeGrabColliderDetectionForMilliseconds(800);

            legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.LEGACY_EMPTY_RULE;
            legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
            legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
            legacyPlayerMovementStateInfo.movementVelocity.y = 0;

            playerController.SetCurrentRule(
                RuleAirFactory.GetFreeFallingAirRule(
                    playerController,
                    legacyPlayerMovementMetrics.intentionalMovingDirection));
        }

        public static void EnterRuleAirJumpingFromRuleLedgeGrabbing(
            Transform transform,
            RayCollider rayCollider,
            GameObject animatedPlayerPart,
            AnimationController animationController,
            PlayerMovementStateInfo legacyPlayerMovementStateInfo,
            PlayerMovementMetrics legacyPlayerMovementMetrics,
            LedgeColliderInfo ledgeColliderInfo,
            Vector3 grabNormal,
            PlayerController playerController)
        {
            // leaving ledge with jump
            Vector3 nearestPointOnEdge = LedgeGrabbingHelper.GetNearestPointOnEdgeFrom(
                transform.position,
                ledgeColliderInfo.transform.TransformPoint(ledgeColliderInfo.edgePointALocal),
                ledgeColliderInfo.transform.TransformPoint(ledgeColliderInfo.edgePointBLocal));

            animatedPlayerPart.transform.position = transform.position;

            transform.position = nearestPointOnEdge + playerController.controllerContext.gravityDirection * 1.78f + grabNormal.normalized * 0.82f;
            rayCollider.DisableLedgeGrabColliderDetectionForMilliseconds(800);

            // allow starting hanging on ledge anim to be interrupted if player 
            // immediately pressed jump button while this animation is still being played
            animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.StartingHangingOnLedgeFromFallingStateName(), 1);

            animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.ClimbingFromHangingOnLedgeStateName(), 1);

            legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.LEGACY_EMPTY_RULE;
            legacyPlayerMovementStateInfo.isJumping = true;
            legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
            legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;

            playerController.SetCurrentRule(
                RuleAirFactory.GetJumpingAirRule(
                    playerController,
                    legacyPlayerMovementMetrics.intentionalMovingDirection
                    ));
        }
    }
}
