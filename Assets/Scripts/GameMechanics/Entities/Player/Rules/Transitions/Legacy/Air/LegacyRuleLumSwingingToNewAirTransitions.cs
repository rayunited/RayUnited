﻿using Assets.Scripts.GameMechanics.Entities.Player.Rules.Factories;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Aspects;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy
{
    public static class LegacyRuleLumSwingingToNewAirTransitions
    {
        public static void EnterRuleAirJumpingFromRuleLumSwinging(
            PlayerMovementMetrics playerMovementMetrics,
            PlayerMovementStateInfo legacyPlayerMovementStateInfo,
            PlayerShootingAspect playerShootingAspect,
            GameObject animatedPlayerPart,
            GameObject lumForSwinging,
            bool caughtByRightHand,
            PlayerController playerController,
            Vector3? intentionalMovingDirection = null)
        {
            if (intentionalMovingDirection == null)
            {
                intentionalMovingDirection = playerMovementMetrics.intentionalMovingDirection;
            }

            playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_FOLLOW;
            playerController.controllerContext.cameraController.controllerContext.cameraRulesTransitions.EnterFollow();
            legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.LEGACY_EMPTY_RULE;
            legacyPlayerMovementStateInfo.isJumping = true;
            legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
            legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;

            HandShootingProjectilesFactory.SpawnHandProjectileGoingBackToPlayer(
                playerShootingAspect, animatedPlayerPart.transform,
                lumForSwinging.transform.position, caughtByRightHand);

            playerController.SetCurrentRule(
                RuleAirFactory.GetJumpingAirRule(
                    playerController,
                    (Vector3)intentionalMovingDirection
                    ));
        }

        public static void EnterRuleAirOrGroundHittingObstacleFromRuleLumSwinging(
            RayCollider rayCollider,
            PlayerShootingAspect playerShootingAspect,
            GameObject animatedPlayerPart,
            GameObject lumForSwinging,
            PlayerMovementMetrics playerMovementMetrics,
            PlayerMovementStateInfo legacyPlayerMovementStateInfo,
            bool caughtByRightHand,
            PlayerController playerController,
            Vector3 gravityDirection,
            Vector3? intentionalMovingDirection = null)
        {
            if (intentionalMovingDirection == null)
            {
                intentionalMovingDirection = playerMovementMetrics.intentionalMovingDirection;
            }

            bool isTouchingTheGround = rayCollider.IsHittingGround(gravityDirection);
            playerController.controllerContext.cameraController.controllerContext.cameraRulesTransitions.EnterFollow();
            playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_FOLLOW;

            if (isTouchingTheGround)
            {
                legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_GROUND;
            }
            else
            {
                legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.LEGACY_EMPTY_RULE;
                playerController.SetCurrentRule(
                    RuleAirFactory.GetFreeFallingAirRule(
                        playerController,
                        (Vector3)intentionalMovingDirection
                    ));
            }

            legacyPlayerMovementStateInfo.isJumping = false;
            legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
            legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;

            HandShootingProjectilesFactory.SpawnHandProjectileGoingBackToPlayer(
                playerShootingAspect, animatedPlayerPart.transform,
                lumForSwinging.transform.position, caughtByRightHand);
        }
    }
}
