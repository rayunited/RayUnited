﻿using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions
{
    public class RuleGroundTransitions
    {
        public static GroundContext GetGroundContext(GroundContext previousContext)
        {
            return new()
            {
                isStrafing = previousContext.isStrafing,
            };
        }
    }
}
