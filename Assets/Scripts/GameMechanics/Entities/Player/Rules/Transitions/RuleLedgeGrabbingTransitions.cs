﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions
{
    public static class RuleLedgeGrabbingTransitions
    {
        public static LedgeGrabbingContext GetLedgeGrabbingContext(RaycastHit ledgeGrabHit)
        {
            return new()
            {
                ledgeColliderHit = ledgeGrabHit,
            };
        }
    }
}
