﻿using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions
{
    public static class EmptyPlayerTransitions
    {
        public static EmptyContext GetEmptyContext()
        {
            return new() { };
        }
    }
}
