﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions
{
    public static class RuleRoofHangingTransitions
    {
        public static RoofHangingContext GetRoofHangingContext(RaycastHit roofHangingHit)
        {
            return new() { roofHangingHit = roofHangingHit };
        }
    }
}
