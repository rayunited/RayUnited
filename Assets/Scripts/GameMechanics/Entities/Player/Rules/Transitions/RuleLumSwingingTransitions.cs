﻿using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Args.LumSwinging;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions
{
    public static class RuleLumSwingingTransitions
    {
        public static LumSwingingContext GetLumSwingingContextFromAny(
            PlayerMovementRuleLumSwingingArgs playerMovementRuleLumSwingingArgs)
        {
            return new()
            {
                lumForSwinging = playerMovementRuleLumSwingingArgs.lumForSwinging,
                baseSwingingGravityDirection = playerMovementRuleLumSwingingArgs.baseSwingingGravityDirection,
                baseSwingingGravityForward = playerMovementRuleLumSwingingArgs.baseSwingingGravityForward,
                caughtByRightHand = playerMovementRuleLumSwingingArgs.caughtByRightHand,
                handObject = playerMovementRuleLumSwingingArgs.handObject,

                minimalSwingingDeflectionAngleDegrees = playerMovementRuleLumSwingingArgs.minimalSwingingDeflectionAngleDegrees,
                maximalSwingingDeflectionAngleDegrees = playerMovementRuleLumSwingingArgs.maximalSwingingDeflectionAngleDegress,
                maxSwingingRadius = playerMovementRuleLumSwingingArgs.maxSwingingRadius
            };
        }
    }
}
