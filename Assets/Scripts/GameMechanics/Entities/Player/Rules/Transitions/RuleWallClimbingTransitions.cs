﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions
{
    public static class RuleWallClimbingTransitions
    {
        public static WallClimbingContext GetWallClimbingContext(RaycastHit wallClimbHit)
        {
            return new() { wallClimbHit = wallClimbHit };
        }
    }
}
