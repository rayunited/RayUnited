﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy;
using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules
{
    public static class PlayerMovementRuleLumSwingingEffects
    {
        public const string INIT_EFFECT = "INIT_EFFECT";
    }

    public static class PlayerMovementRuleLumSwingingStates
    {
        public const string SWINGING_RADIUS = "SWINGING_RADIUS";
        public const string SWINGING_PERIODS_ANGULAR_SPEED = "SWINGING_PERIODS_ANGULAR_SPEED";
        public const string SWINGING_FLAT_DIRECTION = "SWINGING_FLAT_DIRECTION";
        public const string SWINGING_PERIODS_ANGULAR_PROGRESS = "SWINGING_PERIODS_ANGULAR_PROGRESS";
        public const string CURRENT_MAX_DEFLECTION_BASE_VECTOR = "CURRENT_MAX_DEFLECTION_BASE_VECTOR";
        public const string LUM_SWINGING_PHASE = "LUM_SWINGING_PHASE";
        public const string LUM_SWINGING_EXTREMUM_PROGRESS = "LUM_SWINGING_EXTREMUM_PROGRESS";
        public const string SWINGING_DEFLECTION_ANGLE_ESTABLISHED = "SWINGING_DEFLECTION_ANGLE_ESTABLISHED";
    }

    public enum LumSwingingPhase
    {
        MIDDLE_MOVEMENT_BACK_TO_FRONT,
        MIDDLE_MOVEMENT_FRONT_TO_BACK,
        EXTREMUM_FRONT_TO_BACK,
        EXTREMUM_BACK_TO_FRONT
    }

    public class PlayerMovementRuleLumSwinging : PlayerMovementRule
    {
        public const string RULE_ID = "PLAYER_MOVEMENT_RULE_LUM_SWINGING";

        public PlayerMovementRuleLumSwinging(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void OnPortalTravel()
        {
            throw new NotImplementedException();
        }

        protected Vector3 UseLumToPlayerVector()
        {
            var args = UseEntityContext();
            var context = UseContext<LumSwingingContext>();

            return args.subject.transform.position - context.lumForSwinging.transform.position;
        }

        protected Vector3 UsePlayerToLumVector()
        {
            var args = UseEntityContext();
            var context = UseContext<LumSwingingContext>();

            return context.lumForSwinging.transform.position - args.subject.transform.position;
        }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            HandleGemsCollectiblesCollisions(args, args.gravityDirection, args.gravityForward);
            var context = UseContext<LumSwingingContext>();

            (LumSwingingPhase lumSwingingPhase, Action<LumSwingingPhase> SetLumSwingingPhase) =
                UseState(PlayerMovementRuleLumSwingingStates.LUM_SWINGING_PHASE, LumSwingingPhase.MIDDLE_MOVEMENT_BACK_TO_FRONT);
            (float lumSwingingExtremumProgress, Action<float> SetLumSwingingExtremumProgress) =
                UseState(PlayerMovementRuleLumSwingingStates.LUM_SWINGING_EXTREMUM_PROGRESS, 0f);
            (bool swingingDeflectionAngleEstablished, Action<bool> SetSwingingDeflectionAngleEstablished) =
                UseState(PlayerMovementRuleLumSwingingStates.SWINGING_DEFLECTION_ANGLE_ESTABLISHED, false);

            (float swingingRadius, Action<float> SetSwingingRadius) =
                UseState(
                    PlayerMovementRuleLumSwingingStates.SWINGING_RADIUS,
                    Vector3.Distance(args.subject.position, context.lumForSwinging.transform.position));
            (float swingingPeriodsAngularSpeed, Action<float> SetSwingingPeriodsAngularSpeed) =
                UseState(
                    PlayerMovementRuleLumSwingingStates.SWINGING_PERIODS_ANGULAR_SPEED,
                    Mathf.PI * Time.fixedDeltaTime // one swing per second
                );

            (Vector3 swingingFlatDirection, Action<Vector3> SetSwingingFlatDirection) =
                UseState(PlayerMovementRuleLumSwingingStates.SWINGING_FLAT_DIRECTION,
                    Vector3.ProjectOnPlane(
                        UsePlayerToLumVector().normalized,
                        -context.baseSwingingGravityDirection).normalized
                );

            (Vector3 currentMaxDeflectionBaseVector, Action<Vector3> SetCurrentMaxDeflectionBaseVector) =
                UseState(
                    PlayerMovementRuleLumSwingingStates.CURRENT_MAX_DEFLECTION_BASE_VECTOR,
                    UseLumToPlayerVector().normalized
                );

            (float swingingPeriodsAngularProgress, Action<float> SetSwingingPeriodsAngularProgress) =
                UseState(PlayerMovementRuleLumSwingingStates.SWINGING_PERIODS_ANGULAR_PROGRESS, 0f);

            UseEffect(PlayerMovementRuleLumSwingingEffects.INIT_EFFECT, () =>
            {
                args.playerAnimationController.StopAnimatorUpdating();
            }, Tuple.Create(0));

            Vector3 swingingRightVector = Vector3.Cross(swingingFlatDirection, -context.baseSwingingGravityDirection).normalized;

            args.subject.position =
                context.lumForSwinging.transform.position +
                    (
                        Quaternion.AngleAxis(
                            -Mathf.Cos(swingingPeriodsAngularProgress) *
                                Vector3.Angle(currentMaxDeflectionBaseVector, context.baseSwingingGravityDirection),
                                swingingRightVector
                        )
                        * context.baseSwingingGravityDirection.normalized * swingingRadius
                    );

            float swingingPeriodRemainder = swingingPeriodsAngularProgress % (2 * Mathf.PI);

            if (swingingPeriodRemainder <= Mathf.PI && lumSwingingPhase == LumSwingingPhase.MIDDLE_MOVEMENT_BACK_TO_FRONT)
            {
                SetSwingingPeriodsAngularProgress(swingingPeriodsAngularProgress + swingingPeriodsAngularSpeed);
                args.playerAnimationController.SetAnimationFrameInManualUpdating(
                    RaymanAnimations.LumSwingingSequencePosesInTheMiddleBackToFrontRightHandStateName(),
                    Mathf.Clamp((swingingPeriodRemainder) / (Mathf.PI), 0f, 1f),
                    Time.fixedDeltaTime);
            }
            else if (swingingPeriodRemainder <= Mathf.PI && lumSwingingPhase == LumSwingingPhase.MIDDLE_MOVEMENT_FRONT_TO_BACK)
            {
                SetLumSwingingPhase(LumSwingingPhase.EXTREMUM_BACK_TO_FRONT);
            }
            else if (lumSwingingPhase == LumSwingingPhase.MIDDLE_MOVEMENT_BACK_TO_FRONT)
            {
                SetLumSwingingPhase(LumSwingingPhase.EXTREMUM_FRONT_TO_BACK);
            }
            else if (lumSwingingPhase == LumSwingingPhase.MIDDLE_MOVEMENT_FRONT_TO_BACK)
            {
                SetSwingingPeriodsAngularProgress(swingingPeriodsAngularProgress + swingingPeriodsAngularSpeed);
                args.playerAnimationController.SetAnimationFrameInManualUpdating(
                    RaymanAnimations.LumSwingingSequencePosesInTheMiddleFrontToBackRightHandStateName(),
                    Mathf.Clamp((swingingPeriodRemainder - Mathf.PI) / (Mathf.PI), 0f, 1f),
                    Time.fixedDeltaTime);
            }

            float lumSwingingExtremumProgressingSpeed =
                Time.fixedDeltaTime / 0.035f;

            if (lumSwingingPhase == LumSwingingPhase.EXTREMUM_BACK_TO_FRONT)
            {
                args.playerAnimationController.SetAnimationFrameInManualUpdating(
                    RaymanAnimations.LUM_SWINGING_SEQUENCE_START_POSE_AT_THE_BACK_RIGHT_HAND,
                    Mathf.Clamp(lumSwingingExtremumProgress, 0f, 1f),
                    Time.fixedDeltaTime);

                if (lumSwingingExtremumProgress >= 1f)
                {
                    SetLumSwingingPhase(LumSwingingPhase.MIDDLE_MOVEMENT_BACK_TO_FRONT);
                    SetLumSwingingExtremumProgress(0f);
                }
                else
                {
                    SetLumSwingingExtremumProgress(lumSwingingExtremumProgress + lumSwingingExtremumProgressingSpeed);
                }
            }
            else if (lumSwingingPhase == LumSwingingPhase.EXTREMUM_FRONT_TO_BACK)
            {
                args.playerAnimationController.SetAnimationFrameInManualUpdating(
                    RaymanAnimations.LUM_SWINGING_SEQUENCE_END_POSE_AT_THE_FRONT_RIGHT_HAND,
                    Mathf.Clamp(lumSwingingExtremumProgress, 0f, 1f),
                    Time.fixedDeltaTime);

                if (lumSwingingExtremumProgress >= 1f)
                {
                    SetLumSwingingPhase(LumSwingingPhase.MIDDLE_MOVEMENT_FRONT_TO_BACK);
                    SetLumSwingingExtremumProgress(0f);
                }
                else
                {
                    SetLumSwingingExtremumProgress(lumSwingingExtremumProgress + lumSwingingExtremumProgressingSpeed);
                }
            }

            args.subject.rotation =
                Quaternion.Lerp(
                    args.subject.rotation,
                    Quaternion.LookRotation(
                        swingingFlatDirection,
                        -context.baseSwingingGravityDirection
                    ),
                    Time.fixedDeltaTime
                );

            args.playerAnimatedModel.playerAnimatedModelTransform.rotation =
                Quaternion.LookRotation(
                    -swingingFlatDirection,
                    -context.baseSwingingGravityDirection
            );

            args.playerInputWrapperEffects
                .OnMoveInputMagnitudeBiggerOrEqualThan(0.1f, (wrappedPlayerMoveVector3) =>
                {
                    Vector3 swingingFlatDirectionRotationDirectionVector =
                       Vector3ProjectionHelper.ProjectConsideringDirection(
                           wrappedPlayerMoveVector3, swingingRightVector
                       );

                    bool isRotatingToTheRight =
                        Vector3.Dot(swingingFlatDirectionRotationDirectionVector, swingingRightVector) >= 0;

                    float rotationMagnitude = swingingFlatDirectionRotationDirectionVector.magnitude;
                                  
                    SetSwingingFlatDirection(
                        (Quaternion.AngleAxis(
                            (isRotatingToTheRight ? rotationMagnitude : -rotationMagnitude) * 45f * Time.fixedDeltaTime,
                            -context.baseSwingingGravityDirection) * swingingFlatDirection).normalized
                    );
                    SetCurrentMaxDeflectionBaseVector(
                        (Quaternion.AngleAxis(
                            (isRotatingToTheRight ? rotationMagnitude : -rotationMagnitude) * 45f * Time.fixedDeltaTime,
                            -context.baseSwingingGravityDirection) * currentMaxDeflectionBaseVector).normalized
                        );
                });

            args.playerInputEffects.OnJumpButtonDown(() =>
            {
                if (context.handObject)
                {
                    UnityEngine.Object.Destroy(context.handObject);
                }

                args.playerAnimationController.ResumeAnimatorUpdating();
                LegacyRuleLumSwingingToNewAirTransitions
                    .EnterRuleAirJumpingFromRuleLumSwinging(
                        args.legacyPlayerMovementMetrics,
                        args.legacyPlayerMovementStateInfo,
                        args.legacyPlayerShootingAspect,
                        args.playerAnimatedModel.playerAnimatedModelTransform.gameObject,
                        context.lumForSwinging,
                        context.caughtByRightHand,
                        args.playerController,                       
                        swingingFlatDirection);
            });

            if (swingingRadius > context.maxSwingingRadius)
            {
                SetSwingingRadius(
                    Mathf.Lerp(swingingRadius, context.maxSwingingRadius, 0.5f * Time.fixedDeltaTime));
            }

            float currentDeflectionAngle = 
                Mathf.Abs(Vector3.SignedAngle(
                    context.baseSwingingGravityDirection, currentMaxDeflectionBaseVector, swingingRightVector));

            if (swingingPeriodRemainder >= (Mathf.PI / 2f) && !swingingDeflectionAngleEstablished)
            {
                if (currentDeflectionAngle < context.minimalSwingingDeflectionAngleDegrees)
                {
                    SetCurrentMaxDeflectionBaseVector(
                        (Quaternion.AngleAxis(
                            -context.minimalSwingingDeflectionAngleDegrees, swingingRightVector) * 
                            context.baseSwingingGravityDirection.normalized).normalized    
                    );
                } else if (currentDeflectionAngle > context.maximalSwingingDeflectionAngleDegrees)
                {
                    SetCurrentMaxDeflectionBaseVector(
                        (Quaternion.AngleAxis(
                            -context.maximalSwingingDeflectionAngleDegrees, swingingRightVector) *
                            context.baseSwingingGravityDirection.normalized).normalized
                    );
                }
                
                SetSwingingDeflectionAngleEstablished(true);
            }

            SetSwingingPeriodsAngularSpeed((context.maxSwingingRadius / swingingRadius) * (Mathf.PI) * Time.fixedDeltaTime * 1.25f);

            if (args.legacyRayCollider.HasHitTheSolidObstacle(
                context.baseSwingingGravityDirection,
                context.baseSwingingGravityForward))
            {
                if (context.handObject)
                {
                    UnityEngine.Object.Destroy(context.handObject);
                }

                args.playerAnimationController.ResumeAnimatorUpdating();
                LegacyRuleLumSwingingToNewAirTransitions.
                    EnterRuleAirOrGroundHittingObstacleFromRuleLumSwinging(
                       args.legacyRayCollider,
                       args.legacyPlayerShootingAspect,
                       args.playerAnimatedModel.playerAnimatedModelTransform.gameObject,
                       context.lumForSwinging,
                       args.legacyPlayerMovementMetrics,
                       args.legacyPlayerMovementStateInfo,
                       context.caughtByRightHand,
                       args.playerController,
                       context.baseSwingingGravityDirection,
                       swingingFlatDirection
                    );

                return;
            }
        }
    }
}
