﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Utils
{
    public static class PlayerOrientationHelper
    {
        public static void OrientatePlayerObject(
            Transform playerObject,
            Vector3 intentionalDirectionForward,
            Vector3 gravityForward,
            Vector3 gravityRight,
            Vector3 gravityDirection)
        {      
            playerObject.SetPositionAndRotation(
                playerObject.position,
                Quaternion.LookRotation(intentionalDirectionForward.normalized, -gravityDirection.normalized));
        }

        public static void OrientatePlayerAnimatedPart(
            Transform animatedPlayerPart,
            Vector3 intentionalDirectionForward,
            Vector3 gravityForward,
            Vector3 gravityRight,
            Vector3 gravityDirection
        )
        {
            animatedPlayerPart.SetPositionAndRotation(
                 animatedPlayerPart.position,
                 Quaternion.LookRotation(-intentionalDirectionForward.normalized, -gravityDirection.normalized));
        }
    }
}
