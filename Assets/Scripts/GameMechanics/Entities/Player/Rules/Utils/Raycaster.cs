﻿using Assets.Scripts.Common;
using Assets.Scripts.Utils;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics.Utils
{
    public static class Raycaster
    {
        public static bool CylinderRaycasterSingle(
            Vector3 origin,
            Vector3 direction,
            Vector3 gravityDirection,
            float radius,
            float height,
            int layers,
            int raysCount,
            out RaycastHit hit,
            int layerMask = Layers.defaultLayerMask,
            bool debug = false,
            Color? debugColor = null)
        {
            for (int i = 0; i < layers; i++)
            {
                for (int j = 0; j < raysCount; j++)
                {
                    var Origin = origin + (-gravityDirection) * height / layers * i;
                    var RayVector = (Quaternion.AngleAxis(360f / raysCount * j, -gravityDirection) * direction).normalized;
                    if (PhysicsRaycaster.Raycast(
                        Origin,
                        RayVector,
                        out RaycastHit innerHit,
                        radius,
                        layerMask,
                        debugColor,
                        debug))
                    {
                        Debug.DrawRay(Origin, RayVector, Color.red);
                        hit = innerHit;
                        return true;
                    }
                }
            }

            hit = new();
            return false;
        }
    }
}
