﻿using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Utils
{
    public static class PlayerInputKinematicsUtils
    {
        public static void FadeOutHorizontalVelocityOnNoInput(
            PlayerContext args
            )
        {
            args.playerInputWrapperEffects
                .OnMoveInputMagnitudeLowerThan(0.1f, (wrappedPlayerMoveVector3) =>
                {
                    const float movementSmoothingInterpolation = 0.6f;
                    Vector3 velocity = args.kinematics.velocity;
                    Vector3 targetVelocity =
                        Vector3ProjectionHelper.ProjectConsideringDirection(args.kinematics.velocity, args.gravityDirection);
                    args.kinematics.velocity =
                        Vector3.Lerp(velocity, targetVelocity, movementSmoothingInterpolation);
                });
        }

        public static void FadeInHorizontalVelocityOnInput(
            PlayerContext args,
            float maximumSpeed
            )
        {
            args.playerInputWrapperEffects
                .OnMoveInputMagnitudeBiggerOrEqualThan(0.1f, (wrappedPlayerMoveVector3) =>
                {
                    const float movementSmoothingInterpolation = 0.25f;
                    Vector3 translation =
                        wrappedPlayerMoveVector3 * maximumSpeed;
                    Vector3 velocity = args.kinematics.velocity;

                    Vector3 fallingVectorPart =
                        Vector3ProjectionHelper.ProjectConsideringDirection(args.kinematics.velocity, args.gravityDirection);

                    Vector3 targetVelocity = Vector3.ProjectOnPlane(translation, args.gravityDirection) + fallingVectorPart;

                    args.kinematics.velocity =
                        Vector3.Lerp(velocity, targetVelocity, movementSmoothingInterpolation);

                    PlayerOrientationHelper.OrientatePlayerObject(
                        args.subject,
                        PlayerPortalsHelper.TransformPortalsProjectedDirectionToWorldDirectionThroughChainOfPortalsTransitions(
                            args.playerController.controllerContext.playerPortalTraveller.passedPortals,
                            args.playerController.controllerContext.baseCameraOrientedForwardDirectionForPlayer.Invoke(),
                            args.playerController.controllerContext.cameraGravityDirection
                        ),
                        args.gravityForward,
                        args.gravityRight,
                        args.gravityDirection
                    );

                    PlayerOrientationHelper.OrientatePlayerAnimatedPart(
                        args.playerAnimatedModel.playerAnimatedModelTransform,
                        Vector3.ProjectOnPlane(translation, -args.gravityDirection),
                        args.gravityForward,
                        args.gravityRight,
                        args.gravityDirection
                    );
                });
        }
    }
}
