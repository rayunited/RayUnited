﻿using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Animations.Effects
{
    public class PlayerRuleAirAnimationEffects
    {
        protected AnimationController playerAnimationController;

        public PlayerRuleAirAnimationEffects(
            AnimationController playerAnimationController)
        {
            this.playerAnimationController = playerAnimationController;
        }

        public void OnJumpStartRunningBounceFromGroundEnded(
            Action onAnimEndAction)
        {
            AnimationStateActionHelper.ExecuteOnAnimationEnd(
                this.playerAnimationController,
                RaymanAnimations
                    .JumpStartRunningBounceFromGroundAnimationStateName(),
                onAnimEndAction
                );
        }

        public void OnAirRollFromJumpStartRunningBounceFromGroundEnded(
            Action onAnimEndAction)
        {
            AnimationStateActionHelper.ExecuteOnAnimationEnd(
                this.playerAnimationController,
                RaymanAnimations
                    .AirRollFromJumpStartRunningBounceFromGroundAnimationStateName(),
                onAnimEndAction
                );
        }

        public void OnJumpStartBounceFromGroundAnimation(
            Action<float> onAnimationAction)
        {
            throw new NotImplementedException();
        }
    }
}
