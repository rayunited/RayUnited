﻿using Assets.Scripts.Animations;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Animations.Effects
{
    public static class AnimationStateActionHelper
    {
        public static void ExecuteOnAnimationEnd(
            AnimationController animationController,
            string animationStateName,
            Action action)
        {
            if (animationController.GetCurrentAnimationState().Equals(
                  animationStateName) &&
                  animationController.HasAnimationEnded())
            {
                action.Invoke();
            }
        }
    }

    public class PlayerAnimationEffects
    {
        protected AnimationController playerAnimationController;
        protected PlayerRuleAirAnimationEffects playerRuleAirAnimationEffects;

        public PlayerAnimationEffects(
            AnimationController playerAnimationController
            )
        {
            this.playerAnimationController =
                playerAnimationController;
            this.playerRuleAirAnimationEffects =
                new PlayerRuleAirAnimationEffects(
                    playerAnimationController);
        }

        public PlayerRuleAirAnimationEffects GetRuleAirEffects()
        {
            return this.playerRuleAirAnimationEffects;
        }
    }
}
