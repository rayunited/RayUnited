﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.Common;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Utils;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.Utils;
using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules
{
    public static class PlayerMovementRuleRoofHangingStates
    {
        public const string CAN_CONTROL_PLAYER = "CAN_CONTROL_PLAYER";
        public const string IS_ALREADY_MOVING = "IS_ALREADY_MOVING";
    }

    public static class PlayerMovementRuleRoofHangingEffects
    {
        public const string INIT_EFFECT = "INIT_EFFECT";
    }

    public class PlayerMovementRuleRoofHanging : PlayerMovementRule
    {
        public const string RULE_ID = "PLAYER_MOVEMENT_RULE_ROOF_HANGING";

        public PlayerMovementRuleRoofHanging(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void OnPortalTravel()
        {
            
        }

        private void EnablePlayerControlAfterMilliseconds(int milliseconds, RefObject<bool> canControlPlayer)
        {
            var args = UseEntityContext();
            args.playerController.StartCoroutine(EnableCanControlPlayerFlagAfterTimePeriod(milliseconds, canControlPlayer));
        }

        private IEnumerator EnableCanControlPlayerFlagAfterTimePeriod(int milliseconds, RefObject<bool> canControlPlayer)
        {
            yield return new WaitForSeconds(milliseconds / 1000f);
            canControlPlayer.current = true;
        }

        private bool WillStillCollideWithRoofHangAndDoesNotCollideWithWall(Vector3 translationInWorldSpace)
        {
            var args = UseEntityContext();
            var context = UseContext<RoofHangingContext>();

            float wallCollisionCheckRayLength = 0.3f;

            bool willCollideWithWall =
                PhysicsRaycaster.Raycast(
                    args.playerAnimatedModel.playerAnimatedModelTransform.transform.position + translationInWorldSpace,
                        -args.playerAnimatedModel.playerAnimatedModelTransform.transform.forward, wallCollisionCheckRayLength,
                        Layers.generalEnvironmentLayersMask);

            float roofHangingCheckRayLength = 2f;
            float roofHangingCheckRayDepth = 3f;

            RaycastHit roofHangingCollideHit = new RaycastHit();

            bool willCollideWithRoofHanging = PhysicsRaycaster.Raycast(
                args.playerAnimatedModel.playerAnimatedModelTransform.transform.position + translationInWorldSpace.normalized * roofHangingCheckRayLength,
                -args.playerController.controllerContext.gravityDirection, out roofHangingCollideHit, roofHangingCheckRayDepth);

            willCollideWithRoofHanging = willCollideWithRoofHanging &&
                roofHangingCollideHit.collider.gameObject.tag.Equals(Tags.wallClimbTag);

            return willCollideWithRoofHanging && !willCollideWithWall;
        }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            HandleGemsCollectiblesCollisions(args, args.gravityDirection, args.gravityForward);
            var context = UseContext<RoofHangingContext>();

            RefObject<bool> canControlPlayer = UseRef(PlayerMovementRuleRoofHangingStates.CAN_CONTROL_PLAYER, false);
            RefObject<bool> isAlreadyMoving = UseRef(PlayerMovementRuleRoofHangingStates.IS_ALREADY_MOVING, false);

            UseEffect(PlayerMovementRuleRoofHangingEffects.INIT_EFFECT, () =>
            {
                Vector3 roofHangingNormal = context.roofHangingHit.normal;
                args.subject.transform.position = context.roofHangingHit.point + roofHangingNormal * 2.7f;

                args.playerAnimationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.RoofHangingIdleStateName(),
                    priority: 5);

                //args.playerController.playerParams.soundParams
                //    .roofHangingSounds.roofCatchingSound
                //    .PlayOnceWithAudioSource(
                //        args.playerController.controllerContext.playerAudioHandles
                //        .roofHangingAudioHandles.roofCatchingLeavingAudioSource);

                EnablePlayerControlAfterMilliseconds(250, canControlPlayer);
            }, Tuple.Create(0));

            float playerHangingMovingSpeed = 0.05f;

            if (canControlPlayer.current)
            {
                if (!args.legacyPlayerMovementInput.GetJumpButton())
                {
                    args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
                }

                Vector3 inputTranslation = args.legacyPlayerMovementInput.GetTranslation();
                Vector3 translation = args.legacyPlayerMovementInput.GetTranslation() * playerHangingMovingSpeed;

                if (args.legacyPlayerMovementInput.GetJumpButton() && args.legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState)
                {
                    LegacyRuleRoofHangingToNewAirTransitions.
                        EnterRuleAirFallingOffFromRuleRoofHanging(
                            args.legacyRayCollider,
                            args.playerAnimationController,
                            args.legacyPlayerMovementStateInfo,
                            args.legacyPlayerMovementMetrics,
                            args.playerController
                        );
                    return;
                }

                Vector3 translationInWorldSpace = args.subject.transform.TransformDirection(translation);

                if (inputTranslation.magnitude > 0.1f && WillStillCollideWithRoofHangAndDoesNotCollideWithWall(
                    translationInWorldSpace))
                {
                    args.playerAnimationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.RoofHangingGoingForwardStateName(),
                        priority: 5);

                    PlayerOrientationHelper.OrientatePlayerAnimatedPart(
                        args.playerAnimatedModel.playerAnimatedModelTransform.transform,
                        Vector3.ProjectOnPlane(translationInWorldSpace, -args.playerController.controllerContext.gravityDirection).normalized,
                        args.playerController.controllerContext.gravityForward,
                        args.playerController.controllerContext.gravityRight,
                        args.playerController.controllerContext.gravityDirection
                    );

                    args.legacyPlayerMovementMetrics.intentionalMovingDirection =
                        Vector3.ProjectOnPlane(translationInWorldSpace, -args.playerController.controllerContext.gravityDirection).normalized;

                    PlayerOrientationHelper.OrientatePlayerObject(
                        args.subject.transform,
                        PlayerPortalsHelper.TransformPortalsProjectedDirectionToWorldDirectionThroughChainOfPortalsTransitions(
                            args.playerController.controllerContext.playerPortalTraveller.passedPortals,
                            args.playerController.controllerContext.baseCameraOrientedForwardDirectionForPlayer.Invoke(),
                            args.playerController.controllerContext.gravityDirection
                        ),
                        args.playerController.controllerContext.gravityForward,
                        args.playerController.controllerContext.gravityRight,
                        args.playerController.controllerContext.gravityDirection
                    );

                    args.subject.transform.Translate(translation);

                    if (!isAlreadyMoving.current)
                    {
                        //this.playerController.playerParams.soundParams
                        //    .roofHangingSounds.underRoofMovementSound
                        //    .PlayLoopedWithChangingModulationEachTimeWithAudioSource(
                        //        this.playerController,
                        //        this.playerController.controllerContext.playerAudioHandles
                        //        .roofHangingAudioHandles.roofMovementAudioSource);
                    }

                    isAlreadyMoving.current = true;
                }
                else
                {
                    //this.playerController.playerParams.soundParams
                    //    .roofHangingSounds.underRoofMovementSound
                    //    .StopPlayingWithAudioSource(
                    //        this.playerController.controllerContext.playerAudioHandles
                    //        .roofHangingAudioHandles.roofMovementAudioSource);
                    args.playerAnimationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.RoofHangingIdleStateName(),
                        priority: 5);

                    isAlreadyMoving.current = false;
                }
            }
        }
    }
}
