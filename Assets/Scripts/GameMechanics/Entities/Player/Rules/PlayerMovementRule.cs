﻿using Assets.Scripts.Collectibles;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Strafing;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics.Rules;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules
{
    public abstract class PlayerMovementRule :
        NewEntityRule<PlayerContext>
    {
        protected float lastFistChargePowerNormalized = 0f;

        protected bool IsInStrafingKindRule()
        {
            return GetRuleClassName().Equals(typeof(StrafingGroundSubrule).Name) ||
                GetRuleClassName().Equals(typeof(StrafingAirSubrule).Name);
        }

        public virtual void OrientatePlayerAccordingToPortals(Transform fromPortal, Transform toPortal)
        {

        }

        protected void HandleGemsCollectiblesCollisions(PlayerContext args, Vector3 gravityDirection, Vector3 gravityForward)
        {
            List<RaycastHit> gemsHits =
                args.legacyRayCollider.GetAllCollectibleCrystalsThePlayerCollidesWith(
                    gravityDirection, gravityForward);
            foreach (RaycastHit gem in gemsHits)
            {
                gem.collider.gameObject.GetComponent<ICollectible>()
                    .InvokeCollectedBehaviour(args.playerAnimatedModel.playerAnimatedModelTransform.transform);
                args.scoreHunterHook.ScorePointsInTime(10f, 1000);
            }
        }

        protected void HandleShooting(
            PlayerContext args)
        {
            Vector3 inputTranslation = args.legacyPlayerMovementInput.GetTranslation().normalized;

            if (args.legacyPlayerShootingAspect.HasHandsToShoot()
                && args.legacyPlayerShootingAspect.CanTriggerShootingHand()
                && args.legacyPlayerShootingAspect.IsPressingShootingButtonInstantShot())
            {
                if (!IsInStrafingKindRule())
                {
                    args.legacyPlayerShootingAspect.ShootHand();
                    args.playerController.playerParams.soundParams
                        .shootingSounds.unchargedPlainFistShootingSound.PlayOnceWithAudioSource(
                            args.playerController.controllerContext
                                .playerAudioHandles.shootingAudioHandles.shootingAudioSource
                    );
                }
                else
                {
                    if (!args.legacyPlayerShootingAspect.IsGoingForwardOrBackwardsForShooting(inputTranslation))
                    {
                        Vector3 shootingDirection =
                            CurvedShootingDirectionHelper.GetCurvedShootingDirection(
                                Vector3.ProjectOnPlane(
                                      args.subject.TransformDirection(inputTranslation).normalized,
                                    -args.gravityDirection),
                                args.playerAnimatedModel.forward.normalized,
                                args.playerAnimatedModel.right.normalized,
                                args.gravityDirection);
                        args.legacyPlayerShootingAspect
                            .ShootHandFreelyInCurvedPath(
                                shootingDirection);
                        args.playerController.playerParams.soundParams
                            .shootingSounds.unchargedPlainFistShootingSound.PlayOnceWithAudioSource(
                                args.playerController.controllerContext
                                    .playerAudioHandles.shootingAudioHandles.shootingAudioSource
                    );
                    }
                    else
                    {
                        args.legacyPlayerShootingAspect.ShootHand();
                        args.playerController.playerParams.soundParams
                            .shootingSounds.unchargedPlainFistShootingSound.PlayOnceWithAudioSource(
                                args.playerController.controllerContext
                                    .playerAudioHandles.shootingAudioHandles.shootingAudioSource
                    );
                    }
                }
                return;
            }
            else if (args.legacyPlayerShootingAspect.HasHandsToShoot()
              && args.legacyPlayerShootingAspect.CanTriggerShootingHand()
              && args.legacyPlayerShootingAspect.IsPressingShootingButtonForCharging())
            {
                if (args.legacyPlayerShootingAspect
                        .HasJustStartedPressingShootingButtonForCharging())
                {
                    args.legacyPlayerShootingAspect
                        .InitiateFistChargingCycleForAppropriateHand();
                    return;
                }

                args.playerController.playerParams.soundParams
                    .shootingSounds.chargingFistSound.AlterAudioSourcePitch(
                       args.playerController.controllerContext
                       .playerAudioHandles.shootingAudioHandles.chargingAudioSource,
                       1.5f - args.legacyPlayerShootingAspect.GetCurrentChargingPowerNormalized()
                    );
                args.legacyPlayerShootingAspect.KeepChargingCycle();
                return;
            }
            else if (args.legacyPlayerShootingAspect
                .HasHandsToShoot()
              && args.legacyPlayerShootingAspect.WantsToFinishChargingAndShoot())
            {
                if (!IsInStrafingKindRule())
                {
                    var power = args.legacyPlayerShootingAspect.FinishFistChargingCycleAndShootHand();
                    args.playerController.playerParams.soundParams
                        .shootingSounds.chargingFistSound.StopPlayingWithAudioSource(
                            args.playerController.controllerContext
                            .playerAudioHandles.shootingAudioHandles.chargingAudioSource
                        );
                    args.playerController.playerParams.soundParams
                        .shootingSounds.chargedFistShootingSound.PlayOnceWithAudioSourceAndPitchFactor(
                            args.playerController.controllerContext
                                .playerAudioHandles.shootingAudioHandles.shootingAudioSource,
                            1.5f - args.legacyPlayerShootingAspect.GetCurrentChargingPowerNormalized()
                    );
                    OnShooting(power);
                }
                else
                {
                    if (!args.legacyPlayerShootingAspect
                        .IsGoingForwardOrBackwardsForShooting(inputTranslation))
                    {
                        Vector3 shootingDirection =
                            CurvedShootingDirectionHelper.GetCurvedShootingDirection(
                                Vector3.ProjectOnPlane(
                                        args.subject.TransformDirection(inputTranslation).normalized,
                                    -args.gravityDirection),
                                args.playerAnimatedModel.forward.normalized,
                                args.playerAnimatedModel.right.normalized,
                                args.gravityDirection);
                        args.playerController.playerParams.soundParams
                            .shootingSounds.chargingFistSound.StopPlayingWithAudioSource(
                                args.playerController.controllerContext
                                .playerAudioHandles.shootingAudioHandles.chargingAudioSource
                        );
                        var power = args.legacyPlayerShootingAspect
                            .FinishFistChargingCycleAndShootHandFreelyInCurvedPath(
                                shootingDirection);
                        OnShooting(power);
                    }
                    else
                    {
                        args.playerController.playerParams.soundParams
                            .shootingSounds.chargingFistSound.StopPlayingWithAudioSource(
                                args.playerController.controllerContext
                                .playerAudioHandles.shootingAudioHandles.chargingAudioSource
                        );
                        this.lastFistChargePowerNormalized = args.legacyPlayerShootingAspect
                            .FinishFistChargingCycleAndShootHand();
                        OnShooting(this.lastFistChargePowerNormalized);
                    }
                }
                return;
            }
        }

        public abstract void OnPortalTravel();

        public PlayerMovementRule(string ruleId, object controller) 
            : base(ruleId, controller) { }

        #region ContextRegion

        protected override PlayerContext UseEntityContext()
        {
            return GetController().controllerContext;
        }

        protected PlayerController GetController()
        {
            return GetCast<PlayerController>(this.entityController);
        }

        public virtual void OnShooting(float fistShootingPowerNormalized)
        {

        }

        #endregion
    }
}
