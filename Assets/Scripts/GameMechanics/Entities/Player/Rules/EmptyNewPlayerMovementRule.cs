﻿namespace Assets.Scripts.GameMechanics.Entities.Player.Rules
{
    public class EmptyNewPlayerMovementRule : PlayerMovementRule
    {
        public const string RULE_ID = "EMPTY_NEW_PLAYER_MOVEMENT_RULE";

        public EmptyNewPlayerMovementRule(object controller) : base(RULE_ID, controller) { }

        public override void OnPortalTravel()
        {
            
        }
    }
}
