﻿using Assets.Scripts.Engine.LevelEditor;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using UnityEngine;
using System.Linq;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules
{
    public class PlayerMovementRuleEditor : PlayerMovementRule
    {
        public const string RULE_ID = "PLAYER_MOVEMENT_RULE_EDITOR";

        public override void OnPortalTravel()
        {
            
        }

        public PlayerMovementRuleEditor(
            object controller) : base(RULE_ID, controller)
        {
            
        }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<EditorContext>();

            var input = args.legacyPlayerMovementInput.GetNonInterpolatedTranslation();
            var cameraRelativeInput = args.cameraController.transform.TransformDirection(input);


            //if (UnityEngine.Input.GetKey(KeyCode.R))
            //{
            //    args.playerRulesTransitions.EnterAirFreeFalling(Vector3.ProjectOnPlane(-args.gravityDirection, -args.subject.forward).normalized);
            //    args.cameraController.controllerContext.cameraRulesTransitions.EnterFollow();
            //    args.cameraController.GetComponent<UnityEngine.Camera>().orthographic = false;

            //    UnityEngine.GameObject.Find("EditorMenuSuperObject").SetActive(false);
            //    UnityEngine.Object.FindObjectsOfType<InSceneEditorAsset>().ToList().ForEach(x => x.enabled = false);
            //}
        }
    }
}
