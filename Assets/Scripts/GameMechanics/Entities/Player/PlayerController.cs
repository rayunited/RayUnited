﻿using Assets.Scripts.Animations;
using Assets.Scripts.Engine.Input;
using Assets.Scripts.GameMechanics.Collisions;
using Assets.Scripts.GameMechanics.Entities.Camera;
using Assets.Scripts.GameMechanics.Entities.Entity.Physics;
using Assets.Scripts.GameMechanics.Entities.Entity.Rules.Common;
using Assets.Scripts.GameMechanics.Entities.Entity.Rules.Common.Collision;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player;
using Assets.Scripts.GameMechanics.Entities.Player.Handles;
using Assets.Scripts.GameMechanics.Entities.Player.Params;
using Assets.Scripts.GameMechanics.Entities.Player.Rules;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Animations.Effects;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Input;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Input.Effects;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions;
using Assets.Scripts.HUD.ScoreCounting;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Aspects;
using Assets.Scripts.PlayerMechanics.Aspects.Shooting;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.PlayerMechanics.Rules.Common;
using UnityEngine;

namespace Assets.Scripts.NewPlayerMechanics
{
    public class PlayerController :
        NewEntityController<PlayerMovementRule, PlayerContext>
    {
        public PlayerParams playerParams = new();

        protected override void BehaviourStart()
        {
            base.BehaviourStart();

            var platformerCollisionHub = new PlatformerCollisionHub();
            var playerAnimationController = GetComponentInChildren<AnimationController>();
            var playerMovementInputWrapper =
                playerParams.controlledProgrammatically ?
                gameObject.AddComponent<PlayerMovementInput>() :
                    FindObjectOfType<GameStateInfo>().GetComponent<PlayerMovementInput>();
            var kinematics = new Kinematics();
            var playerInputHubStateHub = GetComponent<PlayerInputHubStateHub>();
            var cameraScriptsHandle = FindObjectOfType<CameraScriptsHandle>();

            var playerInputHub = playerParams.controlledProgrammatically ? 
                gameObject.AddComponent<PlayerInputHub>() : 
                FindObjectOfType<GameStateInfo>().GetComponent<PlayerInputHub>();

            var cameraController = FindObjectOfType<CameraController>();

            this.controllerContext = new()
            {
                subject = this.transform,
                kinematics = kinematics,
                playerController = this,
                cameraController = cameraController,
                playerInputHub = playerInputHub,
                platformerCollisionHub = platformerCollisionHub,
                cameraScriptsHandle = cameraScriptsHandle,
                playerMovementInputWrapper = playerMovementInputWrapper,
                playerAnimationController = playerAnimationController,
                legacyPlayerMovementInput = playerMovementInputWrapper,
                legacyPlayerMovementStateInfo = GetComponent<PlayerMovementStateInfo>(),
                legacyPlayerMovementMetrics = GetComponent<PlayerMovementMetrics>(),
                legacyRayCollider = GetComponent<RayCollider>(),
                scoreHunterHook = FindObjectOfType<ScoreAccumulatorDeccumulator>(),
                legacyPlayerGroundRollingAspect = GetComponent<PlayerGroundRollingAspect>(),
                playerAudioHandles =
                    (new PlayerAudioHandles())
                        .PropagateWithAudioSourcesRefs(this.transform),
                platformerCollisionEffects = new PlatformerCollisionEffects(platformerCollisionHub),
                playerAnimationEffects = new PlayerAnimationEffects(
                                            playerAnimationController),
                playerInputWrapperEffects = new PlayerInputWrapperEffects(
                                                playerMovementInputWrapper,
                                                this.transform
                                            ),
                playerRulesTransitions = new PlayerRulesTransitions(this),
                playerAnimatedModel = new PlayerAnimatedModelHandle(
                                        playerAnimatedModelTransform:
                                        GetComponentInChildren<Animator>().transform),
                playerInputHubStateHub = playerInputHubStateHub,
                legacyPlayerShootingAspect = GetComponent<PlayerShootingAspect>(),
                legacyPlayerTargetingAspect = GetComponent<TargettingAspect>(),
                legacyFistChargingAspect = GetComponent<FistChargingAspect>(),
                playerInputEffects = new PlayerInputEffects(playerInputHubStateHub),
                cameraOrientedForward = cameraScriptsHandle.cameraOrientedForward,
                playerParams = this.playerParams,
                gravityDirection = Quaternion.AngleAxis(0f, Vector3.forward) * Vector3.down,
                gravityRight = Quaternion.AngleAxis(0f, Vector3.forward) * Vector3.right,
                gravityForward = Quaternion.AngleAxis(0f, Vector3.forward) * Vector3.forward,

                cameraGravityDirection = Quaternion.AngleAxis(0f, Vector3.forward) * Vector3.down,
                cameraGravityRight = Quaternion.AngleAxis(0f, Vector3.forward) * Vector3.right,
                cameraGravityForward = Quaternion.AngleAxis(0f, Vector3.forward) * Vector3.forward,

                programmaticBaseCameraOrientedForwardDirectionForPlayer = Vector3.forward,

                playerPortalTraveller = GetComponent<PlayerPortalTraveller>(),
                baseCameraOrientedForwardDirectionForPlayer = 
                    playerParams.controlledProgrammatically ? () => 
                    controllerContext.programmaticBaseCameraOrientedForwardDirectionForPlayer : () => cameraController.controllerContext.baseCameraOrientedForwardDirectionForPlayer,

                isSpeedBoost = false
            };

            this.transform.forward = this.controllerContext.gravityForward;
            this.transform.right = this.controllerContext.gravityRight;
            this.transform.up = -this.controllerContext.gravityDirection;

            this.controllerContext.playerRulesTransitions.EnterAirFreeFalling(Vector3.zero);
            //this.SetCurrentRule(new PlayerMovementRuleEditor(this));
        }

        protected override void GameplayFixedUpdate()
        {
            var args = this.controllerContext;

            args.playerAnimationController.ManualGameplayFixedUpdate();
            if (IsValidMovementRule())
            {
                args.platformerCollisionHub.GameplayFixedUpdate(
                    args.subject, args.kinematics,
                    args.gravityDirection);
                base.GameplayFixedUpdate();
                args.kinematics.GameplayFixedUpdateSubject(this.transform);
                args.playerInputHubStateHub.DismissStatesFixedUpdate();
            }
        }

        protected override void GameplayUpdate(float deltaTime)
        {
            var args = this.controllerContext;

            args.playerAnimationController.ManualGameplayUpdate(deltaTime);
            if (IsValidMovementRule())
            {
                args.cameraOrientedForward =
                    args.cameraScriptsHandle.cameraOrientedForward;
                base.GameplayUpdate(deltaTime);
            }
        }

        private bool IsValidMovementRule()
        {
            return
                this.currentRule != null &&
                this.currentRule.ruleId != null &&
                !this.currentRule.ruleId.Equals(
                    EmptyNewPlayerMovementRule.RULE_ID);
        }

        public void OrientatePlayerAccordingToPortals(Transform fromPortal, Transform toPortal)
        {
            var args = this.controllerContext;
            
            this.controllerContext.gravityRight =
                 toPortal.transform.TransformDirection(fromPortal.InverseTransformDirection(this.controllerContext.gravityRight)).normalized;
            this.controllerContext.gravityForward =
                 toPortal.transform.TransformDirection(fromPortal.InverseTransformDirection(this.controllerContext.gravityForward)).normalized;
            this.controllerContext.gravityDirection = -Vector3.Cross(this.controllerContext.gravityForward, this.controllerContext.gravityRight).normalized;

            this.controllerContext.legacyPlayerMovementMetrics
                .OrientatePlayerAccordingToPortals(fromPortal, toPortal);
            this.controllerContext.legacyPlayerGroundRollingAspect
                .OrientatePlayerAccordingToPortals(fromPortal, toPortal);
            this.currentRule.OrientatePlayerAccordingToPortals(fromPortal, toPortal);

            args.cameraOrientedForward =
                args.cameraScriptsHandle.cameraOrientedForward;
            args.playerInputHubStateHub.DismissStatesFixedUpdate();

            this.transform.rotation = 
                Quaternion.LookRotation(
                    toPortal.transform.TransformDirection(
                        fromPortal.InverseTransformDirection(this.transform.forward)).normalized,
                    -this.controllerContext.gravityDirection
                );
        }
    }
}
