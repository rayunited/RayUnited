﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;

namespace Assets.Scripts.GameMechanics.Entities.Camera.Contexts
{
    public class LedgeGrabContext : Context
    {
        public bool blockCameraFollow = false;
    }
}
