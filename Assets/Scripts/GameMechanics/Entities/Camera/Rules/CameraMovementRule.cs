﻿using Assets.Scripts.GameMechanics.Entities.NewEntity;

namespace Assets.Scripts.GameMechanics.Entities.Camera.Rules
{
    public class CameraMovementRule : NewEntityRule<CameraContext>
    {
        public CameraMovementRule(string ruleId, object controller)
            : base(ruleId, controller) { }

        #region ContextRegion

        protected override CameraContext UseEntityContext()
        {
            return GetController().controllerContext;
        }

        protected CameraController GetController()
        {
            return GetCast<CameraController>(this.entityController);
        }

        #endregion
    }
}
