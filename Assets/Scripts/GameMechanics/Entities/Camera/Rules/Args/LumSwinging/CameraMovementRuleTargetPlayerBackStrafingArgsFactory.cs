﻿namespace Assets.Scripts.GameMechanics.Entities.Camera.Rules.Args.Follow
{
    public static class CameraMovementRuleTargetPlayerBackStrafingArgsFactory
    {
        public static CameraMovementRuleTargetPlayerBackStrafingArgs GetTargetPlayerBackStrafingArgs()
        {
            return new CameraMovementRuleTargetPlayerBackStrafingArgs();
        }
    }
}
