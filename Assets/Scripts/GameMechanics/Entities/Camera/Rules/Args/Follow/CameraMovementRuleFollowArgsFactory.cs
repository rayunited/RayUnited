﻿namespace Assets.Scripts.GameMechanics.Entities.Camera.Rules.Args.Follow
{
    public static class CameraMovementRuleFollowArgsFactory
    {
        public static CameraMovementRuleStrafingArgs GetFollowArgs()
        {
            return new CameraMovementRuleStrafingArgs();
        }
    }
}
