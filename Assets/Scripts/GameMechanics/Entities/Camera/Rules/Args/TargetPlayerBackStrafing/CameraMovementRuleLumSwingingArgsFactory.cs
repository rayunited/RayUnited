﻿namespace Assets.Scripts.GameMechanics.Entities.Camera.Rules.Args.Follow
{
    public static class CameraMovementRuleLumSwingingArgsFactory
    {
        public static CameraMovementRuleLumSwingingArgs GetLumSwingingArgs()
        {
            return new CameraMovementRuleLumSwingingArgs();
        }
    }
}
