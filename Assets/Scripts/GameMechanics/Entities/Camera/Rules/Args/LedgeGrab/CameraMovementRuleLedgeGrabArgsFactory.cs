﻿namespace Assets.Scripts.GameMechanics.Entities.Camera.Rules.Args.Follow
{
    public static class CameraMovementRuleLedgeGrabArgsFactory
    {
        public static CameraMovementRuleLedgeGrabArgs GetLedgeGrabArgs()
        {
            return new CameraMovementRuleLedgeGrabArgs();
        }
    }
}
