﻿namespace Assets.Scripts.GameMechanics.Entities.Camera.Rules.Args.Follow
{
    public static class CameraMovementRuleStrafingArgsFactory
    {
        public static CameraMovementRuleStrafingArgs GetStrafingArgs()
        {
            return new CameraMovementRuleStrafingArgs();
        }
    }
}
