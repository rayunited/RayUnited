﻿using Assets.Scripts.GameMechanics.Entities.Camera.Contexts;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Camera.Rules
{
    public class CameraMovementRuleLumSwinging : CameraMovementRule
    {
        public const string RULE_ID = "CAMERA_MOVEMENT_RULE_LUM_SWINGING";

        public CameraMovementRuleLumSwinging(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void GameplayFixedUpdate()
        {
            var context = UseEntityContext();
            var args = UseContext<LumSwingingContext>();

            context.baseCameraOrientedForwardDirectionForPlayer =
                GetController().DeriveBaseCameraOrientedForwardDirectionForPlayer();

            var playerPositionToLookAt = context.playerPositionToLookAt;
            var cameraGravityDirection = context.cameraGravityDirection;
            var cameraGravityForward = context.cameraGravityForward;
            var playerGravityDirection = context.playerGravityDirection;
            var playerGravityForward = context.playerGravityForward;
            var UpdatePlayerMovementMetrics = context.UpdatePlayerMovementMetrics;

            var animatedPlayerPartForward = context.animatedPlayerPartForward;

            float smoothSpeed = 0.1f;
            float distanceFromObject = 8f;

            Vector3 lookDirection = (playerPositionToLookAt.Invoke() - context.subject.position).normalized;
            UpdatePlayerMovementMetrics(
                Vector3.ProjectOnPlane(lookDirection, -cameraGravityDirection.Invoke()).normalized,
                playerGravityDirection.Invoke(),
                playerGravityForward.Invoke());

            context.subject.rotation =
                Quaternion.LookRotation(
                    Vector3.Lerp(context.subject.forward, lookDirection, smoothSpeed),
                    -cameraGravityDirection.Invoke()
                );

            Vector3 playerLastPosition;
            playerLastPosition =
                playerPositionToLookAt.Invoke() + (-cameraGravityDirection.Invoke()) +
                (-animatedPlayerPartForward.Invoke()) * distanceFromObject;

            playerLastPosition =
                playerLastPosition + ((-cameraGravityDirection.Invoke()).normalized * distanceFromObject / 2.5f);

            Vector3 newDesiredCameraPosition = Vector3.Lerp(context.subject.position, playerLastPosition, smoothSpeed);
            Vector3 cameraTranslation = newDesiredCameraPosition - context.subject.position;

            cameraTranslation = WallCollisionHelper.GetMovementVelocityConsideringSolidEnvironmentCollisions(
                context.subject.gameObject, context.rayCollider, cameraTranslation.normalized, cameraTranslation,
                cameraGravityDirection.Invoke(),
                cameraGravityForward.Invoke());
            context.subject.position = context.subject.position + cameraTranslation;
        }

        public override void GameplayUpdate(float deltaTime)
        {
        }
    }
}
