﻿namespace Assets.Scripts.GameMechanics.Entities.Camera.Rules
{
    public class CameraMovementRuleCutscenePlaceholder : CameraMovementRule
    {
        public const string RULE_ID = "CAMERA_MOVEMENT_RULE_CUTSCENE_PLACEHOLDER";

        public CameraMovementRuleCutscenePlaceholder(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void GameplayFixedUpdate()
        {
        }

        public override void GameplayUpdate(float deltaTime)
        {
        }
    }
}
