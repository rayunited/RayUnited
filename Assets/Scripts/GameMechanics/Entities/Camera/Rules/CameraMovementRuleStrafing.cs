﻿using Assets.Scripts.GameMechanics.Entities.Camera.Contexts;
using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Camera.Rules
{
    public class CameraMovementRuleStrafing : CameraMovementRule
    {
        public const string RULE_ID = "CAMERA_MOVEMENT_RULE_STRAFING";

        public CameraMovementRuleStrafing(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<StrafingContext>();

            args.baseCameraOrientedForwardDirectionForPlayer =
                GetController().DeriveBaseCameraOrientedForwardDirectionForPlayer();

            Vector3 lookDirection = ((args.playerPositionToLookAt.Invoke() -
                args.subject.transform.position).normalized
                + (-args.playerController.controllerContext.gravityDirection) * 0.05f).normalized;

            float smoothSpeed = 0.1f;
            float distanceFromObject = 8f;

            args.subject.transform.rotation =
                Quaternion.LookRotation(
                    Vector3.Lerp(args.subject.transform.forward, lookDirection, smoothSpeed),
                    -args.cameraGravityDirection.Invoke());

            Vector3 playerLastPosition;
            playerLastPosition =
                args.playerPositionToLookAt.Invoke() + (-args.cameraGravityDirection.Invoke()) +
                    Vector3.ProjectOnPlane(-args.subject.transform.forward, -args.cameraGravityDirection.Invoke()).normalized
                    * distanceFromObject;

            playerLastPosition = playerLastPosition + (((-args.cameraGravityDirection.Invoke()) * distanceFromObject) / 2.5f);

            Vector3 newDesiredCameraPosition = Vector3.Lerp(args.subject.transform.position, playerLastPosition, smoothSpeed);
            Vector3 cameraTranslation = newDesiredCameraPosition - args.subject.transform.position;

            cameraTranslation = WallCollisionHelper.GetMovementVelocityConsideringSolidEnvironmentCollisions(
                args.subject.gameObject, args.rayCollider, cameraTranslation.normalized, cameraTranslation,
                args.cameraGravityDirection.Invoke(),
                args.cameraGravityForward.Invoke());

            args.subject.transform.SetPositionAndRotation(
                args.subject.transform.position + cameraTranslation,
                Quaternion.LookRotation(
                    Vector3.Lerp(args.subject.transform.forward, lookDirection, smoothSpeed),
                    -args.cameraGravityDirection.Invoke()
                )
            );

        }

        public override void GameplayUpdate(float deltaTime)
        {
        }
    }
}
