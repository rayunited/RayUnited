﻿namespace Assets.Scripts.GameMechanics.Entities.Camera.Rules
{
    public class CameraMovementRuleForwardDirectionForPlayer : CameraMovementRule
    {
        public const string RULE_ID = "CAMERA_MOVEMENT_RULE_FORWARD_DIRECTION_FOR_PLAYER";

        public CameraMovementRuleForwardDirectionForPlayer(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void GameplayFixedUpdate()
        {
            var context = UseEntityContext();
            context.baseCameraOrientedForwardDirectionForPlayer =
                GetController().DeriveBaseCameraOrientedForwardDirectionForPlayer();
            context.UpdatePlayerMovementMetrics.Invoke(
                context.baseCameraOrientedForwardDirectionForPlayer,
                context.playerController.controllerContext.gravityDirection, context.playerController.controllerContext.gravityForward);
        }

        public override void GameplayUpdate(float deltaTime)
        {
        }
    }
}
