﻿using System;
using UnityEngine;
using Assets.Scripts.GameMechanics.Entities.Camera.Rules.Args.Follow;

namespace Assets.Scripts.GameMechanics.Entities.Camera.Rules.Transitions
{
    public class CameraRulesTransitions
    {
        protected CameraController cameraController;

        protected void ValidateRule(Type ruleType)
        {
            Player.Rules.Transitions.EntityRulesTransitionsValidator
                .ValidateRule(
                    this.cameraController, ruleType);
        }

        public CameraRulesTransitions(
            CameraController cameraController)
        {
            this.cameraController = cameraController;
        }

        public void PrepareTransition()
        {
            this.cameraController.controllerContext.kinematics.velocity
                = Vector3.zero;
        }

        public void EnterFollow()
        {
            PrepareTransition();

            this.cameraController.controllerContext.contextsHolder
                .SetContext(CameraMovementRuleFollow.RULE_ID,
                    RuleFollowTransitions.GetFollowContext(
                        CameraMovementRuleFollowArgsFactory.GetFollowArgs()));

            this.cameraController.SetCurrentRule(
                new CameraMovementRuleFollow(this.cameraController));
        }

        public void EnterTargetPlayerBackStrafing()
        {
            PrepareTransition();

            this.cameraController.controllerContext.contextsHolder
                .SetContext(CameraMovementRuleTargetPlayerBackStrafing.RULE_ID,
                    RuleTargetPlayerBackStrafingTransitions.GetStrafingContext(
                        CameraMovementRuleTargetPlayerBackStrafingArgsFactory.GetTargetPlayerBackStrafingArgs()));

            this.cameraController.SetCurrentRule(
                new CameraMovementRuleTargetPlayerBackStrafing(this.cameraController));
        }

        public void EnterStrafing()
        {
            PrepareTransition();

            this.cameraController.controllerContext.contextsHolder
                .SetContext(CameraMovementRuleStrafing.RULE_ID,
                    RuleStrafingTransitions.GetStrafingContext(
                        CameraMovementRuleStrafingArgsFactory.GetStrafingArgs()));

            this.cameraController.SetCurrentRule(
                new CameraMovementRuleStrafing(this.cameraController));
        }

        public void EnterLumSwinging()
        {
            PrepareTransition();

            this.cameraController.controllerContext.contextsHolder
                .SetContext(CameraMovementRuleLumSwinging.RULE_ID,
                    RuleLumSwingingTransitions.GetLumSwingingContext(
                        CameraMovementRuleLumSwingingArgsFactory.GetLumSwingingArgs()));

            this.cameraController.SetCurrentRule(
                new CameraMovementRuleLumSwinging(this.cameraController));
        }

        public void EnterLedgeGrab()
        {
            PrepareTransition();

            this.cameraController.controllerContext.contextsHolder
                .SetContext(CameraMovementRuleLedgeGrab.RULE_ID,
                    RuleLedgeGrabTransitions.GetLedgeGrabContext(
                        CameraMovementRuleLedgeGrabArgsFactory.GetLedgeGrabArgs()));

            this.cameraController.SetCurrentRule(
                new CameraMovementRuleLedgeGrab(this.cameraController));
        }

        internal void EnterCutscenePlaceholder()
        {
            PrepareTransition();
            this.cameraController.SetCurrentRule(
                new CameraMovementRuleCutscenePlaceholder(this.cameraController));
        }
    }
}
