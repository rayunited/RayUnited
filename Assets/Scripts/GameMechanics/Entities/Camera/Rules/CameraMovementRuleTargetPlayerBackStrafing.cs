﻿using Assets.Scripts.GameMechanics.Entities.Camera.Contexts;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Camera.Rules
{
    public class CameraMovementRuleTargetPlayerBackStrafing : CameraMovementRule
    {
        public const string RULE_ID = "CAMERA_MOVEMENT_RULE_TARGET_PLAYER_BACK_STRAFING";

        public CameraMovementRuleTargetPlayerBackStrafing(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<TargetPlayerBackStrafingContext>();

            args.baseCameraOrientedForwardDirectionForPlayer =
                GetController().DeriveBaseCameraOrientedForwardDirectionForPlayer();

            float smoothSpeed = 0.1f;
            float distanceFromObject = 8f;

            if (args.playerController.controllerContext.legacyPlayerTargetingAspect
                ?.currentTarget?.transform?.position != null)
            {
                Vector3 lookDirection =
                    PlayerPortalsHelper.TransformWorldDirectionToPortalsProjectedDirectionThroughChainOfPortalsTransitions(
                        args.playerPortalTraveller.passedPortals,
                        (args.playerController.controllerContext.legacyPlayerTargetingAspect.currentTarget.transform.position - args.subject.transform.position).normalized,
                        args.cameraGravityDirection.Invoke()
                    );

                args.UpdatePlayerMovementMetrics(
                    Vector3.ProjectOnPlane(
                        lookDirection, (-args.cameraGravityDirection.Invoke())).normalized,
                        args.playerGravityDirection.Invoke(),
                        args.playerGravityForward.Invoke());

                args.subject.transform.rotation =
                    Quaternion.LookRotation(
                        Vector3.Lerp(args.subject.transform.forward, lookDirection, smoothSpeed),
                        -args.cameraGravityDirection.Invoke()
                    );

                Vector3 playerLastPosition;
                playerLastPosition =
                    args.playerPositionToLookAt.Invoke() + (-args.cameraGravityDirection.Invoke()) +
                    (-args.baseCameraOrientedForwardDirectionForPlayer) * distanceFromObject;

                playerLastPosition = playerLastPosition + (-args.cameraGravityDirection.Invoke()) * distanceFromObject / 7.5f;

                Vector3 newDesiredCameraPosition = Vector3.Lerp(args.subject.transform.position, playerLastPosition, smoothSpeed);
                Vector3 cameraTranslation = newDesiredCameraPosition - args.subject.transform.position;

                cameraTranslation = WallCollisionHelper.GetMovementVelocityConsideringSolidEnvironmentCollisions(
                    args.subject.gameObject, args.rayCollider, cameraTranslation.normalized, cameraTranslation,
                    args.cameraGravityDirection.Invoke(),
                    args.cameraGravityForward.Invoke());
                args.subject.transform.position = args.subject.transform.position + cameraTranslation;
            }
            else
            {
                return;
            }

        }

        public override void GameplayUpdate(float deltaTime)
        {
        }
    }
}
