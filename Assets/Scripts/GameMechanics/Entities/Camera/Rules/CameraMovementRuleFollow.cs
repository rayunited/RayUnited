﻿using Assets.Scripts.Engine.LevelEditor;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Camera;
using Assets.Scripts.GameMechanics.Entities.Camera.Contexts;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player.Rules;
using Assets.Scripts.Utils;
using System;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Camera.Rules
{
    public static class CameraMovementRuleFollowStates
    {
        public const string LAST_HORIZONTAL_DISTANCE_TO_PLAYER = "LAST_HORIZONTAL_DISTANCE_TO_PLAYER";
        public const string CAMERA_HEIGHT_OFFSET = "CAMERA_HEIGHT_OFFSET";
        public const string SMOOTH_SPEED_CAMERA_ROTATION = "SMOOTH_SPEED_CAMERA_ROTATION";
    }

    public class CameraMovementRuleFollow : CameraMovementRule
    {
        public const string RULE_ID = "CAMERA_MOVEMENT_RULE_FOLLOW";

        public CameraMovementRuleFollow(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void GameplayFixedUpdate()
        {
            var context = UseEntityContext();
            var args = UseContext<FollowContext>();

            //if (UnityEngine.Input.GetKey(KeyCode.T))
            //{
            //    context.playerController.SetCurrentRule(new PlayerMovementRuleEditor(context.playerController));
            //    this.GetController().SetCurrentRule(new CameraMovementRuleEditor(this.GetController()));

            //    UnityEngine.GameObject.FindObjectsByType<Transform>(FindObjectsInactive.Include, FindObjectsSortMode.None).First(x => x.name.Equals("EditorMenuSuperObject")).gameObject.SetActive(true);
            //    UnityEngine.Object.FindObjectsByType<InSceneEditorAsset>(FindObjectsInactive.Include, FindObjectsSortMode.None).ToList().ForEach(x => x.enabled = true);
            //}

            context.baseCameraOrientedForwardDirectionForPlayer =
                GetController().DeriveBaseCameraOrientedForwardDirectionForPlayer();

            var playerPositionToLookAt = context.playerPositionToLookAt;
            var cameraGravityDirection = context.cameraGravityDirection;
            var cameraGravityForward = context.cameraGravityForward;
            var playerGravityDirection = context.playerGravityDirection;
            var playerGravityForward = context.playerGravityForward;
            var UpdatePlayerMovementMetrics = context.UpdatePlayerMovementMetrics;

            RefObject<float> lastHorizontalDistanceToPlayer = 
                UseRef(
                    CameraMovementRuleFollowStates.LAST_HORIZONTAL_DISTANCE_TO_PLAYER,
                    Vector3.ProjectOnPlane(
                        playerPositionToLookAt.Invoke() - context.subject.transform.position,
                        -cameraGravityDirection.Invoke()
                    ).magnitude
                );
            RefObject<float> cameraHeightOffset =
                UseRef(CameraMovementRuleFollowStates.CAMERA_HEIGHT_OFFSET, 0f);
            RefObject<float> smoothSpeedCameraRotation =
                UseRef(CameraMovementRuleFollowStates.SMOOTH_SPEED_CAMERA_ROTATION, 0f);

            Vector2 translation = context.cameraMovementInput.GetTranslation();

            Vector2 horizontalTranslation = new Vector2(translation.x, 0f);
            Vector2 verticalTranslation = new Vector2(0f, translation.y);

            float cameraHeightOffsetChangingStep = 0.15f;
            float cameraMaxHeightOffset = 6f;
            float cameraMinHeightOffset = -3f;
            float cameraControlSpeed = 0.3f;
            float smoothSpeed = 0.1f;
            float distanceFromObject = 8f;

            float cameraRotationSpeedMultiplyingFactor =
                PlayerPrefs.GetInt(
                    CameraSettingsUtilsInfo.CAMERA_ROTATION_SENSITIVITY_PLAYER_PREFS_KEY,
                    CameraSettingsUtilsInfo.DEFAULT_CAMERA_ROTATION_SENSITIVITY) /
                (float)CameraSettingsUtilsInfo.DEFAULT_CAMERA_ROTATION_SENSITIVITY;

            bool invertHorizontalAxis =
                PlayerPrefs.GetInt(
                    CameraSettingsUtilsInfo.INVERT_CAMERA_HORIZONTAL_AXIS_PLAYER_PREFS_KEY, 0)
                    == 1;
            bool invertVerticalAxis =
                PlayerPrefs.GetInt(
                    CameraSettingsUtilsInfo.INVERT_CAMERA_VERTICAL_AXIS_PLAYER_PREFS_KEY, 0)
                    == 1;

            Vector3 playerInputCameraTranslation;

            Vector3 horizontalTranslationPart =
                context.subject.transform.right * horizontalTranslation.magnitude;

            if (invertHorizontalAxis)
            {
                horizontalTranslationPart = -horizontalTranslationPart;
            }

            if (Vector3.Angle(Vector3.right, horizontalTranslation) < 90f)
            {
                playerInputCameraTranslation =
                    horizontalTranslationPart;
            }
            else
            {
                playerInputCameraTranslation =
                    -horizontalTranslationPart;
            }

            float verticalTranslationPart =
                verticalTranslation.magnitude
                   * cameraHeightOffsetChangingStep *
                   cameraRotationSpeedMultiplyingFactor;

            bool verticalAngleCriteria = invertVerticalAxis ?
                Vector3.Angle(Vector3.up, verticalTranslation) >= 90f
                :
                Vector3.Angle(Vector3.up, verticalTranslation) < 90f;

            if (verticalAngleCriteria && verticalTranslation.magnitude > 0.01f
                && lastHorizontalDistanceToPlayer.current > 5f)
            {
                // go up
                if (cameraHeightOffset.current + verticalTranslationPart < cameraMaxHeightOffset)
                {
                    cameraHeightOffset.current += verticalTranslationPart;
                } else
                {
                    cameraHeightOffset.current = cameraMaxHeightOffset;
                }
            }
            else if (verticalTranslation.magnitude > 0.01f)
            {
                // go down
                if (cameraHeightOffset.current - verticalTranslationPart > cameraMinHeightOffset)
                {
                    cameraHeightOffset.current -= verticalTranslationPart;
                } else
                {
                    cameraHeightOffset.current = cameraMinHeightOffset;
                }
            }

            if (lastHorizontalDistanceToPlayer.current <= 8f && cameraHeightOffset.current > 0f)
            {
                cameraHeightOffset.current -= (cameraHeightOffsetChangingStep / 2f);
            }

            playerInputCameraTranslation *= cameraControlSpeed * cameraRotationSpeedMultiplyingFactor;

            if (playerInputCameraTranslation.magnitude > 0.01f)
            {
                smoothSpeedCameraRotation.current = (Mathf.Lerp(smoothSpeedCameraRotation.current, 1f, 0.01f));
            }
            else
            {
                smoothSpeedCameraRotation.current = Mathf.Lerp(smoothSpeedCameraRotation.current, smoothSpeed, 0.01f);
            }

            Vector3 lookDirection = ((playerPositionToLookAt.Invoke() - context.subject.transform.position)
                .normalized +
                (-cameraGravityDirection.Invoke()) * 0.05f).normalized;

            UpdatePlayerMovementMetrics.Invoke(
                Vector3.ProjectOnPlane(lookDirection, -cameraGravityDirection.Invoke()).normalized,
                playerGravityDirection.Invoke(),
                playerGravityForward.Invoke());

            Vector3 playerLastPosition;
            playerLastPosition = playerPositionToLookAt.Invoke() + playerInputCameraTranslation +
                Vector3.ProjectOnPlane(-lookDirection.normalized, -cameraGravityDirection.Invoke()) * distanceFromObject
                + (((distanceFromObject / 2.5f) + cameraHeightOffset.current) *
                (-cameraGravityDirection.Invoke()));

            Vector3 newDesiredCameraPosition = Vector3.Lerp(context.subject.transform.position, playerLastPosition, smoothSpeed);
            Vector3 cameraTranslation = newDesiredCameraPosition - context.subject.transform.position;

            cameraTranslation = WallCollisionHelper.GetMovementVelocityConsideringSolidEnvironmentCollisions(
                context.subject.gameObject, context.rayCollider, cameraTranslation.normalized, cameraTranslation,
                cameraGravityDirection.Invoke(),
                cameraGravityDirection.Invoke());

            context.subject.SetPositionAndRotation(
                context.subject.position + cameraTranslation,
                Quaternion.LookRotation(
                    Vector3.Lerp(context.subject.forward, lookDirection,
                        playerInputCameraTranslation.magnitude > 0.01f ? smoothSpeedCameraRotation.current : smoothSpeed),
                    -cameraGravityDirection.Invoke()
                )
            );

            lastHorizontalDistanceToPlayer.current = Vector3.ProjectOnPlane(
                playerPositionToLookAt.Invoke() - context.subject.transform.position, -cameraGravityDirection.Invoke()
            ).magnitude;
        }

        public override void GameplayUpdate(float deltaTime)
        {
        }
    }
}
