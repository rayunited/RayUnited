﻿using Assets.Scripts.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Camera.Rules
{
    public enum SurfaceType
    {
        Opaque,
        Transparent
    }

    public enum BlendMode
    {
        Alpha,
        Premultiply,
        Additive,
        Multiply
    }

    public class CameraMovementRuleEditor : CameraMovementRule
    {
        public const string RULE_ID = "CAMERA_MOVEMENT_RULE_EDITOR";

        protected GameObject cubeCursor;
        protected float heightOffset = 0f;

        protected Vector3 prevMousePosition = Vector3.zero;

        bool clickedLPM = false;

        public CameraMovementRuleEditor(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            //args.subject.transform.position = args.playerController.transform.position + args.playerController.transform.forward * 8f + args.playerController.transform.up * 3f;
            //

            UseEffect("INIT_EFFECT", () =>
            {
                //this.cubeCursor = GameObject.Find("3dcursor");
                //this.cubeCursor.transform.localScale = Vector3.one * 3f;

                //renderer.gameObject.SetActive(false);
                //renderer.gameObject.SetActive(true);
                UnityEngine.Object.FindObjectOfType<DrawPlayerScoreCounter>().enabled = false;

            }, Tuple.Create(0));


            //args.subject.transform.LookAt(args.playerController.transform.position);
            var direction = (args.subject.position - args.playerController.transform.position).normalized * 8f;

            //if (Input.GetKey(KeyCode.RightArrow))
            //{
                 
            //    direction = Quaternion.AngleAxis(1f, args.subject.up) * direction;
                
            //    args.subject.transform.LookAt(args.playerController.transform.position);
            //}
            //if (Input.GetKey(KeyCode.LeftArrow))
            //{
            //    direction = Quaternion.AngleAxis(-1f, args.subject.up) * direction;
            //    args.subject.position = args.playerController.transform.position + direction;
            //    args.subject.transform.LookAt(args.playerController.transform.position);
            //}
            //if (Input.GetKey(KeyCode.UpArrow))
            //{
            //    direction = Quaternion.AngleAxis(-1f, args.subject.right) * direction;
            //    args.subject.position = args.playerController.transform.position + direction;
            //    args.subject.transform.LookAt(args.playerController.transform.position);
            //}
            //if (Input.GetKey(KeyCode.DownArrow))
            //{
            //    direction = Quaternion.AngleAxis(1f, args.subject.right) * direction;
            //    args.subject.position = args.playerController.transform.position + direction;
            //    args.subject.transform.LookAt(args.playerController.transform.position);
            //}

            //args.subject.position = args.playerController.transform.position + direction;
            //args.subject.transform.LookAt(args.playerController.transform.position);

            //this.cubeCursor.transform.position = args.playerController.transform.position + args.playerController.transform.up + -args.playerController.transform.forward;
            //var cubeCursorPos = this.cubeCursor.transform.position;

            RaycastHit hit;

            Ray ray = GetController().GetComponent<UnityEngine.Camera>().ScreenPointToRay(Input.mousePosition);
                //var hasHit = Physics.Raycast(ray, out hit, 100f, Layers.environmentLayerMask);

            //if (hasHit)
            //{
            //    Vector3 pos = new Vector3((int)hit.point.x, (int)hit.point.y, (int)hit.point.z);
            //    this.cubeCursor.transform.position = pos;
            //}

            //if (UnityEngine.Input.GetMouseButton(0) && !this.clickedLPM)
            //{
            //    this.clickedLPM = true;
            //    var newCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //    newCube.transform.position = this.cubeCursor.transform.position;
            //    newCube.transform.localScale = this.cubeCursor.transform.localScale;
            //    newCube.layer = Layers.environmentLayerIndex;
            //    newCube.AddComponent<BoxCollider>();
            //} else if (!UnityEngine.Input.GetMouseButton(0))
            //{
            //    this.clickedLPM = false;
            //}

            if (Input.GetMouseButton(1))
            {
                Vector3 delta = Input.mousePosition - this.prevMousePosition;
                float horizontalRotation = delta.x;
                float verticalRotation = -delta.y;
                args.subject.RotateAround(args.subject.position, Vector3.up, horizontalRotation * 0.25f);
                args.subject.RotateAround(args.subject.position, args.subject.right, verticalRotation * 0.25f);
            }

            if (UnityEngine.Input.GetKey(KeyCode.W))
            {
                args.subject.transform.Translate(Vector3.forward * 0.25f);
                if (UnityEngine.Input.GetKey(KeyCode.LeftShift))
                {
                    args.subject.transform.Translate(Vector3.forward * 0.5f);
                }
            }
            if (UnityEngine.Input.GetKey(KeyCode.S))
            {
                args.subject.transform.Translate(Vector3.back * 0.25f);
                if (UnityEngine.Input.GetKey(KeyCode.LeftShift))
                {
                    args.subject.transform.Translate(Vector3.back * 0.5f);
                }
            }
            //if (UnityEngine.Input.GetKey(KeyCode.W))
            //{
            //    args.subject.transform.Translate(Vector3.up);

            //}
            //if (UnityEngine.Input.GetKey(KeyCode.S))
            //{
            //    args.subject.transform.Translate(Vector3.down);

            //}
            if (UnityEngine.Input.GetKey(KeyCode.A))
            {
                args.subject.transform.Translate(Vector3.left * 0.25f);
                if (UnityEngine.Input.GetKey(KeyCode.LeftShift))
                {
                    args.subject.transform.Translate(Vector3.left * 0.5f);
                }

            }
            if (UnityEngine.Input.GetKey(KeyCode.D))
            {
                args.subject.transform.Translate(Vector3.right * 0.25f);
                if (UnityEngine.Input.GetKey(KeyCode.LeftShift))
                {
                    args.subject.transform.Translate(Vector3.right * 0.5f);
                }

            }

            this.prevMousePosition = Input.mousePosition;
        }
    }
}
