﻿using Assets.Scripts.GameMechanics.Entities.Camera.Rules;
using Assets.Scripts.GameMechanics.Entities.Camera.Rules.Transitions;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Camera;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using System;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Camera
{
    public class CameraController : NewEntityController<CameraMovementRule, CameraContext>
    {
        public Vector3 DeriveBaseCameraOrientedForwardDirectionForPlayer()
        {
            return Vector3.ProjectOnPlane(
                this.transform.forward,
                -this.controllerContext.playerController.controllerContext.cameraGravityDirection)
                .normalized;
        }

        public Vector3 ProjectWorldPositionToPortalsProjectedPositionThroughChainOfPortalsTransitions(Vector3 baseWorldPosition)
        {
            return PlayerPortalsHelper
                .TransformWorldPositionToPortalsProjectedPositionThroughChainOfPortalsTransitions(
                    this.controllerContext.playerPortalTraveller.passedPortals,
                    baseWorldPosition,
                    this.controllerContext.cameraGravityDirection.Invoke()
                );
        }

        protected override void BehaviourStart()
        {
            base.BehaviourStart();

            var playerPortalTraveller = 
                FindObjectsOfType<PlayerPortalTraveller>().Where(x => x.GetComponent<PlayerController>()?.playerParams?.controlledProgrammatically == false).First();
            var playerController = FindObjectsOfType<PlayerController>().Where(x => x.GetComponent<PlayerController>()?.playerParams?.controlledProgrammatically == false).First();
            var playerMovementMetrics = FindObjectsOfType<PlayerMovementMetrics>().Where(x => x.GetComponent<PlayerController>()?.playerParams?.controlledProgrammatically == false).First();

            Func<Vector3> cameraGravityDirection = () =>
            {
                return playerController.controllerContext.cameraGravityDirection;
            };

            Func<Vector3> cameraGravityForward = () =>
            {
                return playerController.controllerContext.cameraGravityForward;
            };

            Func<Vector3> cameraGravityRight = () =>
            {
                return playerController.controllerContext.cameraGravityRight;
            };


            Func<Vector3> playerGravityDirection = () =>
            {
                return playerController.controllerContext.gravityDirection;
            };

            Func<Vector3> playerGravityForward = () =>
            {
                return playerController.controllerContext.gravityForward;
            };

            Func<Vector3> playerGravityRight = () =>
            {
                return playerController.controllerContext.gravityRight;
            };

            Func<Vector3> animatedPlayerPartForward = () =>
            {
                return playerController.controllerContext.playerAnimatedModel.forward;
            };


            Func<Vector3> playerPositionToLookAt = () =>
            {
                return
                    PlayerPortalsHelper
                        .TransformWorldPositionToPortalsProjectedPositionThroughChainOfPortalsTransitions(
                            playerPortalTraveller.passedPortals,
                            playerPortalTraveller.transform.position,
                            cameraGravityDirection.Invoke());
            };

            Action<Vector3, Vector3, Vector3> UpdatePlayerMovementMetrics = (
                Vector3 forwardVector, Vector3 gravityDirection, Vector3 gravityForward) =>
            {

                var refinedForwardVector =
                    PlayerPortalsHelper.TransformPortalsProjectedDirectionToWorldDirectionThroughChainOfPortalsTransitions(
                        playerPortalTraveller.passedPortals,
                        forwardVector, gravityDirection);

                playerMovementMetrics.UpdateForwardDirectionAndRightDirection(
                    refinedForwardVector,
                    Vector3.SignedAngle(
                        playerController.controllerContext.cameraGravityForward,
                        this.controllerContext.baseCameraOrientedForwardDirectionForPlayer,
                        -cameraGravityDirection.Invoke()));
            };


            this.controllerContext = new()
            {
                subject = this.transform,
                playerController = playerController,
                playerPortalTraveller = playerPortalTraveller,
                playerMovementMetrics = playerMovementMetrics,
                kinematics = new Entity.Physics.Kinematics(),
                baseCameraOrientedForwardDirectionForPlayer =
                    Vector3.ProjectOnPlane(
                        this.transform.forward,
                        Vector3.up).normalized,
                cameraRulesTransitions = new CameraRulesTransitions(this),
                playerPositionToLookAt = playerPositionToLookAt,

                cameraGravityDirection = cameraGravityDirection,
                cameraGravityForward = cameraGravityForward,
                cameraGravityRight = cameraGravityRight,

                playerGravityDirection = playerGravityDirection,
                playerGravityForward = playerGravityForward,
                playerGravityRight = playerGravityRight,

                animatedPlayerPartForward = animatedPlayerPartForward,

                UpdatePlayerMovementMetrics = UpdatePlayerMovementMetrics,
                cameraMovementInput = FindObjectOfType<CameraMovementInput>(),
                rayCollider = GetComponent<RayCollider>()
            };

            this.controllerContext.cameraRulesTransitions.EnterFollow();
            //this.currentRule = new CameraMovementRuleForwardDirectionForPlayer(this);
        }
    }
}
