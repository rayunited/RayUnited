﻿using Assets.Scripts.GameMechanics.Entities.Camera.Rules.Transitions;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Camera;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Camera
{
    public class CameraContext : NewEntityContext
    {
        public Vector3 baseCameraOrientedForwardDirectionForPlayer;
        public PlayerController playerController;
        public PlayerPortalTraveller playerPortalTraveller;

        public CameraRulesTransitions cameraRulesTransitions;

        public CameraMovementInput cameraMovementInput;

        public Func<Vector3> playerPositionToLookAt;
        public Func<Vector3> cameraGravityDirection;
        public Func<Vector3> cameraGravityForward;
        public Func<Vector3> cameraGravityRight;

        public Func<Vector3> playerGravityDirection;
        public Func<Vector3> playerGravityForward;
        public Func<Vector3> playerGravityRight;

        public Func<Vector3> animatedPlayerPartForward;

        public RayCollider rayCollider;

        public Action<Vector3, Vector3, Vector3> UpdatePlayerMovementMetrics;

        public PlayerMovementMetrics playerMovementMetrics;

        public CameraContext() : base() { }
    }
}
