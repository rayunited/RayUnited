﻿using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Camera
{
    public class CameraScriptsHandle : MonoBehaviour
    {
        protected PlayerMovementMetrics playerMovementMetrics;
        protected CameraController cameraController;
        protected PlayerController playerController;

        public Vector3 cameraOrientedForward =>
            this.cameraController?.controllerContext != null &&
            this.playerController?.controllerContext != null ?
            PlayerPortalsHelper.TransformPortalsProjectedDirectionToWorldDirectionThroughChainOfPortalsTransitions(
                this.playerController.controllerContext.playerPortalTraveller.passedPortals,
                this.cameraController.controllerContext.baseCameraOrientedForwardDirectionForPlayer,
                this.playerController.controllerContext.cameraGravityDirection
            ): Vector3.zero;

        private void Start()
        {
            this.playerMovementMetrics =
                FindObjectsOfType<PlayerMovementMetrics>().Where(x => x.GetComponent<PlayerController>()?.playerParams?.controlledProgrammatically == false).First();
            this.cameraController = FindObjectOfType<CameraController>();
            this.playerController = FindObjectOfType<PlayerController>();
        }
    }
}
