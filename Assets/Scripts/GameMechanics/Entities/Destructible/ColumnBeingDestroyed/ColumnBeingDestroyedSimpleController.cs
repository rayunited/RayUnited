﻿using Assets.Scripts.GameMechanics.Entities.Common.SimpleEntity;
using Assets.Scripts.GameMechanics.Entities.Destructible.Shrapnel;
using System;
using System.Collections.Generic;

namespace Assets.Scripts.GameMechanics.Entities.Destructible.ColumnBeingDestroyed
{
    public class ColumnBeingDestroyedSimpleController : SimpleEntityController
    {
        public Dictionary<string, ShrapnelSimpleController> shrapnelHandles =
            new Dictionary<string, ShrapnelSimpleController>();

        protected override void GameplayFixedUpdate()
        {
            if (this.shrapnelHandles.Count <= 0)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
