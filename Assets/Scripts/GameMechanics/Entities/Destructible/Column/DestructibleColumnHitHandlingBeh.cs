﻿using Assets.Scripts.GameMechanics.Destructible;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Destructible.Column
{
    public class DestructibleColumnHitHandlingBeh : DestructibleHitHandlingBeh
    {
        public override void OnHit(GameObject hittingElligibleGameObject)
        {
            GetComponent<DestructibleColumnSimpleController>()
                .TransformIntoFragmentedDestructedPhysicsVariant(
                    hittingElligibleGameObject
                );
        }
    }
}
