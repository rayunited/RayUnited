﻿using Assets.Scripts.GameMechanics.Entities.Common.SimpleEntity;
using Assets.Scripts.GameMechanics.Entities.Destructible.ColumnBeingDestroyed;
using Assets.Scripts.GameMechanics.Entities.Destructible.Shrapnel;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Destructible.Column
{
    public class DestructibleColumnSimpleController : SimpleEntityController
    {
        public GameObject columnPrefabToTransformIntoOnDestruction;

        [Range(0.1f, 10f)]
        public float minimumShrapnelLifeTimeSeconds = 3f;

        [Range(0.1f, 10f)]
        public float maximumShrapnelLifeTimeSeconds = 10f;

        public void TransformIntoFragmentedDestructedPhysicsVariant(
            GameObject hittingElligibleGameObject)
        {
            var columnBeingDestroyed = 
                Instantiate(this.columnPrefabToTransformIntoOnDestruction);
            columnBeingDestroyed.transform.position = this.transform.position;
            columnBeingDestroyed.transform.localScale = this.transform.lossyScale;

            Dictionary<string, ShrapnelSimpleController> shrapnelHandles = 
                new Dictionary<string, ShrapnelSimpleController>();

            var columnBeingDestroyedController =
                columnBeingDestroyed.AddComponent<ColumnBeingDestroyedSimpleController>();
            columnBeingDestroyedController.shrapnelHandles = shrapnelHandles;
            columnBeingDestroyed
                .GetComponentsInChildren<Rigidbody>()
                .ToList()
                .ForEach(x =>
                {
                    var newShrapnelController = 
                        x.gameObject.AddComponent<ShrapnelSimpleController>();
                    newShrapnelController.timeToLive = UnityEngine.Random.Range(
                        this.minimumShrapnelLifeTimeSeconds, this.maximumShrapnelLifeTimeSeconds);
                    newShrapnelController.parent = columnBeingDestroyedController;
                    shrapnelHandles.Add(x.name, newShrapnelController);
                });    
            
            Destroy(this.gameObject);
        }
    }
}
