﻿using Assets.Scripts.GameMechanics.Entities.Common.SimpleEntity;
using Assets.Scripts.GameMechanics.Entities.Destructible.ColumnBeingDestroyed;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Destructible.Shrapnel
{
    public class ShrapnelSimpleController : SimpleEntityController
    {
        public float timeToLive = 1f;
        public ColumnBeingDestroyedSimpleController parent;

        protected float timeLived = 0f;

        protected override void GameplayFixedUpdate()
        {
            this.timeLived += Time.fixedDeltaTime;
            if (this.timeLived >= this.timeToLive)
            {
                this.parent.shrapnelHandles.Remove(this.name);
                Destroy(this.gameObject);
            }
        }
    }
}
