﻿using Assets.Scripts.GameMechanics.Entities.Entity.Physics;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Portal.Handles;
using Assets.Scripts.GameMechanics.Entities.Portal.Params;
using Assets.Scripts.GameMechanics.Entities.Portal.Rules;
using Assets.Scripts.GameMechanics.Entities.Portal.Rules.Transitions;
using Assets.Scripts.PlayerMechanics.Camera;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Portal
{
    public class PortalController : NewEntityController<PortalBehaviourRule, PortalContext>
    {
        public PortalParams portalParams = new();

        protected bool hasBeenInitialized = false;

        public PortalController Initialize()
        {
            if (!this.hasBeenInitialized) { 
                this.hasBeenInitialized = true;
                var kinematics = new Kinematics();

                var portalCamera = GetComponentInChildren<UnityEngine.Camera>();
                portalCamera.transform.SetParent(this.transform, false);
                portalCamera.enabled = false;

                // GetComponentInChildren<Renderer>().enabled = false;            

                this.controllerContext = new()
                {
                    kinematics = kinematics,
                    subject = this.transform,
                    portalRulesTransitions = new PortalRulesTransitions(this),
                    portalCamera = portalCamera,
                    portalCameraAsSeenByPortal = new Dictionary<string, UnityEngine.Camera>(),
                    playerCamera = FindObjectOfType<CameraActorHandle>().GetComponent<UnityEngine.Camera>(),

                    portalScreenHandle = new PortalScreenHandle(GetComponentInChildren<Renderer>(), GetComponentInChildren<Renderer>().material),
                    portalScreenHandleAsSeenByPortal = new Dictionary<string, PortalScreenHandle>(),

                    renderingBehaviour = GetComponent<PortalRenderingBehaviour>()
                };
                this.controllerContext.portalRulesTransitions.EnterPortalOn();
            }
            return this;
        }

        protected override void BehaviourStart()
        {
            base.BehaviourStart();
            if (!this.hasBeenInitialized)
            {
                Initialize();
            }          
        }

        public bool IsActivated()
        {
            return this.controllerContext.renderingBehaviour.GetPortalCollider().enabled;
        }
    }
}
