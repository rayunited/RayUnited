﻿using Assets.Scripts.GameMechanics.Entities.NewEntity;

namespace Assets.Scripts.GameMechanics.Entities.Portal.Rules
{
    public class PortalBehaviourRule : NewEntityRule<PortalContext>
    {
        public PortalBehaviourRule(string ruleId, object controller)
            : base(ruleId, controller) { }
    }
}
