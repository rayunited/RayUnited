﻿using System;

namespace Assets.Scripts.GameMechanics.Entities.Portal.Rules
{
    public class PortalOnRule : PortalBehaviourRule
    {
        public const string RULE_ID = "PORTAL_ON_PLACEHOLDER";

        public PortalOnRule(object controller)
            : base(RULE_ID, controller)
        { }

        public override void GameplayFixedUpdate()
        {
            // throw new NotImplementedException();
        }
    }
}
