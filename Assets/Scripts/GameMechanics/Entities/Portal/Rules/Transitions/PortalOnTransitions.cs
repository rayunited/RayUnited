﻿using Assets.Scripts.GameMechanics.Entities.Portal.Contexts;

namespace Assets.Scripts.GameMechanics.Entities.Portal.Rules.Transitions
{
    public static class PortalOnTransitions
    {
        public static PortalOnContext GetPortalOnContext(PortalOnContext savedContext)
        {
            return savedContext;
        }
    }
}
