﻿using Assets.Scripts.GameMechanics.Entities.Portal.Contexts;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Portal.Rules.Transitions
{
    public class PortalRulesTransitions
    {
        protected PortalController portalController;

        public PortalRulesTransitions(PortalController portalController)
        {
            this.portalController = portalController;
        }

        public void EnterPortalOn()
        {
            var savedContext = this.portalController
                .controllerContext.contextsHolder.UseContext<PortalOnContext>(
                PortalOnRule.RULE_ID);

            this.portalController.controllerContext.contextsHolder.
                SetContext(PortalOnRule.RULE_ID,
                    PortalOnTransitions.GetPortalOnContext(savedContext));
            this.portalController.SetCurrentRule(
                new PortalOnRule(this.portalController));
        }
    }
}
