﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

namespace Assets.Scripts.GameMechanics.Entities.Portal
{
    public class PortalsCamerasRenderingOrchestrator : MonoBehaviour
    {
        public List<ICustomizableRenderCamera> handlers = new List<ICustomizableRenderCamera>();

        private void Start()
        {
            RenderPipelineManager.beginCameraRendering += BeginCameraRendering;
            RenderPipelineManager.endCameraRendering += EndCameraRendering;
            //RenderPipelineManager.beginFrameRendering += BeginFrameRendering;
        }

        //void BeginFrameRendering(ScriptableRenderContext context, UnityEngine.Camera camera)
        //{
        //    this.handlers.FirstOrDefault(x => x.IsAddressedToMe(camera))?.BeginFrameRendering();
        //}

        void BeginCameraRendering(ScriptableRenderContext context, UnityEngine.Camera camera)
        {
            this.handlers.FirstOrDefault(x => x.IsAddressedToMe(camera))?.BeginCameraRendering();
        }

        void EndCameraRendering(ScriptableRenderContext context, UnityEngine.Camera camera)
        {
            this.handlers.FirstOrDefault(x => x.IsAddressedToMe(camera))?.EndCameraRendering();
        }
    }
}
