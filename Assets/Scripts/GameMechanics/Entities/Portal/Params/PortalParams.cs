﻿using System;

namespace Assets.Scripts.GameMechanics.Entities.Portal.Params
{
    [Serializable]
    public class PortalParams
    {
        public PortalController linkedPortal;
        public bool invertForwardDirection = false;
    }
}
