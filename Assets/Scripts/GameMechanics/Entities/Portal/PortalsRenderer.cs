﻿using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics.Camera;
using Assets.Scripts.Utils.Portals;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Portal
{
    public class PortalsCamerasPool
    {
        protected Dictionary<string, Dictionary<int, PortalCameraHandle>> pool = new Dictionary<string, Dictionary<int, PortalCameraHandle>>();

        protected RenderTexture blankTexture = null;

        public PortalCameraHandle GetCameraTransformedByPortalsTunnel(
            PlayerController playerController,
            PortalCameraHandle rootCamera, PortalCameraHandle baseCamera, PortalHandle entrancePortal, PortalHandle exitPortal,
            int recursionDepth)
        {
            //recursionDepth = 0;
            if (!this.pool.ContainsKey(exitPortal.GetName()))
            {
                this.pool[exitPortal.GetName()] = new Dictionary<int, PortalCameraHandle>();
            }

            if (!this.pool[exitPortal.GetName()].ContainsKey(recursionDepth))
            {
                GameObject newCameraGameObject = new GameObject();
                var component = newCameraGameObject.AddComponent<UnityEngine.Camera>();
                
                component.enabled = false;

                this.pool[exitPortal.GetName()][recursionDepth] = PortalCameraHandle.FromCamera(component);
            }

            var camera = this.pool[exitPortal.GetName()][recursionDepth];

            camera.GetCamera().transform.position = baseCamera.GetCamera().transform.position;
            camera.GetCamera().transform.rotation = baseCamera.GetCamera().transform.rotation;
            camera.GetCamera().projectionMatrix = baseCamera.GetCamera().projectionMatrix;

            camera.GetCamera().transform.SetPositionAndRotation(
                exitPortal.GetController().transform.TransformPoint(
                    entrancePortal.GetController().transform.InverseTransformPoint(baseCamera.GetCamera().transform.position)),
                Quaternion.LookRotation(
                    exitPortal.GetController().transform.TransformDirection(
                        entrancePortal.GetController().transform.InverseTransformDirection(baseCamera.GetCamera().transform.forward)),
                    exitPortal.GetController().transform.TransformDirection(
                        entrancePortal.GetController().transform.InverseTransformDirection(baseCamera.GetCamera().transform.up))
                )
            );

            return camera;
        }
    }

    public class RenderTexturesPool
    {
        protected Dictionary<string, Dictionary<int, RenderTexture>> pool = new Dictionary<string, Dictionary<int, RenderTexture>>();

        protected RenderTexture blankTexture = null;

        public RenderTexture GetBlankTexture()
        {
            if (this.blankTexture == null)
            {
                this.blankTexture = new RenderTexture(Screen.width, Screen.height, 0);
            }
            return this.blankTexture;
        }

        public RenderTexture GetTexture(string id, int recursionDepth)
        {
            if (!this.pool.ContainsKey(id))
            {
                this.pool[id] = new Dictionary<int, RenderTexture>();
            }

            if (!this.pool[id].ContainsKey(recursionDepth))
            {
                this.pool[id][recursionDepth] = new RenderTexture(Screen.width, Screen.height, 0);
            }

            return this.pool[id][recursionDepth];
        }
    }

    public class PortalsRenderScreensPool
    {

    }

    public class PortalCameraHandle
    {
        protected UnityEngine.Camera camera;

        public PortalCameraHandle(UnityEngine.Camera camera)
        {
            this.camera = camera;
        }

        public void SetNearClipPlaneByOtherCamera(PortalHandle entrancePortal, PortalHandle exitPortal, PortalCameraHandle otherCamera)
        {
            var linkedPortal = exitPortal.GetController();
            var playerCam = otherCamera.GetCamera();
            var portalCam = this.camera;

            float nearClipOffset = 0.1f;
            float nearClipLimit = 0.5f;

            // Learning resource:
            // http://www.terathon.com/lengyel/Lengyel-Oblique.pdf
            Transform clipPlane = exitPortal.GetController().transform;
            int dot = System.Math.Sign(Vector3.Dot(clipPlane.forward, exitPortal.GetController().transform.position - portalCam.transform.position));

            Vector3 camSpacePos = portalCam.worldToCameraMatrix.MultiplyPoint(clipPlane.position);
            Vector3 camSpaceNormal = portalCam.worldToCameraMatrix.MultiplyVector(clipPlane.forward) * dot;
            float camSpaceDst = -Vector3.Dot(camSpacePos, camSpaceNormal) + nearClipOffset;

            // Don't use oblique clip plane if very close to portal as it seems this can cause some visual artifacts
            if (Mathf.Abs(camSpaceDst) > nearClipLimit)
            {
                Vector4 clipPlaneCameraSpace = new Vector4(camSpaceNormal.x, camSpaceNormal.y, camSpaceNormal.z, camSpaceDst);

                // Update projection based on new clip plane
                // Calculate matrix with player cam so that player camera settings (fov, etc) are used
                portalCam.projectionMatrix = playerCam.CalculateObliqueMatrix(clipPlaneCameraSpace);
            }
            else
            {
                portalCam.projectionMatrix = playerCam.projectionMatrix;
            }
        }

        public static PortalCameraHandle FromCamera(UnityEngine.Camera camera)
        {
            return new PortalCameraHandle(camera);
        }

        public void RenderToTexture(RenderTexture renderTexture)
        {
            this.camera.targetTexture = renderTexture;
            this.camera.Render();
        }

        public UnityEngine.Camera GetCamera()
        {
            return this.camera;
        }
    }

    public class PortalHandle
    {
        protected PortalController portalController;

        public PortalHandle(PortalController portalController)
        {
            this.portalController = portalController;
        }

        public PortalController GetController()
        {
            return this.portalController;
        }

        public void DoNotDisplay()
        {
            this.portalController.controllerContext.portalScreenHandle.screenHandle.material.SetInt("displayMask", 0);
        }

        public void Display()
        {
            this.portalController.controllerContext.portalScreenHandle.screenHandle.material.SetInt("displayMask", 1);
        }

        public static PortalHandle From(PortalController portal)
        {
            return new PortalHandle(portal);
        }

        public void ClearScreenTexture(RenderTexturesPool pool)
        {
            this.portalController.controllerContext.portalScreenHandle.screenHandle.material.SetTexture("_MainTex", pool.GetBlankTexture());
        }

        public string GetName()
        {
            return this.portalController.name;
        }

        public PortalCameraHandle GetCameraTransformedBySpectator(PortalCameraHandle portalCameraHandle)
        {
            var camera = this.portalController.controllerContext.portalCamera;
            camera.transform.position = this.portalController.transform.position;
            camera.transform.rotation = this.portalController.transform.rotation;

            camera.projectionMatrix = portalCameraHandle.GetCamera().projectionMatrix;
            var localToWorldMatrix = portalCameraHandle.GetCamera().transform.localToWorldMatrix;
            camera.transform.SetPositionAndRotation(localToWorldMatrix.GetColumn(3), localToWorldMatrix.rotation);

            return PortalCameraHandle.FromCamera(camera);
        }

        public void SetScreenTexture(RenderTexture texture)
        {
            this.portalController.controllerContext.portalScreenHandle.screenHandle.material.SetTexture("_MainTex", texture);
        }

        public void ScreenCastShadowsOnly()
        {
            this.portalController.controllerContext.portalScreenHandle.screenHandle.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
        }

        public void ScreenNormalRendering()
        {
            this.portalController.controllerContext.portalScreenHandle.screenHandle.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        }

        public Renderer GetScreenRenderer()
        {
            return this.portalController.controllerContext.portalScreenHandle.screenHandle;
        }
    }

    public static class CameraUtilityHelper
    {
        public static bool VisibleFromCamera(PortalHandle portalThreshold, PortalCameraHandle spectatorCamera)
        {
            return CameraUtility.VisibleFromCamera(portalThreshold.GetScreenRenderer(), spectatorCamera.GetCamera());
        }
    }

    public class PortalsRenderer : MonoBehaviour
    {
        protected List<Tuple<PortalHandle, PortalHandle>> portals;

        protected RenderTexturesPool renderTexturesPool;
        protected PortalsCamerasPool portalsCamerasPool;

        protected PortalsRenderScreensPool renderScreensPool;

        protected UnityEngine.Camera playerCamera;
        protected PortalCameraHandle playerCameraHandle;

        protected PlayerController playerController;

        private void Start()
        {
            this.renderTexturesPool = new RenderTexturesPool();
            this.renderScreensPool = new PortalsRenderScreensPool();
            this.portalsCamerasPool = new PortalsCamerasPool();
            this.playerController = FindObjectOfType<PlayerController>();

            this.portals = FindObjectsOfType<PortalRenderingBehaviour>(includeInactive: true)
                .Select(x =>
                {
                    x.Initialize();
                    x.InitializeStart();
                    var entrancePortal = x.portalController.portalParams.linkedPortal;
                    var exitPortal = x.portalController;
                    entrancePortal.GetComponent<PortalRenderingBehaviour>().Initialize();
                    entrancePortal.GetComponent<PortalRenderingBehaviour>().InitializeStart();

                    return Tuple.Create(PortalHandle.From(entrancePortal), PortalHandle.From(exitPortal));
                }).ToList();

            this.playerCamera = FindObjectOfType<CameraActorHandle>().GetComponent<UnityEngine.Camera>();
            this.playerCameraHandle = PortalCameraHandle.FromCamera(this.playerCamera);
        }

        private void Update()
        {
            RenderPortals(this.playerCamera, 0, 2);
        }

        private void RenderPortalToTexture(
            PortalCameraHandle spectatorCamera,
            RenderTexture texture,
            PortalHandle entrancePortal,
            PortalHandle exitPortal,
            int currentRecursionDepth,
            int maxRecursionDepth)
        {
            entrancePortal.GetController().controllerContext.renderingBehaviour.InitializeStart();

            if (currentRecursionDepth >= maxRecursionDepth)
            {
                foreach ((var entrancePortall, var exitPortall) in this.portals)
                {
                    entrancePortall.DoNotDisplay();
                }
            }

            if (currentRecursionDepth < maxRecursionDepth)
            {
                var exitPortalCameraa = this.portalsCamerasPool.GetCameraTransformedByPortalsTunnel(
                    this.playerController,
                    this.playerCameraHandle,
                    spectatorCamera,
                    entrancePortal,
                    exitPortal,
                    currentRecursionDepth
                );

                exitPortalCameraa.GetCamera().enabled = true;

                var visiblePortals = 
                    this.portals
                        .Where(x => x.Item1.GetController().gameObject.activeInHierarchy && CameraUtilityHelper.VisibleFromCamera(x.Item1, exitPortalCameraa))
                        .Where(
                            x => {exitPortal.GetController().controllerContext.renderingBehaviour.InitializeStart();  return CameraUtility.BoundsOverlap(
                                exitPortal.GetController().controllerContext.renderingBehaviour.screenMeshFilter,
                                x.Item1.GetController().controllerContext.renderingBehaviour.screenMeshFilter,
                                exitPortalCameraa.GetCamera()); }
                            )
                        .ToList();

                foreach ((var entrancePortalNested, var exitPortalNested) in visiblePortals)
                {
                    RenderPortalToTexture(
                        exitPortalCameraa,
                        this.renderTexturesPool.GetTexture(entrancePortalNested.GetName(), currentRecursionDepth + 1),
                        entrancePortalNested,
                        exitPortalNested,
                        currentRecursionDepth + 1,
                        maxRecursionDepth);
                }

                foreach ((var entrancePortalNested, var exitPortalNested) in visiblePortals)
                {
                    entrancePortalNested.SetScreenTexture(this.renderTexturesPool.GetTexture(entrancePortalNested.GetName(), currentRecursionDepth + 1));
                    entrancePortalNested.Display();
                }

                exitPortalCameraa.GetCamera().enabled = false;
            }

            var exitPortalCamera = this.portalsCamerasPool.GetCameraTransformedByPortalsTunnel(
                this.playerController,
                this.playerCameraHandle,
                spectatorCamera,
                entrancePortal,
                exitPortal,
                currentRecursionDepth);

            exitPortalCamera.GetCamera().enabled = true;

            exitPortal.ScreenCastShadowsOnly();
            exitPortalCamera.SetNearClipPlaneByOtherCamera(entrancePortal, exitPortal, this.playerCameraHandle);
            exitPortalCamera.RenderToTexture(texture);
            exitPortal.ScreenNormalRendering();

            exitPortalCamera.GetCamera().enabled = false;
        }

        private void RenderPortals(UnityEngine.Camera spectatorCamera, int currentRecursionDepth, int maxRecursionDepth)
        {
            var visiblePortals = this.portals.Where(
                x => x.Item1.GetController().IsActivated() && CameraUtilityHelper.VisibleFromCamera(x.Item1, PortalCameraHandle.FromCamera(spectatorCamera))).ToList();

            foreach ((var entrancePortal, var exitPortal) in visiblePortals)
            {
                entrancePortal.ClearScreenTexture(this.renderTexturesPool);
                entrancePortal.DoNotDisplay();
            }

            foreach ((var entrancePortal, var exitPortal) in visiblePortals)
            {
                RenderPortalToTexture(
                    PortalCameraHandle.FromCamera(spectatorCamera),
                    this.renderTexturesPool.GetTexture(entrancePortal.GetName(), currentRecursionDepth),
                    entrancePortal,
                    exitPortal,
                    currentRecursionDepth,
                    maxRecursionDepth);
            }

            foreach ((var entrancePortal, var exitPortal) in visiblePortals)
            {
                entrancePortal.SetScreenTexture(this.renderTexturesPool.GetTexture(entrancePortal.GetName(), currentRecursionDepth));
                entrancePortal.Display();
            }
        }
    }
}
