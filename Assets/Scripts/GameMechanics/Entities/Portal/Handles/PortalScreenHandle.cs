﻿using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Portal.Handles
{
    public class PortalScreenHandle
    {
        public Renderer screenHandle;
        public Material renderMaterial;

        public PortalScreenHandle(Renderer screenHandle, Material renderMaterial)
        {
            this.screenHandle = screenHandle;
            this.renderMaterial = renderMaterial;
        }
    }
}
