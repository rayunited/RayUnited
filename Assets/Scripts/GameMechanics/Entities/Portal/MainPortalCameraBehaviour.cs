﻿using System;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Portal
{
    public class MainPortalCameraBehaviour : MonoBehaviour, ICustomizableRenderCamera
    {
        public Renderer mainPortalRenderer;
        public PortalRenderingBehaviour portalRenderingBehaviour;

        protected UnityEngine.Camera camera;

        private void Awake()
        {
            this.camera = GetComponent<UnityEngine.Camera>();
        }

        public void BeginCameraRendering()
        {
            //this.portalRenderingBehaviour.portalController.portalParams.linkedPortal.controllerContext.portalScreenHandleAsSeenByPortal.ToList().ForEach(x => {
            //    x.Value.screenHandle.enabled = false;
            //});
            ////this.mainPortalRenderer.enabled = true;
            //this.mainPortalRenderer.enabled = false;
        }

        public void EndCameraRendering()
        {
            //this.portalRenderingBehaviour.portalController.controllerContext.portalScreenHandleAsSeenByPortal.ToList().ForEach(x => x.Value.screenHandle.enabled = true);
        }

        public bool IsAddressedToMe(UnityEngine.Camera camera)
        {
            return camera == this.camera;
        }
    }
}
