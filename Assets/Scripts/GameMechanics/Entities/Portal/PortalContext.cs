﻿using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Portal.Handles;
using Assets.Scripts.GameMechanics.Entities.Portal.Rules.Transitions;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Portal
{
    public class PortalContext : NewEntityContext
    {
        public PortalRulesTransitions portalRulesTransitions;
        
        public UnityEngine.Camera portalCamera;
        public Dictionary<string, UnityEngine.Camera> portalCameraAsSeenByPortal = new Dictionary<string, UnityEngine.Camera>();

        public UnityEngine.Camera playerCamera;

        public PortalScreenHandle portalScreenHandle;
        public Dictionary<string, PortalScreenHandle> portalScreenHandleAsSeenByPortal;

        public RenderTexture renderTexture;
        public Dictionary<string, RenderTexture> renderTextureAsSeenByPortal = new Dictionary<string, RenderTexture>();


        public PortalRenderingBehaviour renderingBehaviour;
    }
}
