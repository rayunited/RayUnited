﻿using Assets.Scripts.GameMechanics.Portals;
using Assets.Scripts.Utils.Portals;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Assets.Scripts.GameMechanics.Entities.Portal.Handles;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Portal
{
    public class PortalRenderingBehaviour : MonoBehaviour
    {
        public PortalController portalController;
        [Header("Advanced Settings")]
        public float nearClipOffset = 0.05f;
        public float nearClipLimit = 0.2f;

        protected Transform threshold;
        public MeshFilter screenMeshFilter;

        protected List<PortalRenderingBehaviour> portals;

        protected PortalsCamerasRenderingOrchestrator portalsCamerasRenderingOrchestrator;

        protected Dictionary<int, Dictionary<string, List<RenderTexture>>> renderTexturesBank;

        protected List<RenderTexture> renderTextureRoot;

        protected Collider portalCollider;

        protected bool initialized = false;
        protected bool initializedStart = false;

        public Vector3 portalDerivedForward
        {
            get
            {
                return this.portalController.portalParams.invertForwardDirection ? -this.transform.forward : this.transform.forward;
            }
        }

        public List<PortalTraveller> trackedTravellers;

        public void InitializeStart()
        {
            if (!this.initializedStart)
            {
                this.threshold = GetComponentsInChildren<Transform>().First(x => x.name.Equals("Threshold"));
                this.screenMeshFilter = GetComponentInChildren<MeshFilter>();
                this.portalCollider = GetComponentInChildren<Collider>();
                this.initializedStart = true;
            }
        }

        void Start()
        {
            InitializeStart();
        }

        private void InitializeSeenByPortalCamerasAndRenderers()
        {
            var mainScreenPrototype = GetComponentInChildren<Renderer>(includeInactive: true);
            var mainPortalCamera = GetComponentInChildren<UnityEngine.Camera>(includeInactive: true);
             
            var mainPortalCameraBehaviour = mainPortalCamera.gameObject.AddComponent<MainPortalCameraBehaviour>();
            mainPortalCameraBehaviour.mainPortalRenderer = mainScreenPrototype;
            mainPortalCameraBehaviour.portalRenderingBehaviour = this;

            this.portalsCamerasRenderingOrchestrator.handlers.Add(mainPortalCameraBehaviour);

            foreach (var portal in this.portals)
            {
                if (!portal.name.Equals(this.name) && !portal.name.Equals(this.portalController.portalParams.linkedPortal.name))
                {
                    GameObject seenByPortalCamera = new GameObject();
                    var seenByPortalCameraComponent = seenByPortalCamera.AddComponent<UnityEngine.Camera>();
                    seenByPortalCameraComponent.enabled = false;

                    seenByPortalCamera.name = "Seen_By_Portal_" + portal.name;

                    var seenByPortalCameraRendererComponentGameObject = Instantiate(mainScreenPrototype);

                    seenByPortalCameraRendererComponentGameObject.name = "Screen_Seen_By_Portal_" + portal.name;
                    
                    seenByPortalCameraRendererComponentGameObject.enabled = false;

                    var seenByPortalCameraBehaviour = seenByPortalCamera.AddComponent<SeenByPortalCameraBehaviour>();
                    seenByPortalCameraBehaviour.portalRenderingBehaviour = this;

                    this.portalsCamerasRenderingOrchestrator.handlers.Add(seenByPortalCameraBehaviour);

                    seenByPortalCameraBehaviour.visibleByPortalRenderer = seenByPortalCameraRendererComponentGameObject;
                    seenByPortalCameraBehaviour.mainPortalRenderer = mainScreenPrototype;

                    seenByPortalCamera.transform.SetParent(this.transform, false);
                    seenByPortalCameraRendererComponentGameObject.transform.SetParent(this.transform, false);

                    this.portalController.Initialize().controllerContext.portalCameraAsSeenByPortal[portal.name] = seenByPortalCameraComponent;
                    this.portalController.Initialize().controllerContext.portalScreenHandleAsSeenByPortal[portal.name] = new PortalScreenHandle(
                        seenByPortalCameraRendererComponentGameObject, seenByPortalCameraRendererComponentGameObject.material);
                }
            }
        }

        public Vector3 InverseTransformDirection(Vector3 dir)
        {
            GameObject temp = new GameObject();
            temp.transform.SetPositionAndRotation(
                this.transform.position,
                Quaternion.LookRotation(this.portalDerivedForward, this.transform.up)
            );
            var result = temp.transform.InverseTransformDirection(dir);
            Destroy(temp.gameObject);
            return result;
        }

        public Vector3 TransformDirection(Vector3 dir)
        {
            GameObject temp = new GameObject();
            temp.transform.SetPositionAndRotation(
                this.transform.position,
                Quaternion.LookRotation(this.portalDerivedForward, this.transform.up)
            );
            var result = temp.transform.TransformDirection(dir);
            Destroy(temp.gameObject);
            return result;
        }

        public Vector3 TransformPoint(Vector3 pos)
        {
            GameObject temp = new GameObject();
            temp.transform.SetPositionAndRotation(
                this.transform.position,
                Quaternion.LookRotation(this.portalDerivedForward, this.transform.up)
            );
            var result = temp.transform.TransformPoint(pos);
            Destroy(temp.gameObject);
            return result;
        }

        public Vector3 InverseTransformPoint(Vector3 pos)
        {
            GameObject temp = new GameObject();
            temp.transform.SetPositionAndRotation(
                this.transform.position,
                Quaternion.LookRotation(this.portalDerivedForward, this.transform.up)
            );
            var result = temp.transform.InverseTransformPoint(pos);
            Destroy(temp.gameObject);
            return result;
        }

        public Matrix4x4 getLocalToWorldMatrix()
        {
            GameObject temp = new GameObject();
            temp.transform.SetPositionAndRotation(
                this.transform.position,
                Quaternion.LookRotation(this.portalDerivedForward, this.transform.up)
            );
            var result = temp.transform.localToWorldMatrix;
            Destroy(temp.gameObject);
            return result;
        }

        public Matrix4x4 getWorldToLocalMatrix()
        {
             GameObject temp = new GameObject();
             temp.transform.SetPositionAndRotation(
                this.transform.position,
                Quaternion.LookRotation(this.portalDerivedForward, this.transform.up)
             );
             var result = temp.transform.worldToLocalMatrix;
             Destroy(temp.gameObject);
             return result;
        }

        public void Initialize()
        {
            if (!this.initialized)
            {
                this.portalController = GetComponent<PortalController>();
                this.trackedTravellers = new List<PortalTraveller>();

                this.portals = FindObjectsOfType<PortalRenderingBehaviour>(includeInactive: true).ToList();

                this.portalsCamerasRenderingOrchestrator = FindObjectOfType<PortalsCamerasRenderingOrchestrator>();

                this.renderTexturesBank = new Dictionary<int, Dictionary<string, List<RenderTexture>>>();

                InitializeSeenByPortalCamerasAndRenderers();
                this.initialized = true;
            }
        }

        private void Awake()
        {
            Initialize();
        }

        private void FixedUpdate()
        {
            var playerCam = this.portalController.controllerContext.playerCamera;
            Debug.DrawRay(this.transform.position, this.portalDerivedForward * 10f);
            HandleTravellers();
            
            
            
            
            
            
            //ProtectScreenFromClipping(playerCam.transform.position);





            //HandleTravellers();
        }

        private void LateUpdate()
        {
            var playerCam = this.portalController.controllerContext.playerCamera;
            HandleTravellers();
            
            
            
            
            
            //ProtectScreenFromClipping(playerCam.transform.position);
        }

        private void Update()
        {
            var playerCam = this.portalController.controllerContext.playerCamera;
           
            //var tex = RenderToTextureAsSeenFromSpectator(
            //    spectatorCam,
            //    this.renderTextureRoot,
            //    this.renderTexturesBank,
            //    0, 0);

            //this.portalController.portalParams.linkedPortal.controllerContext.portalScreenHandle.screenHandle.material
            //    .SetTexture("_MainTex", tex);

            //this.renderTexturesBank.ToList().ForEach(x => x.Value.ToList().ForEach(y => y.Value.Release()));

            ProtectScreenFromClipping(playerCam, playerCam.transform.position);

            //renderTexture.Release();
            // HandleTravellers();
        }

        public bool ContainsTravellerWithinBoundaries(PortalTraveller traveller)
        {
            Vector3 direction1 = this.portalDerivedForward;
            Vector3 direction2 = -this.portalDerivedForward;
            Vector3 startingPoint = this.transform.position;

            Ray ray1 = new Ray(startingPoint, direction1);
            Ray ray2 = new Ray(startingPoint, direction2);
            float distance1 = Vector3.Cross(ray1.direction, traveller.transform.position - ray1.origin).magnitude;
            float distance2 = Vector3.Cross(ray2.direction, traveller.transform.position - ray2.origin).magnitude;
            return distance1 < 10f || distance2 < 10f;
        }

        private void HandleTravellers()
        {
            var linkedPortal = this.portalController.portalParams.linkedPortal.GetComponent<PortalRenderingBehaviour>();

            for (int i = 0; i < this.trackedTravellers.Count; i++)
            {
                PortalTraveller traveller = this.trackedTravellers[i];
                Transform travellerT = traveller.transform;
                var m = linkedPortal.getLocalToWorldMatrix()
                    * this.getWorldToLocalMatrix() * travellerT.localToWorldMatrix;

                Vector3 offsetFromPortal = travellerT.position - this.transform.position;
                int portalSide = System.Math.Sign(Vector3.Dot(offsetFromPortal, this.portalDerivedForward));
                int portalSideOld = System.Math.Sign(Vector3.Dot(traveller.previousOffsetFromPortal, this.portalDerivedForward));

                // Teleport the traveller if it has crossed from one side of the portal to the other
                if (portalSide != portalSideOld)
                {
                    traveller.Teleport(
                        this.transform, linkedPortal.transform, m.GetColumn(3), m.rotation,
                        traveller.GetGravityDirection(), traveller.GetGravityForward());
                    
                    linkedPortal.portalController.controllerContext.renderingBehaviour.OnTravellerEnterPortal(traveller);               

                    this.trackedTravellers.RemoveAt(i);
                    i--;
                }
                else
                {
                    traveller.previousOffsetFromPortal = offsetFromPortal;
                }
            }
        }

        void CreateViewTexture()
        {
            if (this.portalController.controllerContext.renderTexture == null ||
                this.portalController.controllerContext.renderTexture.width != UnityEngine.Screen.width ||
                this.portalController.controllerContext.renderTexture.height != UnityEngine.Screen.height)
            {
                if (this.portalController.controllerContext.renderTexture != null)
                {
                    this.portalController.controllerContext.renderTexture.Release();
                }
                this.portalController.controllerContext.renderTexture = new RenderTexture(UnityEngine.Screen.width, UnityEngine.Screen.height, 0);
                this.portalController.controllerContext.portalCamera.targetTexture = this.portalController.controllerContext.renderTexture;
                //this.portalController.portalParams.linkedPortal.controllerContext
                //    .portalScreenHandle.renderMaterial.SetTexture("_MainTex", this.portalController.controllerContext.renderTexture);
            }
        }

        void ApplyViewTexture()
        {
            //RenderTexture capturedRenderTexture = new RenderTexture(UnityEngine.Screen.width, UnityEngine.Screen.height, 0);
            //Graphics.CopyTexture(this.portalController.controllerContext.portalCamera.targetTexture, capturedRenderTexture);

            this.portalController.portalParams.linkedPortal.controllerContext
                .portalScreenHandle.renderMaterial.SetTexture(
                    "_MainTex", this.portalController.controllerContext.renderTexture);
        }

        // Called before any portal cameras are rendered for the current frame
        public void PrePortalRender()
        {
            foreach (var traveller in this.trackedTravellers)
            {
                UpdateSliceParams(traveller);
            }
        }

        public void PostPortalRender()
        {
            var playerCam = this.portalController.controllerContext.playerCamera;

            foreach (var traveller in this.trackedTravellers)
            {
                UpdateSliceParams(traveller);
            }
            ProtectScreenFromClipping(playerCam.transform.position);
        }

        public float ProtectScreenFromClipping(UnityEngine.Camera playerCam, Vector3 viewPoint)
        {
            var screen = this.portalController.controllerContext.portalScreenHandle.screenHandle;

            float halfHeight = playerCam.nearClipPlane * Mathf.Tan(playerCam.fieldOfView * 0.5f * Mathf.Deg2Rad);
            float halfWidth = halfHeight * playerCam.aspect;
            float dstToNearClipPlaneCorner = new Vector3(halfWidth, halfHeight, playerCam.nearClipPlane).magnitude;
            float screenThickness = dstToNearClipPlaneCorner;

            Transform screenT = screen.transform;
            bool camFacingSameDirAsPortal = Vector3.Dot(this.transform.forward, this.transform.position - viewPoint) > 0;
            screenT.localScale = new Vector3(screenThickness, screenT.localScale.y, screenT.localScale.z);
            screenT.localPosition = Vector3.forward * screenThickness * ((camFacingSameDirAsPortal) ? 0.5f : -0.5f);
            return screenThickness;
        }

        public float ProtectScreenFromClipping(Vector3 viewPoint)
        {
            var playerCam = this.portalController.controllerContext.playerCamera;
            var screen = this.portalController.controllerContext.portalScreenHandle.screenHandle;

            float halfHeight = playerCam.nearClipPlane * Mathf.Tan(playerCam.fieldOfView * 0.5f * Mathf.Deg2Rad);
            float halfWidth = halfHeight * playerCam.aspect;
            float dstToNearClipPlaneCorner = new Vector3(halfWidth, halfHeight, playerCam.nearClipPlane).magnitude;
            float screenThickness = dstToNearClipPlaneCorner;

            Transform screenT = screen.transform;
            bool camFacingSameDirAsPortal = Vector3.Dot(this.transform.forward, this.transform.position - viewPoint) > 0;
            screenT.localScale = new Vector3(screenThickness, screenT.localScale.y, screenT.localScale.z);
            screenT.localPosition = Vector3.forward * screenThickness * ((camFacingSameDirAsPortal) ? 0.5f : -0.5f);
            return screenThickness;
        }

        void UpdateSliceParams(PortalTraveller traveller)
        {
            var linkedPortal = this.portalController.portalParams.linkedPortal;
            var screen = this.portalController.controllerContext.portalScreenHandle.screenHandle;
            var playerCam = this.portalController.controllerContext.playerCamera;

            // Calculate slice normal
            int side = SideOfPortal(traveller.transform.position);
            Vector3 sliceNormal = this.portalDerivedForward * -side;
            Vector3 cloneSliceNormal = linkedPortal.controllerContext.renderingBehaviour.portalDerivedForward * side;

            // Calculate slice centre
            Vector3 slicePos = this.transform.position;
            Vector3 cloneSlicePos = linkedPortal.transform.position;

            // Adjust slice offset so that when player standing on other side of portal to the object, the slice doesn't clip through
            float sliceOffsetDst = 0;
            float cloneSliceOffsetDst = 0;
            float screenThickness = screen.transform.localScale.z;

            bool playerSameSideAsTraveller = SameSideOfPortal(playerCam.transform.position, traveller.transform.position);
            if (!playerSameSideAsTraveller)
            {
                sliceOffsetDst = -screenThickness;
            }
            bool playerSameSideAsCloneAppearing = side != linkedPortal.controllerContext.renderingBehaviour.SideOfPortal(playerCam.transform.position);
            if (!playerSameSideAsCloneAppearing)
            {
                cloneSliceOffsetDst = -screenThickness;
            }

            // Apply parameters
            for (int i = 0; i < traveller.originalMaterials.Length; i++)
            {
                traveller.originalMaterials[i].SetVector("sliceCentre", slicePos);
                traveller.originalMaterials[i].SetVector("sliceNormal", sliceNormal);
                traveller.originalMaterials[i].SetFloat("sliceOffsetDst", sliceOffsetDst);

                traveller.cloneMaterials[i].SetVector("sliceCentre", cloneSlicePos);
                traveller.cloneMaterials[i].SetVector("sliceNormal", cloneSliceNormal);
                traveller.cloneMaterials[i].SetFloat("sliceOffsetDst", cloneSliceOffsetDst);
            }
        }        

        // Use custom projection matrix to align portal camera's near clip plane with the surface of the portal
        // Note that this affects precision of the depth buffer, which can cause issues with effects like screenspace AO
        void SetNearClipPlaneForPortalCameraAsSeenByOtherPortal(UnityEngine.Camera otherPortalSpectatingCamera, string spectatingPortalName)
        {
            var thatOtherPortalOurPortalCam = this.portalController.controllerContext.portalCameraAsSeenByPortal[spectatingPortalName];

            // Learning resource:
            // http://www.terathon.com/lengyel/Lengyel-Oblique.pdf
            Transform clipPlane = this.transform;
            int dot = System.Math.Sign(Vector3.Dot(clipPlane.forward, this.transform.position - thatOtherPortalOurPortalCam.transform.position));

            Vector3 camSpacePos = thatOtherPortalOurPortalCam.worldToCameraMatrix.MultiplyPoint(clipPlane.position);
            Vector3 camSpaceNormal = thatOtherPortalOurPortalCam.worldToCameraMatrix.MultiplyVector(clipPlane.forward) * dot;

            float camSpaceDst = -Vector3.Dot(camSpacePos, camSpaceNormal) + this.nearClipOffset;

            // Don't use oblique clip plane if very close to portal as it seems this can cause some visual artifacts
            if (Mathf.Abs(camSpaceDst) > this.nearClipLimit)
            {
                Vector4 clipPlaneCameraSpace = new Vector4(camSpaceNormal.x, camSpaceNormal.y, camSpaceNormal.z, camSpaceDst);

                // Update projection based on new clip plane
                // Calculate matrix with player cam so that player camera settings (fov, etc) are used
                thatOtherPortalOurPortalCam.projectionMatrix = otherPortalSpectatingCamera.CalculateObliqueMatrix(clipPlaneCameraSpace);
            }
            else
            {
                thatOtherPortalOurPortalCam.projectionMatrix = otherPortalSpectatingCamera.projectionMatrix;
            }
        }

        void SetNearClipPlane(
            UnityEngine.Camera spectatorCamera)
        {
            Transform clipPlane = this.transform;
            var portalCam = this.portalController.controllerContext.portalCamera;
            Vector3 playerCamPosition = spectatorCamera.transform.position;

            int dot = System.Math.Sign(Vector3.Dot(clipPlane.forward, this.transform.position - playerCamPosition));

            Vector3 camSpacePos = portalCam.worldToCameraMatrix.MultiplyPoint(clipPlane.position);
            Vector3 camSpaceNormal = portalCam.worldToCameraMatrix.MultiplyVector(clipPlane.forward) * dot;
            float camSpaceDst = -Vector3.Dot(camSpacePos, camSpaceNormal) + this.nearClipOffset;

            // Don't use oblique clip plane if very close to portal as it seems this can cause some visual artifacts
            if (Mathf.Abs(camSpaceDst) > this.nearClipLimit)
            {
                Vector4 clipPlaneCameraSpace = new Vector4(camSpaceNormal.x, camSpaceNormal.y, camSpaceNormal.z, camSpaceDst);

                // Update projection based on new clip plane
                // Calculate matrix with player cam so that player camera settings (fov, etc) are used
                portalCam.projectionMatrix = spectatorCamera.CalculateObliqueMatrix(clipPlaneCameraSpace);
            }
            else
            {
                portalCam.projectionMatrix = spectatorCamera.projectionMatrix;
            }
        }

        // Use custom projection matrix to align portal camera's near clip plane with the surface of the portal
        // Note that this affects precision of the depth buffer, which can cause issues with effects like screenspace AO
        void SetNearClipPlane()
        {
            var linkedPortal = this.portalController.portalParams.linkedPortal;
            var playerCam = this.portalController.controllerContext.playerCamera;
            var portalCam = this.portalController.controllerContext.portalCamera;

            // Learning resource:
            // http://www.terathon.com/lengyel/Lengyel-Oblique.pdf
            Transform clipPlane = this.transform;
            int dot = System.Math.Sign(Vector3.Dot(clipPlane.forward, this.transform.position - portalCam.transform.position));

            Vector3 camSpacePos = portalCam.worldToCameraMatrix.MultiplyPoint(clipPlane.position);
            Vector3 camSpaceNormal = portalCam.worldToCameraMatrix.MultiplyVector(clipPlane.forward) * dot;
            float camSpaceDst = -Vector3.Dot(camSpacePos, camSpaceNormal) + this.nearClipOffset;

            // Don't use oblique clip plane if very close to portal as it seems this can cause some visual artifacts
            if (Mathf.Abs(camSpaceDst) > this.nearClipLimit)
            {
                Vector4 clipPlaneCameraSpace = new Vector4(camSpaceNormal.x, camSpaceNormal.y, camSpaceNormal.z, camSpaceDst);

                // Update projection based on new clip plane
                // Calculate matrix with player cam so that player camera settings (fov, etc) are used
                portalCam.projectionMatrix = playerCam.CalculateObliqueMatrix(clipPlaneCameraSpace);
            }
            else
            {
                portalCam.projectionMatrix = playerCam.projectionMatrix;
            }
        }

        void OnTravellerEnterPortal(PortalTraveller traveller)
        {
            if (!this.trackedTravellers.Contains(traveller))
            {
                traveller.EnterPortalThreshold();
                traveller.previousOffsetFromPortal = traveller.transform.position - this.transform.position;
                //traveller.lastPortal = this.portalController.portalParams.linkedPortal.transform;
                this.trackedTravellers.Add(traveller);
            }
        }

        void HandleClipping()
        {
            var linkedPortal = this.portalController.portalParams.linkedPortal;
            var playerCam = this.portalController.controllerContext.playerCamera;
            var portalCam = this.portalController.controllerContext.portalCamera;

            var portalCamPos = portalCam.transform.position;

            // There are two main graphical issues when slicing travellers
            // 1. Tiny sliver of mesh drawn on backside of portal
            //    Ideally the oblique clip plane would sort this out, but even with 0 offset, tiny sliver still visible
            // 2. Tiny seam between the sliced mesh, and the rest of the model drawn onto the portal screen
            // This function tries to address these issues by modifying the slice parameters when rendering the view from the portal
            // Would be great if this could be fixed more elegantly, but this is the best I can figure out for now
            const float hideDst = -1000;
            const float showDst = 1000;
            float screenThickness = linkedPortal.controllerContext.renderingBehaviour.ProtectScreenFromClipping(portalCam.transform.position);

            foreach (var traveller in this.trackedTravellers)
            {
                if (SameSideOfPortal(traveller.transform.position, portalCamPos))
                {
                    // Addresses issue 1
                    traveller.SetSliceOffsetDst(hideDst, false);
                }
                else
                {
                    // Addresses issue 2
                    traveller.SetSliceOffsetDst(showDst, false);
                }

                // Ensure clone is properly sliced, in case it's visible through this portal:
                int cloneSideOfLinkedPortal = -SideOfPortal(traveller.transform.position);
                bool camSameSideAsClone = linkedPortal.controllerContext.renderingBehaviour.SideOfPortal(portalCamPos) == cloneSideOfLinkedPortal;
                if (camSameSideAsClone)
                {
                    traveller.SetSliceOffsetDst(screenThickness, true);
                }
                else
                {
                    traveller.SetSliceOffsetDst(-screenThickness, true);
                }
            }

            var offsetFromPortalToCam = portalCamPos - this.transform.position;
            foreach (var linkedTraveller in linkedPortal.controllerContext.renderingBehaviour.trackedTravellers)
            {
                var travellerPos = linkedTraveller.graphicsObject.transform.position;
                var clonePos = linkedTraveller.graphicsClone.transform.position;
                // Handle clone of linked portal coming through this portal:
                bool cloneOnSameSideAsCam = linkedPortal.controllerContext.renderingBehaviour.SideOfPortal(travellerPos) != SideOfPortal(portalCamPos);
                if (cloneOnSameSideAsCam)
                {
                    // Addresses issue 1
                    linkedTraveller.SetSliceOffsetDst(hideDst, true);
                }
                else
                {
                    // Addresses issue 2
                    linkedTraveller.SetSliceOffsetDst(showDst, true);
                }

                // Ensure traveller of linked portal is properly sliced, in case it's visible through this portal:
                bool camSameSideAsTraveller = linkedPortal.controllerContext.renderingBehaviour.SameSideOfPortal(linkedTraveller.transform.position, portalCamPos);
                if (camSameSideAsTraveller)
                {
                    linkedTraveller.SetSliceOffsetDst(screenThickness, false);
                }
                else
                {
                    linkedTraveller.SetSliceOffsetDst(-screenThickness, false);
                }
            }
        }

        public void OnThresholdCollisionEnter(PortalTraveller portalTraveller)
        {
            OnTravellerEnterPortal(portalTraveller);            
        }

        public void OnThresholdCollisionExit(PortalTraveller portalTraveller)
        {
            if (portalTraveller && this.trackedTravellers.Contains(portalTraveller))
            {
                //traveller.ExitPortalThreshold();
                this.trackedTravellers.Remove(portalTraveller);
            }
        }

        void OnTriggerExit(Collider other)
        {
            //var traveller = other.GetComponent<PortalTraveller>();
            //if (traveller && this.trackedTravellers.Contains(traveller))
            //{
            //    traveller.ExitPortalThreshold();
            //    this.trackedTravellers.Remove(traveller);
            //}
        }

        /*
        ** Some helper/convenience stuff:
        */

        int SideOfPortal(Vector3 pos)
        {
            //return System.Math.Sign(Vector3.Dot(pos - this.transform.position, this.transform.forward));
            return System.Math.Sign(Vector3.Dot(pos - this.transform.position, this.portalDerivedForward));
        }

        bool SameSideOfPortal(Vector3 posA, Vector3 posB)
        {
            return SideOfPortal(posA) == SideOfPortal(posB);
        }   

        public Collider GetPortalCollider()
        {
            return this.portalCollider;
        }
    }
}
