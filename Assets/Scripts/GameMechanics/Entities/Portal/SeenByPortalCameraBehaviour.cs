﻿using System;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Portal
{
    public interface ICustomizableRenderCamera
    {
        void BeginCameraRendering();

        void EndCameraRendering();

        bool IsAddressedToMe(UnityEngine.Camera camera);
    }

    public class SeenByPortalCameraBehaviour : MonoBehaviour, ICustomizableRenderCamera
    {
        public Renderer visibleByPortalRenderer;
        public Renderer mainPortalRenderer;
        public PortalRenderingBehaviour portalRenderingBehaviour;

        protected UnityEngine.Camera camera;

        private void Awake()
        {
            this.camera = GetComponent<UnityEngine.Camera>();
        }

        public void BeginCameraRendering()
        {
            //this.portalRenderingBehaviour.portalController
            //    .portalParams.linkedPortal.controllerContext.portalScreenHandleAsSeenByPortal.ToList().ForEach(x => x.Value.screenHandle.enabled = false);
            //this.mainPortalRenderer.enabled = false;
            //this.visibleByPortalRenderer.gameObject.SetActive(true);
            
            //this.visibleByPortalRenderer.enabled = true;
        }

        public void EndCameraRendering()
        {
            //this.portalRenderingBehaviour.portalController.controllerContext.portalScreenHandleAsSeenByPortal.ToList().ForEach(x => x.Value.screenHandle.enabled = true);
            //this.mainPortalRenderer.enabled = true;
        }

        public bool IsAddressedToMe(UnityEngine.Camera camera)
        {
            return camera == this.camera;
        }
    }
}
