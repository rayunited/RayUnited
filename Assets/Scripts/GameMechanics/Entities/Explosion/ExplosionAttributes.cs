﻿namespace Assets.Scripts.GameMechanics.Entities.Explosion
{
    public class ExplosionAttributes
    {
        public float explosionRadius = 1f;
        public float explosionForce = 1f;
    }
}
