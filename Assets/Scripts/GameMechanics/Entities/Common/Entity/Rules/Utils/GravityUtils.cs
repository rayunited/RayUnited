﻿using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Entity.Rules.Utils
{
    public static class GravityUtils
    {
        public static bool FallingSpeedLowerThan(Vector3 velocity, Vector3 gravityDirection, float limitFallSpeed)
        {
            float fallingSpeed =
                (Vector3ProjectionHelper.ProjectConsideringDirection(velocity, gravityDirection)).magnitude *
                    Mathf.Sign(Vector3.Dot(velocity, gravityDirection));
            return fallingSpeed < limitFallSpeed;
        }

        public static bool FallingSpeedHigherThan(Vector3 velocity, Vector3 gravityDirection, float limitFallSpeed)
        {
            float fallingSpeed =
                (Vector3ProjectionHelper.ProjectConsideringDirection(velocity, gravityDirection)).magnitude *
                    Mathf.Sign(Vector3.Dot(velocity, gravityDirection));
            return fallingSpeed > limitFallSpeed;
        }

        public static Vector3 AccelerateGravitationallyUpUntilLimitFallSpeed(
            Vector3 velocity,
            Vector3 gravityDirection,
            float gravityAcceleration,
            float limitFallSpeed)
        {
            if (FallingSpeedLowerThan(velocity, gravityDirection, limitFallSpeed))
            {
                return velocity + (gravityDirection * gravityAcceleration);
            } else if (FallingSpeedHigherThan(velocity, gravityDirection, limitFallSpeed))
            {
                return Vector3.Lerp(velocity, gravityDirection * limitFallSpeed, 0.005f);
            } else
            {
                return velocity;
            }
        }

        public static Vector3 FallAtSpeed(Vector3 velocity, Vector3 gravityDirection, float fallSpeed)
        {
            return Vector3.ProjectOnPlane(velocity, gravityDirection) + (gravityDirection.normalized * fallSpeed);
        }
    }
}
