﻿using Assets.Scripts.PlayerMechanics.Rules.Common;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Entity.Rules.Common
{
    public enum ThresholdCheckManner
    {
        INCLUSIVE,
        EXCLUSIVE
    }

    public enum ThresholdCheckDirection
    {
        BIGGER,
        LOWER
    }

    public enum InputInterpolationMode
    {
        INTERPOLATED,
        NON_INTERPOLATED
    }

    public static class PlayerInputWrapperEffectsHelper
    {
        public static void OnMoveInputMagnitudeExceeding(
            Transform playerObjectTransform,
            PlayerMovementInput playerMovementInputWrapper,
            float magnitudeThreshold,
            ThresholdCheckDirection thresholdCheckDirection,
            ThresholdCheckManner thresholdCheckManner,
            InputInterpolationMode inputInterpolationMode,
            Action<Vector3> onInputAction
            )
        {
            Vector3 translation;

            if (inputInterpolationMode == InputInterpolationMode.INTERPOLATED)
            {
                translation = playerObjectTransform.TransformDirection(
                    playerMovementInputWrapper.GetTranslation());
            }
            else
            {
                translation = playerObjectTransform.TransformDirection(
                    playerMovementInputWrapper.GetNonInterpolatedTranslation());
            }

            if (thresholdCheckDirection == ThresholdCheckDirection.LOWER)
            {
                if (thresholdCheckManner == ThresholdCheckManner.EXCLUSIVE)
                {
                    if (translation.magnitude < magnitudeThreshold)
                    {
                        onInputAction.Invoke(translation);
                    }
                }
                else
                {
                    if (translation.magnitude <= magnitudeThreshold)
                    {
                        onInputAction.Invoke(translation);
                    }
                }
            }
            else
            {
                if (thresholdCheckManner == ThresholdCheckManner.EXCLUSIVE)
                {
                    if (translation.magnitude > magnitudeThreshold)
                    {
                        onInputAction.Invoke(translation);
                    }
                }
                else
                {
                    if (translation.magnitude >= magnitudeThreshold)
                    {
                        onInputAction.Invoke(translation);
                    }
                }
            }
        }

        public static void OnMoveInputMagnitudeBetween(
            Transform playerObjectTransform,
            PlayerMovementInput playerMovementInputWrapper,
            float magnitudeRangeA,
            float magnitudeRangeB,
            ThresholdCheckManner magnitudeRangeACheckManner,
            ThresholdCheckManner magnitudeRangeBCheckManner,
            InputInterpolationMode inputInterpolationMode,
            Action<Vector3> onInputAction
            )
        {
            Vector3 translation;

            if (inputInterpolationMode == InputInterpolationMode.INTERPOLATED)
            {
                translation = playerObjectTransform.TransformDirection(
                    playerMovementInputWrapper.GetTranslation());
            }
            else
            {
                translation = playerObjectTransform.TransformDirection(
                    playerMovementInputWrapper.GetNonInterpolatedTranslation());
            }

            bool rangeASatisfied =
                (magnitudeRangeACheckManner == ThresholdCheckManner.EXCLUSIVE &&
                    translation.magnitude > magnitudeRangeA)
                    ||
                (magnitudeRangeACheckManner == ThresholdCheckManner.INCLUSIVE &&
                    translation.magnitude >= magnitudeRangeA);
            bool rangeBSatisfied =
                (magnitudeRangeBCheckManner == ThresholdCheckManner.EXCLUSIVE &&
                    translation.magnitude < magnitudeRangeB)
                    ||
                (magnitudeRangeBCheckManner == ThresholdCheckManner.INCLUSIVE &&
                    translation.magnitude <= magnitudeRangeB); ;

            if (rangeASatisfied && rangeBSatisfied)
            {
                onInputAction.Invoke(translation);
            }
        }
    }

    public class PlayerInputWrapperEffects
    {
        protected PlayerMovementInput playerMovementInputWrapper;
        protected Transform playerObjectTransform;

        public PlayerInputWrapperEffects(
            PlayerMovementInput playerMovementInputWrapper,
            Transform playerObjectTransform)
        {
            this.playerMovementInputWrapper = playerMovementInputWrapper;
            this.playerObjectTransform = playerObjectTransform;
        }

        public void OnMoveInputMagnitudeLowerThan(
            float magnitudeThreshold, Action<Vector3> onInputAction)
        {
            PlayerInputWrapperEffectsHelper.OnMoveInputMagnitudeExceeding(
                this.playerObjectTransform,
                this.playerMovementInputWrapper,
                magnitudeThreshold,
                ThresholdCheckDirection.LOWER,
                ThresholdCheckManner.EXCLUSIVE,
                InputInterpolationMode.INTERPOLATED,
                onInputAction
                );
        }

        public void OnMoveInputMagnitudeBiggerOrEqualThan(
            float magnitudeThreshold, Action<Vector3> onInputAction)
        {
            PlayerInputWrapperEffectsHelper.OnMoveInputMagnitudeExceeding(
                this.playerObjectTransform,
                this.playerMovementInputWrapper,
                magnitudeThreshold,
                ThresholdCheckDirection.BIGGER,
                ThresholdCheckManner.INCLUSIVE,
                InputInterpolationMode.INTERPOLATED,
                onInputAction
                );
        }
    }
}
