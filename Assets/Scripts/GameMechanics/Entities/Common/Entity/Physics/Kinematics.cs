﻿using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Entity.Physics
{
    public class Kinematics
    {
        public Vector3 velocity { get; set; } = new Vector3();

        public void GameplayFixedUpdateSubject(
            Transform subject)
        {
            subject.position += this.velocity;
        }
    }
}
