﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Assets.Scripts.GameMechanics.Entities.Entity
{
    public class EffectInfo
    {
        public Action effectAction;
        protected bool calledAtLeastOnce;
        protected ITuple effectDependenciesLastValues;

        public EffectInfo(
            Action effectAction,
            ITuple effectDependenciesLastValues)
        {
            this.effectAction = effectAction;
            this.calledAtLeastOnce = false;
            this.effectDependenciesLastValues = effectDependenciesLastValues;
        }

        public void Invoke()
        {
            this.effectAction.Invoke();
            this.calledAtLeastOnce = true;
        }

        public bool CanBeCalled(ITuple effectDependenciesCurrentValues)
        {
            bool haveDependenciesChanged =

                (this.effectDependenciesLastValues == null
                    && effectDependenciesCurrentValues != null)

                    ||

                (this.effectDependenciesLastValues != null
                    && effectDependenciesCurrentValues == null)

                    ||

                (this.effectDependenciesLastValues != null
                    &&
                    !this.effectDependenciesLastValues.Equals(
                    effectDependenciesCurrentValues));

            if (haveDependenciesChanged)
            {
                this.effectDependenciesLastValues =
                    effectDependenciesCurrentValues;
            }

            return haveDependenciesChanged || !this.calledAtLeastOnce;
        }
    }

    public class EntityStateEffects
    {
        public Dictionary<string, object>
            stateFields = new Dictionary<string, object>();

        public void OnStateNotNull<T>(
            string stateKey, Action<T> stateValueAction)
        {
            if (this.stateFields.ContainsKey(stateKey))
            {
                T stateValue = (T)this.stateFields[stateKey];
                if (stateValue != null)
                {
                    stateValueAction(stateValue);
                }
            }
        }

        public void OnStateNotEqual<T>(
            string stateKey,
            T value,
            Action<T> stateValueAction)
        {
            if (this.stateFields.ContainsKey(stateKey))
            {
                T stateValue = (T)this.stateFields[stateKey];
                if (!stateValue.Equals(value))
                {
                    stateValueAction(stateValue);
                }
            }
        }

        public Tuple<T, Action<T>> UseState<T>(
            string identifier, T initialValue)
        {
            if (!this.stateFields.ContainsKey(identifier))
            {
                this.stateFields[identifier] = initialValue;
            }
            return Tuple.Create<T, Action<T>>(
                (T)this.stateFields[identifier],
                (value) => { this.stateFields[identifier] = value; });
        }
    }

    public class EntityRule<T> where T : EntityComponentsUpdateInput
    {
        private Dictionary<string, EffectInfo> effects =
            new Dictionary<string, EffectInfo>();

        protected EntityStateEffects stateEffects =
            new EntityStateEffects();

        public string ruleId { get; protected set; }

        public virtual void GameplayUpdate(float deltaTime, T updateInputArgs) { }
        public virtual void GameplayFixedUpdate(T updateInputArgs) { }

        public EntityRule(string ruleId)
        {
            this.ruleId = ruleId;
        }

        protected Tuple<T, Action<T>> UseState<T>(
            string identifier, T initialValue)
        {
            return this.stateEffects.UseState(identifier, initialValue);
        }

        protected void UseEffect(
            string identifier, Action effectAction,
            ITuple effectDependencies = null)
        {
            if (!this.effects.ContainsKey(identifier))
            {
                this.effects[identifier] =
                    new EffectInfo(effectAction, effectDependencies);
            }
            EffectInfo effectInfo = this.effects[identifier];
            if (effectInfo.CanBeCalled(effectDependencies))
            {
                effectInfo.Invoke();
            }
        }

        public string GetRuleClassName()
        {
            return GetType().Name;
        }
    }
}
