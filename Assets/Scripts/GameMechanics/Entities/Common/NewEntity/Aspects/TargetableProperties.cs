﻿using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewEntity.Aspects
{
    public class TargetableProperties : MonoBehaviour
    {
        [Range(1f, 100f)]
        public float maxTargetingDistance = 18f;

        public bool drawShootSight = true;
    }
}
