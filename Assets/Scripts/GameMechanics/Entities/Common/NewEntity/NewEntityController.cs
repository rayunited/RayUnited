using Assets.Scripts.Engine.Behaviours;

namespace Assets.Scripts.GameMechanics.Entities.NewEntity
{
    public class NewEntityController<T, U> : GameplayOnlyMonoBehaviour
        where T : NewEntityRule<U>
        where U : NewEntityContext, new()
    {
        protected T currentRule;
        public U controllerContext;

        public string GetCurrentRuleClassName()
        {
            return this.currentRule.GetRuleClassName();
        }

        public T GetCurrentRule()
        {
            return this.currentRule;
        }

        public void SetCurrentRule(T currentRule)
        {
            this.currentRule = currentRule;
        }

        protected override void BehaviourStart()
        {
            base.BehaviourStart();
        }

        protected override void GameplayUpdate(float deltaTime)
        {
            base.GameplayUpdate(deltaTime);
            this.currentRule.GameplayUpdate(
                deltaTime);
        }

        protected override void GameplayFixedUpdate()
        {
            base.GameplayFixedUpdate();
            this.currentRule.GameplayFixedUpdate();
        }
    }
}
