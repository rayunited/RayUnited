﻿using Assets.Scripts.Engine.Behaviours;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Destructible
{
    public abstract class DestructibleHitHandlingBeh : GameplayOnlyMonoBehaviour
    {
        public abstract void OnHit(GameObject hittingElligibleGameObject);
    }
}
