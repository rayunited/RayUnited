﻿using Assets.Scripts.Common;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics.Utils;
using Assets.Scripts.GameMechanics.Entities.Portal;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Portals
{
    public class PortalTraveller : MonoBehaviour
    {
        protected PortalRenderingBehaviour lastCollidedPortal;

        public bool handleCollisions = true;

        public GameObject graphicsObject;
        public GameObject graphicsClone { get; set; }
        public Vector3 previousOffsetFromPortal { get; set; }

        public Transform lastPortal { get; set; }

        public Material[] originalMaterials { get; set; }
        public Material[] cloneMaterials { get; set; }

        public virtual Vector3 GetGravityDirection()
        {
            return Vector3.down;
        }

        public virtual Vector3 GetGravityForward()
        {
            return Vector3.forward;
        }

        protected virtual void HandleEnteringPortalThreshold()
        {
           RaycastHit portalThresholdHit;

           if (CheckPortalCollision(out portalThresholdHit, GetGravityDirection(), GetGravityForward())) {
                if (portalThresholdHit.collider.gameObject.layer == Layers.portalsLayerIndex)
                {
                    var portalRenderingBehaviour = portalThresholdHit.collider.transform.parent.GetComponent<PortalRenderingBehaviour>();

                    if (this.lastCollidedPortal == null || !this.lastCollidedPortal.name.Equals(portalRenderingBehaviour.name))
                    {
                        portalRenderingBehaviour.OnThresholdCollisionEnter(this);
                    }                       

                    if (this.lastCollidedPortal != null && !this.lastCollidedPortal.name.Equals(portalRenderingBehaviour.name))
                    {
                        this.lastCollidedPortal.OnThresholdCollisionExit(this);
                    } 

                    this.lastCollidedPortal = portalRenderingBehaviour;
                    
                } else
                {

                }              
           } else
           {
                if (this.lastCollidedPortal != null &&
                    !this.lastCollidedPortal.ContainsTravellerWithinBoundaries(this))
                {
                    this.lastCollidedPortal.OnThresholdCollisionExit(this);
                    this.lastCollidedPortal = null;
                }
            }                      
        }

        protected virtual bool CheckPortalCollision(
            out RaycastHit portalThresholdHit, Vector3 gravityDirection, Vector3 gravityForward)
        {
            if (!this.handleCollisions)
            {
                portalThresholdHit = new RaycastHit();
                return false;
            }

            return Raycaster.CylinderRaycasterSingle(
               this.transform.position, gravityForward, gravityDirection,
               1f, 1f, 5, 100, out portalThresholdHit, layerMask: Layers.portalsLayerMask, debug: true, debugColor: Color.yellow);
        }

        private void FixedUpdate()
        {           
            HandleEnteringPortalThreshold();
        }

        public virtual void Teleport(
            Transform fromPortal,
            Transform toPortal,
            Vector3 position,
            Quaternion rotation,
            Vector3 gravityDirection,
            Vector3 gravityForward)
        {
            this.transform.position = position;
            this.transform.rotation = rotation;

            OnPortalTravel(fromPortal, toPortal, gravityDirection, gravityForward);
        }

        protected virtual void OnPortalTravel(Transform fromPortal, Transform toPortal, Vector3 gravityDirection, Vector3 gravityForward)
        {

        }

        // Called when first touches portal
        public virtual void EnterPortalThreshold()
        {
            //if (this.graphicsClone == null)
            //{
            //    this.graphicsClone = Instantiate(this.graphicsObject);
            //    this.graphicsClone.transform.parent = this.graphicsObject.transform.parent;
            //    this.graphicsClone.transform.localScale = this.graphicsObject.transform.localScale;
            //    this.originalMaterials = GetMaterials(this.graphicsObject);
            //    this.cloneMaterials = GetMaterials(this.graphicsClone);
            //}
            //else
            //{
            //    this.graphicsClone.SetActive(true);
            //}
        }

        // Called once no longer touching portal (excluding when teleporting)
        public virtual void ExitPortalThreshold()
        {
            //this.graphicsClone.SetActive(false);
            //// Disable slicing
            //for (int i = 0; i < this.originalMaterials.Length; i++)
            //{
            //    this.originalMaterials[i].SetVector("sliceNormal", Vector3.zero);
            //}
        }

        public void SetSliceOffsetDst(float dst, bool clone)
        {
            for (int i = 0; i < this.originalMaterials.Length; i++)
            {
                if (clone)
                {
                    this.cloneMaterials[i].SetFloat("sliceOffsetDst", dst);
                }
                else
                {
                    this.originalMaterials[i].SetFloat("sliceOffsetDst", dst);
                }

            }
        }

        Material[] GetMaterials(GameObject g)
        {
            var renderers = g.GetComponentsInChildren<MeshRenderer>();
            var matList = new List<Material>();
            foreach (var renderer in renderers)
            {
                foreach (var mat in renderer.materials)
                {
                    matList.Add(mat);
                }
            }
            return matList.ToArray();
        }
    }
}
