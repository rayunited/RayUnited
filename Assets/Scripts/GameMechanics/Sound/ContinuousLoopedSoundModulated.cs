﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Sound
{
    [Serializable]
    public class ContinuousLoopedSoundModulated
    {
        public SoundVariant[] variants;
        protected bool isPlaying = false;
        protected const float EPSILON = 0.01f;

        protected float GetAlteredPitch(int variantIndex)
        {
            return UnityEngine.Random.Range(
                this.variants[variantIndex].minPitch,
                this.variants[variantIndex].maxPitch);
        }

        private IEnumerator ScheduledLoopPlayingIterations(
            MonoBehaviour coroutineHost, AudioSource audioSource, bool alterPitchEachTime)
        {
            if (this.isPlaying)
            {
                var variantToPlayInfo = SoundVariantsHelper.GetVariantToPlay(this.variants);
                if (variantToPlayInfo != null)
                {
                    float pitch = GetAlteredPitch(variantToPlayInfo.Item4);
                    audioSource.loop = false;
                    audioSource.clip = variantToPlayInfo.Item1;
                    audioSource.volume = variantToPlayInfo.Item2;
                    audioSource.maxDistance = variantToPlayInfo.Item3;
                    if (alterPitchEachTime)
                    {
                        audioSource.pitch = pitch;
                    }                    
                    audioSource.Play();
                    while (audioSource.isPlaying)
                    {
                        yield return new WaitForSeconds(Time.fixedDeltaTime);
                    }
                    if (this.isPlaying && !audioSource.isPlaying)
                    {
                        PlayLoopedWithChangingModulationEachTimeWithAudioSource(coroutineHost, audioSource);
                    }
                }                   
            }
        }

        public void AlterAudioSourcePitch(AudioSource audioSource, float pitch)
        {
            audioSource.pitch = pitch;
        }

        public void PlayLoopedWithoutChangingModulationWithAudioSource(
            MonoBehaviour coroutineHost, AudioSource audioSource)
        {
            this.isPlaying = true;
            audioSource.Stop();
            coroutineHost.StartCoroutine(
                ScheduledLoopPlayingIterations(coroutineHost, audioSource, alterPitchEachTime: false));
        }

        public void PlayLoopedWithChangingModulationEachTimeWithAudioSource(
            MonoBehaviour coroutineHost, AudioSource audioSource)
        {
            this.isPlaying = true;
            audioSource.Stop();
            coroutineHost.StartCoroutine(
                ScheduledLoopPlayingIterations(coroutineHost, audioSource, alterPitchEachTime: true));
        }

        public void StopPlayingWithAudioSource(AudioSource audioSource)
        {
            this.isPlaying = false;
            audioSource.Stop();
        }
    }
}
