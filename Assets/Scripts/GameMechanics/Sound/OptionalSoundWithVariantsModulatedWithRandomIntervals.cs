﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Sound
{
    [Serializable]
    public class OptionalSoundWithVariantsModulatedWithRandomIntervals
    {
        public SoundVariant[] variants;

        [Range(0.2f, 15f)]
        public float minimumBreakIntervalSeconds = 2f;

        [Range(0.2f, 15f)]
        public float maximumBreakIntervalSeconds = 10f;

        [Range(0.1f, 1f)]
        public float probabilityOfBeingPlayedAtAllEachTime = 1f;

        protected bool isPlaying = false;
        protected const float EPSILON = 0.01f;

        protected float GetAlteredPitch(int variantIndex)
        {
            return UnityEngine.Random.Range(
                this.variants[variantIndex].minPitch,
                this.variants[variantIndex].maxPitch);
        }

        protected void OnSuccessPlay(Action onSuccessPlayAction)
        {
            float chosenRandom = UnityEngine.Random.Range(0f, 1f);
            if (chosenRandom < this.probabilityOfBeingPlayedAtAllEachTime)
            {
                onSuccessPlayAction.Invoke();
            }
        }

        private IEnumerator ScheduledRepeatedRandomPlaying(
            MonoBehaviour coroutineHost, AudioSource audioSource)
        {
            if (this.isPlaying)
            {
                yield return new WaitForSeconds(
                    UnityEngine.Random.Range(
                        this.minimumBreakIntervalSeconds,
                        this.maximumBreakIntervalSeconds));
                if (this.isPlaying)
                {
                    float chosenRandom = UnityEngine.Random.Range(0f, 1f);
                    if (chosenRandom < this.probabilityOfBeingPlayedAtAllEachTime)
                    {
                        var variantToPlayInfo = SoundVariantsHelper.GetVariantToPlay(this.variants);
                        if (variantToPlayInfo != null)
                        {
                            float pitch = GetAlteredPitch(variantToPlayInfo.Item4);
                            audioSource.loop = false;
                            audioSource.clip = variantToPlayInfo.Item1;
                            audioSource.volume = variantToPlayInfo.Item2;
                            audioSource.maxDistance = variantToPlayInfo.Item3;
                            audioSource.pitch = pitch;
                            audioSource.Play();
                            yield return new WaitForSeconds(
                                (audioSource.clip.length * (1f / audioSource.pitch)) + EPSILON);                           
                        }
                    }
                    if (this.isPlaying && !audioSource.isPlaying)
                    {
                        PlayRandomlyRepeatedWithChangingModulationEachTimeWithAudioSource(coroutineHost, audioSource);
                    }
                }
            }
        }

        public void PlayRandomlyRepeatedWithChangingModulationEachTimeWithAudioSource(
            MonoBehaviour coroutineHost, AudioSource audioSource)
        {
            this.isPlaying = true;
            audioSource.Stop();
            coroutineHost.StartCoroutine(
               ScheduledRepeatedRandomPlaying(coroutineHost, audioSource));
        }

        public void StopPlayingWithAudioSource(
            AudioSource audioSource)
        {
            this.isPlaying = false;
            audioSource.Stop();
        }
    }
}
