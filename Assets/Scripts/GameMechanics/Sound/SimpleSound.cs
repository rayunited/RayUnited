﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Sound
{
    [Serializable]
    public class SimpleSound
    {
        public AudioClip clip;

        [Range(0.0f, 1.0f)]
        public float volume = 0.5f;

        [Range(0.0f, 100f)]
        public float maxDistance = 10f;

        [Range(0.25f, 5f)]
        public float minPitch = 0.75f;

        [Range(0.25f, 5f)]
        public float maxPitch = 1.5f;

        public void PlayOnceWithAudioSource(AudioSource audioSource)
        {
            audioSource.Stop();
            if (this.clip != null)
            {           
                audioSource.loop = false;
                audioSource.volume = this.volume;
                audioSource.maxDistance = this.maxDistance;
                audioSource.pitch = UnityEngine.Random.Range(this.minPitch, this.maxPitch);
                audioSource.clip = this.clip;
                audioSource.Play();
            }
        }

        public void Stop(AudioSource audioSource)
        {
            audioSource.Stop();
        }
    }
}
