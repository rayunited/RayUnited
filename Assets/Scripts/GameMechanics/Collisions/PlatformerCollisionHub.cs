﻿using Assets.Scripts.GameMechanics.Entities.Entity.Physics;
using Assets.Scripts.PlayerMechanics.Collisions;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Collisions
{
    public class PlatformerCollisionHub
    {
        public PlatformerCollisionState previousCollisionState
        { get; protected set; } = new PlatformerCollisionState();

        public PlatformerCollisionState previousVelocityCollisionState
        { get; protected set; } = new PlatformerCollisionState();

        public PlatformerCollisionState currentCollisionState
        { get; protected set; } = new PlatformerCollisionState();

        public PlatformerCollisionState currentVelocityCollisionState
        { get; protected set; } = new PlatformerCollisionState();

        protected RaycastHit? GetSteadyGroundHit(PlatformerCollisionState collisionState)
        {
            if (!collisionState.isHittingGroundGeneral)
            {
                return null;
            }

            if (collisionState.isHittingFlatGround)
            {
                return collisionState.flatGroundHit;
            }
            else if (collisionState.isHittingSlope)
            {
                return collisionState.slopeHit;
            }
            else
            {
                return null;
            }
        }

        public Vector3? steadyEnvironmentVelocityGroundCollisionNormal =>
            GetSteadyGroundHit(this.currentVelocityCollisionState)?.normal;
        public Vector3? steadyEnvironmentVelocityGroundCollisionPoint =>
            GetSteadyGroundHit(this.currentVelocityCollisionState)?.point;

        public void GameplayFixedUpdate(
            Transform subject, Kinematics kinematics,
            Vector3 gravityDirection,
            Vector3? originPositionOffsetArg = null)
        {
            this.previousCollisionState
                .ApplyStateValues(this.currentCollisionState);
            this.previousVelocityCollisionState
                .ApplyStateValues(this.currentVelocityCollisionState);

            Vector3 originPositionOffset = Vector3.zero;
            if (originPositionOffsetArg != null)
            {
                originPositionOffset = (Vector3)originPositionOffsetArg;
            }

            const float maxGroundRaycastDistance = 1.2f;
            const float maxGroundSlopeRaycastDistance = 1.5f;

            this.currentCollisionState.isHittingFlatGround =
                RayColliderRules.IsHittingFlatGround(
                    subject, gravityDirection, out this.currentCollisionState.flatGroundHit,
                        null,
                        maxGroundRaycastDistance);
            this.currentCollisionState.isHittingSlope =
                RayColliderRules.IsHittingSlope(
                    subject, gravityDirection, out this.currentCollisionState.slopeHit,
                        null,
                        maxGroundSlopeRaycastDistance);
            this.currentVelocityCollisionState.isHittingFlatGround =
                RayColliderRules.IsHittingFlatGround(
                    subject, gravityDirection, out this.currentVelocityCollisionState.flatGroundHit,
                        kinematics.velocity,
                        maxGroundRaycastDistance);
            this.currentCollisionState.isHittingSlope =
                RayColliderRules.IsHittingSlope(
                    subject, gravityDirection, out this.currentVelocityCollisionState.slopeHit,
                        kinematics.velocity,
                        maxGroundSlopeRaycastDistance);
        }
    }
}
