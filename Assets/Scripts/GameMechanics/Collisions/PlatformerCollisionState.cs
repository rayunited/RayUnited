﻿using UnityEngine;

namespace Assets.Scripts.GameMechanics.Collisions
{
    public class PlatformerCollisionState
    {
        public bool isHittingGroundGeneral => this.isHittingFlatGround || this.isHittingSlope;
        public bool isHittingFlatGround { get; set; }
        public bool isHittingSlope { get; set; }

        public RaycastHit flatGroundHit;
        public RaycastHit slopeHit;

        public void ApplyStateValues(
            PlatformerCollisionState collisionState)
        {
            this.isHittingFlatGround = collisionState.isHittingFlatGround;
            this.isHittingSlope = collisionState.isHittingSlope;
            this.flatGroundHit = collisionState.flatGroundHit;
            this.slopeHit = collisionState.slopeHit;
        }
    }
}
