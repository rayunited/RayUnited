using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using Assets.Scripts.ScenePreparation.LedgeGrabbing;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Assets.Scripts.Common;

namespace Assets.Scripts.GameMechanics.DynamicLedgeGenerator
{
    public class LedgeGenerator
    {
        public static GameObject CreateMeshLedgeGrabCollider(Transform transform, Mesh mesh, GameObject ledgeGameObject)
        {
            Vector3[] vertices = mesh.vertices;
            Vector3[] verticesInWorldTransform =
                vertices.Select(x => transform.TransformPoint(x)).ToArray();

            Vector3[] rotatedNormals = mesh.normals.Select(x => transform.TransformDirection(x)).ToArray();

            Debug.Log("TRIANGLES: " + mesh.triangles.Length);

            List<Triangle> triangles =
                MeshHelper.GetFacesInWorldSpaceWithNormals(mesh.triangles,
                    verticesInWorldTransform, rotatedNormals);

            List<Triangle> appropriateFacesForLedgeColliders =
                LedgeGeneratorHelper.GetFacesWithRightAngleNormalToHorizontal(triangles);

            List<EdgeWithLedgeColliderNormalInfo> edgesWithLedgeGrabNormals =
                LedgeGeneratorHelper.GetEdgesWithLedgeGrabNormals(appropriateFacesForLedgeColliders);

            List<EdgeWithLedgeColliderNormalInfo> edgesEligibleForLedgeColliders =
                LedgeGeneratorHelper.GetUniqueEdges(edgesWithLedgeGrabNormals);

            var parent = new GameObject();
            parent.name = transform.name;
            parent.transform.parent = ledgeGameObject.transform;

            foreach (EdgeWithLedgeColliderNormalInfo edge in edgesEligibleForLedgeColliders)
            {
                CreateGrabLedgeCollider(
                    edge.pointA, edge.pointB, edge.ledgeGrabNormalWorldTransform,
                    edge.ledgeGrabUpNormalWorldTransform,
                    parent);
            }

            return parent;
        }

        public static void CreateGrabLedgeCollider(
            Vector3 pointA, Vector3 pointB, Vector3 ledgeGrabNormalWorldTransform, Vector3 ledgeGrabUpNormalWorldTransform,
            GameObject parent)
        {
            GameObject ledgeColliderGameObject =
                InstantiateStretchedBoxBetweenPoints(parent, pointA, pointB, depth: 0.05f, height: 0.5f);
            LedgeColliderInfo info = ledgeColliderGameObject.AddComponent<LedgeColliderInfo>();
            info.normal = ledgeGrabNormalWorldTransform;
            info.upNormal = ledgeGrabUpNormalWorldTransform;
            info.edgePointALocal = pointA;
            info.edgePointBLocal = pointB;
            Object.Destroy(ledgeColliderGameObject.GetComponent<MeshRenderer>());
            ledgeColliderGameObject.layer = Layers.ledgeCollidersLayerIndex;
            ledgeColliderGameObject.tag = Tags.ledgeColliderTag;
        }

        public static GameObject InstantiateStretchedBoxBetweenPoints(
            GameObject parent,
            Vector3 pointA,
            Vector3 pointB,
            float depth = 1.0f,
            float height = 1.0f)
        {
            
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.name = $"{pointA} - {pointB}";
            cube.transform.parent = parent.transform;

            Vector3 between = pointB - pointA;
            float distance = between.magnitude;
            cube.transform.localScale = new Vector3(distance, height, depth);
            cube.transform.position = pointA + (between / 2.0f);
            cube.transform.LookAt(pointB);

            cube.transform.Rotate(0.0f, 90f, 0.0f, Space.Self);
            return cube;
        }
    }

    public static class LedgeGeneratorHelper
    {
        private static float angleThresholdForProperFaces = 60f;

        public static List<Triangle> GetFacesWithRightAngleNormalToHorizontal
            (List<Triangle> triangles)
        {
            return triangles.Where(x =>
                Vector3.Angle(x.faceNormalWorldSpace, Vector3.up) < angleThresholdForProperFaces).ToList();
        }

        public static List<EdgeWithLedgeColliderNormalInfo>
            GetEdgesWithLedgeGrabNormals(List<Triangle> appropriateFacesForLedgeColliders)
        {
            return appropriateFacesForLedgeColliders.SelectMany(
                x => x.GetEdgesWithLedgeColliderNormalInfo()).ToList();
        }

        public static List<EdgeWithLedgeColliderNormalInfo>
            GetUniqueEdges(List<EdgeWithLedgeColliderNormalInfo> edgesWithLedgeGrabNormals)
        {
            return edgesWithLedgeGrabNormals
                .Where(x => !EdgesEliminationHelper
                    .HasEquivalentEdge(x, edgesWithLedgeGrabNormals)).ToList();
        }
    }
}
