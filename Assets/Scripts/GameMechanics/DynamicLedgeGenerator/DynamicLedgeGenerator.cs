using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.DynamicLedgeGenerator
{
    public class DynamicLedge
    {
        public Transform subject;
        private LedgeCollector collector;
        private Dictionary<int, GameObject> ledgeObjects = new();
        private GameObject ledgeParentGameObject;

        public DynamicLedge(Transform subject)
        {
            this.subject = subject;
            this.collector = Object.FindObjectOfType<LedgeCollector>();

            this.ledgeParentGameObject = new("Ledge Grab Parent GameObject");
        }

        public void DynamicGenerate()
        {
            DeleteMissing();
            CalculateDifference();
        }

        private Dictionary<int, MeshCollider> CollectObjects()
        {
            return this.collector.ledgeObjectsToGenerate;
        }

        private void DeleteMissing()
        {
            var ledgesToDelete = new List<int>();

            foreach (var item in this.ledgeObjects.Keys)
            {
                if (!CollectObjects().ContainsKey(item))
                {
                    ledgesToDelete.Add(item);
                }
            }

            foreach (var item in ledgesToDelete)
            {
                Object.Destroy(this.ledgeObjects[item]);
                this.ledgeObjects.Remove(item);
            }
        }

        private void CalculateDifference()
        {
            foreach (var item in CollectObjects())
            {
                if (!this.ledgeObjects.ContainsKey(item.Key))
                {
                    this.ledgeObjects.Add(item.Key,
                    LedgeGenerator.CreateMeshLedgeGrabCollider(
                        item.Value.transform,
                        item.Value.sharedMesh,
                        this.ledgeParentGameObject));
                }
            }
        }
    }
}