using Assets.Scripts.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.DynamicLedgeGenerator
{
    public class LedgeCollector : MonoBehaviour
    {
        public Dictionary<int, MeshCollider> ledgeObjectsToGenerate = new();

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == Layers.ledgeCollectorLayerIndex)
            {
                if (!this.ledgeObjectsToGenerate.ContainsKey(other.gameObject.GetInstanceID()))
                    this.ledgeObjectsToGenerate.Add(other.gameObject.GetInstanceID(), other.GetComponent<MeshCollider>());
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.layer == Layers.ledgeCollectorLayerIndex)
            {
                if (this.ledgeObjectsToGenerate.ContainsKey(other.gameObject.GetInstanceID()))
                    this.ledgeObjectsToGenerate.Remove(other.gameObject.GetInstanceID());
            }
        }
    }
}