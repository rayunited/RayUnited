﻿using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.ScenePreparation.LedgeGrabbing
{
    public static class EdgesEliminationHelper
    {
        public static bool
            HasEquivalentEdge(
            EdgeWithLedgeColliderNormalInfo edgeToConsider,
            List<EdgeWithLedgeColliderNormalInfo> edgesWithLedgeGrabNormals)
        {
            return edgesWithLedgeGrabNormals.Where(x =>
            {
            return
                (x.pointA == edgeToConsider.pointA &&
                x.pointB == edgeToConsider.pointB)
                    ||
                    (x.pointB == edgeToConsider.pointA &&
                    x.pointA == edgeToConsider.pointB);
            }).Count() > 1;
        }
    }

    public static class LedgeColliderEdgesHelper
    {
       private static float angleThresholdForProperFaces = 60f;

        public static List<Triangle> GetFacesWithRightAngleNormalToHorizontal
            (GameObject ledgeGrabbableObj, List<Triangle> triangles)
        {
            return triangles.Where(x =>
                Vector3.Angle(x.faceNormalWorldSpace, Vector3.up) < angleThresholdForProperFaces).ToList()
                .Concat(
                triangles.Where(x =>
                    Vector3.Angle(x.faceNormalWorldSpace, -Vector3.up) < angleThresholdForProperFaces).ToList()).ToList();
        }

        public static List<EdgeWithLedgeColliderNormalInfo>
            GetEdgesWithLedgeGrabNormals(GameObject ledgeGrabbableObj, List<Triangle> appropriateFacesForLedgeColliders)
        {
            return appropriateFacesForLedgeColliders.SelectMany(
                x => x.GetEdgesWithLedgeColliderNormalInfo()).ToList();
        }

        public static List<EdgeWithLedgeColliderNormalInfo>
            GetUniqueEdges(List<EdgeWithLedgeColliderNormalInfo> edgesWithLedgeGrabNormals)
        {
            return edgesWithLedgeGrabNormals
                .Where(x => !EdgesEliminationHelper
                    .HasEquivalentEdge(x, edgesWithLedgeGrabNormals)).ToList();
        }
    }
}
