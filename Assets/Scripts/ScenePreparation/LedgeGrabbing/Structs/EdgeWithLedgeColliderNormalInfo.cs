﻿using UnityEngine;

namespace Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs
{
    public struct EdgeWithLedgeColliderNormalInfo
    {
        public Vector3 pointA;
        public Vector3 pointB;
        public Vector3 ledgeGrabNormalWorldTransform;
        public Vector3 ledgeGrabUpNormalWorldTransform;

        public Vector3 normalAWorldSpace;
        public Vector3 normalBWorldSpace;

        public int vertexAIndex;
        public int vertexBIndex;

        public EdgeWithLedgeColliderNormalInfo(
            Vector3 pointA,
            Vector3 pointB,
            Vector3 ledgeGrabNormalWorldTransform,
            Vector3 normalAWorldSpace,
            Vector3 normalBWorldSpace,
            int vertexAIndex,
            int vertexBIndex
            )
        {
            this.pointA = pointA;
            this.pointB = pointB;
            this.ledgeGrabNormalWorldTransform = ledgeGrabNormalWorldTransform;
            this.ledgeGrabUpNormalWorldTransform = -Vector3.Cross((pointB - pointA).normalized, ledgeGrabNormalWorldTransform).normalized;

            this.normalAWorldSpace = normalAWorldSpace;
            this.normalBWorldSpace = normalBWorldSpace;

            this.vertexAIndex = vertexAIndex;
            this.vertexBIndex = vertexBIndex;
        }
    }
}
