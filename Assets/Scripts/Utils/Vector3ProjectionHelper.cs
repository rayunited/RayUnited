﻿using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class Vector3ProjectionHelper
    {
        public static Vector3 ProjectConsideringDirection(Vector3 vector, Vector3 onNormal)
        {
            bool areInTheSameDirection = Vector3.Dot(vector, onNormal) >= 0;
            return Vector3.Project(vector, areInTheSameDirection ? onNormal : -onNormal); 
        }
    }
}
