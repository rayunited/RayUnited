﻿using Assets.Scripts.Common;
using System;
using UnityEngine;

namespace Assets.Scripts.Utils.Movement
{
    public static class RandomizedFairyLikeMovementUtils
    {
        public static Vector3 EvaluateVelocityForFairyLikeMovement(
            Vector3 currentVelocity,
            Vector3 translationToCurrentDestination,
            float movementDirectionRotationAngularSpeed,
            float maxVelocityValue
            )
        {
            Vector3 result = Vector3.RotateTowards(
                    currentVelocity, translationToCurrentDestination,
                    movementDirectionRotationAngularSpeed,
                    maxVelocityValue / 5f);
            result = Vector3.ClampMagnitude(result,
                maxVelocityValue);
            return result;
        }
    }
}
