﻿using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class PhysicsRaycaster
    {
        public static bool Raycast(Vector3 position, Vector3 direction, out RaycastHit hit, float maxDistance,
            int layerMask = Physics.DefaultRaycastLayers,
            Color? debugColor = null, bool debug = false)
        {
            if (debugColor == null)
            {
                debugColor = Color.white;
            }

            if (debug)
            {
                Debug.DrawRay(position, direction.normalized * maxDistance, (Color)debugColor);
            }
            return Physics.Raycast(position, direction, out hit, maxDistance, layerMask);
        }

        public static bool Raycast(Vector3 position, Vector3 direction, float maxDistance,
            int layerMask = Physics.DefaultRaycastLayers, Color? debugColor = null, bool debug = false)
        {
            if (debugColor == null)
            {
                debugColor = Color.white;
            }

            if (debug)
            {
                Debug.DrawRay(position, direction.normalized * maxDistance, (Color)debugColor);
            }
            return Physics.Raycast(position, direction, maxDistance, layerMask);
        }
    }
}
