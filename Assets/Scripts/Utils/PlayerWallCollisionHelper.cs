﻿using Assets.Scripts.Engine.Debug;
using Assets.Scripts.PlayerMechanics;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class PlayerWallCollisionHelper
    {
        public static Vector3 GetMovementVelocityConsideringWallCollisions(
            GameObject gameObject, RayCollider rayCollider, Vector3 entityForwardDirectionVector,
            Vector3 velocity, Vector3 gravityDirection, Vector3 gravityForward, float? wallHitCheckingRadius = null)
        {
            return WallCollisionHelper.GetMovementVelocityConsideringWallCollisions(
                gameObject, rayCollider, entityForwardDirectionVector,
                velocity, gravityDirection, gravityForward, wallHitCheckingRadius, -gravityDirection,
                PlayerWallCollisionDebugFlags.debugInitialWallHitCheck,
                PlayerWallCollisionDebugFlags.debugInitialWallHitCheckColor,
                PlayerWallCollisionDebugFlags.debugAveragingWallCollisionNormal,
                PlayerWallCollisionDebugFlags.debugAveragingWallCollisionNormalColor,
                PlayerWallCollisionDebugFlags.debugHittingWallInGivenDirectionAtCorner,
                PlayerWallCollisionDebugFlags.debugHittingWallInGivenDirectionAtCornerColor,
                PlayerWallCollisionDebugFlags.debugResultVelocity,
                PlayerWallCollisionDebugFlags.debugVelocityLengthMultiplier,
                PlayerWallCollisionDebugFlags.debugResultVelocityColor,
                PlayerWallCollisionDebugFlags.debugResultWallNormal,
                PlayerWallCollisionDebugFlags.debugResultWallNormalColor,
                PlayerWallCollisionDebugFlags.debugEntityForwardDirection,
                PlayerWallCollisionDebugFlags.debugEntityForwardDirectionLengthMultiplier,
                PlayerWallCollisionDebugFlags.debugEntityForwardDirectionColor
                );
        }
    }
}
