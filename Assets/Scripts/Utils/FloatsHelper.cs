﻿using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class FloatsHelper
    {
        public static bool NearlyEqual(float a, float b, float epsilon = 0.0001f)
        {
            return Mathf.Abs(a - b) < epsilon;
        }
    }
}
