﻿using UnityEngine;

namespace Assets.Scripts.Utils.Audio
{
    public class AudioClipWithAudioSourceParams
    {
        public AudioClip clip;
        public float volume;
        public float maxDistance;
        public float pitch;
        public int variantIndex;
    }
}
