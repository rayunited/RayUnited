﻿using UnityEngine;

namespace Assets.Scripts.Utils.Assets
{
    public static class FontsFactory
    {
        public static Font GetMenuFont()
        {
            return (Font)Resources.Load("Fonts/menu_hud_font");
        }
    }
}
