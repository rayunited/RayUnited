﻿using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class DebugHelper
    {
        public static void DrawRay(Vector3 start, Vector3 dir, Color? color = null, bool draw = false)
        {
            if (draw)
            {
                Debug.DrawRay(start, dir, color != null ? (Color)color : Color.white);
            }
        }
    }
}
