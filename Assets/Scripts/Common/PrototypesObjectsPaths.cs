﻿namespace Assets.Scripts.Common
{
    public static class PrototypesObjectsPaths
    {
        public const string goldJewelPrototypePath =
            platformerEngineName + "/" + platformerEngineCollectiblesName + "/" + goldJewelName;

        public const string goldJewelName = "Goldjewel";
        public const string platformerEngineName = "PlatformerEngine";
        public const string platformerEngineCollectiblesName = "Collectibles";
    }
}
