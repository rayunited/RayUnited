﻿using UnityEngine;

namespace Assets.Scripts.Common
{
    public static class Layers
    {
        public const int defaultLayerMask = Physics.DefaultRaycastLayers;

        public static string wallClimbablesLayer = "WallClimbablesLayer";
        public static int wallClimbablesLayerMask = 1 << 6;
        public static int wallClimbablesLayerIndex = 6;

        public static string environmentLayer = "WallClimbablesLayer";
        public static int environmentLayerMask = 1 << 7;
        public static int environmentLayerIndex = 7;

        public static string ledgeCollidersLayer = "LedgeCollidersLayer";
        public static int ledgeCollidersLayerMask = 1 << 8;
        public static int ledgeCollidersLayerIndex = 8;

        public static string collectiblesLayer = "CollectiblesLayer";
        public static int collectiblesLayerMask = 1 << 9;
        public static int collectiblesLayerIndex = 9;

        public static string terrainLayer = "TerrainLayer";
        public static int terrainLayerMask = 1 << 10;
        public static int terrainLayerIndex = 10;

        public static string ledgeCollectorLayer = "LedgeCollectorLayer";
        public static int ledgeCollectorLayerMask = 1 << 11;
        public static int ledgeCollectorLayerIndex = 11;

        public static string waterLayer = "WaterLayer";
        public static int waterLayerMask = 1 << 12;
        public static int waterLayerIndex = 12;

        public static string destructibleEnvironmentLayer = "DestructibleEnvironmentLayer";
        public static int destructibleEnvironmentLayerMask = 1 << 14;
        public static int destructibleEnvironmentLayerIndex = 14;

        public static string movableEnvironmentLayer = "MovableEnvironmentLayer";
        public static int movableEnvironmentLayerMask = 1 << 17;
        public static int movableEnvironmentLayerIndex = 17;

        public static string portalsLayer = "Portals";
        public static int portalsLayerMask = 1 << 18;
        public static int portalsLayerIndex = 18;

        public static string levelEditorLayer = "LevelEditor";
        public static int levelEditorLayerMask = 1 << 19;
        public static int levelEditorLayerIndex = 19;

        public static int generalEnvironmentLayersMask = 
            (1 << environmentLayerIndex) 
            | (1 << destructibleEnvironmentLayerIndex)
            | (1 << movableEnvironmentLayerIndex);

        public static int groundLayerMask = (1 << 10) | (1 << 7);
        public static int collisionObjectLayerMask = (1 << 7) | (1 << 11);

        public static int boostersLayerMask = (1 << 20);
    }
}
