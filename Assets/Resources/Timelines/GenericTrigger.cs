using UnityEngine;
using UnityEngine.Playables;

public class GenericTrigger : MonoBehaviour
{
    public PlayableDirector timeline;

    // Use this for initialization
    private void Start()
    {
        this.timeline = GetComponent<PlayableDirector>();
    }

    private void OnTriggerExit(Collider c)
    {
        if (c.gameObject.tag == "Player")
        {
            this.timeline.Stop();
        }
    }

    private void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "Player")
        {
            this.timeline.Play();
        }
    }
}