import { render } from '@reactunity/renderer';
import styled from 'styled-components';

import "./index.scss"
import "./menu_hud_font.ttf"
import { Logs } from './components/Logs';

import { Monitors } from './components/Monitors';

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  justify-content: center;
  font-family: "menu_hud_font";
  position: relative;
`

function App() {
  return (
    <Wrapper>
      <Logs />
      <Monitors />
    </Wrapper>
  )
}

render(<App />);
