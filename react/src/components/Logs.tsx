import { useGlobals } from "@reactunity/renderer"
import { useEffect, useState } from "react"
import styled from "styled-components"

const LogsContainer = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  width: 30%;
  height: 30%;  
  color: white;
  background-color: rgba(0,0,0,0.9);
  overflow-y: scroll;
`

export const LogEntry = ({ message }) => {
    return (
        <div>{message}</div>
    )
}

export const Logs = () => {
    const { logs } = useGlobals()

    const [logsEntries, setLogsEntries] = useState([])

    useEffect(() => {
        const pollingInterval = setInterval(() => {
            const polled = logs.PollLogs()
            setLogsEntries((prev) => [...prev, ...polled].slice(-20))
        }, 1000)
        return () => {
            clearInterval(pollingInterval)
        }
    }, [])

    return (
        <LogsContainer>
            <scroll>
                {
                    logsEntries.map(x => (
                        <LogEntry key={x.id} message={x.message} />
                    ))
                }
            </scroll>
        </LogsContainer>
    )
}