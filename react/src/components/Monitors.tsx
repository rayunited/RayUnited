import styled from "styled-components"
import { TCPClientHolderMonitor } from "./monitors/TCPClientHolderMonitor"
import { TCPMessagesReceiverMonitor } from "./monitors/TCPMessagesReceiverMonitor"
import { TCPMessagesSenderMonitor } from "./monitors/TCPMessagesSenderMonitor"
import { TCPReceivedMessagesQueueMonitor } from "./monitors/TCPReceivedMessagesQueueMonitor"
import { TCPSendMessagesQueueMonitor } from "./monitors/TCPSendMessagesQueueMonitor"
import { UDPClientHolderMonitor } from "./monitors/UDPClientHolderMonitor"
import { UDPMessagesReceiverMonitor } from "./monitors/UDPMessagesReceiverMonitor"
import { UDPMessagesSenderMonitor } from "./monitors/UDPMessagesSenderMonitor"
import { UDPReceivedMessagesQueueMonitor } from "./monitors/UDPReceivedMessagesQueueMonitor"
import { UDPSendMessagesQueueMonitor } from "./monitors/UDPSendMessagesQueueMonitor"

const Container = styled.div`
  position: absolute;  
  top: 0;
  right: 0;
`

export const Monitors = () => {
    return (
        <Container>
            <TCPClientHolderMonitor />
            <TCPMessagesReceiverMonitor />
            <TCPMessagesSenderMonitor />
            <TCPReceivedMessagesQueueMonitor />
            <TCPSendMessagesQueueMonitor />
            <UDPClientHolderMonitor />
            <UDPMessagesReceiverMonitor />
            <UDPMessagesSenderMonitor />
            <UDPReceivedMessagesQueueMonitor />
            <UDPSendMessagesQueueMonitor />
        </Container>
    )
}