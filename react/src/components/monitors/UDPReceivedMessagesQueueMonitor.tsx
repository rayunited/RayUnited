import { useGlobals } from "@reactunity/renderer"
import { useEffect, useState } from "react"
import styled from "styled-components"

const Container = styled.div`
    color: white;
    background-color: rgba(0,0,0,0.9);
`

export const UDPReceivedMessagesQueueMonitor = () => {
    const { udpReceivedMessagesQueueMonitor } = useGlobals()

    const [queueMessages, setQueueMessages] = useState(0)

    useEffect(() => {
        const pollingInterval = setInterval(() => {
            setQueueMessages(udpReceivedMessagesQueueMonitor.queueMessages)
        }, 100)

        return () => {
            clearInterval(pollingInterval)
        }
    }, [])

    return (
        <Container>
            <div>UDPReceivedMessagesQueueMonitor</div>
            <div>Messages: {queueMessages}</div>
        </Container>
    )
}