import { useGlobals } from "@reactunity/renderer"
import { useEffect, useState } from "react"
import styled from "styled-components"

const Container = styled.div`
    color: white;
    background-color: rgba(0,0,0,0.9);
`

export const UDPSendMessagesQueueMonitor = () => {
    const { udpSendMessagesQueueMonitor } = useGlobals()

    const [queueMessages, setQueueMessages] = useState(0)

    useEffect(() => {
        const pollingInterval = setInterval(() => {
            setQueueMessages(udpSendMessagesQueueMonitor.queueMessages)
        }, 100)

        return () => {
            clearInterval(pollingInterval)
        }
    }, [])

    return (
        <Container>
            <div>UDPSendMessagesQueueMonitor</div>
            <div>Messages: {queueMessages}</div>
        </Container>
    )
}