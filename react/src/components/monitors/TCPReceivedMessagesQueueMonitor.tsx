import { useGlobals } from "@reactunity/renderer"
import { useEffect, useState } from "react"
import styled from "styled-components"

const Container = styled.div`
    color: white;
    background-color: rgba(0,0,0,0.9);
`

export const TCPReceivedMessagesQueueMonitor = () => {
    const { tcpReceivedMessagesQueueMonitor } = useGlobals()

    const [queueMessages, setQueueMessages] = useState(0)

    useEffect(() => {
        const pollingInterval = setInterval(() => {
            setQueueMessages(tcpReceivedMessagesQueueMonitor.queueMessages)
        }, 100)

        return () => {
            clearInterval(pollingInterval)
        }
    }, [])

    return (
        <Container>
            <div>TCPReceivedMessagesQueueMonitor</div>
            <div>Messages: {queueMessages}</div>
        </Container>
    )
}