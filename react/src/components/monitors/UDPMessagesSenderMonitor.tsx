import { useGlobals } from "@reactunity/renderer"
import { useEffect, useState } from "react"
import styled from "styled-components"

const Container = styled.div`
    color: white;
    background-color: rgba(0,0,0,0.9);
`

const Green = styled.span`
    color: green;
`

const Red = styled.span`
    color: red;
`

export const UDPMessagesSenderMonitor = () => {
    const { udpMessagesSenderMonitor } = useGlobals()

    const [threadRunning, setThreadRunning] = useState(false)

    useEffect(() => {
        const pollingInterval = setInterval(() => {
            setThreadRunning(udpMessagesSenderMonitor.threadRunning)
        }, 100)

        return () => {
            clearInterval(pollingInterval)
        }
    }, [])

    return (
        <Container>
            <div>UDPMessagesSenderMonitor</div>
            <div>Thread: {threadRunning ? <Green>Running</Green> : <Red>Not running</Red>}</div>
        </Container>
    )
}